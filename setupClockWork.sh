#! /bin/bash

export HOMELOCATION=/efs/Arka/clockwork
echo "Installing python packages"
echo "set HOMELOCATION to $HOMELOCATION"
python3 -m pip install scipy
python3 -m pip install PyWavelets
python3 -m pip install sklearn
python3 -m pip install parton
python3 -m pip install keras
python3 -m pip install tensorflow
python3 -m pip install ipywidgets==7.5.1
python3 -m pip install --user mplhep==0.2.9
python3 -m pip install --user matplotlib==3.4.2


### Run Autoencoder
cd clockwork-search/Autoencoder
python3 Condor_Autoencoder_Trainer.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -train 5000  -Cone False -NconeR 2 -uncertainty no -outDirLoc /efs/Arka/clockwork
cd

