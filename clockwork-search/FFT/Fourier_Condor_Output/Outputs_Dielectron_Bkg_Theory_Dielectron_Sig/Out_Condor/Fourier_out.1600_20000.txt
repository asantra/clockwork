[7mlsetup              [0m lsetup <tool1> [ <tool2> ...] (see lsetup -h):
[7m lsetup agis        [0m  ATLAS Grid Information System
[7m lsetup asetup      [0m  (or asetup) to setup an Athena release
[7m lsetup atlantis    [0m  Atlantis: event display
[7m lsetup eiclient    [0m  Event Index 
[7m lsetup emi         [0m  EMI: grid middleware user interface 
[7m lsetup ganga       [0m  Ganga: job definition and management client
[7m lsetup lcgenv      [0m  lcgenv: setup tools from cvmfs SFT repository
[7m lsetup panda       [0m  Panda: Production ANd Distributed Analysis
[7m lsetup pod         [0m  Proof-on-Demand (obsolete)
[7m lsetup pyami       [0m  pyAMI: ATLAS Metadata Interface python client
[7m lsetup root        [0m  ROOT data processing framework
[7m lsetup rucio       [0m  distributed data management system client
[7m lsetup views       [0m  Set up a full LCG release
[7m lsetup xcache      [0m  XRootD local proxy cache
[7m lsetup xrootd      [0m  XRootD data access
[7madvancedTools       [0m advanced tools menu
[7mdiagnostics         [0m diagnostic tools menu
[7mhelpMe              [0m more help
[7mprintMenu           [0m show this menu
[7mshowVersions        [0m show versions of installed software

************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_98python3 x86_64-centos7-gcc8-opt ROOT ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package R-2dabd
# found package cairo-aa450
# found package pkg_config-c6baf
# found package fontconfig-0cfee
# found package expat-ad2fa
# found package freetype-5700e
# found package zlib-da225
# found package gperf-699d7
# found package pkg_config-c6baf
# found package pixman-46112
# found package freetype-5700e
# found package png-9c2fe
# found package proj-42070
# found package sqlite-3c47f
# found package pkg_config-c6baf
# found package zeromq-3b844
# found package pkg_config-c6baf
# found package libsodium-0b20d
# found package gdal-9c811
# found package libgeotiff-b844e
# found package tiff-3ce57
# found package proj-42070
# found package netcdfc-cdb7c
# found package zlib-da225
# found package hdf5-d95c5
# found package zlib-da225
# found package tiff-3ce57
# found package zlib-da225
# found package sqlite-3c47f
# found package hdf5-d95c5
# found package pcre-5c0c1
# found package zlib-da225
# found package pkg_config-c6baf
# found package libgit2-eb69e
# found package gl2ps-37566
# found package zlib-da225
# found package png-9c2fe
# found package srm_ifce-be254
# found package Vc-d25d4
# found package vdt-992df
# found package oracle-1837a
# found package xrootd-4dc0e
# found package zlib-da225
# found package libxml2-3501e
# found package Python-7a85e
# found package sqlite-3c47f
# found package libffi-26487
# found package blas-69123
# found package cfitsio-e4bb8
# found package gfal-6fc75
# found package fftw-102c2
# found package mysql-15ce4
# found package lz4-9bdfe
# found package zeromq-3b844
# found package Boost-a880e
# found package zlib-da225
# found package numpy-0aa5c
# found package blas-69123
# found package setuptools-d1e85
# found package Python-7a85e
# found package cython-58bbc
# found package setuptools-d1e85
# found package Python-7a85e
# found package Python-7a85e
# found package Python-7a85e
# found package zlib-da225
# found package libxml2-3501e
# found package msgpackc-c4011
# found package bison-08bf1
# found package m4-b8d0d
# found package jemalloc-8154a
# found package tbb-daa7e
# found package libxml2-3501e
# found package numpy-0aa5c
# found package GSL-ecdfc
# found package zlib-da225
# found package dcap-cdd28
# found package Davix-7db19
# found package libxml2-3501e
# found package Boost-a880e
# found package Python-7a85e
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scipy ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package setuptools-3ae5d
# found package Python-b96a9
# found package numpy-76586
# found package Python-b96a9
# found package setuptools-3ae5d
# found package lapack-bb169
# found package blas-bb5ea
# found package blas-bb5ea
# found package lapack-bb169
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scikitlearn ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package joblib-07653
# found package Python-b96a9
# found package setuptools-3ae5d
# found package Python-b96a9
# found package numpy-76586
# found package Python-b96a9
# found package setuptools-3ae5d
# found package lapack-bb169
# found package blas-bb5ea
# found package blas-bb5ea
# found package scipy-13cc5
# found package Python-b96a9
# found package setuptools-3ae5d
# found package numpy-76586
# found package lapack-bb169
# found package cython-4a7c0
# found package Python-b96a9
# found package setuptools-3ae5d
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt numpy ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package setuptools-3ae5d
# found package Python-b96a9
# found package lapack-bb169
# found package blas-bb5ea
# found package blas-bb5ea
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package kiwisolver-e3272
# found package Python-b96a9
# found package setuptools-3ae5d
# found package Python-b96a9
# found package python_dateutil-5e867
# found package Python-b96a9
# found package setuptools-3ae5d
# found package setuptools_scm-3baaf
# found package Python-b96a9
# found package setuptools-3ae5d
# found package six-97eb3
# found package Python-b96a9
# found package setuptools-3ae5d
# found package tornado-29a14
# found package Python-b96a9
# found package setuptools-3ae5d
# found package certifi-cdf1d
# found package Python-b96a9
# found package setuptools-3ae5d
# found package backports-21940
# found package Python-b96a9
# found package setuptools-3ae5d
# found package setuptools_scm-3baaf
# found package pip-1ebac
# found package Python-b96a9
# found package setuptools-3ae5d
# found package wheel-fe3cb
# found package Python-b96a9
# found package setuptools-3ae5d
# found package cycler-28855
# found package Python-b96a9
# found package setuptools-3ae5d
# found package six-97eb3
# found package pycairo-d66bb
# found package Python-b96a9
# found package setuptools-3ae5d
# found package cairo-724fa
# found package fontconfig-3cd3a
# found package expat-ad2fa
# found package pkg_config-c6baf
# found package gperf-699d7
# found package freetype-e0596
# found package zlib-da225
# found package pkg_config-c6baf
# found package pixman-46112
# found package freetype-e0596
# found package png-9c2fe
# found package six-97eb3
# found package pyparsing-1247b
# found package Python-b96a9
# found package setuptools-3ae5d
# found package nose-7166f
# found package Python-b96a9
# found package setuptools-3ae5d
# found package backports-21940
# found package setuptools-3ae5d
# found package numpy-76586
# found package Python-b96a9
# found package setuptools-3ae5d
# found package lapack-bb169
# found package blas-bb5ea
# found package blas-bb5ea
# found package certifi-cdf1d
# found package pytz-3dccb
# found package Python-b96a9
# found package setuptools-3ae5d
# found package mock-9e36b
# found package Python-b96a9
# found package setuptools-3ae5d
# found package messaging-b0bb0
# found package Python-b96a9
# found package setuptools-3ae5d
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt wrapt ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package setuptools-3ae5d
# found package Python-b96a9
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
************************************************************************
Requested:  lcgenv ... 
 Setting up [4mlcgenv HEAD[0m ... 
  lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt pip ...
# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package Python-b96a9
# found package sqlite-472f2
# found package libffi-26487
# found package setuptools-3ae5d
# found package Python-b96a9
# found package wheel-fe3cb
# found package Python-b96a9
# found package setuptools-3ae5d
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
 lcgenv:
   Note that lcgenv sets up a long list of library paths.
   This may slow down executables; they look at each library path item.
   A faster alternative is to setup the entire LCG stack with views:
    lsetup "views <LCG release> <platform>"
************************************************************************
Requirement already up-to-date: parton in /home/slawlor/.local/lib/python3.7/site-packages (0.1)
Requirement already satisfied, skipping upgrade: setuptools in /cvmfs/sft.cern.ch/lcg/releases/setuptools/41.0.0-3ae5d/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (41.0.0.post20200313)
Requirement already satisfied, skipping upgrade: scipy in /cvmfs/sft.cern.ch/lcg/releases/scipy/1.3.0-13cc5/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (1.3.0)
Requirement already satisfied, skipping upgrade: appdirs in /home/slawlor/.local/lib/python3.7/site-packages (from parton) (1.4.4)
Requirement already satisfied, skipping upgrade: numpy in /cvmfs/sft.cern.ch/lcg/releases/numpy/1.16.4-76586/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (1.16.4)
Requirement already satisfied, skipping upgrade: pyyaml in /cvmfs/sft.cern.ch/lcg/releases/pyyaml/5.1-03e91/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (5.1)

[1mRooFit v3.60 -- Developed by Wouter Verkerke and David Kirkby[0m 
                Copyright (C) 2000-2013 NIKHEF, University of California & Stanford University
                All rights reserved, please read http://roofit.sourceforge.net/license.txt

Starting program...

Importing files...
p0: 0.7933596019725393
p1: -234.1182991816887
p2: 61575.12643748015
p3: -10460036.578378828
p4: 963491071.2946823
p5: -36798875936.82348
p6: -9.10564559877461e-06
p7: 4.220995572403854e-10
Files imported.

Reading settings...
Mass_5
20000.0
K
1600.0
Signal/Bkg evaluated.

Creation of the directory Outputs_Dielectron_Bkg_Theory_Dielectron_Sig failed
Setting read

Starting new point...
Evaluating signal...
Beginning toy experiment of background + signal
100
Toy experiments of background + signal done.

Beginnning toy experiment of background only
100
Computing test statistics...
Method1: Min Exclusion value is: 0
Method1: Min Discovery value is: 0.3100000000000001
Method2: Min Exclusion value is: 0.2800000000000001
Method2: Min Discovery value is: 0.0
Print statistics computations done.

