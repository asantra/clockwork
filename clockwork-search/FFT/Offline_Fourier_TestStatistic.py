# from __future__ import division
#####################
#   Classifier FT   #
#####################

#Explanations
# - This code uses Fourier transforms to determine whether a signal is present or not.

#Import
print("Starting program...")
print("")
print("Importing files...")

#Standard
import math, numpy, scipy, random, os, sys, scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
#Custom
#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

import SigBkg_Functions as SB
import Fourier_Transform as FT

import mplhep as hep
hep.set_style(hep.style.ROOT)

print("Files imported.")
print("")


import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-lumDiv', metavar='', help='Devide by luminosity')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-stepFT', metavar='', help='Step size of window - see Fourier_Transform.py for definition')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from Bkg+Sig Toy')
parser.add_argument('-uncertainty', metavar='', help='Profile Uncertainties of the Background and Signal models')
argus = parser.parse_args()

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 

#Mass and scale Lists
massList = numpy.linspace(xmin, xmax, nBins)

#Settings
print("Reading settings...")

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

#Define input Signal parameters and 

M5 = float(argus.M5)
k = float(argus.k)

print("Mass_5")
print(M5)
print("K")
print (k)

#Define event type to give interger or float number of events - USE float!!! Will remove interger option
if(argus.event=="int"):
  eventType = "int"
if(argus.event=="float"):
  eventType = "float"

#Generate signal and Background

#####Signal#####
if(argus.s=="Tail_Damped_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax, eventType)

if(argus.s=="Theory_Diphoton_36fb_Sig"):
  luminosity = 36.1
  year = '2015-16'
  SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins, eventType)

if(argus.s=="Theory_Dielectron_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins, eventType)

if(argus.s=="Theory_HighMass_Diphoton_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins, eventType)

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins, eventType)

if(argus.b=="Diphoton_36fb_Bkg"):
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList, eventType)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins, eventType)

print("Signal/Bkg evaluated.")
print("")

#Fourier transform steps
stepFT = int(argus.stepFT)

#Number of trials for background test statistic and signal + background test statistic
nTrials1 = int(argus.test)
nTrials2 = int(argus.test)

#Background subtraction
backgroundSub = bool(argus.bkgsub)

#Division by luminosity - can unhighlight to overide argparse signal lum
lumDiv = bool(argus.lumDiv)

#Define Test Statitic Binning
bins = int(argus.bin)


#Lets name a folder based on the bkg to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_bkgsub"+(argus.bkgsub)+"_stepFT"+(argus.stepFT)+"_lumDiv"+(argus.lumDiv)+""
  os.mkdir("Fourier_Offline_Output/"+Output_Folder)
  os.mkdir("Fourier_Offline_Output/"+Output_Folder+"/Pvalues")
  os.mkdir("Fourier_Offline_Output/"+Output_Folder+"/TestStatistic_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)



#Make empty lists to append Pvalues
P_Value_List_Method1_Exclusion = []
P_Value_List_Method2_Exclusion = []
P_Value_List_Method1_Discovery = []
P_Value_List_Method2_Discovery = []

Pvalue_out_Method1_Exclusion = open(Directory+"Fourier_Offline_Output/"+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Exclusion = open(Directory+"Fourier_Offline_Output/"+Output_Folder+"/Pvalues/Pvalue_Method2_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method1_Discovery = open(Directory+"Fourier_Offline_Output/"+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery = open(Directory+"Fourier_Offline_Output/"+Output_Folder+"/Pvalues/Pvalue_Method2_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")


print("Setting read")
print("")



#Start loop
print("Starting new point...")
print("Evaluating signal...")


#Generate trial background + signal
#Initialize variables
print("Beginning toy experiment of background + signal")

statTestFT1 = []
statTestFT1_nominal = []
statTestFT1_uncert = []

for i in range(0, nTrials1):

  if(argus.uncertainty=="no"):
    #Generate random binned events
    if(argus.t=="Poisson"):
      eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    if(argus.t=="RooFit"):
      eventsTrial1 = SB.SigBkgRooFitToy(BkgFix, SigFix, xmin, xmax, nBins, 1, 1)
    if(argus.s=="Tail_Damped_Sig"):
      statTestFT1.append(FT.FourierTS_NoLum(eventsTrial1, BkgFix, backgroundSub, 1, k, M5, massList, stepFT, xmin, xmax))
    else:
      statTestFT1.append(FT.FourierTS(eventsTrial1, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))
  
  if(argus.uncertainty=="yes"):
    BkgFix_UncertianitesAll  = SB.BkgFixed_AllUncertainties_ToysThrown_VarAll(xmin, xmax, i)
    SigFix_UncertaintiesAll = SB.SigUncertainty_Toy(k, M5, xmin, xmax,i)
    if(argus.t=="Poisson"):
      eventsTrial1_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
      eventsTrial1_uncert = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, SigFix_UncertaintiesAll, 1, 1)
    if(argus.t=="RooFit"):
      eventsTrial1 = SB.ToyDataGenerator_withSignal(k, M5, xmin, xmax, "bkgsig", 1)
    #Calculate test statistic FT
    statTestFT1_nominal.append(FT.FourierTS(eventsTrial1_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))
    statTestFT1_uncert.append(FT.FourierTS(eventsTrial1_uncert, BkgFix_UncertianitesAll, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))

print(statTestFT1)
print(len(statTestFT1)) 

print("Toy experiments of background + signal done.")
print("")



#Generate trial background
#Initialize variables
print("Beginnning toy experiment of background only")

statTestFT2 = []
statTestFT2_nominal = []
statTestFT2_uncert = []

for i in range(0, nTrials2):
    
  if(argus.uncertainty=="no"):
    #Generate random binned events
    if(argus.t=="Poisson"):
      eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.t=="RooFit"):
      eventsTrial2 = SB.SigBkgRooFitToy(BkgFix, SigFix, xmin, xmax, nBins, 1, 0)
    #Calculate test statistic FT
    if(argus.s=="Tail_Damped_Sig"):
      statTestFT2.append(FT.FourierTS_NoLum(eventsTrial2, BkgFix, backgroundSub, 1, k, M5, massList, stepFT, xmin, xmax))
    else:
      statTestFT2.append(FT.FourierTS(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))
  if(argus.uncertainty=="yes"):
    BkgFix_UncertianitesAll  = SB.BkgFixed_AllUncertainties_ToysThrown_VarAll(xmin, xmax, i)
    SigFix_UncertaintiesAll = SB.SigUncertainty_Toy(k, M5, xmin, xmax,i)
    if(argus.t=="Poisson"):
      eventsTrial2_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
      eventsTrial2_uncert = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, SigFix_UncertaintiesAll, 1, 1)
    if(argus.t=="RooFit"):
      eventsTrial2 = SB.ToyDataGenerator_withSignal(k, M5, xmin, xmax, "bkgsig", 1)
    #Calculate test statistic FT
    statTestFT2_nominal.append(FT.FourierTS(eventsTrial2_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))
    statTestFT2_uncert.append(FT.FourierTS(eventsTrial2_uncert, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, stepFT, xmin, xmax))

print(statTestFT2)
print(len(statTestFT2)) 

##############################
####Hugues/Yevgeny Method 1###
##############################
#Distribution of statistical significance from Fourier transform
print("Computing test statistics...")

significanceDistributionFTTotal_Discovery = []
significanceDistributionFTTotal_Exclusion = []

for n in range(0, stepFT):

  significanceDistributionFT_Discovery = []
  significanceDistributionFT_Exclusion = []
    
  for i in range(0, nTrials1):
    
    #Initialize variables
    temp_Discovery = 0
  
    #Discovery
    #Loop over background events
    for j in range(0, nTrials2):
      if statTestFT2[j][n] >= statTestFT1[i][n]:
        temp_Discovery += 1/nTrials2
    
    #Append to list
    significanceDistributionFT_Discovery.append(temp_Discovery)

  
  for i in range(0, nTrials2):

    #Initialize variables
    temp_Exclusion = 0

    #Exclusion
    #Loop over background events
    for j in range(0, nTrials1):
      if statTestFT2[i][n] >= statTestFT1[j][n]:
        temp_Exclusion += 1/nTrials1


    #Append to list
    significanceDistributionFT_Exclusion.append(temp_Exclusion)

  #Append to list of significances
  significanceDistributionFTTotal_Exclusion.append(significanceDistributionFT_Exclusion)
  significanceDistributionFTTotal_Discovery.append(significanceDistributionFT_Discovery)


templist_method1_Exclusion = []
templist_method1_Discovery = []

#Median Exclusion

for i in range(0, stepFT):
  t1 = numpy.sort(significanceDistributionFTTotal_Exclusion[i])
  temp_Exclusion = t1[int(numpy.floor(0.5*len(significanceDistributionFTTotal_Exclusion[i])))]
  # print(i, temp_Exclusion)
  templist_method1_Exclusion.append(temp_Exclusion)

templist_method1_Exclusion = numpy.nan_to_num(templist_method1_Exclusion)

print("Method1: Min Exclusion value is:", min(templist_method1_Exclusion, default=0))

#Median Discovery

for i in range(0, stepFT):
  t1 = numpy.sort(significanceDistributionFTTotal_Discovery[i])
  temp_Discovery = t1[int(numpy.floor(0.5*len(significanceDistributionFTTotal_Discovery[i])))]
  # print(i, temp_Discovery)
  templist_method1_Discovery.append(temp_Discovery)

templist_method1_Discovery = numpy.nan_to_num(templist_method1_Discovery)


print("Method1: Min Discovery value is:", min(templist_method1_Discovery, default=0))

##############################
######Glen/Seans Method2######
##############################
templist_method2_Exclusion = []
templist_method2_Discovery = []

for n in range(0, stepFT):
  BSstats = []
  Bonlystats =[] 
  Lambda_Bonly_hist = []
  Lambda_BS_hist = []
  for i in range(0,nTrials2):
    Bonlystats.append(statTestFT2[i][n])
    BSstats.append(statTestFT1[i][n])

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(Bonlystats)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Bonlystats)))]
  #Discovery Median
  BS_SortedList     = numpy.sort(BSstats)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(BSstats)))]

  if(argus.uncertainty=="no"):
    Lambda_BS_hist = numpy.histogram(BSstats,range=(numpy.nanmin(Bonlystats), numpy.nanmax(Bonlystats)))
    Lambda_Bonly_hist =numpy.histogram(Bonlystats, range=(numpy.nanmin(BSstats), numpy.nanmax(BSstats)))

  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist,bins)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist,bins)

  # Make Test Statistic plot
  Probably_Dist = plt.figure()
  ax = Probably_Dist.add_subplot()
  ax1 = Probably_Dist.add_subplot()
  ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
  if(argus.uncertainty=="no"):
    # plt.hist(Lambda_Bonly_hist, label="Bkg Only Nom", alpha=0.4)
    # plt.hist(Lambda_BS_hist, label="Bkg + Signal Nom" , alpha=0.4) 
    plt.hist(Bonlystats, bins, label="Bkg Only Nom", alpha=0.4)
    plt.hist(BSstats, bins, label="Bkg + Signal Nom" , alpha=0.4) 
  if(argus.uncertainty=="yes"):
      plt.hist(statTestFT2_nominal[n], bins,label="Bkg Only Nom", alpha=0.4)
      plt.hist(statTestFT1_nominal[n], bins, label="Bkg + Signal Nom" , alpha=0.4) 
      plt.hist(statTestFT2_uncert[n], bins,label="Bkg Only Uncert", alpha=0.4)
      plt.hist(statTestFT1_uncert[n], bins, label="Bkg + Signal Uncert" , alpha=0.4) 
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  t = ax1.xaxis.get_offset_text()
  z = ax1.Yaxis.get_offset_text()
  t.set_x(-0.05)
  z.set_y(1)
  plt.xlabel(r'$\mathrm{y(P(T))}$', ha='right', x=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"Fourier_Offline_Output/"+Output_Folder+"/TestStatistic_plots/TestDistribution_FTrange"+str(n)+"_"+str(k)+"_"+str(M5)+".png")



  #Try and caluclate P-value for Exclusion, account for which way around each set of test stats is by looking at which median is higher
  if BS_Median > B_OnlyMedian:
    P_Value_Exclusion = (Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)
  if BS_Median < B_OnlyMedian:
    P_Value_Exclusion = (1-Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)


  #Try and caluclate P-value for Discovery
  if BS_Median > B_OnlyMedian:
    P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)
  if BS_Median < B_OnlyMedian:
    P_Value_Discovery = (Lambda_Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)


#Convert Nan to 0
templist_method2_Exclusion = numpy.nan_to_num(templist_method2_Exclusion)
templist_method2_Discovery = numpy.nan_to_num(templist_method2_Discovery)

print("Method2: Min Exclusion value is:", min(templist_method2_Exclusion, default=0))
P_Value_List_Method2_Exclusion.append(min(templist_method2_Exclusion,default=0))

print("Method2: Min Discovery value is:", min(templist_method2_Discovery,default=0))
P_Value_List_Method2_Discovery.append(min(templist_method2_Discovery,default=0))



print("Print statistics computations done.")
print("")

#Save Pvalues from the 2 methods, Limts from exclusion and discovery 
numpy.savetxt(Pvalue_out_Method1_Discovery, P_Value_List_Method1_Discovery, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method1_Exclusion, P_Value_List_Method1_Exclusion, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")

