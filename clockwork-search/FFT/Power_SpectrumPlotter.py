from __future__ import division
#####################
#   Classifier FT   #
#####################

#Explanations
# - This code uses Fourier transforms to determine whether a signal is present or not.

#Import
print("Starting program...")
print("")
print("Importing files...")

#Standard
import math, numpy, scipy, random, os, sys, scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from scipy.interpolate import splev, splrep

#Custom
import Fourier_Transform_Alternative  as FT


#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

import Basic
import SigBkg_Functions as SB


import mplhep as hep
hep.set_style(hep.style.ROOT)

print("Files imported.")
print("")


import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-lumDiv', metavar='', help='Devide by luminosity only for FTTinclude_Optimization yes')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from tempalte before FFT')
parser.add_argument('-uncertainty', metavar='', help='Profile Uncertainties of the Background and Signal models')
parser.add_argument('-bands', metavar='', help='Do detailed PowerSpectrum with smoothing for plotting')
parser.add_argument('-DoSignalFFT', metavar='', help='Do Signal FFT')
parser.add_argument('-DoBkgFFT', metavar='', help='Do Bkg FFT')
parser.add_argument('-FTTinclude_Optimization', metavar='', help='FTTinclude_Optimization - include g(m) factor in the exponential')
parser.add_argument('-PlotAll', metavar='', help='Make plots with everything one for given type')
parser.add_argument('-log', metavar='', help='log P(T)')
argus = parser.parse_args()


def smooth(freq, oldSignal, newfreq):
  spl = splrep(freq, oldSignal)
  newSignal = splev(newfreq, spl)
  return newSignal

def Smooth_MassSpectrum(InputMassList,Strength,Width,ArbParameter):

  NewMassList = []
  for i in range(0, len(InputMassList)):
    mass = InputMassList[i]
    NewMass = FT.DoubleSided_ErrorFunc(mass,Strength,Width,ArbParameter)
    print(NewMass)
    NewMassList.append(NewMass)

  return NewMassList

#Define input Signal parameters and 
M5 = float(argus.M5)
k = float(argus.k)

print("Mass_5")
print(M5)
print("K")
print (k)

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 500
  xmax = 1500
  nBins   = 1000

if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 2550

if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 


#Mass and scale Lists
massList = numpy.linspace(xmin, xmax, nBins)
massList_Plot = numpy.linspace(xmin, xmax, nBins)


#Define a range of frequencies to compute over
TList = numpy.linspace(0.8, 1.2, 501)

Smoothed_OptimisedFreq_bins = numpy.linspace(0.8, 1.2, 5001)

#Settings
print("Reading settings...")

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

Include_ResonanceOptimization = str(argus.FTTinclude_Optimization)


#Division by luminosity 
lumDiv = bool(argus.lumDiv)


#Number of trials for background test statistic and signal + background test statistic
nTrials1 = 50
nTrials2 = 50

#Background subtraction
backgroundSub = bool(argus.bkgsub)


##########Generate signal and Backgrounds##########

if(argus.DoBkgFFT=="yes"):
  #####Background#####
  if(argus.b=="Dielectron_Bkg"):
    luminosity = 139
    year = '2015-18'
    BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

  if(argus.b=="Diphoton_36fb_Bkg"):
    luminosity = 36.1
    year = '2015-16'
    BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

  if(argus.b=="HighMass_Diphoton_Bkg"):
    luminosity = 139
    year = '2015-18'
    BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

  lum = [1] * nBins

if(argus.DoSignalFFT=="yes"):

  #####Background#####
  if(argus.b=="Dielectron_Bkg"):
    luminosity = 139
    year = '2015-18'
    BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

  if(argus.b=="Diphoton_36fb_Bkg"):
    luminosity = 36.1
    year = '2015-16'
    BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

  if(argus.b=="HighMass_Diphoton_Bkg"):
    luminosity = 139
    year = '2015-18'
    BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

  #####Signal#####
  if(argus.s=="Tail_Damped_Sig"):
    luminosity = 139
    year = '2015-18'
    SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax)

  if(argus.s=="GenericSignal"):
    luminosity = 139
    year = '2015-18'
    SigFix_NoTF = SB.SigFix_Generic(0.3, xmin, xmax, nBins)
    SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP = SB.SigFix_Generic_Uncertainties(0.3,xmin, xmax, nBins, "All")

  if(argus.s=="Theory_Diphoton_36fb_Sig"):
    luminosity = 36.1
    year = '2015-16'
    SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins)

  if(argus.s=="Theory_Dielectron_Sig"):
    luminosity = 139
    year = '2015-18'
    if(argus.uncertainty=="no"):
      SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
    if(argus.uncertainty=="yes"):
      SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, lum = SB.SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, "All")


  if(argus.s=="Theory_HighMass_Diphoton_Sig"):
    luminosity = 139
    year = '2015-18'
    SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins)

SigBkg_Fixed = SB.SigBkgFixed(BkgFix, SigFix)


print("Signal/Bkg evaluated.")
print("")


#Lets name a folder based on the bkg and other terminal arguments to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""
  os.mkdir("Fourier_Condor_Output/"+Output_Folder)
  os.mkdir("Fourier_Condor_Output/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir("Fourier_Condor_Output/"+Output_Folder+"/TestStatistic_plots")
  os.mkdir("Fourier_Condor_Output/"+Output_Folder+"/Error_Condor")
  os.mkdir("Fourier_Condor_Output/"+Output_Folder+"/Logs_Condor")
  os.mkdir("Fourier_Condor_Output/"+Output_Folder+"/Out_Condor")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)

print("Setting read")
print("")



#Start loop
print("Starting Plotting...")

if str(argus.FTTinclude_Optimization) == "no":
  Frequency_SigFixedPeak = FT.Default_DFT_SignalPeakFinder(SigFix, BkgFix, backgroundSub, xmin, xmax)

print("Evaluating ...")


if(argus.DoBkgFFT=="yes"):

  if(argus.uncertainty=="no"):

    if(argus.log=="no"):
      Power_BkgFixed = FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList)
      # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)

    if(argus.log=="yes"):
      Power_BkgFixed = numpy.log10(FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList))
      # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)

    # print(Power_BkgFixed)

  if(argus.uncertainty=="yes"):

    if str(argus.FTTinclude_Optimization) == "yes":

      if(argus.log=="no"):
        Power_BkgFixed = FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList)
        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)

        BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, 100)
        Power_BkgToy_Unc_Fixed_test = FT.FourierTS_PeakPlotter(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        # Power_BkgToy_Unc_Fixed_Smooth = smooth(TList, Power_BkgToy_Unc_Fixed_test, Smoothed_OptimisedFreq_bins)

      if(argus.log=="yes"):
        Power_BkgFixed = numpy.log(FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList))
        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # print("Power_BkgFixed",Power_BkgFixed)
        BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, 100)
        Power_BkgToy_Unc_Fixed_test = numpy.log10(FT.FourierTS_PeakPlotter(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        # Power_BkgToy_Unc_Fixed_Smooth = smooth(TList, Power_BkgToy_Unc_Fixed_test, Smoothed_OptimisedFreq_bins)


    if str(argus.FTTinclude_Optimization) == "no":

      Power_BkgFixed, freq = FT.Default_DFT(BkgFix, BkgFix, backgroundSub, xmin, xmax)
      # Power_BkgFixed_Smooth = smooth(freq, Power_BkgFixed, freq*10)

      BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, 100)
      Power_BkgToy_Unc_Fixed_test, freq = FT.Default_DFT(BkgFix_UncertianitesAll, BkgFix, backgroundSub, xmin, xmax)
      # Power_BkgToy_Unc_Fixed_Smooth = smooth(freq, Power_BkgToy_Unc_Fixed_test, freq*10)


if(argus.DoSignalFFT=="yes"):

  if(argus.uncertainty=="no"):

    if str(argus.FTTinclude_Optimization) == "yes":

      if(argus.log=="no"):
        Power_SigFixed = FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, k, xmax, TList)
        Power_SigBkgFixed = FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, k, xmax, TList)

        # Power_SigFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

      if(argus.log=="yes"):
        Power_SigFixed = numpy.log(FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigBkgFixed = numpy.log(FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))

        # Power_SigFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

    if str(argus.FTTinclude_Optimization) == "no":

      testSig = [x1 - x2 for (x1, x2) in zip(SigFix, BkgFix)]

      SigBkg_Fixed_SmoothedMassSpectrum = Smooth_MassSpectrum(testSig,5,1480,10)

      fig_Smooth, ax = plt.subplots()
      ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
      plt.plot(massList,SigBkg_Fixed_SmoothedMassSpectrum, 'r', alpha=1, label=r"Signal Generic ee smooth")
      plt.plot(massList,testSig, 'g', alpha=1, label=r"Signal Generic ee")
      plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
      plt.ylabel('Events', ha='right', y=1.0)
      plt.margins(0)
      plt.legend(loc='best')
      # plt.ylim(bottom=1e-4)
      plt.xlim(left=200)
      # plt.yscale('log')
      # plt.xscale('log')
      fig_Smooth.savefig('smoothingtest.pdf')

      zeroList = [0] * nBins

      Power_BkgFixed, freq = FT.Default_DFT(BkgFix, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed, freq = FT.Default_DFT(SigFix, zeroList, backgroundSub, xmin, xmax)
      Power_SigBkgFixed, freq = FT.Default_DFT(SigBkg_Fixed, BkgFix, backgroundSub, xmin, xmax)



      # Power_BkgFixed_Smooth = smooth(freq, Power_BkgFixed, freq*10)
      # Power_SigFixed_Smooth = smooth(freq, Power_SigFixed, freq*10)
      # Power_SigBkgFixed_Smooth = smooth(freq, Power_SigBkgFixed, freq*10)   


  if(argus.uncertainty=="yes"):

    if str(argus.FTTinclude_Optimization) == "yes":
    
      if(argus.log=="no"):
        
        Power_BkgFixed = FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed = FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyScale_UP = FT.FourierTS_PeakPlotter(SigFix_EnergyScale_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyScale_DOWN = FT.FourierTS_PeakPlotter(SigFix_EnergyScale_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyResolution_UP = FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyResolution_DOWN = FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigBkgFixed = FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)

        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_Smooth = smooth(TList, Power_SigFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_UP_Smooth = smooth(TList, Power_SigFixed_EnergyScale_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyScale_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_UP_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

      if(argus.log=="yes"):

        Power_BkgFixed = numpy.log10(FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed = numpy.log10(FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyScale_UP = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyScale_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyScale_DOWN = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyScale_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyResolution_UP = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyResolution_DOWN = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigBkgFixed = numpy.log10(FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
   
        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_Smooth = smooth(TList, Power_SigFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_UP_Smooth = smooth(TList, Power_SigFixed_EnergyScale_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyScale_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_UP_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

      if(argus.log=="no"):
        Power_BkgFixed = FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed = FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyScale_UP = FT.FourierTS_PeakPlotter(SigFix_EnergyScale_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyScale_DOWN = FT.FourierTS_PeakPlotter(SigFix_EnergyScale_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyResolution_UP = FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigFixed_EnergyResolution_DOWN = FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        Power_SigBkgFixed = FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_Smooth = smooth(TList, Power_SigFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_UP_Smooth = smooth(TList, Power_SigFixed_EnergyScale_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyScale_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_UP_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

      if(argus.log=="yes"):
       
        Power_BkgFixed = numpy.log10(FT.FourierTS_PeakPlotter(BkgFix, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed = numpy.log10(FT.FourierTS_PeakPlotter(SigFix, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyScale_UP = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyScale_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyScale_DOWN = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyScale_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyResolution_UP = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_UP, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigFixed_EnergyResolution_DOWN = numpy.log10(FT.FourierTS_PeakPlotter(SigFix_EnergyResolution_DOWN, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        Power_SigBkgFixed = numpy.log10(FT.FourierTS_PeakPlotter(SigBkg_Fixed, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))

        # Power_BkgFixed_Smooth = smooth(TList, Power_BkgFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_Smooth = smooth(TList, Power_SigFixed, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_UP_Smooth = smooth(TList, Power_SigFixed_EnergyScale_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyScale_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyScale_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_UP_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_UP, Smoothed_OptimisedFreq_bins)
        # Power_SigFixed_EnergyResolution_DOWN_Smooth = smooth(TList, Power_SigFixed_EnergyResolution_DOWN, Smoothed_OptimisedFreq_bins)
        # Power_SigBkgFixed_Smooth = smooth(TList, Power_SigBkgFixed, Smoothed_OptimisedFreq_bins)

    if str(argus.FTTinclude_Optimization) == "no":

      Power_BkgFixed, freq = FT.Default_DFT(BkgFix, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed, freq = FT.Default_DFT(SigFix, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed_EnergyScale_UP, freq = FT.Default_DFT(SigFix_EnergyScale_UP, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed_EnergyScale_DOWN, freq = FT.Default_DFT(SigFix_EnergyScale_DOWN, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed_EnergyResolution_UP, freq =  FT.Default_DFT(SigFix_EnergyResolution_UP, BkgFix, backgroundSub, xmin, xmax)
      Power_SigFixed_EnergyResolution_DOWN, freq =  FT.Default_DFT(SigFix_EnergyResolution_DOWN, BkgFix, backgroundSub, xmin, xmax)
      Power_SigBkgFixed, freq = FT.Default_DFT(SigBkg_Fixed, BkgFix, backgroundSub, xmin, xmax)

      # Power_BkgFixed_Smooth = smooth(freq, Power_BkgFixed, freq*10)
      # Power_SigFixed_Smooth = smooth(freq, Power_SigFixed, freq*10)
      # Power_SigFixed_EnergyScale_UP_Smooth = smooth(freq, Power_SigFixed_EnergyScale_UP, freq*10)
      # Power_SigFixed_EnergyScale_DOWN_Smooth = smooth(freq, Power_SigFixed_EnergyScale_DOWN, freq*10)
      # Power_SigFixed_EnergyResolution_UP_Smooth = smooth(freq, Power_SigFixed_EnergyResolution_UP, freq*10)
      # Power_SigFixed_EnergyResolution_DOWN_Smooth = smooth(freq, Power_SigFixed_EnergyResolution_DOWN, freq*10)
      # Power_SigBkgFixed_Smooth = smooth(freq, Power_SigBkgFixed, freq*10)   

#Generate trial background + signal
#Initialize variables
print("Beginning toy experiment of background + signal")


if(argus.DoSignalFFT=="yes"):

  PowerSB_Alltoys = []
  PowerS_Alltoys_Unc_Fixed = []
  PowerS_Alltoys_Unc_T1_Fixed = []

  PowerSB_Alltoys_Smooth = []
  PowerS_Alltoys_Unc_Fixed_Smooth = []

  for i in range(0, nTrials1):

    if(argus.uncertainty=="no"):
      #Generate random binned events
      eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)

      if str(argus.FTTinclude_Optimization) == "no":
        Power_SigBkgToy, freq = FT.Default_DFT(eventsTrial1, BkgFix, backgroundSub, xmin, xmax)
        # Power_SigBkgToy_Smooth = smooth(freq, Power_SigBkgToy, freq*10)   


      if str(argus.FTTinclude_Optimization) == "yes":
        if(argus.s=="Tail_Damped_Sig"):
          if(argus.log=="no"):
            Power_SigBkgToy = FT.FourierTS_PeakPlotter_NoLum(eventsTrial1, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList)
          if(argus.log=="yes"):
            Power_SigBkgToy = numpy.log10(FT.FourierTS_PeakPlotter_NoLum(eventsTrial1, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList))
        else:
          if(argus.log=="no"):
            Power_SigBkgToy = FT.FourierTS_PeakPlotter(eventsTrial1, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
          if(argus.log=="yes"):
            Power_SigBkgToy = numpy.log10(FT.FourierTS_PeakPlotter(eventsTrial1, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        # Power_SigBkgToy_Smooth = smooth(TList, Power_SigBkgToy, Smoothed_OptimisedFreq_bins)  

    if(argus.uncertainty=="yes"):

      Sig_Unc = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, BkgFix, xmin, xmax,i,"WT")


      if str(argus.FTTinclude_Optimization) == "no":

        Power_SigBkgToy_Unc_Fixed, freq = FT.Default_DFT(Sig_Unc, BkgFix, backgroundSub, xmin, xmax)
        # Power_SigBkgToy_Unc_Fixed_Smooth = smooth(freq, Power_SigBkgToy_Unc_Fixed, freq*10) 

        Power_SigBkgToy_Unc_T1_Fixed = FT.Default_DFT_TestStatistic(Sig_Unc, BkgFix, backgroundSub, Frequency_SigFixedPeak, xmin, xmax)
        #Generate random binned events
        eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
  
        Power_SigBkgToy, freq = FT.Default_DFT(eventsTrial1, BkgFix, backgroundSub, xmin, xmax)
        # Power_SigBkgToy_Smooth = smooth(freq, Power_SigBkgToy, freq*10)  

      if str(argus.FTTinclude_Optimization) == "yes":

        if(argus.s=="Tail_Damped_Sig"):
          if(argus.log=="no"):
            Power_SigBkgToy_Unc_Fixed = FT.FourierTS_PeakPlotter_NoLum(Sig_Unc, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList)
          if(argus.log=="yes"):
            Power_SigBkgToy_Unc_Fixed = FT.FourierTS_PeakPlotter_NoLum(Sig_Unc, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList)
        else:
          if(argus.log=="no"):
            Power_SigBkgToy_Unc_Fixed = FT.FourierTS_PeakPlotter(Sig_Unc, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList)
            Power_SigBkgToy_Unc_T1_Fixed = FT.FourierTS(Sig_Unc, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax)
          if(argus.log=="yes"):
            Power_SigBkgToy_Unc_Fixed = numpy.log10(FT.FourierTS_PeakPlotter(Sig_Unc, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
            Power_SigBkgToy_Unc_T1_Fixed = numpy.log10(FT.FourierTS(Sig_Unc, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax))
        # Power_SigBkgToy_Unc_Fixed_Smooth = smooth(TList, Power_SigBkgToy_Unc_Fixed, Smoothed_OptimisedFreq_bins)  

        #Generate random binned events
        eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
        if(argus.s=="Tail_Damped_Sig"):
          if(argus.log=="no"):
            Power_SigBkgToy = FT.FourierTS_PeakPlotter_NoLum(eventsTrial1, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList)
          if(argus.log=="yes"):
            Power_SigBkgToy = numpy.log10(FT.FourierTS_PeakPlotter_NoLum(eventsTrial1, BkgFix, backgroundSub, k, M5, massList, xmin, xmax, TList))
        else:
          if(argus.log=="no"):
            Power_SigBkgToy = FT.FourierTS_PeakPlotter(eventsTrial1, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList) 
          if(argus.log=="yes"):
            Power_SigBkgToy = numpy.log10(FT.FourierTS_PeakPlotter(eventsTrial1, BkgFix, backgroundSub,  lum, lumDiv, k, M5, massList, xmin, xmax, TList))
        # Power_SigBkgToy_Smooth = smooth(TList, Power_SigBkgToy, Smoothed_OptimisedFreq_bins)  



    PowerSB_Alltoys.append(Power_SigBkgToy)
    # PowerSB_Alltoys_Smooth.append(Power_SigBkgToy_Smooth)
    if(argus.uncertainty=="yes"):
      PowerS_Alltoys_Unc_Fixed.append(Power_SigBkgToy_Unc_Fixed)
      # PowerS_Alltoys_Unc_Fixed_Smooth.append(Power_SigBkgToy_Unc_Fixed_Smooth)
      PowerS_Alltoys_Unc_T1_Fixed.append(Power_SigBkgToy_Unc_T1_Fixed)

  print("Toy experiments of background + signal done.")
  print("")

#Define extra bins amount for smoothed numpy power spectrum

if str(argus.FTTinclude_Optimization) == "no":

  Smoothed_NumpyFreq_bins = 10*freq


if(argus.DoBkgFFT=="yes"):
  #Generate trial background
  #Initialize variables
  print("Beginnning toy experiment of background only")
  PowerB_Alltoys = []
  PowerB_Alltoys_Smooth = []
  PowerB_Alltoys_Unc_Fixed = []
  PowerB_Alltoys_Unc_Fixed_Smooth = []
  PowerBkg_FT_nominal_ToysBkg = []

  PowerB_Alltoys_T1_Fixed = []
  PowerB_Alltoys_Unc_T1_Fixed = []
  BkgUncMasslist = []
  Bkg_NomUnc_Diff = []

  for i in range(0, nTrials2):

    #Generate random binned events
    eventsTrial2_nominal_ToysBkg = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.FourierTS_NoLum(eventsTrial2_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.FourierTS(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTTinclude_Optimization=="no"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.Default_DFT_TestStatistic(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

      
    if(argus.uncertainty=="no"):

      if str(argus.FTTinclude_Optimization) == "no":
        #Generate random binned events
        eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
        #Calculate test statistic FT
        Power_BkgToy, freq = FT.Default_DFT(eventsTrial2, BkgFix, backgroundSub, xmin, xmax)
        # Power_BkgToy_Smooth = smooth(freq, Power_BkgToy, freq*10) 

      if str(argus.FTTinclude_Optimization) == "yes":    
        #Generate random binned events
        eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
        #Calculate test statistic FT
        if(argus.log=="no"):
          Power_BkgToy = FT.FourierTS_PeakPlotter(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList)
        if(argus.log=="yes"):
          Power_BkgToy = numpy.log10(FT.FourierTS_PeakPlotter(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv,k, M5, massList, xmin, xmax, TList))
        # Power_BkgToy_Smooth = smooth(TList, Power_BkgToy, Smoothed_OptimisedFreq_bins)  

    if(argus.uncertainty=="yes"):

      BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i)


      if str(argus.FTTinclude_Optimization) == "no":

        Bkg_NomUnc_Diff.append([x1 - x2 for (x1, x2) in zip(BkgFix_UncertianitesAll, BkgFix)])
        BkgUncMasslist.append(BkgFix_UncertianitesAll)
        #Generate random binned events
        eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
        #Calculate test statistic FT
        Power_BkgToy, freq = FT.Default_DFT(eventsTrial2, BkgFix, backgroundSub, xmin, xmax)
        Power_BkgToy_Unc_Fixed, freq = FT.Default_DFT(BkgFix_UncertianitesAll, BkgFix, backgroundSub, xmin, xmax)
        # Power_BkgToy_Smooth = smooth(freq, Power_BkgToy, freq*10)  
        # Power_BkgToy_Unc_Fixed_Smooth = smooth(freq, Power_BkgToy_Unc_Fixed, freq*10)  

        Power_BkgToy_Unc_T1_Fixed = FT.Default_DFT_TestStatistic(BkgFix_UncertianitesAll, BkgFix, backgroundSub, Frequency_SigFixedPeak,k, xmax)
        print("Power_BkgToy_Unc_T1_Fixed",Power_BkgToy_Unc_T1_Fixed)
        # Power_BkgToy_Unc_T1_Fixed = FT.FourierTS(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax)
        Power_BkgToy_T1_Fixed = FT.Default_DFT_TestStatistic(eventsTrial2, BkgFix, backgroundSub, Frequency_SigFixedPeak,k, xmax)

      if str(argus.FTTinclude_Optimization) == "yes":

        Bkg_NomUnc_Diff.append([x1 - x2 for (x1, x2) in zip(BkgFix_UncertianitesAll, BkgFix)])
        BkgUncMasslist.append(BkgFix_UncertianitesAll)
        #Generate random binned events
        eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
        #Calculate test statistic FT
        if(argus.log=="no"):
          Power_BkgToy = FT.FourierTS_PeakPlotter(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList)
          Power_BkgToy_Unc_Fixed = FT.FourierTS_PeakPlotter(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList)
          # Power_BkgToy_Smooth = smooth(TList, Power_BkgToy, Smoothed_OptimisedFreq_bins)  
          # Power_BkgToy_Unc_Fixed_Smooth = smooth(TList, Power_BkgToy_Unc_Fixed, Smoothed_OptimisedFreq_bins)

          Power_BkgToy_Unc_T1_Fixed = FT.FourierTS(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax)
          Power_BkgToy_T1_Fixed = FT.FourierTS(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax)
        if(argus.log=="yes"):
          Power_BkgToy = numpy.log10(FT.FourierTS_PeakPlotter(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList))
          Power_BkgToy_Unc_Fixed = numpy.log10(FT.FourierTS_PeakPlotter(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList))
          Power_BkgToy_Smooth = smooth(TList, Power_BkgToy, Smoothed_OptimisedFreq_bins)  
          Power_BkgToy_Unc_Fixed_Smooth = smooth(TList, Power_BkgToy_Unc_Fixed, Smoothed_OptimisedFreq_bins)


          Power_BkgToy_Unc_T1_Fixed = numpy.log10(FT.FourierTS(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax))
          Power_BkgToy_T1_Fixed = numpy.log10(FT.FourierTS(eventsTrial2, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax))



    PowerB_Alltoys.append(Power_BkgToy)
    # PowerB_Alltoys_Smooth.append(Power_BkgToy_Smooth)
    if(argus.uncertainty=="yes"):
      PowerB_Alltoys_Unc_Fixed.append(Power_BkgToy_Unc_Fixed)
      # PowerB_Alltoys_Unc_Fixed_Smooth.append(Power_BkgToy_Unc_Fixed_Smooth)
      PowerB_Alltoys_Unc_T1_Fixed.append(Power_BkgToy_Unc_T1_Fixed)
      PowerB_Alltoys_T1_Fixed.append(Power_BkgToy_T1_Fixed)

  print("Toy experiments of background  done.")
  print("")



#Find Quantiles

if(argus.DoSignalFFT=="yes"):

  if(argus.uncertainty=="yes"):
    PowerBkgSig_FT_uncert_UpperQuantile = numpy.quantile(PowerS_Alltoys_Unc_T1_Fixed, 0.66)
    PowerBkgSig_FT_uncert_LowerQuantile = numpy.quantile(PowerS_Alltoys_Unc_T1_Fixed, 0.51)

if(argus.DoBkgFFT=="yes"):

  if(argus.uncertainty=="yes"):
    PowerBkg_FT_uncert_UpperQuantile = numpy.quantile(PowerB_Alltoys_Unc_T1_Fixed, 0.95)
    PowerBkg_FT_uncert_LowerQuantile = numpy.quantile(PowerB_Alltoys_Unc_T1_Fixed, 0.05)
    PowerBkg_FT_uncert_Median = numpy.median(PowerB_Alltoys_T1_Fixed)

    PowerBkg_stDev = numpy.std(PowerBkg_FT_nominal_ToysBkg)
    PowerBkg_mean = numpy.mean(PowerBkg_FT_nominal_ToysBkg)
    PowerBkg_stDevUP = PowerBkg_mean + PowerBkg_stDev/2
    PowerBkg_stDevDOWN = PowerBkg_mean - PowerBkg_stDev/2

if(argus.PlotAll=="yes") and (argus.DoBkgFFT=="yes") and (argus.DoSignalFFT=="yes"):

  if str(argus.FTTinclude_Optimization) == "no":
    
    if(argus.bands=="no"):
      figure_FFT_Toys_Numpy= plt.figure()
      ax = figure_FFT_Toys_Numpy.add_subplot()
      ax1 = figure_FFT_Toys_Numpy.add_subplot()
      ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
      # plt.plot(freq,Power_BkgFixed, 'b',linewidth=2, label=r"Analtyical Sig",zorder=2)
      # plt.plot(TList,numpy.average(PowerSB_Alltoys[n],axis=0), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg")
      # plt.plot(TList,numpy.average(PowerB_Alltoys[n],axis=0), 'b',linewidth=2, label=r"Fluctatuted Bkg")
      for i in range(0, nTrials2): 
        if(argus.uncertainty=="no"):
          plt.plot(freq,Power_SigFixed, 'g',linewidth=3, label=r"Analtyical Sig",zorder=2)
          plt.plot(freq,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(freq,PowerB_Alltoys[i], 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
        if(argus.uncertainty=="yes"):
          plt.plot(freq,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(freq,PowerB_Alltoys[i], 'black',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
          plt.plot(freq,Power_SigFixed, 'royalblue',linewidth=4, label=r"Analtyical Sig",zorder=2)
          # plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Signal+Bkg",zorder=0)
          # plt.plot(freq,PowerB_Alltoys_Unc_Fixed[i], 'orange',linewidth=2, label=r"Bkg Unc",zorder=2)
          plt.plot(freq,PowerS_Alltoys_Unc_Fixed[i], 'yellow',linewidth=2,linestyle='solid', label=r"Signal Unc",zorder=2)
          plt.scatter(Frequency_SigFixedPeak, PowerBkgSig_FT_uncert_UpperQuantile, color='green',marker='s',linewidths=5, label = "Sig_Unc_95%Q",zorder=3)
          plt.scatter(Frequency_SigFixedPeak, PowerBkgSig_FT_uncert_LowerQuantile, color='blue',marker='s',linewidths=5, label = "Sig_Unc_5%Q",zorder=3)
          plt.plot(freq,PowerB_Alltoys_Unc_Fixed[i], 'teal',linewidth=2, label=r"Bkg Unc",zorder=2)
          plt.scatter(Frequency_SigFixedPeak, PowerBkg_FT_uncert_UpperQuantile, color='pink',marker='s',linewidths=5,label = "Bkg_Unc_95%Q",zorder=3)
          plt.scatter(Frequency_SigFixedPeak, PowerBkg_FT_uncert_LowerQuantile,color='brown',marker='s',linewidths=5, label = "Bkg_Unc_5%Q",zorder=3)
          plt.scatter(Frequency_SigFixedPeak, PowerBkg_FT_uncert_Median,color='purple',marker='s',linewidths=5, label = "Bkg_Toys_Median",zorder=3)
          plt.scatter(Frequency_SigFixedPeak, PowerBkg_stDevUP,color='orange',marker='s',linewidths=5, label = "Bkg_Stat_StdDevUP",zorder=3)
          plt.scatter(Frequency_SigFixedPeak, PowerBkg_stDevDOWN,color='orange',marker='s',linewidths=5, label = "Bkg_Stat_StdDevDOWN",zorder=3)
          plt.plot(freq,Power_BkgFixed, 'purple',linewidth=2, label=r"bkg nominal",zorder=2)
          # plt.plot(TList,PowerB_Alltoys[i], 'grey',linewidth=2, label=r"Bkg Toys",zorder=0)
        if i == 0: 
          plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))
      t = ax1.yaxis.get_offset_text()
      t.set_x(-0.1)
      ax1.patches = []
      # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
      # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
      plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
      plt.ylabel('P(T)', ha='right', y=1.0)
      plt.margins(0)
      # plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
      # if(argus.log=="yes"):
        # plt.ylim(top=20)
        # plt.ylim(bottom=0)
      plt.xlim(right=0.1)
      # plt.ylim(bottom=1e17)
      # plt.yscale('log')
      # plt.xscale('log')
      figure_FFT_Toys_Numpy.savefig("Power_Spectrum/PowerSpectrum_Toys_FTrange_Window_AllUncertainty"+str(k)+"_"+str(M5)+".pdf")
    
    if(argus.bands=="yes"):
      # Plotting the Power spectrum for  Signal and Background Stat fluctauated shapes
      for i in range(0, nTrials2):
        figure_FFT_Toys = plt.figure()
        ax = figure_FFT_Toys.add_subplot()
        ax1 = figure_FFT_Toys.add_subplot()
        ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
        #Fill plots between ymin andymax to clean up presentation
        if(argus.uncertainty=="no"):
          plt.fill_between(freq*10,0,PowerB_Alltoys_Smooth[i], facecolor='blue', label="Bkg", alpha=1, zorder=0)
          plt.fill_between(freq*10,0,PowerSB_Alltoys_Smooth[i], facecolor='red', label="Bkg+Signal", alpha=1, zorder=0)
          #Plot Fixed Signal
          plt.plot(freq*10,Power_SigFixed_Smooth[i], 'g',linewidth=2, label=r"Analtyical Sig", zorder=1)
        if(argus.uncertainty=="yes"):
          plt.fill_between(freq*10,0,PowerB_Alltoys_Smooth[i], facecolor='blue', label="Bkg", alpha=1, zorder=0)
          plt.fill_between(freq*10,0,PowerSB_Alltoys_Smooth[i], facecolor='red', label="Bkg+Signal", alpha=1, zorder=0)
          plt.fill_between(freq*10,0,PowerS_Alltoys_Unc_Fixed_Smooth[i], facecolor='yellow', label="Bkg+Signal_Unc", alpha=1, zorder=0)
          #Plot Fixed Signal
          plt.plot(freq*10,Power_SigFixed_Smooth, 'g',linewidth=2, label=r"Analtyical Sig", zorder=1)
        t = ax1.yaxis.get_offset_text()
        t.set_x(-0.1)
        ax1.patches = []
        plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
        plt.ylabel('P(T)', ha='right', y=1.0)
        plt.margins(0)
        plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small")
        # plt.ylim(bottom=0)
        # plt.yscale('log')
        # plt.xscale('log')
        plt.xlim(right=0.1)
        figure_FFT_Toys.savefig("Power_Spectrum/PowerSpectrum_bandsToysNumpy_FTrange_Window_"+str(k)+"_"+str(M5)+".pdf")


  if str(argus.FTTinclude_Optimization) == "yes":

    if(argus.bands=="no"):
      figure_FFT_Toys_SignalUnc = plt.figure()
      ax = figure_FFT_Toys_SignalUnc.add_subplot()
      ax1 = figure_FFT_Toys_SignalUnc.add_subplot()
      ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
      # plt.plot(freq,Power_BkgFixed, 'b',linewidth=2, label=r"Analtyical Sig",zorder=2)
      # plt.plot(TList,numpy.average(PowerSB_Alltoys[n],axis=0), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg")
      # plt.plot(TList,numpy.average(PowerB_Alltoys[n],axis=0), 'b',linewidth=2, label=r"Fluctatuted Bkg")
      for i in range(0, nTrials2): 
        if(argus.uncertainty=="no"):
          plt.plot(TList,Power_SigFixed, 'g',linewidth=3, label=r"Analtyical Sig",zorder=2)
          plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(TList,PowerB_Alltoys[i], 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
        if(argus.uncertainty=="yes"):
          plt.plot(TList,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig",zorder=2)
          # plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Signal+Bkg",zorder=0)
          plt.plot(TList,PowerB_Alltoys_Unc_Fixed[i], 'b',linewidth=2, label=r"Bkg Unc",zorder=1)
          plt.plot(TList,PowerS_Alltoys_Unc_Fixed[i], 'yellow',linewidth=2,linestyle='solid', label=r"Signal Unc",zorder=0)
          plt.scatter(1, PowerBkgSig_FT_uncert_UpperQuantile, color='green',marker='s',linewidths=5, label = "Sig_Unc_95%Q",zorder=3)
          plt.scatter(1, PowerBkgSig_FT_uncert_LowerQuantile, color='blue',marker='s',linewidths=5, label = "Sig_Unc_5%Q",zorder=3)
          plt.plot(TList,PowerB_Alltoys_Unc_Fixed[i], 'teal',linewidth=2, label=r"Bkg Unc",zorder=1)
          plt.scatter(1, PowerBkg_FT_uncert_UpperQuantile, color='pink',marker='s',linewidths=5,label = "Bkg_Unc_95%Q",zorder=3)
          plt.scatter(1, PowerBkg_FT_uncert_LowerQuantile,color='brown',marker='s',linewidths=5, label = "Bkg_Unc_5%Q",zorder=3)
          plt.scatter(1, PowerBkg_FT_uncert_Median,color='red',marker='s',linewidths=7, label = "Bkg_Toys_Median",zorder=3)
          plt.scatter(1, PowerBkg_FT_uncert_UpperQuantile, color='pink',marker='s',linewidths=5,zorder=3)
          plt.scatter(1, PowerBkg_stDevUP,color='orange',marker='s',linewidths=5, label = "Bkg_Stat_StdDevUP",zorder=3)
          plt.scatter(1, PowerBkg_stDevDOWN,color='orange',marker='s',linewidths=5, label = "Bkg_Stat_StdDevDOWN",zorder=3)
          plt.plot(TList,Power_BkgFixed, 'r',linewidth=2, label=r"bkg nominal",zorder=2)
          plt.plot(TList,PowerB_Alltoys[i], 'black',linewidth=2,alpha=0.7, label=r"Bkg Toys",zorder=0)
        if i == 0: 
          plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))
      t = ax1.yaxis.get_offset_text()
      t.set_x(-0.1)
      ax1.patches = []
      # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
      # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
      plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
      plt.ylabel('P(T)', ha='right', y=1.0)
      plt.margins(0)
      plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
      # if(argus.log=="yes"):
        # plt.ylim(top=20)
        # plt.ylim(bottom=0)
      # plt.ylim(top=1e18)
      # plt.ylim(bottom=1e17)
      # plt.yscale('log')
      # plt.xscale('log')
      figure_FFT_Toys_SignalUnc.savefig("Power_Spectrum/PowerSpectrum_Toys_FTrange_Window_AllUncertainty"+str(k)+"_"+str(M5)+".pdf")

    if(argus.bands=="yes"):
      # Plotting the Power spectrum for  Signal and Background Stat fluctauated shapes
      for i in range(0, nTrials2): 
        figure_FFT_Toys = plt.figure()
        ax = figure_FFT_Toys.add_subplot()
        ax1 = figure_FFT_Toys.add_subplot()
        ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
        #Fill plots between ymin andymax to clean up presentation
        if(argus.uncertainty=="no"):
          plt.fill_between(Smoothed_OptimisedFreq_bins,0,PowerB_Alltoys_Smooth[i], facecolor='blue', label="Bkg", alpha=1, zorder=0)
          plt.fill_between(Smoothed_OptimisedFreq_bins,0,PowerSB_Alltoys_Smooth[i],facecolor='red', label="Bkg+Signal", alpha=1, zorder=0)
          #Plot Fixed Signal
          plt.plot(Smoothed_OptimisedFreq_bins,Power_SigFixed_Smooth[i], 'g',linewidth=2, label=r"Analtyical Sig", zorder=1)
        if(argus.uncertainty=="yes"):
          plt.fill_between(Smoothed_OptimisedFreq_bins,0,PowerB_Alltoys_Smooth[i], facecolor='blue', label="Bkg", alpha=1, zorder=0)
          plt.fill_between(Smoothed_OptimisedFreq_bins,0,PowerSB_Alltoys_Smooth[i], facecolor='red', label="Bkg+Signal", alpha=1, zorder=0)
          plt.fill_between(Smoothed_OptimisedFreq_bins,0,PowerS_Alltoys_Unc_Fixed_Smooth[i], facecolor='yellow', label="Bkg+Signal_Unc", alpha=1, zorder=0)
          #Plot Fixed Signal
          plt.plot(Smoothed_OptimisedFreq_bins,Power_SigFixed_Smooth, 'g',linewidth=2, label=r"Analtyical Sig", zorder=1)
        t = ax1.yaxis.get_offset_text()
        t.set_x(-0.1)
        ax1.patches = []
        plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
        plt.ylabel('P(T)', ha='right', y=1.0)
        plt.margins(0)
        plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small")
        # plt.ylim(bottom=0)
        # plt.yscale('log')
        # plt.xscale('log')
        figure_FFT_Toys.savefig("Power_Spectrum/PowerSpectrum_bandsToys_FTrange_WindowOptimised_"+str(k)+"_"+str(M5)+".pdf")



if(argus.DoSignalFFT=="yes"):

  if str(argus.FTTinclude_Optimization) == "yes":

    figure_FFT_Fixed = plt.figure()
    ax = figure_FFT_Fixed.add_subplot()
    ax1 = figure_FFT_Fixed.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    # for n in range(0, stepFT):
    # plt.plot(TList,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig")
    # plt.plot(TList,Power_SigFixed_EnergyResolution_DOWN, 'yellow',linewidth=2, label=r"Analtyical Sig_ResDOWN")
    # plt.plot(TList,Power_SigFixed_EnergyResolution_UP, 'orange',linewidth=2, label=r"Analtyical Sig_ResUP")
    # plt.plot(TList,Power_SigFixed_EnergyScale_UP[n], 'lightblue',linewidth=2, label=r"Analtyical Sig_ScaleUP")
    if(argus.uncertainty=="no"):
      plt.plot(TList,Power_SigBkgFixed, 'r',linewidth=2, label=r"Sig+Bkg")
      plt.plot(TList,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
    if(argus.uncertainty=="yes"):
      plt.plot(TList,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
      # plt.plot(TList,Power_BkgToy_Unc_Fixed_test, 'g',linewidth=2, label=r"BkgUnc")
      plt.plot(TList,Power_SigBkgFixed, 'r',linewidth=2, label=r"Sig+Bkg")
      # plt.plot(TList,Power_SigBkgToy_Unc_Fixed, 'teal',linewidth=2, label=r"Analtyical Sig_Unc")
    t = ax1.yaxis.get_offset_text()
    ax1.patches = []
    t.set_x(-0.1)
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
    plt.margins(0)
    plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))    
    if(argus.log=="yes"):
      plt.ylim(top=20)
      plt.ylim(bottom=0)
    # plt.ylim(bottom=0)
    # plt.yscale('log')
    # plt.xscale('log')
    figure_FFT_Fixed.savefig("Power_Spectrum/PowerSpectrum_SigFixed_FTrange_Window_"+str(k)+"_"+str(M5)+".pdf")

    if(argus.bands=="no"):
      figure_FFT_Toys_SignalUnc = plt.figure()
      ax = figure_FFT_Toys_SignalUnc.add_subplot()
      ax1 = figure_FFT_Toys_SignalUnc.add_subplot()
      ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
      # plt.plot(freq,Power_BkgFixed, 'b',linewidth=2, label=r"Analtyical Sig",zorder=2)
      # plt.plot(TList,numpy.average(PowerSB_Alltoys[n],axis=0), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg")
      # plt.plot(TList,numpy.average(PowerB_Alltoys[n],axis=0), 'b',linewidth=2, label=r"Fluctatuted Bkg")
      for i in range(0, nTrials2): 
        if(argus.uncertainty=="no"):
          plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(TList,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig",zorder=2)
        if(argus.uncertainty=="yes"):
          plt.plot(TList,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig",zorder=2)
          plt.plot(TList,PowerS_Alltoys_Unc_Fixed[i], 'yellow',linewidth=2,linestyle='solid', label=r"Signal+Bkg Unc",zorder=0)
          # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
          # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
        if i == 0: 
          plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))      
      t = ax1.yaxis.get_offset_text()
      t.set_x(-0.1)
      ax1.patches = []
      # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
      # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
      plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
      plt.ylabel('P(T)', ha='right', y=1.0)
      plt.margins(0)
      plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
      if(argus.log=="yes"):
        plt.ylim(top=20)
        plt.ylim(bottom=0)
      # plt.yscale('log')
      # plt.xscale('log')
      figure_FFT_Toys_SignalUnc.savefig("Power_Spectrum/PowerSpectrum_Toys_FTrange_Window_SignalUncertainty"+str(k)+"_"+str(M5)+".pdf")



  if str(argus.FTTinclude_Optimization) == "no":

    figure_FFT_Fixed = plt.figure()
    ax = figure_FFT_Fixed.add_subplot()
    ax1 = figure_FFT_Fixed.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    # for n in range(0, stepFT):
    # plt.plot(TList,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig")
    # plt.plot(TList,Power_SigFixed_EnergyResolution_DOWN, 'yellow',linewidth=2, label=r"Analtyical Sig_ResDOWN")
    # plt.plot(TList,Power_SigFixed_EnergyResolution_UP, 'orange',linewidth=2, label=r"Analtyical Sig_ResUP")
    # plt.plot(TList,Power_SigFixed_EnergyScale_UP[n], 'lightblue',linewidth=2, label=r"Analtyical Sig_ScaleUP")
    if(argus.uncertainty=="no"):
      if(argus.log=="no"):
        plt.plot(freq,Power_SigFixed, 'r',linewidth=2, label=r"Sig")
    if(argus.uncertainty=="yes"):
      if(argus.log=="no"):
        plt.plot(freq,Power_SigBkgFixed, 'r',linewidth=2, label=r"Sig+Bkg")
        plt.plot(freq,Power_SigBkgToy_Unc_Fixed, 'teal',linewidth=2, label=r"Analtyical Sig_Unc")
      if(argus.log=="yes"):
        plt.plot(freq,numpy.log10(Power_SigBkgFixed), 'r',linewidth=2, label=r"Sig+Bkg")
        plt.plot(freq,numpy.log10(Power_SigBkgFixed), 'r',linewidth=2, label=r"Sig+Bkg")
        plt.plot(freq,numpy.log10(Power_SigBkgToy_Unc_Fixed), 'teal',linewidth=2, label=r"Analtyical Sig_Unc")
        # plt.ylim(top=20)
        plt.ylim(bottom=0)
    t = ax1.yaxis.get_offset_text()
    ax1.patches = []
    t.set_x(-0.1)
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    plt.margins(0)
    # plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
    plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small",bbox_to_anchor=(1.1, 1.05))
    # plt.ylim(bottom=0)
    # plt.yscale('log')
    # plt.xscale('log')
    plt.xlim(right=0.1)
    figure_FFT_Fixed.savefig("Power_Spectrum/DefaultPowerSpectrum_SigFixed_FTrange_Window_"+str(k)+"_"+str(M5)+".pdf")


    if(argus.bands=="no"):
      figure_FFT_Toys_SignalUnc = plt.figure()
      ax = figure_FFT_Toys_SignalUnc.add_subplot()
      ax1 = figure_FFT_Toys_SignalUnc.add_subplot()
      ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
      for i in range(0, nTrials2): 
        if(argus.uncertainty=="no"):
          if(argus.log=="no"):
            plt.plot(freq,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
            plt.plot(freq,PowerB_Alltoys[i], 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
          if(argus.log=="yes"):
            plt.plot(freq,numpy.log10(PowerSB_Alltoys[i]), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
            plt.plot(freq,numpy.log10(PowerB_Alltoys[i]), 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
        if(argus.uncertainty=="yes"):
          if(argus.log=="no"):
            plt.plot(freq,Power_SigFixed, 'g',linewidth=2, label=r"Analtyical Sig",zorder=2)
            plt.plot(freq,PowerS_Alltoys_Unc_Fixed[i], 'yellow',linewidth=2,linestyle='solid', label=r"Signal+Bkg Unc",zorder=0)
            # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
            # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
          if(argus.log=="yes"):
            plt.plot(freq,numpy.log10(Power_SigFixed), 'g',linewidth=2, label=r"Analtyical Sig",zorder=2)
            plt.plot(freq,numpy.log10(PowerS_Alltoys_Unc_Fixed[i]), 'yellow',linewidth=2,linestyle='solid', label=r"Signal+Bkg Unc",zorder=0)
            # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
            # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
            plt.ylim(top=20)
            plt.ylim(bottom=0)
        if i == 0: 
          plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small",bbox_to_anchor=(1.1, 1.05))
      t = ax1.yaxis.get_offset_text()
      t.set_x(-0.1)
      ax1.patches = []
      # plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
      # plt.axhline(y=PowerBkgSig_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Sig_Unc_66%Q")
      # plt.axhline(y=PowerBkgSig_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Sig_Unc_34%Q")
      plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
      plt.ylabel('P(T)', ha='right', y=1.0)
      plt.margins(0)
      # plt.ylim(top=1e18)
      # plt.ylim(bottom=1e17)
      # plt.yscale('log')
      # plt.xscale('log')
      plt.xlim(right=0.1)
      figure_FFT_Toys_SignalUnc.savefig("Power_Spectrum/DefaultPowerSpectrum_Toys_FTrange_Window_SignalUncertainty"+str(k)+"_"+str(M5)+".pdf")

  if(argus.s=="GenericSignal"):
    # Fixed Signal Cosine Plotting
    fig_SigBkg, ax = plt.subplots()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    # plt.plot(massList,BkgFix, 'b',linewidth=2, label=r"Background")
    plt.plot(massList,SigFix_NoTF, 'r',linewidth=2, label=r"Signal") 
    plt.plot(massList,SigFix, 'purple',linewidth=2, label=r"Signal_NominalTF")
    plt.plot(massList,SigFix_EnergyResolution_DOWN, 'yellow',linewidth=2, label=r"SignalEnergyResolution_DOWN")
    plt.plot(massList,SigFix_EnergyScale_DOWN, 'y',linewidth=2, label=r"SignalEnergyScale_DOWN")
    plt.plot(massList,SigFix_EnergyResolution_UP, 'g',linewidth=2, label=r"SignalEnergyResolution_UP")
    plt.plot(massList,SigFix_EnergyScale_UP, 'teal',linewidth=2, label=r"SignalEnergyScale_UP")
    plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small")
    plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
    plt.ylabel('Events', ha='right', y=1.0)
    plt.margins(0)
    # plt.legend()
    plt.ylim(bottom=10e-4)
    plt.yscale('log')
    # plt.xscale('log')
    fig_SigBkg.savefig('Power_Spectrum/SigBkg_plot.pdf')

if(argus.DoBkgFFT=="yes"):


  if str(argus.FTTinclude_Optimization) == "yes":

    figure_FFT_Fixed = plt.figure()
    ax = figure_FFT_Fixed.add_subplot()
    ax1 = figure_FFT_Fixed.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    if(argus.uncertainty=="no"):
      plt.plot(TList,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
    if(argus.uncertainty=="yes"):
      plt.plot(TList,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
      plt.plot(TList,Power_BkgToy_Unc_Fixed_test, 'g',linewidth=2, label=r"BkgUnc")
    t = ax1.yaxis.get_offset_text()
    ax1.patches = []
    t.set_x(-0.1)
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
    plt.margins(0)
    plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))    
    if(argus.log=="yes"):
      plt.ylim(top=20)
      plt.ylim(bottom=0)
    # plt.ylim(bottom=0)
    # plt.yscale('log')
    # plt.xscale('log')
    figure_FFT_Fixed.savefig("Power_Spectrum/PowerSpectrum_BkgFixed_FTrange_Window_"+str(k)+"_"+str(M5)+".pdf")


  # Plotting the Power spectrum for  Signal and Background Stat fluctauated shapes
    figure_FFT_Toys_BackgroundUnc = plt.figure()
    ax = figure_FFT_Toys_BackgroundUnc.add_subplot()
    ax1 = figure_FFT_Toys_BackgroundUnc.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    # plt.plot(TList,Power_BkgFixed, 'r',linewidth=2, label=r"bkg nominal",zorder=2)
    # plt.plot(TList,numpy.average(PowerSB_Alltoys[n],axis=0), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg")
    # plt.plot(TList,numpy.average(PowerB_Alltoys[n],axis=0), 'b',linewidth=2, label=r"Fluctatuted Bkg")
    for i in range(0, nTrials2): 
      if(argus.uncertainty=="no"):
        plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
        plt.plot(TList,PowerB_Alltoys[i], 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
      if(argus.uncertainty=="yes"):
        # plt.plot(TList,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Signal+Bkg",zorder=0)
        plt.plot(TList,PowerB_Alltoys_Unc_Fixed[i], 'y',linewidth=2, label=r"Bkg Unc",zorder=1)
        plt.axhline(y=PowerBkg_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Bkg_Unc_95%Q",zorder=3)
        plt.axhline(y=PowerBkg_FT_uncert_LowerQuantile,color='brown', linestyle='--', label = "Bkg_Unc_5%Q",zorder=3)
        # plt.axhline(y=PowerBkg_FT_uncert_Median,color='black', linestyle='--', label = "Bkg_Stat_Median",zorder=3)
        plt.plot(TList,Power_BkgFixed, 'r',linewidth=2, label=r"bkg nominal",zorder=2)
      if i == 0: 
        plt.legend(borderpad=1, frameon=True, framealpha=1, loc=7, fontsize="xx-small",bbox_to_anchor=(1.1, 0.5))    
    t = ax1.yaxis.get_offset_text()
    t.set_x(-0.1)
    ax1.patches = []
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    plt.axvline(x=1,color='black', linestyle='--', label = "T=1")
    if(argus.log=="yes"):
      plt.ylim(top=20)
      plt.ylim(bottom=0)
    # plt.margins(0)
    plt.ylim(bottom=0)
    plt.yscale('log')
    # plt.xscale('log')
    figure_FFT_Toys_BackgroundUnc.savefig("Power_Spectrum/PowerSpectrum_Toys_FTrange_Window_BkgUncertainty"+str(k)+"_"+str(M5)+".pdf")


    fig_BkgUncDiff, ax = plt.subplots()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    for i in range(0, nTrials2): 
      plt.plot(massList,Bkg_NomUnc_Diff[i], 'orange',linewidth=2, label=r"Bkg Unc - Nominal",zorder=1)
      if i == 0: 
        plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small")
    plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
    plt.ylabel('Events', ha='right', y=1.0)
    plt.margins(0)
    # plt.legend()
    # plt.ylim(bottom=10e-4)
    # plt.yscale('log')
    # plt.xscale('log')
    fig_BkgUncDiff.savefig('Power_Spectrum/BkgUncDiff_plot.pdf')


  if str(argus.FTTinclude_Optimization) == "no":

    figure_BkgFFT_Fixed = plt.figure()
    ax = figure_BkgFFT_Fixed.add_subplot()
    ax1 = figure_BkgFFT_Fixed.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    if(argus.uncertainty=="no"):
      if(argus.log=="no"):
        plt.plot(freq,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
      if(argus.log=="yes"):
        plt.plot(freq,numpy.log10(Power_BkgFixed), 'b',linewidth=2, label=r"BkgOnly")
    if(argus.uncertainty=="yes"):
      if(argus.log=="no"):
        plt.plot(freq,Power_BkgFixed, 'b',linewidth=2, label=r"BkgOnly")
        plt.plot(freq,Power_BkgToy_Unc_Fixed_test, 'g',linewidth=2, label=r"BkgUnc")
      if(argus.log=="yes"):
        plt.plot(freq,numpy.log10(Power_BkgFixed), 'b',linewidth=2, label=r"BkgOnly")
        plt.plot(freq,numpy.log10(Power_BkgToy_Unc_Fixed_test), 'g',linewidth=2, label=r"BkgUnc")
    t = ax1.yaxis.get_offset_text()
    ax1.patches = []
    t.set_x(-0.1)
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    plt.margins(0)
    plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small",bbox_to_anchor=(1.1, 1.05))
    # plt.ylim(bottom=0)
    # plt.yscale('log')
    # plt.xscale('log')
    plt.xlim(right=0.1)
    figure_BkgFFT_Fixed.savefig("Power_Spectrum/DefaultPowerSpectrum_BkgFixed_FTrange_Window_"+str(k)+"_"+str(M5)+".pdf")


    figure_FFT_Toys_BackgroundUnc = plt.figure()
    ax = figure_FFT_Toys_BackgroundUnc.add_subplot()
    ax1 = figure_FFT_Toys_BackgroundUnc.add_subplot()
    ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
    # plt.plot(TList,Power_BkgFixed, 'r',linewidth=2, label=r"bkg nominal",zorder=2)
    # plt.plot(TList,numpy.average(PowerSB_Alltoys[n],axis=0), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg")
    # plt.plot(TList,numpy.average(PowerB_Alltoys[n],axis=0), 'b',linewidth=2, label=r"Fluctatuted Bkg")
    for i in range(0, nTrials2): 
      if(argus.uncertainty=="no"):
        if(argus.log=="no"):
          plt.plot(freq,PowerSB_Alltoys[i], 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(freq,PowerB_Alltoys[i], 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
        if(argus.log=="yes"):
          plt.plot(freq,numpy.log10(PowerSB_Alltoys[i]), 'r',linewidth=2, label=r"Fluctatuted Signal+Bkg",zorder=0)
          plt.plot(freq,numpy.log10(PowerB_Alltoys[i]), 'b',linewidth=2, label=r"Fluctatuted Bkg",zorder=1)
      if(argus.uncertainty=="yes"):
        if(argus.log=="no"):
          plt.plot(freq,PowerB_Alltoys_Unc_Fixed[i], 'y',linewidth=2, label=r"Bkg Unc",zorder=1)
          # plt.axhline(y=PowerBkg_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Bkg_Unc_95%Q")
          # plt.axhline(y=PowerBkg_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Bkg_Unc_5%Q")
          # plt.axhline(y=PowerBkg_FT_uncert_Median,color='black', linestyle='--', label = "Bkg_Stat_Median")
          plt.plot(freq,Power_BkgFixed, 'r',linewidth=2, label=r"bkg nominal",zorder=2)
        if(argus.log=="yes"):
          plt.plot(freq,numpy.log10(PowerB_Alltoys_Unc_Fixed[i]), 'y',linewidth=2, label=r"Bkg Unc",zorder=1)
          # plt.axhline(y=PowerBkg_FT_uncert_UpperQuantile,color='purple', linestyle='--', label = "Bkg_Unc_95%Q")
          # plt.axhline(y=PowerBkg_FT_uncert_LowerQuantile,color='orange', linestyle='--', label = "Bkg_Unc_5%Q")
          # plt.axhline(y=PowerBkg_FT_uncert_Median,color='black', linestyle='--', label = "Bkg_Stat_Median")
          plt.plot(freq,numpy.log10(Power_BkgFixed), 'r',linewidth=2, label=r"bkg nominal",zorder=2)
      if i == 0: 
        plt.legend(borderpad=0.8, frameon=False, loc='best', fontsize="xx-small",bbox_to_anchor=(1.1, 1.05))
    t = ax1.yaxis.get_offset_text()
    t.set_x(-0.1)
    ax1.patches = []
    plt.xlabel(r'$\mathrm{T}}$', ha='right', x=1.0)
    plt.ylabel('P(T)', ha='right', y=1.0)
    # plt.margins(0)
    # plt.ylim(bottom=0)
    # plt.yscale('log')
    # plt.xscale('log')
    plt.xlim(right=0.1)
    figure_FFT_Toys_BackgroundUnc.savefig("Power_Spectrum/DefaultPowerSpectrum_Toys_FTrange_Window_BkgUncertainty"+str(k)+"_"+str(M5)+".pdf")

