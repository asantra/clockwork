##########
#   FT   #
##########

### Explanations ###
# - This program makes a test statistic function using Fourier transforms and also does the basic Fourier transform.

#Import generic
import cmath, math, sys, os
import numpy as np
from   bisect import bisect_left
import ROOT
import array
from operator import itemgetter
from scipy import signal

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))
import Basic

MP = 2.435E18  #Reduced Planck scale

#Series of functions to compute the P(T) over different mass windows and also different T values to ploty the spectrum itself
# We now do one mass window that we know by construction is optmised to make things easier to work with
######################################################################
############ AlternativeFourier Transform Test Statisitic functions ##
######################################################################


# Fourier transform
def FourierP_TPeakPlotter(Signal,lum, lumDiv, k, M5, massList, xmin, xmax, T):
  #Initialize variables
  Psq = 0

  
  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  binMax = bisect_left(massList,  max(xmax, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    if lumDiv:
      lumT = lum[b]
    else:
      lumT = 1


    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])


  #Return P(T)
  return (np.absolute(Psq))**2


# Fourier transform
def FourierP_TPeakPlotter_NoLum(Signal, k, M5, massList, xmin, xmax, T):
  #Initialize variables
  Psq = 0
    
  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  # binMin = int(k)
  binMax = bisect_left(massList,  max(xmax, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    lumT = 1
  
    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2


# Fourier transform
def FourierP(Signal, lum, lumDiv, T, k, M5, massList, xmin, xmax):
  #Initialize variables
  Psq = 0

  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  # binMin = int(k)
  binMax = bisect_left(massList,  max(xmax, binMin))

  print("binMin",binMin)
  #Loop over signal
  for b in range(binMin, binMax - 1):


    if lumDiv:
      lumT = lum[b]
    else:
      lumT = 1
    
 
    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2

# Fourier transform
def FourierP_NoLum(Signal, T, k, M5, massList, xmin, xmax):
  #Initialize variables
  Psq = 0

  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  binMax = bisect_left(massList,  max(xmax, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    lumT = 1


    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2

#List of test statistics
def FourierTS(Signal, Background, backgroundSub, lum, lumDiv, T, k, M5, massList, xmin, xmax):

  #Initialize variable
  listTS = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal


  listTS.append(FourierP(SignalInt, lum, lumDiv, T, k, M5, massList, xmin, xmax))

  #Return the list of test statistics
  return listTS

#List of test statistics
def FourierTS_NoLum(Signal, Background, backgroundSub, T, k, M5, massList, xmin, xmax):

  #Initialize variable
  listTS = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal


  listTS.append(FourierP_NoLum(SignalInt, T, k, M5, massList, xmin, xmax))

  #Return the list of test statistics
  return listTS

#List of test statistics
def FourierTS_PeakPlotter_NoLum(Signal, Background, backgroundSub, k, M5, massList, xmin, xmax, TList):

  #Initialize variable
  Psq_TList = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  #Loop over possible values of mpnk
  for T in TList:
    Psq_TList.append(FourierP_TPeakPlotter_NoLum(SignalInt, k, M5, massList, xmin, xmax, T))
  #Return the list of test statistics
  return Psq_TList

#List of test statistics
def FourierTS_PeakPlotter(Signal, Background, backgroundSub, lum, lumDiv, k, M5, massList, xmin, xmax, TList):


  #Initialize variable
  Psq_TList = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  for T in TList:
    Psq_TList.append(FourierP_TPeakPlotter(SignalInt, lum, lumDiv, k, M5, massList, xmin, xmax, T))

  #Return the list of test statistics
  return Psq_TList

#############################################################################
############################ Numpy ##########################################
#############################################################################
##### FFT with no singal optimzation i.e pure numpy functions to comput DFT


def Default_DFT(Signal, Background, backgroundSub, xmin, xmax): ### only do the FFT above a mass threshold x0 to cut the low mass stat...
  
  # print("Signallength",len(Signal))
  # print("Backgroundlength",len(Background))
  # if backgroundSub:
  #   SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  # else:
  #   SignalInt = Signal
  # print("SignalInt",len(SignalInt))
  # mass = []
  # diff = []
  # for b in range(1,len(SignalInt)):
  #   # print("b",b)
  #   if b<xmin: continue
  #   if b>xmax: break
  #   mass.append(b)
  #   diff.append(SignalInt[b])
  # # mass = np.pad(mass, (200, 0), 'constant')
  # # diff = np.pad(diff, (200, 0), 'constant')
  # # print("mass",mass)
  # # create the x-axis
  # N = len(mass)
  # t = np.linspace(mass[0],mass[N-1],N)
  # f = np.asarray(diff)
  # # perform FT and multiply by dt
  # dt = t[1]-t[0]
  # ft = np.fft.fft(f) * dt      
  # freq = np.fft.fftfreq(N, dt)
  # freq = freq[:int((N/2)+1)] #python3 cast as int
  # # print("freq",freq)
  # # get the magnitude
  # mag = []
  # freqmin = 0.005 # was 0.005
  # freqmax = 0.5 # was 0.065 
  # freqs = []
  # for n in range(len(freq)-1):
  #   if(freq[n]<=freqmin): continue
  #   if(freq[n]>=freqmax): break
  #   freqs.append(freq[n])
  #   mag.append( np.abs(ft[:int(N/2+1)])[n] )
  # Nfreqs = len(freqs)
  # dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # # fill the histogram
  # # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  # hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  # magList = []
  # for n in range(len(mag)):
  #   if(freqs[n]>freqmax): break
  #   hFFT.SetBinContent(n+1,mag[n])
  #   x = hFFT.GetBinContent(n)
  #   magList.append(x)
  # # # col = 0
  # # # hFFT.SetLineColor(col)
  # # hFFT.SetLineWidth(2)
  # # hFFT.SetMinimum(0)
  # # hFFT.SetMaximum(hFFT.GetMaximum()*1.5)
  # # hFFT.Write()
  # # # fOut.Close()
  # return magList, freqs

  print("Signallength",len(Signal))
  print("Backgroundlength",len(Background))
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal
  print("SignalInt",len(SignalInt))
  mass = []
  diff = []
  for b in range(1,len(SignalInt)):
    # print("b",b)
    if b<xmin: continue
    if b>xmax: break
    mass.append(b)
    diff.append(SignalInt[b])
  # mass = np.pad(mass, (200, 0), 'constant')
  # diff = np.pad(diff, (200, 0), 'constant')
  # print("mass",mass)
  # create the x-axis
  N = len(mass)
  t = np.linspace(mass[0],mass[N-1],N)
  f = np.asarray(diff)
  # perform FT and multiply by dt
  dt = t[1]-t[0]
  ft = np.fft.rfft(f) * dt      
  freq = np.fft.rfftfreq(N, dt)
  # freq = freq[:int(N/2+1)] #python3 cast as int
  # print("freq",freq)
  # get the magnitude
  mag = []
  freqmin = 0.005 # was 0.005
  freqmax = 0.1 # was 0.065 
  freqs = []
  for n in range(len(freq)-1):
    if(freq[n]<=freqmin): continue
    # if(freq[n]>=freqmax): break
    freqs.append(freq[n])
    # mag.append( np.abs(ft[:int(N/2+1)])[n] )
    mag.append( (np.abs(ft)[n])**2 )
  Nfreqs = len(freqs)
  print("Nfreqs", Nfreqs)
  dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # fill the histogram
  # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  magList = []
  for n in range(len(mag)):
    # if(freqs[n]>freqmax): break
    hFFT.SetBinContent(n+1,mag[n])
    x = hFFT.GetBinContent(n)
    magList.append(x)
  # # col = 0
  # # hFFT.SetLineColor(col)
  # hFFT.SetLineWidth(2)
  # hFFT.SetMinimum(0)
  # hFFT.SetMaximum(hFFT.GetMaximum()*1.5)
  # hFFT.Write()
  # # fOut.Close()
  return magList, freqs

def Default_DFT_TestStatistic(Signal, Background, backgroundSub, SearchFrequency, xmin, xmax): ### only do the FFT above a mass threshold x0 to cut the low mass stat...
  
  # if backgroundSub:
  #   SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  # else:
  #   SignalInt = Signal
  # print("SignalInt",len(SignalInt))
  # mass = []
  # diff = []
  # for b in range(1,len(SignalInt)):
  #   # print("b",b)
  #   if b<xmin: continue
  #   if b>xmax: break
  #   mass.append(b)
  #   diff.append(SignalInt[b])
  # # mass = np.pad(mass, (200, 0), 'constant')
  # # diff = np.pad(diff, (200, 0), 'constant')
  # # print("mass",mass)
  # # create the x-axis
  # N = len(mass)
  # t = np.linspace(mass[0],mass[N-1],N)
  # f = np.asarray(diff)
  # # perform FT and multiply by dt
  # dt = t[1]-t[0]
  # ft = np.fft.fft(f) * dt      
  # freq = np.fft.fftfreq(N, dt)
  # freq = freq[:int(N/2+1)] #python3 cast as int
  # # print("freq",freq)
  # # get the magnitude
  # mag = []
  # freqmin = 0.005 # was 0.005
  # freqmax = 0.1 # was 0.065 
  # freqs = []
  # for n in range(len(freq)-1):
  #   if(freq[n]<=freqmin): continue
  #   if(freq[n]>=freqmax): break
  #   freqs.append(freq[n])
  #   mag.append( np.abs(ft[:int(N/2+1)])[n] )
  # Nfreqs = len(freqs)
  # dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # # fill the histogram
  # # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  # hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  # TestStat = []
  # for n in range(len(mag)):
  #   if(freqs[n]>freqmax): break
  #   if freqs[n] == SearchFrequency: continue
  #   hFFT.SetBinContent(n+1,mag[n])
  #   x = hFFT.GetBinContent(n)
  #   TestStat.append(x)

  # return TestStat

  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal
  print("SignalInt",len(SignalInt))
  mass = []
  diff = []
  for b in range(1,len(SignalInt)):
    # print("b",b)
    if b<xmin: continue
    if b>xmax: break
    mass.append(b)
    diff.append(SignalInt[b])
  # mass = np.pad(mass, (200, 0), 'constant')
  # diff = np.pad(diff, (200, 0), 'constant')
  # print("mass",mass)
  # create the x-axis
  N = len(mass)
  t = np.linspace(mass[0],mass[N-1],N)
  f = np.asarray(diff)
  # perform FT and multiply by dt
  dt = t[1]-t[0]
  ft = np.fft.rfft(f) * dt      
  freq = np.fft.rfftfreq(N, dt)
  # freq = freq[:int(N/2+1)] #python3 cast as int
  # print("freq",freq)
  # get the magnitude
  mag = []
  freqmin = 0.005 # was 0.005
  freqmax = 0.1 # was 0.065 
  freqs = []
  for n in range(len(freq)-1):
    if(freq[n]<=freqmin): continue
    if(freq[n]<=0.02): continue
    # if(freq[n]>=freqmax): break
    freqs.append(freq[n])
    # mag.append( np.abs(ft[:int(N/2+1)])[n] )
    mag.append( (np.abs(ft)[n])**2 )
  Nfreqs = len(freqs)
  dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # fill the histogram
  # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  TestStat = []
  for n in range(len(mag)):
    # if(freqs[n]>freqmax): break
    if freqs[n] == SearchFrequency: continue
    hFFT.SetBinContent(n+1,mag[n])
    x = hFFT.GetBinContent(n)
    TestStat.append(x)

  return TestStat


def Default_DFT_SignalPeakFinder(Signal, Background, backgroundSub, xmin, xmax): ### only do the FFT above a mass threshold x0 to cut the low mass stat...
  
  # if backgroundSub:
  #   SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  # else:
  #   SignalInt = Signal

  # mass = []
  # diff = []
  # for b in range(1,len(SignalInt)):
  #   # print("b",b)
  #   if b<xmin: continue
  #   if b>xmax: break
  #   mass.append(b)
  #   diff.append(SignalInt[b])
  # # mass = np.pad(mass, (200, 0), 'constant')
  # # diff = np.pad(diff, (200, 0), 'constant')
  # # print("mass",mass)
  # # create the x-axis
  # N = len(mass)
  # t = np.linspace(mass[0],mass[N-1],N)
  # f = np.asarray(diff)
  # # perform FT and multiply by dt
  # dt = t[1]-t[0]
  # #Add in Norm option here norm=None
  # ft = np.fft.fft(f) * dt      
  # freq = np.fft.fftfreq(N, dt)
  # freq = freq[:int(N/2+1)] #python3 cast as int
  # # print("freq",freq)
  # # get the magnitude
  # mag = []
  # freqmin = 0.05 # was 0.005
  # freqmax = 0.1 # was 0.065 
  # freqs = []
  # for n in range(len(freq)-1):
  #   if(freq[n]<=freqmin): continue
  #   if(freq[n]>=freqmax): break
  #   freqs.append(freq[n])
  #   mag.append( np.abs(ft[:int(N/2+1)])[n] )
  # Nfreqs = len(freqs)
  # dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # # fill the histogram
  # # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  # hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  # magList = []
  # freqList = []
  # for n in range(len(mag)):
  #   if(freqs[n]>freqmax): break
  #   hFFT.SetBinContent(n+1,mag[n])
  #   x = hFFT.GetBinContent(n)
  #   magList.append(x)
  #   freqList.append(freqs[n])
  # #Find highest amplitude after the small frequency slope
  # Combined_MagFreqList = list(zip(freqList, magList))
  # # print("Combined_MagFreqList",Combined_MagFreqList)
  # Max_AmplitudeFrq = max(Combined_MagFreqList,key=itemgetter(1))[0]   
  # print("Max_AmplitudeFrq",Max_AmplitudeFrq)
  # return Max_AmplitudeFrq

  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  mass = []
  diff = []
  for b in range(1,len(SignalInt)):
    # print("b",b)
    if b<xmin: continue
    if b>xmax: break
    mass.append(b)
    diff.append(SignalInt[b])
  # mass = np.pad(mass, (200, 0), 'constant')
  # diff = np.pad(diff, (200, 0), 'constant')
  # print("mass",mass)
  # create the x-axis
  N = len(mass)
  t = np.linspace(mass[0],mass[N-1],N)
  f = np.asarray(diff)
  # perform FT and multiply by dt
  dt = t[1]-t[0]
  #Add in Norm option here norm=None
  ft = np.fft.rfft(f) * dt      
  freq = np.fft.rfftfreq(N, dt)
  # freq = freq[:int(N/2+1)] #python3 cast as int
  # print("freq",freq)
  # get the magnitude
  mag = []
  freqmin = 0.005 # was 0.005
  freqmax = 0.1 # was 0.065 
  freqs = []
  for n in range(len(freq)-1):
    if(freq[n]<=freqmin): continue
    if(freq[n]<=0.02): continue
    freqs.append(freq[n])
    # mag.append( np.abs(ft[:int(N/2+1)])[n] )
    mag.append( (np.abs(ft)[n])**2 )
  Nfreqs = len(freqs)
  dfreq = (freq[Nfreqs-1]-freq[0])/Nfreqs
  # fill the histogram
  # fOut = ROOT.TFile(Directory+"FTOutput.root","RECREATE")
  hFFT = ROOT.TH1D("fft","FFT;Frequency [1/GeV];Amplitude",Nfreqs,freqs[0]-dfreq,freqs[Nfreqs-1]+dfreq)
  magList = []
  freqList = []
  for n in range(len(mag)):
    # if(freqs[n]>freqmax): break
    hFFT.SetBinContent(n+1,mag[n])
    x = hFFT.GetBinContent(n)
    magList.append(x)
    freqList.append(freqs[n])
  #Find highest amplitude after the small frequency slope
  Combined_MagFreqList = list(zip(freqList, magList))
  # print("Combined_MagFreqList",Combined_MagFreqList)
  Max_AmplitudeFrq = max(Combined_MagFreqList,key=itemgetter(1))[0]   
  print("Max_AmplitudeFrq",Max_AmplitudeFrq)
  return Max_AmplitudeFrq

#   if scale_amplitudes is True:
#       fft_amplitudes = 2 * fft_amplitudes / (len(SignalInt))

##############################################################
################# Double Sided Error Function ################
##############################################################

def DoubleSided_ErrorFunc(mass,Strength,Width,ArbParameter):
  DoubleError_func = mass*(2-math.erfc((((Width/2)+mass)/(ArbParameter))))*(2-math.erfc((((Width/2)-mass)/(ArbParameter))))
  return DoubleError_func
