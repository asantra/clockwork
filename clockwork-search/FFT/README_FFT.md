There are alot of scripts/options to run here. This will be updated as projects are finalised. For now the focus will be on PowerSpectrum_Plotter.py

A tyoical script command for the plotter is:

python3 Power_SpectrumPlotter.py -k 500 -M5 4000 -s Theory_Dielectron_Sig -b Dielectron_Bkg -lumDiv False -bkgsub True -uncertainty yes -bands no -DoSignalFFT yes -DoBkgFFT yes -FTTinclude_Optimization yes -PlotAll yes -log no

As you can see there are alot of options -  to analyse the cosine signal something liek the following is used:

python3 Power_SpectrumPlotter.py -k 500 -M5 4000 -s GenericSignal -b Dielectron_Bkg -lumDiv False -bkgsub True -uncertainty yes -bands no -DoSignalFFT yes -DoBkgFFT yes -FTTinclude_Optimization no -PlotAll no -log no

The command:

-s GenericSignal

Will run the cosine based signal. The k and M5 options if this signal is picked are defunct.

The command:

-FTTinclude_Optimization 

controls the g(m) factor in the exponential. The perfect signal requires 'no' here, The fgeneric signal function is defined in line 1460:

SignalBackground_Generation/SigBkg_Functions.py

The range of the FFT is control nerar the start of the PowerSpectrum_Plotter.py
