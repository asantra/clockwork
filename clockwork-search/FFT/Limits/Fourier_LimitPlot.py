#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)
#Import custom
# sys.path.insert(0, '/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Classifier/Custom_Modules')
# from Custom_Modules import WT
# import SigBkg_Classifier as SB

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
# parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
# parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-stepFT', metavar='', help='Step size of window - see Fourier_Transform.py for definition')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from Bkg+Sig Toy')
argus = parser.parse_args()

#import ATLAS MPL Style
# import mplhep as hep
# plt.style.use(hep.style.ROOT)
# import atlas_mpl_style

toy = str(argus.t)
Sig = str(argus.s)
Bkg = str(argus.b)
event = str(argus.event)
test = int(argus.test)
kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)
stepFT = int(argus.stepFT)
bkgsub = str(argus.bkgsub)

Output_Folder = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_bkgsub"+(argus.bkgsub)+"_stepFT"+(argus.stepFT)+""


K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)

N, M = len(K_List), len(M5_List)
Z = numpy.zeros((N, M))
K_List_2D, M5_List_2D = numpy.meshgrid(M5_List, K_List)
for i, (x,y) in enumerate(product(K_List,M5_List)):
    Z[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/Fourier_Condor_Output/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method1_Discovery"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")

#Make Contour plots of Pvalues
fig_pvalue_contour,ax=plt.subplots(1,1)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z)
c_alpha = ax.contourf(K_List_2D, M5_List_2D, Z, levels = [0.0445,0.0455])
ax.set_xlabel('M5 [GeV]', ha='right', x=1.0)
ax.set_ylabel('k [GeV]', ha='right', y=1.0)
ax.semilogy()
plt.xlim((1000,10000))
fig_pvalue_contour.colorbar(c_alpha) # Add a colorbar to a plot
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap.png')
