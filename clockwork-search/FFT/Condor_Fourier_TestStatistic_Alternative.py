# from __future__ import division
#####################
#   Classifier FT   #
#####################

#Explanations
# - This code uses Fourier transforms to determine whether a signal is present or not. This script acts as an alternative to the Yevgeny/Hugues method which just uses the amplitude alone
#Import
print("Starting program...")
print("")
print("Importing files...")

#Standard
import math, numpy, scipy, random, os, sys, scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
#Custom
#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

import SigBkg_Functions as SB
import Fourier_Transform_Alternative as FT

import mplhep as hep
hep.set_style(hep.style.ROOT)

print("Files imported.")
print("")


import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-lumDiv', metavar='', help='Devide by luminosity')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
# parser.add_argument('-startFT', metavar='', help='Step size of window - see Fourier_Transform.py for definition')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from Bkg+Sig Toy')
parser.add_argument('-FTinclude_Optimization', metavar='', help='Subtract background from Bkg+Sig Toy')
parser.add_argument('-uncertainty', metavar='', help='Profile Uncertainties of the Background and Signal models')
argus = parser.parse_args()

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 300
  xmax = 2000
  nBins   = 1700
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 1450 
  nBins = 1300 

#Mass and scale Lists
massList = numpy.linspace(xmin, xmax, nBins)

#Settings
print("Reading settings...")

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

#Define input Signal parameters and 

M5 = float(argus.M5)
k = float(argus.k)

print("Mass_5")
print(M5)
print("K")
print (k)

#Generate signal and Background

#####Signal#####
if(argus.s=="Tail_Damped_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax)

if(argus.s=="Theory_Diphoton_36fb_Sig"):
  luminosity = 36.1
  year = '2015-16'
  SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins)

if(argus.s=="Theory_Dielectron_Sig"):
  luminosity = 139
  year = '2015-18'
  if(argus.uncertainty=="no"):
    SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
  if(argus.uncertainty=="yes"):
    SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, lum = SB.SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, "All")

if(argus.s=="Theory_HighMass_Diphoton_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins)

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

if(argus.b=="Diphoton_36fb_Bkg"):
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

print("Signal/Bkg evaluated.")
print("")



#Number of trials for background test statistic and signal + background test statistic reference toy, K in the FT terminology 
K_Reference_Toys1 = int(argus.test)
K_Reference_Toys2 = int(argus.test)
K_Reference_ToysBkg = int(argus.test)
K_Reference_ToysSig = int(argus.test)

#Number of Signal and Bkg Variations to run over
NSigTemplates = 100
NBkgTemplates = 100

#Background subtraction
backgroundSub = bool(argus.bkgsub)

#Division by luminosity - can unhighlight to overide argparse signal lum
lumDiv = bool(argus.lumDiv)

#Define Test Statitic Binning
bins = int(argus.bin)


#Lets name a folder based on the bkg to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_bkgsub"+(argus.bkgsub)+"_lumDiv"+(argus.lumDiv)+"FTinclude_Optimization"+(argus.FTinclude_Optimization)+""
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder)
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder+"/TestStatistic_plots")
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder+"/Error_Condor")
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder+"/Logs_Condor")
  os.mkdir("Fourier_Alternative_Condor_Output/"+Output_Folder+"/Out_Condor")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)





print("Setting read")
print("")

#Make empty lists to append Pvalues
P_Value_List_Method2_Exclusion = []
P_Value_List_Method2_Discovery = []
P_Value_List_Method2_Exclusion_Uncertainty = []
P_Value_List_Method2_Discovery_Uncertainty = []

Pvalue_out_Method2_Exclusion = open(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2Nominal_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery = open(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2Nominal_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Exclusion_Uncertainty = open(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2_UncertaintyTemplate"+str(NSigTemplates)+"_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_Uncertainty = open(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2_UncertaintyTemplate"+str(NSigTemplates)+"_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")


#Start loop
print("Starting new point...")
print("Evaluating signal...")


if(argus.FTinclude_Optimization=="no"):
  Frequency_SigFixedPeak = FT.Default_DFT_SignalPeakFinder(SigFix, BkgFix, backgroundSub, xmin, xmax)


#Generate trial background + signal
#Initialize variables
print("Beginning toy experiment of background + signal")
PowerBkgSig_FT_nominal = []
PowerBkgSig_FT_uncert = []
PowerBkgSig_FT_nominal_ToysBkg = []

if(argus.uncertainty=="no"):

  for toy in range(1,K_Reference_Toys1):
    #Generate random binned events
    eventsTrial1_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal.append(FT.FourierTS_NoLum(eventsTrial1_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal.append(FT.FourierTS(eventsTrial1_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkgSig_FT_nominal.append(FT.Default_DFT_TestStatistic(eventsTrial1_nominal, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

if(argus.uncertainty=="yes"):

  for toy in range(1,K_Reference_Toys1):
    #Generate random binned events
    eventsTrial1_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal.append(FT.FourierTS_NoLum(eventsTrial1_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal.append(FT.FourierTS(eventsTrial1_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkgSig_FT_nominal.append(FT.Default_DFT_TestStatistic(eventsTrial1_nominal, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

  for i in range(1,NSigTemplates):

    Sig_Unc = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, BkgFix, xmin, xmax,i,"WT")
    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i)
    SigBkg_Unc = SB.SigBkgFixed(BkgFix_UncertianitesAll, Sig_Unc)
    #Calculate test statistic FT
    if(argus.FTinclude_Optimization=="yes"):
      PowerBkgSig_FT_uncert.append(FT.FourierTS(SigBkg_Unc, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList,xmin, xmax))
    if(argus.FTinclude_Optimization=="no"):
      PowerBkgSig_FT_uncert.append(FT.Default_DFT_TestStatistic(SigBkg_Unc, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

  for toy in range(1,K_Reference_ToysSig):
    #Generate random binned events
    eventsTrial2_nominal_ToysBkg = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal_ToysBkg.append(FT.FourierTS_NoLum(eventsTrial2_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkgSig_FT_nominal_ToysBkg.append(FT.FourierTS(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkgSig_FT_nominal_ToysBkg.append(FT.Default_DFT_TestStatistic(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

# print(statTestFT1)
# print(len(statTestFT1)) 

print("Toy experiments of background + signal done.")
print("")

PowerBkg_FT_nominal = []
PowerBkg_FT_uncert = []
PowerBkgFixed_FT_nominal = []
PowerBkg_FT_nominal_ToysBkg = []

if(argus.uncertainty=="no"):

  for toy in range(1,K_Reference_Toys2):
    #Generate random binned events
    eventsTrial2_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal.append(FT.FourierTS_NoLum(eventsTrial2_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal.append(FT.FourierTS(eventsTrial2_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkg_FT_nominal.append(FT.Default_DFT_TestStatistic(eventsTrial2_nominal, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))


if(argus.uncertainty=="yes"):

  for toy in range(1,K_Reference_Toys2):
    #Generate random binned events
    eventsTrial2_nominal = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal.append(FT.FourierTS_NoLum(eventsTrial2_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal.append(FT.FourierTS(eventsTrial2_nominal, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkg_FT_nominal.append(FT.Default_DFT_TestStatistic(eventsTrial2_nominal, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

  for i in range(1,NBkgTemplates):
    
    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i)
    if(argus.FTinclude_Optimization=="yes"):
      PowerBkg_FT_uncert.append(FT.FourierTS(BkgFix_UncertianitesAll, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
    if(argus.FTinclude_Optimization=="no"):
      PowerBkg_FT_uncert.append(FT.Default_DFT_TestStatistic(BkgFix_UncertianitesAll, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

  for toy in range(1,K_Reference_ToysBkg):
    #Generate random binned events
    eventsTrial2_nominal_ToysBkg = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    if(argus.s=="Tail_Damped_Sig"):
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.FourierTS_NoLum(eventsTrial2_nominal, BkgFix, backgroundSub, 1, k, M5, massList, xmin, xmax))
    else:
      if(argus.FTinclude_Optimization=="yes"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.FourierTS(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, lum, lumDiv, 1, k, M5, massList, xmin, xmax))
      if(argus.FTinclude_Optimization=="no"):
        PowerBkg_FT_nominal_ToysBkg.append(FT.Default_DFT_TestStatistic(eventsTrial2_nominal_ToysBkg, BkgFix, backgroundSub, Frequency_SigFixedPeak,xmin, xmax))

#####New Test Statitic#####

# PowerBkg_SortedList     = numpy.sort(PowerBkg_FT_nominal)
# PowerBkg_Median =  PowerBkg_SortedList[int(numpy.floor(0.5*len(PowerBkg_FT_nominal)))]
PowerBkg_Median = numpy.median(PowerBkg_FT_nominal_ToysBkg)
PowerBkg_stDev = numpy.std(PowerBkg_FT_nominal_ToysBkg)
PowerBkgSig_stDev = numpy.std(PowerBkg_FT_nominal_ToysBkg)
print("PowerBkg_Median",PowerBkg_Median)
print("PowerBkg_stDev",PowerBkg_stDev)

PowerBkgSig_TestStatisitc = []
PowerBkg_TestStatisitc  = []
PowerBkgSig_TestStatisitc_Uncertainty = []
PowerBkg_TestStatisitc_Uncertainty  = []


if(argus.uncertainty=="no"):

  for Power_Ref in numpy.ravel(PowerBkgSig_FT_nominal):
    BkgSig_TestStatisitc = Power_Ref-PowerBkg_Median
    if BkgSig_TestStatisitc>0:
      PowerBkgSig_TestStatisitc.append(BkgSig_TestStatisitc)
    else:
      PowerBkgSig_TestStatisitc.append(0)
  print("PowerBkgSig_TestStatisitc",numpy.ravel(PowerBkgSig_TestStatisitc))

  for Power_Ref in numpy.ravel(PowerBkg_FT_nominal):
    Bkg_TestStatisitc = Power_Ref-PowerBkg_Median
    if Bkg_TestStatisitc>0:
      PowerBkg_TestStatisitc.append(Bkg_TestStatisitc)
    else:
      PowerBkg_TestStatisitc.append(0)
  print("PowerBkg_TestStatisitc",numpy.ravel(PowerBkg_TestStatisitc))


if(argus.uncertainty=="yes"):

  PowerBkgSig_FT_uncert_UpperQuantile = numpy.quantile(PowerBkgSig_FT_uncert, 0.95)
  PowerBkgSig_FT_uncert_LowerQuantile = numpy.quantile(PowerBkgSig_FT_uncert, 0.05)
  print("PowerBkgSig_FT_uncert_UpperQuantile",PowerBkgSig_FT_uncert_UpperQuantile)
  print("PowerBkgSig_FT_uncert_LowerQuantile",PowerBkgSig_FT_uncert_LowerQuantile)

  PowerBkg_FT_uncert_UpperQuantile = numpy.quantile(PowerBkg_FT_uncert, 0.95)
  PowerBkg_FT_uncert_LowerQuantile = numpy.quantile(PowerBkg_FT_uncert, 0.05)
  print("PowerBkg_FT_uncert_UpperQuantile",PowerBkg_FT_uncert_UpperQuantile)
  print("PowerBkg_FT_uncert_LowerQuantile",PowerBkg_FT_uncert_LowerQuantile)

  BkgSig_Uncertainty = ((PowerBkgSig_FT_uncert_UpperQuantile - PowerBkgSig_FT_uncert_LowerQuantile)**2 - (PowerBkg_FT_uncert_UpperQuantile - PowerBkg_FT_uncert_LowerQuantile)**2 + PowerBkgSig_stDev**2)**0.5
  # Bkg_Uncertainty = ((PowerBkg_FT_uncert_UpperQuantile - PowerBkg_FT_uncert_LowerQuantile)**2 + PowerBkg_stDev**2)**0.5
  # Bkg_Uncertainty = (PowerBkg_FT_uncert_UpperQuantile - PowerBkg_FT_uncert_LowerQuantile)
  print("BkgSig_Uncertainty",BkgSig_Uncertainty)
  # print("Bkg_Uncertainty",Bkg_Uncertainty)

  for Power_Ref in numpy.ravel(PowerBkgSig_FT_nominal):
   
    print("Power_Ref",Power_Ref)

    BkgSig_TestStatisitc = Power_Ref-PowerBkg_Median
    BkgSig_TestStatisitc_Uncertainty = (Power_Ref-PowerBkg_Median)/(BkgSig_Uncertainty)
    print("BkgSig_TestStatisitc_Uncertainty",BkgSig_TestStatisitc_Uncertainty)
    if BkgSig_TestStatisitc_Uncertainty>0:
      PowerBkgSig_TestStatisitc_Uncertainty.append(numpy.ravel(BkgSig_TestStatisitc_Uncertainty))
    else:
      PowerBkgSig_TestStatisitc_Uncertainty.append(numpy.ravel(0))
    if BkgSig_TestStatisitc>0:
      PowerBkgSig_TestStatisitc.append(numpy.ravel(BkgSig_TestStatisitc))
    else:
      PowerBkgSig_TestStatisitc.append(numpy.ravel(0))

  print("PowerBkgSig_TestStatisitc",numpy.ravel(PowerBkgSig_TestStatisitc))
  print("PowerBkgSig_TestStatisitc_Uncertainty",numpy.ravel(PowerBkgSig_TestStatisitc_Uncertainty))
  

  for Power_Ref in numpy.ravel(PowerBkg_FT_nominal):

    Bkg_TestStatisitc = Power_Ref-PowerBkg_Median
    Bkg_TestStatisitc_Uncertainty = (Power_Ref-PowerBkg_Median)/(BkgSig_Uncertainty)

    if Bkg_TestStatisitc>0:
      PowerBkg_TestStatisitc.append(numpy.ravel(Bkg_TestStatisitc))
    else:
      PowerBkg_TestStatisitc.append(numpy.ravel(0))
    if Bkg_TestStatisitc_Uncertainty>0:
      PowerBkg_TestStatisitc_Uncertainty.append(numpy.ravel(Bkg_TestStatisitc_Uncertainty))
    else:
      PowerBkg_TestStatisitc.append(numpy.ravel(0))

  print("PowerBkg_TestStatisitc",numpy.ravel(PowerBkg_TestStatisitc))
  print("PowerBkg_TestStatisitc_Uncertainty",numpy.ravel(PowerBkg_TestStatisitc_Uncertainty))

if(argus.uncertainty=="no"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(PowerBkg_TestStatisitc)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(PowerBkg_TestStatisitc)))]
  #Discovery Median
  BS_SortedList     = numpy.sort(PowerBkgSig_TestStatisitc)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(PowerBkgSig_TestStatisitc)))]

  BS_hist = numpy.histogram(PowerBkgSig_TestStatisitc,range=(numpy.nanmin(PowerBkgSig_TestStatisitc), numpy.nanmax(PowerBkgSig_TestStatisitc)))
  Bonly_hist =numpy.histogram(PowerBkg_TestStatisitc, range=(numpy.nanmin(PowerBkg_TestStatisitc), numpy.nanmax(PowerBkg_TestStatisitc)))

  BS_distribution_rv = scipy.stats.rv_histogram(BS_hist,bins)
  Bonly_rv = scipy.stats.rv_histogram(Bonly_hist,bins)

  # Make Test Statistic plot
  Probably_Dist = plt.figure()
  ax = Probably_Dist.add_subplot()
  ax1 = Probably_Dist.add_subplot()
  ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
  if(argus.uncertainty=="no"):
    plt.hist(numpy.ravel(PowerBkg_TestStatisitc), bins, label="Bkg Only Nom", alpha=0.4)
    plt.hist(numpy.ravel(PowerBkgSig_TestStatisitc), bins, label="Bkg + Signal Nom" , alpha=0.4) 
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  t = ax1.xaxis.get_offset_text()
  z = ax1.yaxis.get_offset_text()
  t.set_x(-0.05)
  z.set_y(1)
  plt.xlabel(r'$\mathrm{y(P(T))}$', ha='right', x=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/TestStatistic_plots/TestDistribution_FTrange_"+str(k)+"_"+str(M5)+".png")



  #Try and caluclate P-value for Exclusion, account for which way around each set of test stats is by looking at which median is higher
  templist_method2_Exclusion = []
  templist_method2_Discovery = []

  if BS_Median > B_OnlyMedian:
    P_Value_Exclusion = (BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)
  if BS_Median < B_OnlyMedian:
    P_Value_Exclusion = (BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)


  #Try and caluclate P-value for Discovery
  if BS_Median > B_OnlyMedian:
    P_Value_Discovery = (Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)
  if BS_Median < B_OnlyMedian:
    P_Value_Discovery = (Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)


  #Convert Nan to 0
  templist_method2_Exclusion = numpy.nan_to_num(templist_method2_Exclusion)
  templist_method2_Discovery = numpy.nan_to_num(templist_method2_Discovery)

  print("Method2: Min Exclusion value is:", min(templist_method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(templist_method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(templist_method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(templist_method2_Discovery,default=0))



  print("Print statistics computations done.")
  print("")

  #Save Pvalues from the 2 methods, Limts from exclusion and discovery 
  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")

if(argus.uncertainty=="yes"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(PowerBkg_TestStatisitc)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(PowerBkg_TestStatisitc)))]

  B_Only_SortedList_Uncertainty    = numpy.sort(PowerBkg_TestStatisitc_Uncertainty)
  B_OnlyMedian_Uncertainty = B_Only_SortedList_Uncertainty[int(numpy.floor(0.5*len(PowerBkg_TestStatisitc_Uncertainty)))]

  #Discovery Median
  BS_SortedList     = numpy.sort(PowerBkgSig_TestStatisitc)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(PowerBkgSig_TestStatisitc)))]
  BS_SortedList_Uncertainty     = numpy.sort(PowerBkgSig_TestStatisitc_Uncertainty)
  BS_Median_Uncertainty = BS_SortedList_Uncertainty[int(numpy.floor(0.5*len(PowerBkgSig_TestStatisitc_Uncertainty)))]

  BS_hist = numpy.histogram(PowerBkgSig_TestStatisitc,range=(numpy.nanmin(PowerBkgSig_TestStatisitc), numpy.nanmax(PowerBkgSig_TestStatisitc)))
  BS_hist_Uncertainty = numpy.histogram(PowerBkgSig_TestStatisitc_Uncertainty,range=(numpy.nanmin(PowerBkgSig_TestStatisitc_Uncertainty), numpy.nanmax(PowerBkgSig_TestStatisitc_Uncertainty)))

  Bonly_hist =numpy.histogram(PowerBkg_TestStatisitc, range=(numpy.nanmin(PowerBkg_TestStatisitc), numpy.nanmax(PowerBkg_TestStatisitc)))
  Bonly_hist_Uncertainty =numpy.histogram(PowerBkg_TestStatisitc_Uncertainty, range=(numpy.nanmin(PowerBkg_TestStatisitc_Uncertainty), numpy.nanmax(PowerBkg_TestStatisitc_Uncertainty)))


  BS_distribution_rv = scipy.stats.rv_histogram(BS_hist,bins)
  BS_Uncertainty_distribution_rv = scipy.stats.rv_histogram(BS_hist_Uncertainty,bins)

  Bonly_rv = scipy.stats.rv_histogram(Bonly_hist,bins)
  Bonly_Uncertainty_rv = scipy.stats.rv_histogram(Bonly_hist_Uncertainty,bins)

  # Make Test Statistic plot
  Probably_Dist_Unc = plt.figure()
  ax = Probably_Dist_Unc.add_subplot()
  ax1 = Probably_Dist_Unc.add_subplot()
  ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
  plt.hist(numpy.ravel(PowerBkg_TestStatisitc_Uncertainty), bins, label="Bkg Unc" , alpha=0.9) 
  plt.hist(numpy.ravel(PowerBkgSig_TestStatisitc_Uncertainty), bins, label="Bkg + Signal Unc" , alpha=0.9) 
  plt.axvline(x=B_OnlyMedian_Uncertainty,color='black', linestyle='--', label = "B_Median_Unc")
  plt.axvline(x=BS_Median_Uncertainty,color='purple', linestyle='--', label = "BS_Median_Unc")
  t = ax1.xaxis.get_offset_text()
  z = ax1.yaxis.get_offset_text()
  t.set_x(-0.05)
  z.set_y(1)
  plt.xlabel(r'$\mathrm{y(P(T))}$', ha='right', x=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
  plt.legend()
  # plt.margins(0)
  Probably_Dist_Unc.savefig(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/TestStatistic_plots/Uncertianty_TestDistribution_FTrange_"+str(k)+"_"+str(M5)+".png")

  # Make Test Statistic plot
  Probably_Dist = plt.figure()
  ax = Probably_Dist.add_subplot()
  ax1 = Probably_Dist.add_subplot()
  ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
  plt.hist(numpy.ravel(PowerBkg_TestStatisitc), bins, label="Bkg Only Nom", alpha=0.4)
  plt.hist(numpy.ravel(PowerBkgSig_TestStatisitc), bins, label="Bkg + Signal Nom" , alpha=0.4) 
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  t = ax1.xaxis.get_offset_text()
  z = ax1.yaxis.get_offset_text()
  t.set_x(-0.05)
  z.set_y(1)
  plt.xlabel(r'$\mathrm{y(P(T))}$', ha='right', x=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"Fourier_Alternative_Condor_Output/"+Output_Folder+"/TestStatistic_plots/TestDistribution_FTrange_"+str(k)+"_"+str(M5)+".png")


  #Try and caluclate P-value for Exclusion, account for which way around each set of test stats is by looking at which median is higher
  templist_method2_Exclusion = []
  templist_method2_Discovery = []
  templist_method2_Exclusion_Uncertainty = []
  templist_method2_Discovery_Uncertainty = []

  if BS_Median >= B_OnlyMedian:
    P_Value_Exclusion = (BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)
  if BS_Median < B_OnlyMedian:
    P_Value_Exclusion = (BS_distribution_rv.cdf(B_OnlyMedian))
    templist_method2_Exclusion.append(P_Value_Exclusion)

  if BS_Median_Uncertainty >= B_OnlyMedian_Uncertainty:
    P_Value_Exclusion_Uncertainty = (BS_Uncertainty_distribution_rv.cdf(B_OnlyMedian_Uncertainty))
    templist_method2_Exclusion_Uncertainty.append(P_Value_Exclusion_Uncertainty)
  if BS_Median_Uncertainty < B_OnlyMedian_Uncertainty:
    P_Value_Exclusion_Uncertainty = (BS_Uncertainty_distribution_rv.cdf(B_OnlyMedian_Uncertainty))
    templist_method2_Exclusion_Uncertainty.append(P_Value_Exclusion_Uncertainty)

  #Try and caluclate P-value for Discovery
  if BS_Median >= B_OnlyMedian:
    P_Value_Discovery = (Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)
  if BS_Median < B_OnlyMedian:
    P_Value_Discovery = (Bonly_rv.cdf(BS_Median))
    templist_method2_Discovery.append(P_Value_Discovery)

  if BS_Median_Uncertainty >= B_OnlyMedian_Uncertainty:
    P_Value_Discovery_Uncertainty = (Bonly_Uncertainty_rv.cdf(BS_Median_Uncertainty))
    templist_method2_Discovery_Uncertainty.append(P_Value_Discovery_Uncertainty)
  if BS_Median_Uncertainty < B_OnlyMedian_Uncertainty:
    P_Value_Discovery_Uncertainty = (Bonly_Uncertainty_rv.cdf(BS_Median_Uncertainty))
    templist_method2_Discovery_Uncertainty.append(P_Value_Discovery_Uncertainty)

  #Convert Nan to 0
  templist_method2_Exclusion = numpy.nan_to_num(templist_method2_Exclusion)
  templist_method2_Discovery = numpy.nan_to_num(templist_method2_Discovery)

  templist_method2_Exclusion_Uncertainty = numpy.nan_to_num(templist_method2_Exclusion_Uncertainty)
  templist_method2_Discovery_Uncertainty = numpy.nan_to_num(templist_method2_Discovery_Uncertainty)

  print("Method2: Min Exclusion value is:", min(templist_method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(templist_method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(templist_method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(templist_method2_Discovery,default=0))

  print("Method2: Min Exclusion value with Uncertainty  is:", min(templist_method2_Exclusion_Uncertainty, default=0))
  P_Value_List_Method2_Exclusion_Uncertainty.append(min(templist_method2_Exclusion_Uncertainty,default=0))

  print("Method2: Min Discovery value with Uncertainty is:", min(templist_method2_Discovery_Uncertainty,default=0))
  P_Value_List_Method2_Discovery_Uncertainty.append(min(templist_method2_Discovery_Uncertainty,default=0))

  print("Print statistics computations done.")
  print("")

  #Save Pvalues from the 2 methods, Limts from exclusion and discovery 
  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_Uncertainty, P_Value_List_Method2_Discovery_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion_Uncertainty, P_Value_List_Method2_Exclusion_Uncertainty, fmt="%1.10f")


# ##Plot Signal and Background shapes for debugging

#Fixed Signal and Background Mass Distribution Plotting
fig_SigBkg, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# plt.plot(massList,BkgFix, 'b',linewidth=2, label=r"Background")
plt.plot(massList,SigFix, 'r',linewidth=2, label=r"Signal Toy")
# plt.plot(massList,eventsTrial2, 'g',linewidth=2, label=r"Background")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(bottom=10e-4)
plt.yscale('log')
plt.xscale('log')
fig_SigBkg.savefig('Power_Spectrum/SigBkg_plot.pdf')

