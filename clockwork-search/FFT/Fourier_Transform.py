##########
#   FT   #
##########

### Explanations ###
# - This program makes a test statistic function using Fourier transforms and also does the basic Fourier transform.

#Import generic
import cmath, math, sys, os
import numpy as np
from   bisect import bisect_left
import ROOT
import array

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))
import Basic

MP = 2.435E18  #Reduced Planck scale

#Series of functions to compute the P(T) over different mass windows and also different T values to ploty the spectrum itself

######################################################################
############ Fourier Transform Test Statisitic functions #############
######################################################################


# Fourier transform
def FourierP_TPeakPlotter(Signal,lum, lumDiv, k, M5, massList, mpnk, xmin, xmax, T):
  #Initialize variables
  Psq = 0

  
  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  binMax = bisect_left(massList,  max((1 + mpnk)*k, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    if lumDiv:
      lumT = lum[b]
    else:
      lumT = 1

    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2


# Fourier transform
def FourierP_TPeakPlotter_NoLum(Signal, k, M5, massList, mpnk, xmin, xmax, T):
  #Initialize variables
  Psq = 0
    
  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  # binMin = int(k)
  binMax = bisect_left(massList,  max((1 + mpnk)*k, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    lumT = 1
  
    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])


  #Return P(T)
  return (np.absolute(Psq))**2


# Fourier transform
def FourierP(Signal, lum, lumDiv, T, k, M5, massList, mpnk, xmin, xmax):
  #Initialize variables
  Psq = 0

  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  # binMin = int(k)
  binMax = bisect_left(massList,  max((1 + mpnk)*k, binMin))


  #Loop over signal
  for b in range(binMin, binMax - 1):


    if lumDiv:
      lumT = lum[b]
    else:
      lumT = 1
    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2

# Fourier transform
def FourierP_NoLum(Signal, T, k, M5, massList, mpnk, xmin, xmax):
  #Initialize variables
  Psq = 0

  #Bins to scan over
  binMin = bisect_left(massList,  max(k,            xmin  ))
  binMax = bisect_left(massList,  max((1 + mpnk)*k, binMin))

  #Loop over signal
  for b in range(binMin, binMax - 1):

    lumT = 1
    Psq += 1/(2*math.pi)**0.5*Signal[b]/lumT*cmath.exp(1j*2*math.pi*Basic.R(M5, k)*(massList[b]**2 - k**2)**0.5/T)*(massList[b + 1] - massList[b])

  #Return P(T)
  return (np.absolute(Psq))**2

#List of test statistics
def FourierTS(Signal, Background, backgroundSub, lum, lumDiv, T, k, M5, massList, stepFT, xmin, xmax):

  #Different values of upper limit
  mpnkList = np.linspace(5/stepFT, 5, stepFT)

  #Initialize variable
  listTS = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  #Loop over possible values of mpnk
  for mpnk in mpnkList:
    listTS.append(FourierP(Signal, lum, lumDiv, T, k, M5, massList, mpnk, xmin, xmax))

  #Return the list of test statistics
  return listTS

#List of test statistics
def FourierTS_NoLum(Signal, Background, backgroundSub, T, k, M5, massList, stepFT, xmin, xmax):

  #Different values of upper limit
  mpnkList = np.linspace(5/stepFT, 5, stepFT)


  #Initialize variable
  listTS = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  #Loop over possible values of mpnk
  for mpnk in mpnkList:
    listTS.append(FourierP_NoLum(Signal, T, k, M5, massList, mpnk, xmin, xmax))

  #Return the list of test statistics
  return listTS

#List of test statistics
def FourierTS_PeakPlotter_NoLum(Signal, Background, backgroundSub, k, M5, massList, stepFT, xmin, xmax, TList):

  #Different values of upper limit
  mpnkList = np.linspace(5/stepFT, 5, stepFT)

  #Initialize variable
  Psq_TList = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  #Loop over possible values of mpnk
  for mpnk in mpnkList:
    Psq_TList_massWindow = []
    for T in TList:
      Psq_TList_massWindow.append(FourierP_TPeakPlotter_NoLum(Signal, k, M5, massList, mpnk, xmin, xmax, T))
    Psq_TList.append(Psq_TList_massWindow)
  #Return the list of test statistics
  return Psq_TList

#List of test statistics
def FourierTS_PeakPlotter(Signal, Background, backgroundSub, lum, lumDiv, k, M5, massList, stepFT, xmin, xmax, TList):

  #Different values of upper limit
  mpnkList = np.linspace(5/stepFT, 5, stepFT)

  #Initialize variable
  Psq_TList = []

  #Signal internal (if background is subtracted)
  if backgroundSub:
    SignalInt = [x1 - x2 for (x1, x2) in zip(Signal, Background)]
  else:
    SignalInt = Signal

  #Loop over possible values of mpnk
  # for T in TList:
  #   for mpnk in mpnkList:
  #     Psq_TList.append(FourierP_TPeakPlotter(Signal, lum, lumDiv, k, M5, massList, mpnk, xmin, xmax, T))
  # Psq_TList = np.array(Psq_TList)

  for mpnk in mpnkList:
    Psq_TList_massWindow = []
    for T in TList:
      Psq_TList_massWindow.append(FourierP_TPeakPlotter(Signal, lum, lumDiv, k, M5, massList, mpnk, xmin, xmax, T))
    Psq_TList.append(Psq_TList_massWindow)

  #Return the list of test statistics
  return Psq_TList
