#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
pip3 install --user parton --upgrade
pip3 install --user mplhep --upgrade
python3 Condor_Fourier_TestStatistic.py -t $1 -k $2 -M5 $3 -s $4 -b $5 -event $6 -lumDiv $7 -test $8 -bin $9 -stepFT ${10} -bkgsub ${11} -uncertainty ${12}

