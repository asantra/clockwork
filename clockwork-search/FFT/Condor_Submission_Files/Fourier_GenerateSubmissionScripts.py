# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, scipy, random, os, sys
from itertools import product

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-stepFT', metavar='', help='Step size of window - see Fourier_Transform.py for definition')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from Bkg+Sig Toy')
parser.add_argument('-lumDiv', metavar='', help='Devide by luminosity')
parser.add_argument('-uncertainty', metavar='', help='Profile Uncertainties of the Background nd Signal models')
argus = parser.parse_args()

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/"

toy = str(argus.t)
Sig = str(argus.s)
Bkg = str(argus.b)
event = str(argus.event)
test = int(argus.test)
kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)
bins = int(argus.bin)
stepFT = int(argus.stepFT)
bkgsub = str(argus.bkgsub)
lum = str(argus.lumDiv)
unc = str(argus.uncertainty)

try:
  Output_Folder = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_bkgsub"+(argus.bkgsub)+"_stepFT"+(argus.stepFT)+"_lumDiv"+(argus.lumDiv)+""
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder)
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder+"/Error_Condor")
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder+"/Logs_Condor")
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder+"/Out_Condor")
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir(Directory+"Fourier_Condor_Output/"+Output_Folder+"/TestStatistic_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)

# Open file and write common part
cfile = open('condor_Fourier.sub','w')
common_command = \
'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = Fourier_GenerateSubmissionPreamble.sh \n\
initialdir      = /scratch2/slawlor/Clockwork_Project/clockwork-search/ \n\
Should_transfer_files = YES \n\
+JobFlavour = "medium" \n\
when_to_transfer_output = ON_EXIT \n\
'
cfile.write(common_command)

 
# Loop over various values of an argument and create different output file for each
# Then put it in the queue
# K_List = numpy.linspace(200,5000,25)
# M5_List = numpy.linspace(1000, 20000, 25)
K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)
for i, (k,m) in enumerate(product(K_List,M5_List)):
    run_command =  \
'arguments  = %s %d %d %s %s %s %s %d %d %d %s %s \n\
error                 = FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Error_Condor/Fourier_%d_%d_Clockwork$(Process).err \n\
log                   = FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Logs_Condor/Fourier_%d_%d_Clockwork$(Process).log \n\
transfer_input_files  = SignalBackground_Generation/Inputs_ee, SignalBackground_Generation/Inputs_yy, FFT/Condor_Fourier_TestStatistic.py, FFT/Condor_Submission_Files/Fourier_GenerateSubmissionPreamble.sh , SignalBackground_Generation/SigBkg_Functions.py, FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/TestStatistic_plots, FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Error_Condor, FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Logs_Condor,FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Pvalues_Condor,FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Out_Condor,FFT/Fourier_Transform.py,SignalBackground_Generation/Custom_Modules,SignalBackground_Generation/Graviton_Properties \n\
output     = FFT/Fourier_Condor_Output/Outputs_%s_%s_%s_%s_test%d_bkgsub%s_stepFT%d_lumDiv%s/Out_Condor/Fourier_out.%d_%d.txt \n\
queue 1\n\n' %(toy,k,m,Sig,Bkg,event,lum,test,bins,stepFT,bkgsub,unc,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,k,m,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,k,m,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,toy,Bkg,Sig,event,test,bkgsub,stepFT,lum,k,m) 
    cfile.write(run_command)