#Import standard
import sys, time
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array
from parton import mkPDF

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes

#Disable any warnings
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  

#Set random generator for Briet-Wigner shape used in signals
random = ROOT.TRandom3(0)

############################################################################################################

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/SignalBackground_Generation/"


#Input directories
indir_ee = Directory+"Inputs_ee/"
indir_yy = Directory+"Inputs_yy/"

sys.path.append(os.path.abspath('Custom_Modules/'))
from Custom_Modules import Basic, Background, Couplings, Graviton, KKcascade, Luminosity, PDFcalc, Smearing
import SigBkg_Functions as SB

## get the nominal acceptance*efficiency
fPeterAcc = ROOT.TFile(indir_ee+"ee_accEffFunction.root","READ")
fAcc_ee = fPeterAcc.Get("accEff_TF1")
sfAcc_ee = "[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)"
for n in range(0,fAcc_ee.GetNpar()):
   print (fAcc_ee.GetParName(n)+": "+str(fAcc_ee.GetParameter(n)))
   sfAcc_ee = str(sfAcc_ee.replace("["+str(n)+"]",str(fAcc_ee.GetParameter(n))))
fAcc_ee.SetLineColor(ROOT.kBlack)


import mplhep as hep
hep.set_style(hep.style.ROOT)

### get the Nominal Dielectron Bkg  model
fbkgfinal_ee = ROOT.TFile(indir_ee+"finalbkg.root","READ")
fBkg_ee = fbkgfinal_ee.Get("bkgfit")


Nbins_ee = 5775 #2470
xmin_ee = 225 #230
xmax_ee = 6000 #2700

Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin_ee, xmax_ee)
Acc_ee.SetParameter(0,0.79366)
Acc_ee.SetParameter(1,-234.118)
Acc_ee.SetParameter(2,61575.1)
Acc_ee.SetParameter(3,-1.046e7)
Acc_ee.SetParameter(4,9.63491e8)
Acc_ee.SetParameter(5,-3.67989e10)
Acc_ee.SetParameter(6,-9.10565e-6)
Acc_ee.SetParameter(7,4.221e-10)

#Define mass binning
massList_ee = np.linspace(xmin_ee, xmax_ee, Nbins_ee)


#ee_ID efficiency systematic uncertainties function
sfAcc_ee_IDefficiency = ROOT.TF1("sfAcc_ee_IDefficiency","[0]+[1]*TMath::Exp([2]*(x))+[3]*log(x)+[4]*TMath::TanH([5]*(x-[6]))",xmin_ee, xmax_ee)
sfAcc_ee_IDefficiency.SetParameter(0,0.200345)
sfAcc_ee_IDefficiency.SetParameter(1,-0.219756)
sfAcc_ee_IDefficiency.SetParameter(2,-0.000110541)
sfAcc_ee_IDefficiency.SetParameter(3,0.00922559)
sfAcc_ee_IDefficiency.SetParameter(4,0.0169377)
sfAcc_ee_IDefficiency.SetParameter(5,0.00632574)
sfAcc_ee_IDefficiency.SetParameter(6,530.715)

#sfAcc_ee_Iso_efficiency systematic uncertainties function
sfAcc_ee_Iso_efficiency = ROOT.TF1("sfAcc_ee_Iso_efficiency","[0]+[1]*TMath::TanH([2]*(x-[3]))+[4]*TMath::TanH([5]*(x-[6]))+[7]*log(x)",xmin_ee, xmax_ee)
sfAcc_ee_Iso_efficiency.SetParameter(0,0.00897086)
sfAcc_ee_Iso_efficiency.SetParameter(1,0.018128)
sfAcc_ee_Iso_efficiency.SetParameter(2,-0.000801275)
sfAcc_ee_Iso_efficiency.SetParameter(3,-262.667)
sfAcc_ee_Iso_efficiency.SetParameter(4,0.0130715)
sfAcc_ee_Iso_efficiency.SetParameter(5,0.00145374)
sfAcc_ee_Iso_efficiency.SetParameter(6,453.596)
sfAcc_ee_Iso_efficiency.SetParameter(7,.000817472)

#Generate example of signal model using full Resonant TF shape instead of Guassian
#First define the individual components

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin_ee, xmax_ee)
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)

sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)

alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin_ee, xmax_ee)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)

#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin_ee, xmax_ee)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)

sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)

#K-Parameters
k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin_ee, xmax_ee)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)


#Open up text files

# Bkg_Only_toys = open(Directory+"TextFile_Outputs/Bkg_Only_toys.txt","w")
# SigBkg_toys = open(Directory+"TextFile_Outputs/SigBkg_toys.txt","w")
# BkgFixed = open(Directory+"TextFile_Outputs/BkgFixed.txt","w")
# TailDamp_FixedSig = open(Directory+"TextFile_Outputs/TailDamp_FixedSig.txt","w")


#Generate signal with no random fluctuations
def SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins, eventType):


  #Define resolution with crystal ball + gaussian from the dielectron analysis
  def Resolution(M,n,m):
    sClockwork = "((RESONANCES)*("+(sfAcc_ee)+")"
    sResonances = ""  
    #Gaussain
    mu_Gauss = str(mu_G.Eval(M)*M+M)
    sigma_Guass = str(sigma_G.Eval(M)*M)
    #Crystal_Ball
    alpha_CrystalBall = str(alpha_CB)
    n_CrystalBall = str(n_CB.Eval(M))
    sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
    mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
    #k
    k_p = str(k_param.Eval(M))
    if(n==1): 
      sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
    else:     
      sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    y  = fClockwork.Eval(m)
    return y

  def Smearing(M,n,m, Gamma):
    if m > k:
      scale = str(1)
      sClockwork = "((RESONANCES)*("+(sfAcc_ee)+")"
      sResonances = ""  
      sBreitWigner = "((BREITWIGNER)*("+(sfAcc_ee)+")"
      sWidth = ""
      #Gaussain
      mu_Gauss = str(mu_G.Eval(M)*M+M)
      sigma_Guass = str(sigma_G.Eval(M)*M)
      #Crystal_Ball
      alpha_CrystalBall = str(alpha_CB)
      n_CrystalBall = str(n_CB.Eval(M))
      sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
      mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
      #k
      k_p = str(k_param.Eval(M))
      #Breit-Wigner
      BW_Gamma = str(Gamma)
      M_truth = str(M) #"+M_truth+"
      if(n==1): 
        # sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
        sWidth += "(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+",0)))"
        # sWidth = "(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+")))"
      else:     
        # sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
        sWidth += "+(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+",0)))"
        # sWidth = "(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+")))"
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
      y  = (((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5)
      # y  = fClockwork.Eval(m)
    else:
      y = 0 
    return y



  def BreitWigner(M,m,n,Gamma):
    sBreitWigner = "(BREITWIGNER)"
    sWidth = ""
    BW_Gamma = str(Gamma)
    M_truth = str(M)
    if(n==1): 
      sWidth += "ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+","+M_truth+")"
    else:     
      sWidth += "+ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+","+M_truth+")"
    sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
    fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
    y  = fBreitWigner.Eval(m)
    return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information MSTW2008lo68cl NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  print("KKlist")
  print(massList)
  print("KKlist length")
  print(len(massList))
  while mtemp < massList[-1]:
  # while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRee(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listS_Bin_Middle = []
  listS_Bin_Low = []
  listS_Bin_High = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    BW         = 0
    mass_lo       = massList[i]
    mass_mid      = massList[i]+0.5
    mass_high     = massList[i]+1
    print("mass")
    print(mass_mid)
    deltamass  = massList[i + 1] - massList[i]

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      if eventType == "int":
        total += int(listCS[particle]*Resolution(listMassKK[particle], particle, mass)*listBRee[particle]*Intlum)
      if eventType == "float":
        total += listCS[particle]*Smearing(listMassKK[particle],particle,mass_mid,listGamma[particle])*listBRee[particle]*Intlum
      # BW = total*random.BreitWigner(listMassKK[particle],listGamma[particle])
    #Append to lists
    listS.append(total*deltamass)
    listS_Bin_Middle.append(mass_mid)
    listS_Bin_Low.append(mass_lo)
    listS_Bin_High.append(mass_high)
    print("S")
    print(total*deltamass)
    listCSplot.append(total/Intlum)

  print("total")
  print(total) 

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, listS_Bin_Middle, listS_Bin_Low, listS_Bin_High

####Find shapes and toys

#Generate signal with no random fluctuations - just relative resolution using Gauissian from Resonant Search
def SigFixed_ee_GuassianOnly(k, M5, xmin, xmax, nBins, eventType):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir_ee+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  # fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",nBins,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(8300) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(6.1/5)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc_ee)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if (M5>k) and (M>M5): break
     sM = str(M)
     sW = str(M*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  listS_Bin_Middle = []
  listS_Bin_Low = []
  listS_Bin_High = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  # for i in range(1,N+1):
  for i in range(0, len(massList)):
     t = hsig.GetBinLowEdge(i)
     z = hsig.GetXaxis().GetBinUpEdge(i)
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float":    
      listS.append(y)
      listS_Bin_Middle.append(x)
      listS_Bin_Low.append(t)
      listS_Bin_High.append(z)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
        if eventType == "int":
          listS.append(int(y))
        if eventType == "float":    
          listS.append(y)
          listS_Bin_Middle.append(x)
          listS_Bin_Low.append(t)
          listS_Bin_High.append(z)
  return listS, listS_Bin_Middle, listS_Bin_Low, listS_Bin_High

#Generate signal with no random fluctuations
def SigFixed_ee_withCB(k, M5, xmin, xmax, nBins, eventType):

### get the resolution with nominal parameters
  # TF1 *f = new TF1(“f”, “((x > 2) ? sin(x) : 0)”, -10, 10)
  Step_fn = ROOT.TF1("Step_fn","x > 500 ? x =: 1", xmin, xmax)

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  # fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",nBins,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig")
  kR = str(10)
  scale = str(8300) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc_ee)+")"
  sResonances = ""  
  for n in range(1,100):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if (M5>k) and (M>M5): break
     #Gaussain
     mu_Gauss = str(mu_G.Eval(M)*M+M)
     sigma_Guass = str(sigma_G.Eval(M)*M)
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
     mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS_Bin_Middle = []
  listS_Bin_Low = []
  listS_Bin_High = []
  listS = []

  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  # for i in range(1,N+1):
  for i in range(0, len(massList)):
     x = hsig.GetBinCenter(i)
     t = hsig.GetBinLowEdge(i)
     z = hsig.GetXaxis().GetBinUpEdge(i)
     if x < k:
      y = 0
     else:
      y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float": 
      listS.append(y)    
      listS_Bin_Middle.append(x)
      listS_Bin_Low.append(t)
      listS_Bin_High.append(z)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
        if eventType == "int":
          listS.append(int(y))
        if eventType == "float":
        	listS.append(y)   
        	listS_Bin_Middle.append(x)
        	listS_Bin_Low.append(t)
        	listS_Bin_High.append(z)
  return listS, listS_Bin_Middle, listS_Bin_Low, listS_Bin_High

#Get background with no random fluctuations
def Dielectron_Bkg(xmin, xmax, nBins, eventType):

  fbkgfinal = ROOT.TFile(indir_ee+"finalbkg.root","READ")
  fBkg = fbkgfinal.Get("bkgfit")
  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listBkg_Bin_Middle = []
  listBkg_Bin_Low = []
  listBkg_Bin_High = []
  listBkg = []
  for i in range(0, len(massList)):
     x = hfit.GetBinCenter(i)
     t = hfit.GetBinLowEdge(i)
     z = hfit.GetXaxis().GetBinUpEdge(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     if eventType == "int":
      listBkg.append(int(y))
     if eventType == "float":    
      listBkg.append(y)
      listBkg_Bin_Middle.append(x)
      listBkg_Bin_Low.append(t)
      listBkg_Bin_High.append(z)
     hfit.SetBinContent(i,y)

  return listBkg, listBkg_Bin_Middle, listBkg_Bin_Low, listBkg_Bin_High


def SigBkgPoissonToy(listBkgFixed, listSigFixed, aBkg, aSig):

  listBkgSigrandom = []

  for i in range(0, len(listSigFixed)):
  
    listBkgSigrandom.append(np.random.poisson(lam = aBkg*listBkgFixed[i] + aSig*listSigFixed[i]))

  return listBkgSigrandom

def TextFileMaker(Name,ListCentre, Bin_Middle, Bin_Low, Bin_High):
# Bkg_Only_toys = open(Directory+"TextFile_Outputs/Bkg_Only_toys.txt","w")
	ftemp = open(Directory+"TextFile_Outputs/"+Name+".txt","w")
	for n in range(0, len(ListCentre)):
		ftemp.write(str(Bin_Low[n]) + " " + str(Bin_Middle[n]) + " " + str(Bin_High[n]) + " " + str(ListCentre[n]) + "\n")
	# ftemp.close()
	return ftemp

#Setup 
nBins = 5775
xmin = 225 #230
xmax = 6000 #2700

massList = np.linspace(xmin, xmax, nBins)


nTrials1 = 20
nTrials2 = 20

# massList = numpy.linspace(xmin, xmax, nBins)
#Parameters of the signal
k  = 500
M5 = 6000


SigFix_benchmark, Sig_benchmark_Bin_Middle, Sig_benchmark_Bin_Low, Sig_benchmark_Bin_High = SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins, "float")
SigFix, Sig_Bin_Middle, Sig_Bin_Low, Sig_Bin_High = SigFixed_ee_withCB(k, M5, xmin, xmax, nBins, "float")
BkgFix, Bkg_Bin_Middle, Bkg_Bin_Low, Bkg_Bin_High = Dielectron_Bkg(xmin, xmax, nBins, "float")

SigFix_benchmark_TextFile = TextFileMaker("FixedSignal_Benchmark", SigFix_benchmark, Sig_benchmark_Bin_Middle, Sig_benchmark_Bin_Low, Sig_benchmark_Bin_High)
Signal_TextFile = TextFileMaker("FixedSignal", SigFix, Sig_Bin_Middle, Sig_Bin_Low, Sig_Bin_High)
Bkg_TextFile = TextFileMaker("FixedBackground", BkgFix, Bkg_Bin_Middle, Bkg_Bin_Low, Bkg_Bin_High)

for i in range(0, nTrials1):
	BkgSigToy = SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
	print(BkgSigToy)
	BkgSigToy_TextFile = TextFileMaker("SignalBackgroundToy"+str(i)+"", BkgSigToy, Bkg_Bin_Middle, Bkg_Bin_Low, Bkg_Bin_High)
	BkgSigBenchmark_Toy = SigBkgPoissonToy(BkgFix, SigFix_benchmark, 1, 1)	
	BkgSigBenchmark_Toy_TextFile = TextFileMaker("BenchmarkTheory_SignalBackgroundToy"+str(i)+"", BkgSigBenchmark_Toy, Bkg_Bin_Middle, Bkg_Bin_Low, Bkg_Bin_High)


for i in range(0, nTrials2):
	BkgToy = SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
	BkgToy_TextFile = TextFileMaker("BackgroundToy"+str(i)+"", BkgToy, Bkg_Bin_Middle, Bkg_Bin_Low, Bkg_Bin_High)



##Plot Signal and Background shapes for debugging
#Fixed Signal and Background Mass Distribution Plotting
fig_SigBkg, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList,BkgToy, 'b',linewidth=2, label=r"Background Toy")
plt.plot(massList,SigFix, 'r',linewidth=2, label=r"Analytical Signal with dampening")
plt.plot(massList,SigFix_benchmark, 'yellow',linewidth=2, label=r"Analytical benchmark Signal")
plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(bottom=10e-4)
plt.yscale('log')
plt.xscale('log')
fig_SigBkg.savefig('TextFile_Outputs/SigBkg_plot.pdf')