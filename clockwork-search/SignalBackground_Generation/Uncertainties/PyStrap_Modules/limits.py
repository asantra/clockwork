from __future__ import division
from modules import *
from modules import mc as modelConstructor


# Poisson limit function

# following https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015

def getModels(w,poiName,sVal=None):
    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    # sbModel.SetNuisanceParameters(RooArgSet(w.var("np1")))
    if sVal!=None:
        w.obj(poiName).setVal(sVal)
    # w.obj(poiName).setConstant()
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(0)
    # w.obj(poiName).setConstant()
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    return sbModel,bModel

def generateClsList(w,hint,lambdaVals):
    nSigVals = []
    CLsVals = []
    uOneSigs = []
    poiName = "nSig"
    sbModel,bModel = getModels(w,poiName,sVal=None)
    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(w.obj("data"), bModel, sbModel) # correct for limits
    ac.SetOneSided(True)
    inverter = HypoTestInverter(ac)
    for lambdaVal in lambdaVals:
        w.var("lambdaInv").setVal(lambdaVal)
        nSigVal = hint.getVal()
        # sVal = lambdaVal
        inverter.RunOnePoint(nSigVal)
        n = inverter.GetInterval().ArraySize()-1
        xVal     = inverter.GetInterval().GetXValue(n)
        cls      = inverter.GetInterval().CLs(n)
        uOneSig  = inverter.GetInterval().GetExpectedUpperLimit(1);
        uOneSigs.append(uOneSig)
        CLsVals.append(cls)
        nSigVals.append(nSigVal)
    # print red(uOneSigs); quit()
    uOneSigs   = np.array(uOneSigs)
    lambdaVals   = np.array(lambdaVals)
    nSigVals     = np.array(nSigVals)
    CLsVals      = np.array(CLsVals)
    return {"lambdaVals":lambdaVals,
            "uOneSigs":uOneSigs,
            "nSigVals":nSigVals,"CLsVals":CLsVals}

def interp(y,x1,x2,y1,y2):
    """ Linear interpolation
        Returns x(y) based on points (x1,y1) -- (x2,y2)
    """
    if x1==x2: return None
    slope = (y2-y1)/(x2-x1)
    return (y-y1)/slope + x1


def poissonLimit2l(nBkg, nObs,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=None):
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)


    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)



    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,-50,-10,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = {}
    ret["{0}_lambda_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    ret["{0}_lambda_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_lambda_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_lambda_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_lambda_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_lambda_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_lambda_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_lambda_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference

    return ret

def poissonLimitLumi(nBkg, nObs,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)


    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(prod:sigProd(sum:sigSum(1,np3[0,-5,5]),nSig),prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)



    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,-50,-10,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = {}
    ret["{0}_lambdaLumi_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    ret["{0}_lambdaLumi_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_lambdaLumi_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_lambdaLumi_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_lambdaLumi_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_lambdaLumi_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_lambdaLumi_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_lambdaLumi_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference

    return ret

def poissonLimit3l(nBkg, nObs,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
        Set with AsymptoticCalculator
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)


    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,mean_np2[0],npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,mean_np3[0],npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)



    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3"))) # global observable is measured by "other" experiment
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,-50,-10,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = {}
    ret["{0}_LambdaAc_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    ret["{0}_LambdaAc_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_LambdaAc_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_LambdaAc_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_LambdaAc_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_LambdaAc_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_LambdaAc_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_LambdaAc_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference
    print (green("poissonLimit3l expected",-htir.GetExpectedUpperLimit(0)))
    print (green("poissonLimit3l observed",-htir.UpperLimit()))

    return ret

def poissonCombLimit3l(nBkg_ee, nObs_ee, nBkg_mm, nObs_mm, interference="const", chirality="LL", npWidth1_ee=1, npWidth2_ee=1, npWidth3_ee=1, npWidth1_mm=1, npWidth2_mm=1, npWidth3_mm=1):
# def lambdaLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate combined single bin limit on lambda
        np1_ee = nuissance parameter width  number:1 for ee channel
        np1_mm = nuissance parameter width  number:1 for mm channel
        nBkg is background expectation
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs_ee[{0}]".format(nObs_ee))
    w.factory("nBkg_ee[{0}]".format(nBkg_ee))
    w.factory("nObs_mm[{0}]".format(nObs_mm))
    w.factory("nBkg_mm[{0}]".format(nBkg_mm))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg_ee").setConstant()
    w.var("nBkg_mm").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    
    srMax = 6000
    # Set up morphed signal ee channel 
    srMin_ee = dictionary.ranges["ee"][interference][chirality]["extrapMin"]
    hint_ee = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel="ee",lumi=lumi,
                            srMin=srMin_ee,srMax=srMax,
                           )
    hint_ee.SetName("nSig_ee")
    hint_ee.SetTitle("nSig_ee")
    getattr(w, "import")(hint_ee)
    
    # Set up morphed signal mm channel 
    srMin_mm = dictionary.ranges["mm"][interference][chirality]["extrapMin"]
    hint_mm = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel="mm",lumi=lumi,
                            srMin=srMin_mm,srMax=srMax,
                           )
    hint_mm.SetName("nSig_mm")
    hint_mm.SetTitle("nSig_mm")
    getattr(w, "import")(hint_mm)
    
    ##################################################
    # make poisson model
    ##################################################
    # ee channel 
    w.factory("sum:nExp_ee(nSig_ee,prod:bkgProd_ee(sum:bkgSum_ee(1,np1_ee[0,-5,5],np2_ee[0,-5,5],np3_ee[0,-5,5]),nBkg_ee))")
    w.factory("Gaussian:g1_ee(np1_ee,0,npWidth1_ee[{0}])".format(npWidth1_ee/nBkg_ee));
    w.factory("Gaussian:g2_ee(np2_ee,0,npWidth2_ee[{0}])".format(npWidth2_ee/nBkg_ee));
    w.factory("Gaussian:g3_ee(np3_ee,0,npWidth3_ee[{0}])".format(npWidth3_ee/nBkg_ee));
    w.factory("Poisson:pois_ee(nObs_ee,nExp_ee)")
    w.factory("PROD:model_ee(pois_ee,g1_ee,g2_ee,g3_ee)");
    # mm channel 
    w.factory("sum:nExp_mm(nSig_mm,prod:bkgProd_mm(sum:bkgSum_mm(1,np1_mm[0,-5,5],np2_mm[0,-5,5],np3_mm[0,-5,5]),nBkg_mm))")
    w.factory("Gaussian:g1_mm(np1_mm,0,npWidth1_mm[{0}])".format(npWidth1_mm/nBkg_mm));
    w.factory("Gaussian:g2_mm(np2_mm,0,npWidth2_mm[{0}])".format(npWidth2_mm/nBkg_mm));
    w.factory("Gaussian:g3_mm(np3_mm,0,npWidth3_mm[{0}])".format(npWidth3_mm/nBkg_mm));
    w.factory("Poisson:pois_mm(nObs_mm,nExp_mm)")
    w.factory("PROD:model_mm(pois_mm,g1_mm,g2_mm,g3_mm)");
    
    #making joint model
    w.factory("channel[ee,mm]")
    w.factory("SIMUL:jointModel(channel,ee=model_ee,mm=model_mm)")

    ##################################################
    # make indexed data-set
    ##################################################
    w.defineSet("nObs", "nObs_ee,nObs_mm")
    data_ee = RooDataSet("data_ee","", RooArgSet(w.var("nObs_ee")));
    w.var("nObs_ee").setVal(nObs_ee);
    data_ee.add(RooArgSet(w.var("nObs_ee")))
    data_mm = RooDataSet("data_mm","", RooArgSet(w.var("nObs_mm")));
    w.var("nObs_mm").setVal(nObs_mm);
    data_mm.add(RooArgSet(w.var("nObs_mm")))
    #combine dataset
    data = RooDataSet("data","",w.set("nObs"),RooFit.Index(w.cat("channel")),RooFit.Import("ee",data_ee),RooFit.Import("mm",data_mm));
    data.add(w.set("nObs"));
    getattr(w,"import")(data);

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("jointModel"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.set("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1_ee"),w.var("np2_ee"),w.var("np3_ee"),w.var("np1_mm"),w.var("np2_mm"),w.var("np3_mm")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    hypoCalc.SetFixedScan(100,-50,-10,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = {}
    # ret["{0}_lambda_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    ret["lambda_upperLimit"]      = -htir.UpperLimit();
    ret["lambda_lowerLimit"]      = -htir.LowerLimit();
    ret["lambda_expLimit"]        = -htir.GetExpectedUpperLimit(0);
    ret["lambda_uOneSig"]         = -htir.GetExpectedUpperLimit(1);
    ret["lambda_uTwoSig"]         = -htir.GetExpectedUpperLimit(2);
    ret["lambda_lOneSig"]         = -htir.GetExpectedUpperLimit(-1);
    ret["lambda_lTwoSig"]         = -htir.GetExpectedUpperLimit(-2);
    ret["channel"]                = "ll"
    ret["chirality"]              = chirality
    ret["interference"]           = interference

    return ret

def poissonCombLimit3l_fc(nBkg_ee, nObs_ee, nBkg_mm, nObs_mm, interference="const", chirality="LL", npWidth1_ee=1, npWidth2_ee=1, npWidth3_ee=1, npWidth1_mm=1, npWidth2_mm=1, npWidth3_mm=1,nToys=1000):
# def lambdaLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate combined single bin limit on lambda
        np1_ee = nuissance parameter width  number:1 for ee channel
        np1_mm = nuissance parameter width  number:1 for mm channel
        nBkg is background expectation
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs_ee[{0},0,100]".format(nObs_ee))
    w.factory("nBkg_ee[{0}]".format(nBkg_ee))
    w.factory("nObs_mm[{0},0,100]".format(nObs_mm))
    w.factory("nBkg_mm[{0}]".format(nBkg_mm))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg_ee").setConstant()
    w.var("nBkg_mm").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    
    srMax = 6000
    # Set up morphed signal ee channel 
    srMin_ee = dictionary.ranges["ee"][interference][chirality]["extrapMin"]
    hint_ee = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel="ee",lumi=lumi,
                            srMin=srMin_ee,srMax=srMax,
                           )
    hint_ee.SetName("nSig_ee")
    hint_ee.SetTitle("nSig_ee")
    getattr(w, "import")(hint_ee)
    
    # Set up morphed signal mm channel 
    srMin_mm = dictionary.ranges["mm"][interference][chirality]["extrapMin"]
    hint_mm = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel="mm",lumi=lumi,
                            srMin=srMin_mm,srMax=srMax,
                           )
    hint_mm.SetName("nSig_mm")
    hint_mm.SetTitle("nSig_mm")
    getattr(w, "import")(hint_mm)
    
    bkgNp_ee = np.sqrt(npWidth1_ee ** 2 + npWidth2_ee ** 2 + npWidth3_ee ** 2)
    bkgNp_mm = np.sqrt(npWidth1_mm ** 2 + npWidth2_mm ** 2 + npWidth3_mm ** 2)
    
    ##################################################
    # make poisson model
    ##################################################
    # ee channel 
    w.factory("sum:nExp_ee(nSig_ee,prod:bkgProd_ee(sum:bkgSum_ee(1,np1_ee[0,-5,5],np2_ee[0,-5,5],np3_ee[0,-5,5]),nBkg_ee))")
    w.factory("Gaussian:g1_ee(np1_ee,obs_np1_ee[0],npWidth1_ee[{0}])".format(bkgNp_ee/nBkg_ee));
    w.factory("Poisson:pois_ee(nObs_ee,nExp_ee)")
    w.factory("PROD:model_ee(pois_ee,g1_ee)");
    # mm channel 
    w.factory("sum:nExp_mm(nSig_mm,prod:bkgProd_mm(sum:bkgSum_mm(1,np1_mm[0,-5,5],np2_mm[0,-5,5],np3_mm[0,-5,5]),nBkg_mm))")
    w.factory("Gaussian:g1_mm(np1_mm,obs_np1_mm[0],npWidth1_mm[{0}])".format(bkgNp_mm/nBkg_mm));
    w.factory("Poisson:pois_mm(nObs_mm,nExp_mm)")
    w.factory("PROD:model_mm(pois_mm,g1_mm)");
    
    #making joint model
    w.factory("PROD:jointModel(model_ee,model_mm)")

    ##################################################
    # make indexed data-set
    ##################################################
    w.defineSet("nObs", "nObs_ee,nObs_mm")
    data = RooDataSet("data","", RooArgSet(w.set("nObs")));
    w.var("nObs_ee").setVal(nObs_ee);
    w.var("nObs_mm").setVal(nObs_ee);
    data.add(RooArgSet(w.set("nObs")))

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("jointModel"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.set("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1_ee"), w.var("np1_mm")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("obs_np1_ee"), w.var("obs_np1_mm")))
    
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################
    
    ##################################################
    # create hypotest inverter
    ##################################################
    freqCalc = ROOT.RooStats.FrequentistCalculator(data, bModel, sbModel);
    # freqCalc = ROOT.RooStats.SimpleLikelihoodRatioTestStat(bModel, sbModel); 
    plr = ROOT.RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    plr.SetOneSided(True);
    toymcs = freqCalc.GetTestStatSampler()
    toymcs.SetTestStatistic(plr)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print ('adjusting for non-extended formalism')
   
    freqCalc.SetToys(nToys, nToys)
    hypoCalc = HypoTestInverter(freqCalc);
    hypoCalc.SetFixedScan(100, -50, -10, False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)
    
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # return the nSig corresponding to the limit on lambda
    ret = {}
    # ret["{0}_lambda_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    ret["lambda_upperLimit"]      = -htir.UpperLimit();
    ret["lambda_lowerLimit"]      = -htir.LowerLimit();
    ret["lambda_expLimit"]        = -htir.GetExpectedUpperLimit(0);
    ret["lambda_uOneSig"]         = -htir.GetExpectedUpperLimit(1);
    ret["lambda_uTwoSig"]         = -htir.GetExpectedUpperLimit(2);
    ret["lambda_lOneSig"]         = -htir.GetExpectedUpperLimit(-1);
    ret["lambda_lTwoSig"]         = -htir.GetExpectedUpperLimit(-2);
    ret["channel"]                = "ll"
    ret["chirality"]              = chirality
    ret["interference"]           = interference

    return ret

def poissonLimit1_fc(nBkg, nObs, npWidth1=1,npWidth2=1,npWidth3=1,nToys=None,nSteps=None):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        Sum background NP's in quad, use one gaussian
        FrequentistCalculator
    """
    bkgNp = np.sqrt(npWidth1**2+npWidth2**2+npWidth3**2)

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model
    w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(bkgNp/nBkg));
    w.factory("PROD:model(pois,g1)");
    # w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1")));
    # mc.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(RooArgSet(w.var("mean_np1")))
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));


    # do aysmtotic test
    # ac = AsymptoticCalculator(data, bModel, sbModel);
    # ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    # setup from https://lost-contact.mit.edu/afs//desy.de/project/ilcsoft/sw/x86_64_gcc44_sl6/v01-17-06/root/5.34.18/tutorials/roostats/StandardHypoTestInvDemo.C
    slrts = SimpleLikelihoodRatioTestStat(sbModel.GetPdf(),bModel.GetPdf())
    nullParams = RooArgSet(sbModel.GetSnapshot());
    nullParams.add(sbModel.GetNuisanceParameters());
    slrts.SetNullParameters(nullParams);
    altParams = RooArgSet(bModel.GetSnapshot());
    altParams.add(bModel.GetNuisanceParameters());
    slrts.SetAltParameters(altParams);

    toymcs = freqCalc.GetTestStatSampler();
    # toymcs.SetTestStatistic(slrts);
    toymcs.SetTestStatistic(plr); # plr gives stronger limits

    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)

    freqCalc.SetToys(nToys,nToys)
    # freqCalc.SetNToysInTails(nToys,nToys)

    inverter = HypoTestInverter(freqCalc)
    # inverter = HypoTestInverter(ac)
    # inverter.SetFixedScan(20,-36,-30,False)
    inverter.SetFixedScan(nSteps,0.1,20,True)
    # inverter.SetFixedScan(nSteps,15,17,True)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    # inverter.SetVerbose(False);
    htir = inverter.GetInterval()


    # get resutls
    ret = {}
    ret["nSigFc_upperLimit"] = htir.UpperLimit();
    ret["nSigFc_lowerLimit"] = htir.LowerLimit();
    ret["nSigFc_expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["nSigFc_uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["nSigFc_uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["nSigFc_lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["nSigFc_lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nSigFc_nExpModel"]  = w.obj("nExp").getVal()
    print (green("poissonLimit3_fc",htir.GetExpectedUpperLimit(0)))
    return ret

def poissonLimit1_ac(nBkg, nObs, npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        Sum background NP's in quad, use one gaussian
        AsymptoticCalculator
    """

    bkgNp = np.sqrt(npWidth1**2+npWidth2**2+npWidth3**2)

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # print red("="*50)
    # # w.factory("nObs[6]")
    # w.var("nObs").randomize()
    # w.var("nObs").Print()
    # w.var("nObs").randomize()
    # w.var("nObs").Print()
    # print red("="*50)
    # quit()

    # make poisson model
    # w.factory("nObs[{0},{0},{0}]".format(nObs))
    # w.factory("nObs[{0},0,1000]".format(nObs))
    w.factory("nObs[{0}]".format(nObs))
    w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs,nExp)");
    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(bkgNp/nBkg));
    w.factory("PROD:model(pois,g1)");
    # w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(RooArgSet(w.var("mean_np1")))
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));


    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);
    # w.var("nObs").Print(); quit()



    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(100,0.01,20, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();


    # get resutls
    ret = {}
    ret["nSigAc_upperLimit"] = htir.UpperLimit();
    ret["nSigAc_lowerLimit"] = htir.LowerLimit();
    ret["nSigAc_expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["nSigAc_uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["nSigAc_uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["nSigAc_lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["nSigAc_lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nSigAc_nExpModel"]  = w.obj("nExp").getVal()
    print (green("poissonLimit3_ac",htir.GetExpectedUpperLimit(0)))
    return ret


def poissonLimit3_fc(nBkg, nObs, npWidth1=1,npWidth2=1,npWidth3=1,nToys=None,nSteps=None):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        FrequentistCalculator
    """

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model
    # w.factory("sum:nExp(nSig[1,-300,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    # w.factory("sum:nExp(nSig[1,-10,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    # w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,mean_np2[0],npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,mean_np3[0],npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")));
    # mc.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3")))
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));


    # do aysmtotic test
    # ac = AsymptoticCalculator(data, bModel, sbModel);
    # ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    # setup from https://lost-contact.mit.edu/afs//desy.de/project/ilcsoft/sw/x86_64_gcc44_sl6/v01-17-06/root/5.34.18/tutorials/roostats/StandardHypoTestInvDemo.C
    slrts = SimpleLikelihoodRatioTestStat(sbModel.GetPdf(),bModel.GetPdf())
    nullParams = RooArgSet(sbModel.GetSnapshot());
    nullParams.add(sbModel.GetNuisanceParameters());
    slrts.SetNullParameters(nullParams);
    altParams = RooArgSet(bModel.GetSnapshot());
    altParams.add(bModel.GetNuisanceParameters());
    slrts.SetAltParameters(altParams);

    toymcs = freqCalc.GetTestStatSampler();
    # toymcs.SetTestStatistic(slrts);
    toymcs.SetTestStatistic(plr); # plr gives stronger limits

    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)

    freqCalc.SetToys(nToys,nToys)
    # freqCalc.SetNToysInTails(nToys,nToys)

    inverter = HypoTestInverter(freqCalc)
    # inverter = HypoTestInverter(ac)
    # inverter.SetFixedScan(20,-36,-30,False)
    inverter.SetFixedScan(nSteps,0.1,20,True)
    # inverter.SetFixedScan(nSteps,15,17,True)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    inverter.SetVerbose(False);
    htir = inverter.GetInterval()


    # get resutls
    ret = {}
    ret["nSigFc_upperLimit"] = htir.UpperLimit();
    ret["nSigFc_lowerLimit"] = htir.LowerLimit();
    ret["nSigFc_expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["nSigFc_uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["nSigFc_uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["nSigFc_lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["nSigFc_lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nSigFc_nExpModel"]  = w.obj("nExp").getVal()
    print (green("poissonLimit3_fc",htir.GetExpectedUpperLimit(0)))
    return ret

def poissonLimit3_ac(nBkg, nObs, npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        AsymptoticCalculator
    """

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model
    # w.factory("sum:nExp(nSig[1,-300,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    # w.factory("sum:nExp(nSig[1,-10,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    # w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,mean_np2[0],npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,mean_np3[0],npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")));
    # mc.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3")))
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));


    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(100,0.01,20, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["nSigAc_upperLimit"] = htir.UpperLimit();
    ret["nSigAc_lowerLimit"] = htir.LowerLimit();
    ret["nSigAc_expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["nSigAc_uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["nSigAc_uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["nSigAc_lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["nSigAc_lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nSigAc_nExpModel"]  = w.obj("nExp").getVal()
    print (green("poissonLimit3_ac",htir.GetExpectedUpperLimit(0)))
    return ret

def poissonLimit(nBkg, nObs, npWidth1=1,npWidth2=1):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
    """

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model
    w.factory("sum:nExp(nSig[1,0,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1]),nBkg[{0}]))".format(nBkg))
    # w.factory("sum:nExp(nSig[1,-300,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np2")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(100,0.01,100, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = htir.UpperLimit();
    ret["lowerLimit"] = htir.LowerLimit();
    ret["expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nExpModel"]  = w.obj("nExp").getVal()
    ret["nBkg"]       = nBkg
    ret["nObs"]       = nObs
    ret["npWidth1"]   = npWidth1
    ret["npWidth2"]   = npWidth2
    ret["nSig"]       = w.var("nSig").getVal()
    ret["np1"]        = w.var("np1").getVal()
    ret["np2"]        = w.var("np2").getVal()

    return ret

def nSigLimit(nBkg, nObs,i=0, npWidth1=1,npWidth2=1,npWidth3=1,chirality="LL",channel="ee",interference="const"):
    """ calculate poisson limit on lambda
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        Limits are on "nSignal" directly
    """

    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    x = w.var("x")
    w.factory("nSig[1,{0},100]".format(i))

    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("PROD:model(pois,g1,g2,g3)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w);
    sbModel.SetPdf(w.pdf("model"));
    sbModel.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    sbModel.SetObservables(RooArgSet(w.var("nObs")));
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")));
    sbModel.SetSnapshot(RooArgSet(w.var("nSig")));
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    w.obj("nSig").setVal(0);
    bModel.SetSnapshot(RooArgSet(w.obj("nSig")));
    ##################################################

    ##################################################
    # do aysmtotic test
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    ##################################################
    # create hypotest inverter
    ##################################################
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(100,0.01,100,False);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = htir.UpperLimit();
    return ret

def nSigInvLimit(nBkg, nObs, npWidth1=1,npWidth2=1,npWidth3=1,chirality="LL",channel="ee",interference="const"):
    """ calculate poisson limit on lambda
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        Limits are on "nSignal" inverted
    """

    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    x = w.var("x")

    w.factory("nSigInv[-100,0]")
    w.factory("slope[1]")
    w.factory("offset[100]")
    lambd = RooLinearVar("nSig","nSig",w.var("nSigInv"),w.var("slope"),w.var("offset"))
    getattr(w,"import")(lambd)


    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("PROD:model(pois,g1,g2,g3)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w);
    sbModel.SetPdf(w.pdf("model"));
    sbModel.SetParametersOfInterest(RooArgSet(w.var("nSigInv")));
    sbModel.SetObservables(RooArgSet(w.var("nObs")));
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")));
    sbModel.SetSnapshot(RooArgSet(w.var("nSigInv")));
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    w.obj("nSigInv").setVal(-100);
    bModel.SetSnapshot(RooArgSet(w.obj("nSigInv")));
    ##################################################

    ##################################################
    # do aysmtotic test
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    ##################################################
    # create hypotest inverter
    ##################################################
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(100,-100,-0.01,False);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = -htir.UpperLimit();
    return ret

def poissonLimit1lToys(nBkg, nObs,nSteps=50,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,nToys=1000):
    # print red("Warning, not implemented yet!!!"*50)
    print (green("Setting poissonLimit2lToys limit"))
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    bkgNp = np.sqrt(npWidth1**2+npWidth2**2)
    # Follow: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls_toys.html
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)

    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5]),nBkg))")

    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(bkgNp/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1)");

    # w.var("np1").setConstant()
    # w.var("np2").setConstant()
    # w.var("np3").setConstant()

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    # w.obj(poiName).setVal(-20)
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("mean_np1")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    # print green(w.obj("nExp").getVal())
    # w.obj(poiName).setConstant()
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter - TOY
    ##################################################
    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    # setup from https://lost-contact.mit.edu/afs//desy.de/project/ilcsoft/sw/x86_64_gcc44_sl6/v01-17-06/root/5.34.18/tutorials/roostats/StandardHypoTestInvDemo.C
    slrts = SimpleLikelihoodRatioTestStat(sbModel.GetPdf(),bModel.GetPdf())
    nullParams = RooArgSet(sbModel.GetSnapshot());
    nullParams.add(sbModel.GetNuisanceParameters());
    slrts.SetNullParameters(nullParams);
    altParams = RooArgSet(bModel.GetSnapshot());
    altParams.add(bModel.GetNuisanceParameters());
    slrts.SetAltParameters(altParams);

    toymcs = freqCalc.GetTestStatSampler();
    # toymcs.SetTestStatistic(slrts);
    toymcs.SetTestStatistic(plr); # plr gives stronger limits

    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)

    freqCalc.SetToys(nToys,nToys)

    inverter = HypoTestInverter(freqCalc)
    # inverter = HypoTestInverter(ac)
    # inverter.SetFixedScan(20,-36,-30,False)
    inverter.SetFixedScan(nSteps,-40,-20,False)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    htir = inverter.GetInterval()

    # return the nSig corresponding to the limit on lambda
    ret = {}
    w.obj(poiName).setVal(htir.UpperLimit())
    ret["{0}_LambdaToy1_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    w.obj(poiName).setVal(htir.GetExpectedUpperLimit(0))
    ret["{0}_LambdaToy1_expLimitNSig".format(chirality)]    = w.obj("nSig").getVal()
    ret["{0}_LambdaToy1_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_LambdaToy1_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_LambdaToy1_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_LambdaToy1_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_LambdaToy1_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_LambdaToy1_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_LambdaToy1_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference
    print (yellow("Frequentist limits"))
    for k,v in ret.items():
        print (yellow(k,v))
    print (green("poissonLimit3l expected",-htir.GetExpectedUpperLimit(0)))
    print (green("poissonLimit3l observed",-htir.UpperLimit()))
    return ret

def poissonLimit2lToys(nBkg, nObs,nSteps=50,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,nToys=1000):
    # print red("Warning, not implemented yet!!!"*50)
    print (green("Setting poissonLimit2lToys limit"))
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    # Follow: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls_toys.html
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)

    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5]),nBkg))")

    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,mean_np2[0],npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2)");

    # w.var("np1").setConstant()
    # w.var("np2").setConstant()
    # w.var("np3").setConstant()

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    # w.obj(poiName).setVal(-20)
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    # print green(w.obj("nExp").getVal())
    # w.obj(poiName).setConstant()
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter - TOY
    ##################################################
    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    # setup from https://lost-contact.mit.edu/afs//desy.de/project/ilcsoft/sw/x86_64_gcc44_sl6/v01-17-06/root/5.34.18/tutorials/roostats/StandardHypoTestInvDemo.C
    slrts = SimpleLikelihoodRatioTestStat(sbModel.GetPdf(),bModel.GetPdf())
    nullParams = RooArgSet(sbModel.GetSnapshot());
    nullParams.add(sbModel.GetNuisanceParameters());
    slrts.SetNullParameters(nullParams);
    altParams = RooArgSet(bModel.GetSnapshot());
    altParams.add(bModel.GetNuisanceParameters());
    slrts.SetAltParameters(altParams);

    toymcs = freqCalc.GetTestStatSampler();
    # toymcs.SetTestStatistic(slrts);
    toymcs.SetTestStatistic(plr); # plr gives stronger limits


    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)

    freqCalc.SetToys(nToys,nToys)

    inverter = HypoTestInverter(freqCalc)
    # inverter = HypoTestInverter(ac)
    # inverter.SetFixedScan(20,-36,-30,False)
    inverter.SetFixedScan(nSteps,-40,-20,False)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    htir = inverter.GetInterval()

    # return the nSig corresponding to the limit on lambda
    ret = {}
    w.obj(poiName).setVal(htir.UpperLimit())
    ret["{0}_LambdaToy2_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    w.obj(poiName).setVal(htir.GetExpectedUpperLimit(0))
    ret["{0}_LambdaToy2_expLimitNSig".format(chirality)]    = w.obj("nSig").getVal()
    ret["{0}_LambdaToy2_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_LambdaToy2_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_LambdaToy2_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_LambdaToy2_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_LambdaToy2_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_LambdaToy2_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_LambdaToy2_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference
    print (yellow("Frequentist limits"))
    for k,v in ret.items():
        print (yellow(k,v))
    print (green("poissonLimit3l expected",-htir.GetExpectedUpperLimit(0)))
    print (green("poissonLimit3l observed",-htir.UpperLimit()))
    return ret

def poissonLimit3lToys(nBkg, nObs,nSteps=50,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1,nToys=1000):
    # Todo: NOT WORKING YET
    # print red("Warning, not implemented yet!!!"*50)
    print (green("Setting poissonLimit3lToys limit"))
    """ calculate single bin limit on lambda
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nObs is events observed
    """
    # Follow: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls_toys.html
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")

    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")

    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)

    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")

    w.factory("Gaussian:g1(np1,mean_np1[0],npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,mean_np2[0],npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,mean_np3[0],npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");

    # w.var("np1").setConstant()
    # w.var("np2").setConstant()
    # w.var("np3").setConstant()

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    # w.obj(poiName).setVal(-20)
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    # print green(w.obj("nExp").getVal())
    # w.obj(poiName).setConstant()
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter - TOY
    ##################################################
    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    toymcs = freqCalc.GetTestStatSampler();
    toymcs.SetTestStatistic(plr);

    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)

    freqCalc.SetToys(nToys,nToys)

    inverter = HypoTestInverter(freqCalc)
    # inverter = HypoTestInverter(ac)
    # inverter.SetFixedScan(20,-36,-30,False)
    inverter.SetFixedScan(nSteps,-40,-20,False)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    htir = inverter.GetInterval()

    # return the nSig corresponding to the limit on lambda
    ret = {}
    w.obj(poiName).setVal(htir.UpperLimit())
    ret["{0}_LambdaToy3_upperLimitNSig".format(chirality)]  = w.obj("nSig").getVal()
    w.obj(poiName).setVal(htir.GetExpectedUpperLimit(0))
    ret["{0}_LambdaToy3_expLimitNSig".format(chirality)]    = w.obj("nSig").getVal()
    ret["{0}_LambdaToy3_upperLimit".format(chirality)]      = -htir.UpperLimit();
    ret["{0}_LambdaToy3_lowerLimit".format(chirality)]      = -htir.LowerLimit();
    ret["{0}_LambdaToy3_expLimit".format(chirality)]        = -htir.GetExpectedUpperLimit(0);
    ret["{0}_LambdaToy3_uOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(1);
    ret["{0}_LambdaToy3_uTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(2);
    ret["{0}_LambdaToy3_lOneSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-1);
    ret["{0}_LambdaToy3_lTwoSig".format(chirality)]         = -htir.GetExpectedUpperLimit(-2);
    ret["{0}_channel".format(chirality)]                = channel
    ret["{0}_chirality".format(chirality)]              = chirality
    ret["{0}_interference".format(chirality)]           = interference
    print (yellow("Frequentist limits"))
    for k,v in ret.items():
        print (yellow(k,v))
    return ret

##################################################
# Debug tools validated elsewhere
##################################################

def makeWorkspace(nBkg,nObs,npWidth1=0.01,npWidth2=0.01,npWidth3=0.01):
    """ Return workspace, model configs """
    # set up workspace
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("nSig[0,{0}]".format(nBkg))
    w.var("nBkg").setConstant()
    # w.var("nObs").setConstant()
    # set up model
    poiName = "nSig"
    # npWidth1 = 0.05
    # npWidth1 = 0.01

    w.factory("Gaussian:g1(obs_np1[0,-1,1],mean_np1[0],npWidth1[{0}])".format(npWidth1));
    w.factory("Gaussian:g2(obs_np2[0,-1,1],mean_np2[0],npWidth2[{0}])".format(npWidth2));
    w.factory("Gaussian:g3(obs_np3[0,-1,1],mean_np3[0],npWidth3[{0}])".format(npWidth3));
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,obs_np1,obs_np2,obs_np3),nBkg))")
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");

    # w.var("obs_np1").setConstant()
    # w.var("obs_np2").setConstant()
    # w.var("obs_np3").setConstant()

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.obj("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    # sbModel.SetNuisanceParameters(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("obs_np1"),w.var("obs_np2"),w.var("obs_np3")))
    # sbModel.SetNuisanceParameters(RooArgSet(w.var("npWidth1"),w.var("mean_np1")))
    # sbModel.SetGlobalObservables(RooArgSet(w.var("obs_np1")))
    sbModel.SetGlobalObservables(RooArgSet(w.var("mean_np1"),w.var("mean_np2"),w.var("mean_np3"))) # global observable is measured by "other" experiment
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(0)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    return w,bModel,sbModel

def acLimit(nBkg,nObs,npWidth1=0.01,npWidth2=0.01,npWidth3=0.01,settings=None):
    """ Single bin AC limit """
    # Following: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls.html
    w,bModel,sbModel = makeWorkspace(nBkg,nObs,npWidth1=npWidth1)
    data = w.obj("data")

    nSteps = settings["nSteps"]
    scanMin = settings["scanMin"]
    scanMax = settings["scanMax"]

    ##################################################
    # create hypotest inverter - ASIMOV
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    inverter = HypoTestInverter(ac)
    inverter.SetFixedScan(nSteps,scanMin,scanMax,True)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    inverter.SetVerbose(True)
    # inverter.SetVerbose(False)
    htir = inverter.GetInterval()

    # return result as dictionary
    ret = {}
    ret["observed"] = htir.UpperLimit()
    ret["expected"] = htir.GetExpectedUpperLimit(0)
    # for k,v, in ret.items(): print yellow(k,v)
    return ret

def fcLimit(nBkg,nObs,npWidth1=0.01,npWidth2=0.01,npWidth3=0.01,settings=None):
    """ Single bin FC limit """
    # Follow: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls_toys.html
    w,bModel,sbModel = makeWorkspace(nBkg,nObs,npWidth1=npWidth1)
    data = w.obj("data")

    nToys = settings["nToys"]
    nSteps = settings["nSteps"]
    scanMin = settings["scanMin"]
    scanMax = settings["scanMax"]
    # print w.var("npWidth1").getVal(); quit()

    ##################################################
    # create hypotest inverter - TOY
    ##################################################
    freqCalc = FrequentistCalculator(data, bModel, sbModel); # alt, null
    freqCalc.SetToys(nToys,nToys)

    plr = ProfileLikelihoodTestStat(sbModel.GetPdf());
    plr.SetOneSided(True);
    toymcs = freqCalc.GetTestStatSampler();
    toymcs.SetTestStatistic(plr);
    if not bModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
    # toymcs.SetNuisanceParameters(RooArgSet(w.var("obs_np1")))
    # toymcs.SetPriorNuisance(w.obj("g1"))

    inverter = HypoTestInverter(freqCalc)
    inverter.SetFixedScan(nSteps,scanMin,scanMax,True)
    inverter.SetConfidenceLevel(0.95)
    inverter.UseCLs(True)
    inverter.SetVerbose(False)
    htir = inverter.GetInterval()

    # return result as dictionary
    ret = {}
    ret["observed"] = htir.UpperLimit()
    ret["expected"] = htir.GetExpectedUpperLimit(0)
    # for k,v, in ret.items(): print yellow(k,v)
    return ret

##################################################
##################################################

if __name__=="__main__":
    ### test
    from setup import *

    nBkg = 9.600536804679507
    nObs = 6.0
    # np1  = 0.4589671548155376
    # np2  = 2.1099962524760207
    # np3  = 0.1805705577448986
    np1  = 1
    np2  = 1
    np3  = 1
    limitNSig1=nSigLimit(nBkg, nObs,i=-10,npWidth1=np1,npWidth2=np2,npWidth3=np3)
    limitNSig2=nSigLimit(nBkg, nObs,i=0,npWidth1=np1,npWidth2=np2,npWidth3=np3)
    print ("limitNSig1",limitNSig1)
    print ("limitNSig2",limitNSig2)

    # limitNSig=nSigLimit(nBkg, nObs,npWidth1=np1,npWidth2=np2,npWidth3=np3)
    # limitNSigInv=nSigInvLimit(nBkg, nObs,npWidth1=np1,npWidth2=np2,npWidth3=np3)
    # print "limitNSig",limitNSig
    # print "limitNSigInv",limitNSigInv
