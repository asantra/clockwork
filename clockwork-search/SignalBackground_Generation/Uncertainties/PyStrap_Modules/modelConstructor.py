from __future__ import division

try:
    import plotter
    hasPlotting=True
except:
    hasPlotting=False

# from modules import *
import modules

################################################################################
# This implements the morphing signal model
################################################################################

def scaleNorm(hist,norm):
    """ Convert histogram to RooDataHist, return RDH """
    from ROOT import TH1D
    hist.Scale(norm)
    # for i in range(0,hist.GetNbinsX()+1):
    #     v = hist.GetBinContent(i)
    #     v*=norm
    #     hist.SetBinContent(i,v)
    return hist

def scaleAxis(hist):
    """ Convert histogram to RooDataHist, return RDH """
    from ROOT import TH1D
    name=hist.GetName()
    title=hist.GetTitle()
    # rescale
    factor = 1/1000
    minimum = hist.GetBinLowEdge(1)*factor
    maximum = hist.GetBinLowEdge(hist.GetNbinsX()+1)*factor
    rescaled = TH1D(name,title,hist.GetNbinsX(),minimum,maximum)
    for i in range(0,hist.GetNbinsX()+1):
        c = hist.GetBinCenter(i)
        v = hist.GetBinContent(i)
        rescaled.Fill(c*factor, v)
    return rescaled

def noNeg(hist):
    for i in range(1,hist.GetNbinsX()-1):
        c = hist.GetBinContent(i)
        if c<0:
            hist.SetBinContent(i,1)
    return hist


from ROOT import RooRealVar, TFile, TH1D, RooGaussian
from ROOT import gROOT
gROOT.LoadMacro("PyStrap_Modules/source/morphedSignal.h")
from ROOT import HMorph, HMorphNorm, HInt


modelConstructorFiles = {}
modelConstructorVars = []
modelConstructorPdfs = []
modelConstructorHists = {}
modelConstructorHasRun = False
def reset():
    global modelConstructorFiles 
    global modelConstructorVars 
    global modelConstructorPdfs 
    global modelConstructorHists 
    global modelConstructorHasRun 
    modelConstructorFiles = {}
    modelConstructorVars = []
    modelConstructorPdfs = []
    modelConstructorHists = {}
    modelConstructorHasRun = False

def morphedSignal(interference,chirality,w=None,channel="ee",pathPrefix="",lumi=140,rebin=1):
    """ This function returns an instance of the "morph" class, a custom morphing
        PDF implementation that morphs between TH1's
    """
    global modelConstructorFiles, modelConstructorHasRun, modelConstructorPdfs, modelConstructorVars

    ###################################################
    ## load intepolation module:
    #if not modelConstructorHasRun or 1:
    #    from ROOT import RooRealVar, TFile, TH1D, RooGaussian
    #    from ROOT import gROOT
    #    gROOT.LoadMacro("../source/class.h")
    #    from ROOT import HMorph, HMorphNorm
    ###################################################

    signalPath         = pathPrefix+"../../SharedInputsProj/rel21_ci/templates_r21_{0}_august/CI_Template_{1}.root".format(channel,chirality)
    # load masses
    signalMasses       = np.arange(12,100,2)
    # signalMasses       = np.arange(12,100,10)
    # signalMasses       = [38,40]
    signalNames        = []
    signalHistNames    = []
    for signalMass in signalMasses:
        # interference="const"
        signalName     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
        # signalHistName = "diff_CI_{0}_TeV".format(signalName)
        signalHistName = "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,signalMass)
        signalNames.append(signalName)
        signalHistNames.append(signalHistName)

    # make workspace
    # l = RooRealVar("lambda","lambda",11,10,100)

    # lambdaInv = RooRealVar("lambdaInv","lambdaInv",-30,-100,0)
    # slope     = RooRealVar("slope","slope",-1)
    # offset    = RooRealVar("offset","offset",0)
    # l = RooLinearVar("lambda","lambda",lambdaInv,slope,offset)
    # modelConstructorVars.append(lambdaInv)
    # modelConstructorVars.append(slope)
    # modelConstructorVars.append(offset)

    # x = RooRealVar("x","x",200,150,6000)
    # l = RooRealVar("lambda","lambda",30,10,100)


    # modelConstructorVars.append(x)
    # modelConstructorVars.append(l)

    # load hist
    morphPdf = HMorph("morph","morph",w.var("x"),w.obj("lambda"))
    morphPdfNorm = HMorphNorm("morphNorm","morphNorm",morphPdf,w.var("x"),w.obj("lambda"))


    if signalPath not in modelConstructorFiles.keys():
        f = TFile.Open(signalPath)
        modelConstructorFiles[signalPath]=f

    for name in signalHistNames:
        # print "ADDING HIST",name
        if name not in modelConstructorHists.keys():
            hist = modelConstructorFiles[signalPath].Get(name)
            if not hist: raise BaseException("Did not find hist {0} in file {1}".format(name,signalPath))
            # hist = scaleAxis(hist)
            hist.Scale(lumi/80.5)
            # remove some low stat flucs which screw up fits
            [hist.SetBinContent(i,0) for i in range(300)]
            # hist.Rebin(rebin)
            # hist.Rebin(0)
            # print hist.GetBinWidth(10); quit()
            # hist = noNeg(hist)
            # hist = scaleNorm(hist,lumi/80.5)
            # hist.Draw("hist"); raw_input(); quit()
            modelConstructorHists[name] = hist
            # hist.Print()
        else:
            hist = modelConstructorHists[name]
        # hist.Rebin(100)
        # print signalPath
        # print yellow(name)
        # print yellow(hist)
        # print yellow(hist.GetBinCenter(0))
        # print yellow("RMS",hist.GetRMS())
        # scale hist
        # build morph
        signalMass = int(name.split("_")[4])
        morphPdf.add(hist,signalMass)
    # print ""

    # add a zero histogram as a test at very large lambda
    # hist = hist.Clone("newZeroHist")
    # [hist.SetBinContent(i,0) for i in range(hist.GetNbinsX()+1)]
    # morphPdf.add(hist,100)


    # print green(morphPdfNorm.getVal())
    # print morphPdfNorm.Print(); 
    # print green("DONE")
    # quit()

    # morphPdf = RooGaussian("morph","morph",x,l,l)
    modelConstructorHasRun = True
    modelConstructorPdfs.append([morphPdf,morphPdfNorm])
    sys.stdout.flush()
    return modelConstructorPdfs[-1][0],modelConstructorPdfs[-1][1]

def morphedIntegral(interference,chirality,invert=1,srMin=0,srMax=-1,w=None,channel="ee",pathPrefix="",lumi=140,rebin=1):
    """ This function sets up and returns a RooRealVar 
        representing the normalized integral of the CI
        in the SR
    """
    global modelConstructorFiles, modelConstructorHasRun, modelConstructorPdfs, modelConstructorVars


    signalPath         = pathPrefix+"../../SharedInputsProj/rel21_ci/templates_r21_{0}_august/CI_Template_{1}.root".format(channel,chirality)
    # load masses
    signalMasses       = np.arange(12,101,2)
    signalNames        = []
    signalHistNames    = []
    for signalMass in signalMasses:
        signalName     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
        signalHistName = "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,signalMass)
        signalNames.append(signalName)
        signalHistNames.append(signalHistName)

    # load hist
    hint = HInt("morph","morph",w.obj("lambdaInv"),invert,float(srMin),float(srMax))

    if signalPath not in modelConstructorFiles.keys():
        f = TFile.Open(signalPath)
        modelConstructorFiles[signalPath]=f

    for iName,name in enumerate(signalHistNames):
        # print "ADDING HIST",name
        if name not in modelConstructorHists.keys():
            hist = modelConstructorFiles[signalPath].Get(name)
            if not hist: raise BaseException("Did not find hist {0} in file {1}".format(name,signalPath))
            hist.Scale(lumi/80.5)
            [hist.SetBinContent(i,0) for i in range(300)]
            modelConstructorHists[name] = hist
        else:
            hist = modelConstructorHists[name]
        signalMass = int(name.split("_")[4])
        # set final hist to zero
        if iName==len(signalHistNames)-1:
            [hist.SetBinContent(i,0) for i in range(hist.GetNbinsX()+2)] 
            # print "setting to zero:",iName,name
        # print "adding"
        # add histogram
        hint.add(hist,signalMass)
    # print ""
    # print "TEST"; quit()

    return hint
