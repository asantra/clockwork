############
#   SBLD   #
############

### Explanations ###
# - Calculate the signal associated to the linear dilaton

#Import standard
import sys
sys.path = ["PyStrap_Modules"] + sys.path
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
import dictionary




############################################################################################################
############################################################################################################
#Import pickle of backgrounds with uncertainties to get background variants and hist functions to generate 
#all backgorund variants from the pickle file
############################################################################################################
import copy, glob
import plotter
import pickle
import hist 

# for key in histDict:
#     print (key)
#     for value in histDict[key]:
#         print (value)

#Input directory 
indir = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/SignalBackground_Generation/Inputs_ee"

toyCount = 0
def throwToy(channel,mode="thy"):
    global toyCount
    """ Throws two symmetric toys
    """
    # print yellow("Throwing toy {0} with {1} variations".format(toyCount,len(variations[channel].keys())))
    nuisParam = {}
    # nominal shape
    ### get the Nominal background model from our background fit shape
    # fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
    # fBkg = fbkgfinal.Get("bkgfit")
    # xmax = 6000
    # xmin = 130
    # N = int((xmax-xmin)/10) ## 1 GeV bins
    # htemp = ROOT.TH1F("htemp_ee","",N,xmin,xmax)
    # htemp.SetLineColor(ROOT.kBlack)
    # htemp.SetMarkerColor(ROOT.kBlack)
    # htemp.SetMarkerStyle(20)
    # htemp.SetMinimum(1e-2)
    # fit = fBkg.Clone("fee")
    # nom = htemp.Clone("hfit_ee")
    # nom.Reset()
    # nomList = []
    # for i in range(1,nom.GetNbinsX()+1):
    #     x = nom.GetBinCenter(i)
    #     if(x<fit.GetXmin()): continue
    #     y = fit.Eval(x)
    #     nomList1.append(y)
    #     nomList2.append(y)
    #     nom.SetBinContent(i,y)

    nom = variations[channel]["nominal"]
    # print (nom)
    # additional shape
    add1 = copy.deepcopy(variations[channel]["nominal"])
    add1.zero()
    add2 = copy.deepcopy(variations[channel]["nominal"])
    add2.zero()
    # for channel in histDict:
#     for uncert in histDict[channel]:
#         if "nominal" in uncert: continue
#         elif "backgroundTemplate" in uncert:
    for iName,name in enumerate(variations[channel].keys()):
        print (iName,name)
        if name=="nominal": continue
        # if name=="topVar__1up": continue

        if "__1down" in name: continue
        # if "__1up" in name: continue

        if mode=="thy" and name in dictionary.bkgDetNames[channel]:
            continue
        if mode=="exp" and name not in dictionary.bkgDetNames[channel]:
            continue

    

        nuis = np.random.normal()
        while abs(nuis)>1:nuis = np.random.normal()
        # print yellow(nuis), 

        # # reproduce single variations
        # if toyCount%len(variations[channel].keys())==iName:
        #     print red("Toy for",name)
        #     nuis = 1
        # else: continue


        # nuis = 1/4*nuis
        nuisParam[name] = nuis
        var = variations[channel][name]


        # these systematics APPEAR to be onesidded
        # treat them as such
        forceOneSided = [
            "m_uu_pstOR_MUON_SAGITTA_RESBIAS__1up",

            "m_uu_pstOR_MUON_EFF_BADMUON_STAT__1up",
            "m_uu_pstOR_MUON_EFF_TTVA_STAT__1up",
            "m_uu_pstOR_MUON_EFF_TTVA_SYS__1up",
            "m_uu_pstOR_MUON_ID__1up",
            "m_uu_pstOR_MUON_SAGITTA_RHO__1up",
            "m_uu_pstOR_MUON_SCALE__1up",
            "m_ee_pstOR_EG_RESOLUTION_ALL__1up",
            "m_ee_pstOR_PRW_DATASF__1up",

            "backgroundTemplate_KF_PDF_EW__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_PDF__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_PI__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_ALPHAS__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_SCALE_Z__1up", # needed, otherwise asymmetric behave
        ]
        twoSidedByList = name not in forceOneSided
        twoSidedByName = "__1up" in name
        twoSided = twoSidedByList and twoSidedByName

        # if name not in [
        #     "backgroundTemplate_KF_PDF_EV7",
        #     "backgroundTemplate_KF_PDF_EV6",
        #     "backgroundTemplate_KF_PDF_EV5",
        #     "backgroundTemplate_KF_PDF_EV4",
        #     "backgroundTemplate_KF_PDF_EV3",
        #     "backgroundTemplate_KF_PDF_EV2",
        #     "backgroundTemplate_KF_PDF_EV1",
        #     "backgroundTemplate_KF_CHOICE_HERAPDF20",
        #     "backgroundTemplate_KF_CHOICE_NNPDF30",
        #     "backgroundTemplate_KF_REDCHOICE_NNPDF30",
        #     "backgroundTemplate_KF_PDF__1up",
        #     "backgroundTemplate_KF_PI__1up",
        #     "backgroundTemplate_KF_ALPHAS__1up",
        #     "backgroundTemplate_KF_SCALE_Z__1up",
        #     "m_uu_bmv__1up",                             # new bmv uncert
        #     "m_uu_pstOR_MUON_EFF_RECO_SYS__1up",
        #     "m_uu_pstOR_MUON_EFF_RECO_STAT__1up",
        #     "m_uu_pstOR_MUON_SAGITTA_RESBIAS__1up",
        #     "m_uu_pstOR_MUON_EFF_TrigStatUncertainty__1up",
        #     "m_uu_pstOR_MUON_EFF_TrigSystUncertainty__1up",
        #     "m_uu_pstOR_MUON_EFF_TTVA_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_TTVA_SYS__1up",
        #     "m_uu_pstOR_MUON_SAGITTA_RHO__1up",
        #     "m_uu_pstOR_MUON_EFF_BADMUON_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_ISO_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_ISO_SYS__1up",
        #     "m_uu_pstOR_MUON_ID__1up",
        #     "m_uu_pstOR_MUON_MS__1up",
        #     "m_uu_pstOR_MUON_SCALE__1up",
        #     "m_uu_pstOR_PRW_DATASF__1up",
        # ]: continue

        # var.y = [1 for x in var.x]
        # if "smear" not in name.lower():
            # continue
        if "smear" in name.lower():
            var.y = [(x/10000)*.20 for x in var.x]
            # print name
            # continue
        # if "RESBIAS" in name: continue


        if twoSided:
        # if 1 and "__1up" in name and name not in forceOneSided: # use down/up uncert
        # if 1 and "__1up" in name and "RESBIAS" not in name: # use down/up uncert
            alt = variations[channel][name.replace("__1up","__1down")]
            if nuis>=0:
                add1.y+=nom.y*var.y*abs(nuis)
                add2.y+=nom.y*alt.y*abs(nuis)
            else:
                add2.y+=nom.y*var.y*abs(nuis)
                add1.y+=nom.y*alt.y*abs(nuis)
        else: # use symmetrised uncert
            if 0 and "smear" in name.lower():
                add1.y+=nom.y*var.y*abs(nuis)
                add2.y+=nom.y*var.y*abs(nuis)
            else:
                add1.y+=nom.y*var.y*nuis
                add2.y-=nom.y*var.y*nuis
    # plot nominal and addition
    # nom.y+=add1.y
    if plotOn:
        path = "plots/toy-{0}-{1}-{1}.png".format(channel,toyCount,mode)
        # plotter.plotToys(nom,add1,add2,channel=channel,path=path)
    # add variation with nominal shape
    add1.y+=nom.y
    add2.y+=nom.y
    # assert not (add1.y<0).any() and not (add2.y<0).any()
    add1.y[add1.y<0]=0
    add2.y[add2.y<0]=0
    # normalize to nominal
    add1.y*=sum(nom.y)/sum(add1.y)
    add2.y*=sum(nom.y)/sum(add2.y)
    # set names
    add1.name = "{0}_toy_{1}".format(channel,toyCount)
    add2.name = "{0}_toy_{1}".format(channel,toyCount+1)
    toyCount+=2
    return add1,add2


#Unzip pickle
import zipfile
with zipfile.ZipFile('PyStrap_Modules/relUncerts-2.pickle.zip', 'r') as zip_ref:
    zip_ref.extractall('PyStrap_Modules/')


#Load pickle in python3 format using latin1 encoding
fname = 'PyStrap_Modules/relUncerts-2.pickle'
with open(fname, 'rb') as f:
    histDict = pickle.load(f, encoding='latin1')

# iPath = "pickles/relUncerts.pickle"
iPath = "PyStrap_Modules/relUncerts-2.pickle"
# oPath = "pickles/rootOut.root"
oPath = "rootOut.root"
##########print (yellow("Loading variations..."))
# variations = pickle.load(open(iPath))
#Load pickle in python3 format using latin1 encoding
fname = 'PyStrap_Modules/relUncerts-2.pickle'
with open(fname, 'rb') as f:
    variations = pickle.load(f, encoding='latin1')

plotOn = 0

try:
    channel = sys.argv[1]
    nToys = int(sys.argv[2])
    mode = sys.argv[3]
    oPath = sys.argv[4]
    print (yellow("Toys: CLI mode"))
    print (channel)
    print (nToys)
    print (oPath)
except:
    #######print (red("Toys: Manual mode"))
    channel="ee"
    # channel="mm"
    mode = "all"
    nToys=5000
    oPath = "toys_allUncertainties.root"
    plotOn = 1
    os.popen("rm "+oPath)

#####if plotOn: os.popen("rm plots/*")
oFile = ROOT.TFile.Open(oPath,"RECREATE")

for i in range(nToys):
    print ("Throwing toy {0} of {1}".format(i,nToys))
    toy1,toy2 = throwToy(channel,mode=mode)
    # save toys
    r1 = toy1.th1()
    r2 = toy2.th1()
    r1.Write()
    r2.Write()

oFile.Write()
oFile.Close()












# sysDict = {"theory": {}, "experimental": {}}
# sysDict["theory"]["ee"] = {}
# sysDict["theory"]["mm"] = {}
# sysDict["experimental"]["ee"] = {}
# sysDict["experimental"]["mm"] = {}
# for channel in histDict:
#     for uncert in histDict[channel]:
#         if "nominal" in uncert: continue
#         elif "backgroundTemplate" in uncert:
#             sysDict["theory"][channel][uncert] = histDict[channel][uncert]
#             # sysDict["theory"][value] = 
#         elif not "backgroundTemplate" in uncert:
#             sysDict["experimental"][channel][uncert] = histDict[channel][uncert]
#             sysDict["experimental"][value] = histDict[key][value]
# hnominal = histDict["ee"]["nominal"]
# # print (hnominal.y)

# h = histDict["ee"]["m_ee_pstOR_EG_RESOLUTION_ALL__1up"]
# print (h.y)
# print((h.y*hnominal.y)+hnominal.y)
# # print (hnominal.y) 
# #(sys - nominal)/nominal

# nominal = histDict["ee"]["nominal"]
# nominal.smooth()
# listB_nominal =[]
# listB_nominal.append(nominal.y)
# print (listB_nominal)

############################################################################################################


# ### get the Nominal background model
# fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
# fBkg = fbkgfinal.Get("bkgfit")



# #Get background with no random fluctuations
# def Bfix(xmin, xmax):



#   fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
#   fBkg = fbkgfinal.Get("bkgfit")
#   # xmin = fBkg.GetXmin()
#   # xmax = fBkg.GetXmax()
#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
#   listB = []
#   for i in range(1,hfit.GetNbinsX()+1):
#      x = hfit.GetBinCenter(i)
#      if(x<fit.GetXmin()): continue
#      y = fit.Eval(x)
#      listB.append(y)
#      hfit.SetBinContent(i,y)
#   print (listB)
#   return listB

# xmin = 2300
# xmax = 27000

# fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Wavelets/Systs.root","RECREATE")
# fOut.cd()


# hEG_RESOLUTION_ALL__1up = histDict["ee"]["m_ee_pstOR_EG_RESOLUTION_ALL__1up"]
# hEG_RESOLUTION_ALL__1up.smooth()
# print (hEG_RESOLUTION_ALL__1up.x)
# f = hEG_RESOLUTION_ALL__1up.th1()
# print (f)
# f.Write()

# hnominal = histDict["ee"]["nominal"]
# # hnominal.smooth()
# n = hnominal.th1()
# print (n)
# n.Write()

# # newnominal = ROOT.TH1D("newnominal","newnominal", len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]), min(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]), max(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]))
# # newuncertainty = ROOT.TH1D("newuncertainty","newuncertainty", len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]), min(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]), max(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]))
# newnominal = ROOT.TH1D("newnominal","newnominal", len(hnominal.x), min(hnominal.x), max(hnominal.x))
# newuncertainty = ROOT.TH1D("newuncertainty","newuncertainty", len(hnominal.x), min(hnominal.x), max(hnominal.x))

# skipbin = 0

# # newnominal.FillN(len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]),hnominal.y[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)],np.ones(len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)])))
# # newuncertainty.FillN(len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)]),hEG_RESOLUTION_ALL__1up.y[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)],np.ones(len(hnominal.x[np.logical_and(hnominal.x>xmin,hnominal.x<xmax)])))
# for i,b in enumerate(hnominal.x):
#   # if b<=xmin: 
#   #   skipbin += 1 
#   #   continue
#   # if b>=xmax: 
#   #   skipbin += 1 
#   #   continue
#   # print (i-skipbin)
#   # quit()
#   newnominal.Fill(i-skipbin,hnominal.y[i])
#   newy = (hEG_RESOLUTION_ALL__1up.y[i]*hnominal.y[i]) + hnominal.y[i]
#   newuncertainty.Fill(i-skipbin,newy)

# newnominal.Print()
# newuncertainty.Print()
# newnominal.Write()
# newuncertainty.Write()

# # print (hEG_RESOLUTION_ALL__1up.x)
# # print (hnominal.x)
# # fOut.Write()
# fOut.Close()
# #Delete pickle
os.remove("PyStrap_Modules/relUncerts-2.pickle")
