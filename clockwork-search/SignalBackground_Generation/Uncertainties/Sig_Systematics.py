############
#   SBLD   #
############

### Explanations ###
# - Calculate the signal associated to a generic clockwork graviton model with dampling and Genrat

#Import standard
import sys
sys.path = ["PyStrap_Modules"] + sys.path
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array

# import rootpy
# from rootpy.plotting import Hist

import root_numpy as rn

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes

#import ATLAS MPL Style
import mplhep as hep
# plt.style.use(hep.style.ATLAS)
plt.style.use(hep.style.ROOT)

import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  


############################################################################################################

#Input directory 
indir = "../Inputs/"

############################################################


### get the nominal acceptance*efficiency
fPeterAcc = ROOT.TFile(indir+"ee_accEffFunction.root","READ")
fAcc = fPeterAcc.Get("accEff_TF1")
sfAcc = "[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)"
for n in range(0,fAcc.GetNpar()):
   print (fAcc.GetParName(n)+": "+str(fAcc.GetParameter(n)))
   sfAcc = str(sfAcc.replace("["+str(n)+"]",str(fAcc.GetParameter(n))))
fAcc.SetLineColor(ROOT.kBlack)

### get the Nominal background model
fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
fBkg = fbkgfinal.Get("bkgfit")


Nbins = 5870 #2470
xmin = 130 #230
xmax = 6000 #2700

#Define mass binning
massList = np.linspace(xmin, xmax, Nbins)

### get the UP/DOWN variation acceptance*efficiency, Ae x (1 + function)

#ee_IDefficiency systematic uncertainties function
sfAcc_ee_IDefficiency = ROOT.TF1("sfAcc_ee_IDefficiency","[0]+[1]*TMath::Exp([2]*(x))+[3]*log(x)+[4]*TMath::TanH([5]*(x-[6]))")
sfAcc_ee_IDefficiency.SetParameter(0,0.200345)
sfAcc_ee_IDefficiency.SetParameter(1,-0.219756)
sfAcc_ee_IDefficiency.SetParameter(2,-0.000110541)
sfAcc_ee_IDefficiency.SetParameter(3,0.00922559)
sfAcc_ee_IDefficiency.SetParameter(4,0.0169377)
sfAcc_ee_IDefficiency.SetParameter(5,0.00632574)
sfAcc_ee_IDefficiency.SetParameter(6,530.715)

#sfAcc_ee_Iso_efficiency systematic uncertainties function
sfAcc_ee_Iso_efficiency = ROOT.TF1("sfAcc_ee_Iso_efficiency","[0]+[1]*TMath::TanH([2]*(x-[3]))+[4]*TMath::TanH([5]*(x-[6]))+[7]*log(x)")
sfAcc_ee_Iso_efficiency.SetParameter(0,0.00897086)
sfAcc_ee_Iso_efficiency.SetParameter(1,0.018128)
sfAcc_ee_Iso_efficiency.SetParameter(2,-0.000801275)
sfAcc_ee_Iso_efficiency.SetParameter(3,-262.667)
sfAcc_ee_Iso_efficiency.SetParameter(4,0.0130715)
sfAcc_ee_Iso_efficiency.SetParameter(5,0.00145374)
sfAcc_ee_Iso_efficiency.SetParameter(6,453.596)
sfAcc_ee_Iso_efficiency.SetParameter(7,.000817472)

#Generate example of signal model using full Resonant TF shape instead of Guassian
#First define the individual components

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)")
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)
mu_CB_test = rn.evaluate(mu_CB,massList)


sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)
sigma_CB_test = rn.evaluate(sigma_CB,massList)

alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin, xmax)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)
n_CB_test = rn.evaluate(n_CB,massList)

#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)")
# mu_G = ROOT.TF1("mu_G","[0]+[1]*(x^-1) + [2]*x+[3]*log(x^3)+[4]*((x^2)^-1)+[5]*(x^2)", xmin, xmax)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)
mu_G_test = rn.evaluate(mu_G,massList)

sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)
sigma_G_test = rn.evaluate(sigma_G,massList)

#K-Parameters

k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin, xmax)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)
k_param_test = rn.evaluate(k_param,massList)

########################################################################
########################################################################
########################################################################

#Up-variation Crystal Ball Parameters

#energy scale
mu_CB_UPvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)")
mu_CB_UPvar.SetParameter(0, 0.143853)
mu_CB_UPvar.SetParameter(1, -0.544555)
mu_CB_UPvar.SetParameter(2, -0.00897897)
mu_CB_UPvar.SetParameter(3, 0.000000211052)
mu_CB_UPvartest = rn.evaluate(mu_CB_UPvar,massList)

#energy resolution
sigma_CB_UPvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_UPvar.SetParameter(0, 0.0168464)
sigma_CB_UPvar.SetParameter(1, 0.244998)
sigma_CB_UPvar.SetParameter(2, 0.607754)
sigma_CB_UPvartest = rn.evaluate(sigma_CB,massList)

#Up-variation Gaussian

#energy scale
mu_G_UPvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)")
mu_G_UPvar.SetParameter(0, 0.00437644)
mu_G_UPvar.SetParameter(1, -0.693393)
mu_G_UPvar.SetParameter(2, -0.00000124588)
mu_G_UPvar.SetParameter(3, 0.00000994365)
mu_G_UPvar.SetParameter(4, 3.85433)
mu_G_UPvar.SetParameter(5, 0.0000000000681002)
mu_G_UPvartest = rn.evaluate(mu_G_UPvar,massList)

#energy resolution
sigma_G_UPvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_UPvar.SetParameter(0, 0.00968273)
sigma_G_UPvar.SetParameter(1, 0.110266)
sigma_G_UPvar.SetParameter(2, 1.0617)
sigma_G_UPvartest = rn.evaluate(sigma_G_UPvar,massList)

########################################################################

#Down-variation Crystal Ball Parameters

#energy scale
mu_CB_DOWNvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)")
mu_CB_DOWNvar.SetParameter(0, 0.125578)
mu_CB_DOWNvar.SetParameter(1, -0.350447)
mu_CB_DOWNvar.SetParameter(2, -0.0144995)
mu_CB_DOWNvar.SetParameter(3, 0.00000373014)
mu_CB__DOWNvartest = rn.evaluate(mu_CB_DOWNvar,massList)

#energy resolution
sigma_CB_DOWNvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_DOWNvar.SetParameter(0, 0.011107)
sigma_CB_DOWNvar.SetParameter(1, 0.22346)
sigma_CB_DOWNvar.SetParameter(2, 1.92675)
sigma_CB_DOWNvartest = rn.evaluate(sigma_CB_DOWNvar,massList)

#DOWN-variation Gaussian

#energy scale
mu_G_DOWNvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)")
mu_G_DOWNvar.SetParameter(0, -0.00743235)
mu_G_DOWNvar.SetParameter(1, 0.650265)
mu_G_DOWNvar.SetParameter(2, 0.000000574646)
mu_G_DOWNvar.SetParameter(3, -0.00000498561)
mu_G_DOWNvar.SetParameter(4, 28.7656)
mu_G_DOWNvar.SetParameter(5, -0.0000000000452119)
mu_G_DOWNvartest = rn.evaluate(mu_G_DOWNvar,massList)

#energy resolution
sigma_G_DOWNvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_DOWNvar.SetParameter(0, 0.00337693)
sigma_G_DOWNvar.SetParameter(1, 0.112002)
sigma_G_DOWNvar.SetParameter(2, 1.2219)
sigma_G_DOWNvartest = rn.evaluate(sigma_G_DOWNvar,massList)

########################################################################
########################################################################
########################################################################


#Plot all the response function varables to comapre to the Resonant Internal Note

fig_mu_CB, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, mu_CB_test, 'green', lw=2, label=r"mu_CB")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('mu_CB', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
# plt.ylim(bottom=-0.012)
plt.xlim(right=6000)
fig_mu_CB.savefig('plots_MassDistribution/mu_CB_plot.pdf')


fig_sigma_CB, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, sigma_CB_test, 'green', lw=2, label=r"sigma_CB")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('sigma_CB', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
plt.ylim(bottom=0)
plt.xlim(right=6000)
fig_sigma_CB.savefig('plots_MassDistribution/sigma_CB_plot.pdf')

fig_n_CB, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, n_CB_test, 'green', lw=2, label=r"n_CB")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('n_CB_test', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
plt.ylim(bottom=0)
plt.xlim(right=6000)
fig_n_CB.savefig('plots_MassDistribution/n_CB_plot.pdf')

fig_mu_G, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, mu_G_test, 'green', lw=2, label=r"mu_G")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('mu_G', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
plt.ylim(bottom=-0.004)
plt.xlim(right=6000)
fig_mu_G.savefig('plots_MassDistribution/mu_G_plot.pdf')

fig_sigma_G, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, sigma_G_test, 'green', lw=2, label=r"sigma_G")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('sigma_G', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
# plt.ylim(bottom=1e-1)
plt.xlim(right=6000)
fig_sigma_G.savefig('plots_MassDistribution/sigma_G_plot.pdf')

fig_k_param, ax = plt.subplots()
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
ax.plot(massList, k_param_test, 'green', lw=2, label=r"k_param")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
ax.set_ylabel('k_param', ha='right', y=1.0)
leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
plt.ylim(bottom=0)
plt.xlim(right=6000)
fig_k_param.savefig('plots_MassDistribution/k_param_plot.pdf')


#Generate signal with no random fluctuations
def SigFixed_Ae_Iso_efficiency_UP(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if(M>M5): break
     sM = str(M)
     sW = str(M*(1+sfAcc_ee_Iso_efficiency.Eval(M))*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS

#Generate signal with no random fluctuations
def SigFixed_Ae_ID_efficiency_UP(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if(M>M5): break
     sM = str(M)
     sW = str(M*(1+sfAcc_ee_IDefficiency.Eval(M))*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS

def SigFixed_Ae_Iso_efficiency_DOWN(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if(M>M5): break
     sM = str(M)
     sW = str(M*(1-sfAcc_ee_Iso_efficiency.Eval(M))*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS

#Generate signal with no random fluctuations
def SigFixed_Ae_ID_efficiency_DOWN(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if(M>M5): break
     sM = str(M)
     sW = str(M*(1-sfAcc_ee_IDefficiency.Eval(M))*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS

#Generate signal with no random fluctuations
def SigFixed_withCB(k, M5, xmin, xmax):

### get the resolution with nominal parameters

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+((n*n)/(float(kR)*float(kR))))
     print (f"M={M}")
     if(M>M5): break
     #Gaussain
     mu_Gauss = str(mu_G.Eval(M)*M+M)
     print (f"n={n},mu_Gauss:{mu_Gauss}")
     sigma_Guass = str(sigma_G.Eval(M)*M)
     print (f"n={n},sigma_Guass:{sigma_Guass}")
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
     print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
     mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
     print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
     # sigma_CrystalBall = str(sigma_CB.Eval(M))
     # mu_CrystalBall = str(mu_CB.Eval(M))
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n==1:{sResonances}")
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n>1:{sResonances}")

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  # print (f"Tower Formula:{sResonances}")
  # print ("========================")
  print (f"Final Formula:{fClockwork}")
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  return listS

#Generate signal with no random fluctuations
def SigFixed_withCB_UPvariation(k, M5, xmin, xmax):

### get the resolution with nominal parameters

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+((n*n)/(float(kR)*float(kR))))
     print (f"M={M}")
     if(M>M5): break
     #Gaussain
     mu_Gauss = str(mu_G_UPvar.Eval(M)*M+M)
     # print (f"n={n},mu_Gauss:{mu_Gauss}")
     sigma_Guass = str(sigma_G_UPvar.Eval(M)*M)
     # print (f"n={n},sigma_Guass:{sigma_Guass}")
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB_UPvar.Eval(M)*M)
     # print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
     mu_CrystalBall = str(mu_CB_UPvar.Eval(M)*M+M)
     # print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
     # sigma_CrystalBall = str(sigma_CB.Eval(M))
     # mu_CrystalBall = str(mu_CB.Eval(M))
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n==1:{sResonances}")
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n>1:{sResonances}")

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  # print (f"Tower Formula:{sResonances}")
  # print ("========================")
  print (f"Final Formula:{fClockwork}")
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  return listS

#Generate signal with no random fluctuations
def SigFixed_withCB_DOWNvariation(k, M5, xmin, xmax):

### get the resolution with nominal parameters

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+((n*n)/(float(kR)*float(kR))))
     print (f"M={M}")
     if(M>M5): break
     #Gaussain
     mu_Gauss = str(mu_G_DOWNvar.Eval(M)*M+M)
     # print (f"n={n},mu_Gauss:{mu_Gauss}")
     sigma_Guass = str(sigma_G_DOWNvar.Eval(M)*M)
     # print (f"n={n},sigma_Guass:{sigma_Guass}")
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB_DOWNvar.Eval(M)*M)
     # print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
     mu_CrystalBall = str(mu_CB_DOWNvar.Eval(M)*M+M)
     # print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
     # sigma_CrystalBall = str(sigma_CB.Eval(M))
     # mu_CrystalBall = str(mu_CB.Eval(M))
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n==1:{sResonances}")
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      print (f"Tower Formula n>1:{sResonances}")

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  # print (f"Tower Formula:{sResonances}")
  # print ("========================")
  print (f"Final Formula:{fClockwork}")
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  return listS


#Generate signal with no random fluctuations
def SigFixed(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(6.1/5)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS



def SigFixed_Resolution_SystematicUP(k, M5, xmin, xmax):

  ### get the resolution with parameters shifted from resolution UP uncertainty 
  fPeterRes_SystUP = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes_SystUP = fPeterRes_SystUP.Get("relResol_dedicatedTF")
  sfRes_SystUP = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  fRes_SystUP.SetParameter(5, 0.0168464)
  fRes_SystUP.SetParameter(6, 0.244998)
  fRes_SystUP.SetParameter(7, 0.607754)
  fRes_SystUP.SetParameter(8, 0.00968273)
  fRes_SystUP.SetParameter(9, 0.110266)
  fRes_SystUP.SetParameter(10, 1.0617)
  for n in range(0,fRes_SystUP.GetNpar()):
     print (fRes_SystUP.GetParName(n)+": "+str(fRes_SystUP.GetParameter(n)))
     sfRes_SystUP = sfRes_SystUP.replace("["+str(n)+"]",str(fRes_SystUP.GetParameter(n)))
  fRes_SystUP.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes_SystUP.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS

def SigFixed_Resolution_SystematicDOWN(k, M5, xmin, xmax):

  ### get the resolution with parameters shifted from resolution DOWN uncertainty 
  fPeterRes_SystDOWN = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes_SystDOWN = fPeterRes_SystDOWN.Get("relResol_dedicatedTF")
  sfRes_SystDOWN = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  fRes_SystDOWN.SetParameter(5, 0.011107)
  fRes_SystDOWN.SetParameter(6, 0.22346)
  fRes_SystDOWN.SetParameter(7, 1.92675)
  fRes_SystDOWN.SetParameter(8, 0.00337693)
  fRes_SystDOWN.SetParameter(9, 0.112002)
  fRes_SystDOWN.SetParameter(10, 1.2219)
  for n in range(0,fRes_SystDOWN.GetNpar()):
     print (fRes_SystDOWN.GetParName(n)+": "+str(fRes_SystDOWN.GetParameter(n)))
     sfRes_SystDOWN = sfRes_SystDOWN.replace("["+str(n)+"]",str(fRes_SystDOWN.GetParameter(n)))
  fRes_SystDOWN.SetLineColor(ROOT.kRed)


  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes_SystDOWN.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS


##### 


toyCount = 0
def throwToy_SigSysts(channel,mode="thy"):
    global toyCount
    """ Throws two symmetric toys
    """
    # print yellow("Throwing toy {0} with {1} variations".format(toyCount,len(variations[channel].keys())))
    nuisParam = {}
    # nominal shape
    ### get the Nominal background model from our background fit shape
    # fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
    # fBkg = fbkgfinal.Get("bkgfit")
    # xmax = 6000
    # xmin = 130
    # N = int((xmax-xmin)/10) ## 1 GeV bins
    # htemp = ROOT.TH1F("htemp_ee","",N,xmin,xmax)
    # htemp.SetLineColor(ROOT.kBlack)
    # htemp.SetMarkerColor(ROOT.kBlack)
    # htemp.SetMarkerStyle(20)
    # htemp.SetMinimum(1e-2)
    # fit = fBkg.Clone("fee")
    # nom = htemp.Clone("hfit_ee")
    # nom.Reset()
    # nomList = []
    # for i in range(1,nom.GetNbinsX()+1):
    #     x = nom.GetBinCenter(i)
    #     if(x<fit.GetXmin()): continue
    #     y = fit.Eval(x)
    #     nomList1.append(y)
    #     nomList2.append(y)
    #     nom.SetBinContent(i,y)

    nom = variations[channel]["nominal"]
    # print (nom)
    # additional shape
    add1 = copy.deepcopy(variations[channel]["nominal"])
    add1.zero()
    add2 = copy.deepcopy(variations[channel]["nominal"])
    add2.zero()
    # for channel in histDict:
#     for uncert in histDict[channel]:
#         if "nominal" in uncert: continue
#         elif "backgroundTemplate" in uncert:
    for iName,name in enumerate(variations[channel].keys()):
        print (iName,name)
        if name=="nominal": continue
        # if name=="topVar__1up": continue

        if "__1down" in name: continue
        # if "__1up" in name: continue

        if mode=="thy" and name in dictionary.bkgDetNames[channel]:
            continue
        if mode=="exp" and name not in dictionary.bkgDetNames[channel]:
            continue

    

        nuis = np.random.normal()
        while abs(nuis)>1:nuis = np.random.normal()
        # print yellow(nuis), 

        # # reproduce single variations
        # if toyCount%len(variations[channel].keys())==iName:
        #     print red("Toy for",name)
        #     nuis = 1
        # else: continue


        # nuis = 1/4*nuis
        nuisParam[name] = nuis
        var = variations[channel][name]


        # these systematics APPEAR to be onesidded
        # treat them as such
        forceOneSided = [
            "m_uu_pstOR_MUON_SAGITTA_RESBIAS__1up",

            "m_uu_pstOR_MUON_EFF_BADMUON_STAT__1up",
            "m_uu_pstOR_MUON_EFF_TTVA_STAT__1up",
            "m_uu_pstOR_MUON_EFF_TTVA_SYS__1up",
            "m_uu_pstOR_MUON_ID__1up",
            "m_uu_pstOR_MUON_SAGITTA_RHO__1up",
            "m_uu_pstOR_MUON_SCALE__1up",
            "m_ee_pstOR_EG_RESOLUTION_ALL__1up",
            "m_ee_pstOR_PRW_DATASF__1up",

            "backgroundTemplate_KF_PDF_EW__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_PDF__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_PI__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_ALPHAS__1up", # needed, otherwise asymmetric behave
            "backgroundTemplate_KF_SCALE_Z__1up", # needed, otherwise asymmetric behave
        ]
        twoSidedByList = name not in forceOneSided
        twoSidedByName = "__1up" in name
        twoSided = twoSidedByList and twoSidedByName

        # if name not in [
        #     "backgroundTemplate_KF_PDF_EV7",
        #     "backgroundTemplate_KF_PDF_EV6",
        #     "backgroundTemplate_KF_PDF_EV5",
        #     "backgroundTemplate_KF_PDF_EV4",
        #     "backgroundTemplate_KF_PDF_EV3",
        #     "backgroundTemplate_KF_PDF_EV2",
        #     "backgroundTemplate_KF_PDF_EV1",
        #     "backgroundTemplate_KF_CHOICE_HERAPDF20",
        #     "backgroundTemplate_KF_CHOICE_NNPDF30",
        #     "backgroundTemplate_KF_REDCHOICE_NNPDF30",
        #     "backgroundTemplate_KF_PDF__1up",
        #     "backgroundTemplate_KF_PI__1up",
        #     "backgroundTemplate_KF_ALPHAS__1up",
        #     "backgroundTemplate_KF_SCALE_Z__1up",
        #     "m_uu_bmv__1up",                             # new bmv uncert
        #     "m_uu_pstOR_MUON_EFF_RECO_SYS__1up",
        #     "m_uu_pstOR_MUON_EFF_RECO_STAT__1up",
        #     "m_uu_pstOR_MUON_SAGITTA_RESBIAS__1up",
        #     "m_uu_pstOR_MUON_EFF_TrigStatUncertainty__1up",
        #     "m_uu_pstOR_MUON_EFF_TrigSystUncertainty__1up",
        #     "m_uu_pstOR_MUON_EFF_TTVA_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_TTVA_SYS__1up",
        #     "m_uu_pstOR_MUON_SAGITTA_RHO__1up",
        #     "m_uu_pstOR_MUON_EFF_BADMUON_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_ISO_STAT__1up",
        #     "m_uu_pstOR_MUON_EFF_ISO_SYS__1up",
        #     "m_uu_pstOR_MUON_ID__1up",
        #     "m_uu_pstOR_MUON_MS__1up",
        #     "m_uu_pstOR_MUON_SCALE__1up",
        #     "m_uu_pstOR_PRW_DATASF__1up",
        # ]: continue

        # var.y = [1 for x in var.x]
        # if "smear" not in name.lower():
            # continue
        if "smear" in name.lower():
            var.y = [(x/10000)*.20 for x in var.x]
            # print name
            # continue
        # if "RESBIAS" in name: continue


        if twoSided:
        # if 1 and "__1up" in name and name not in forceOneSided: # use down/up uncert
        # if 1 and "__1up" in name and "RESBIAS" not in name: # use down/up uncert
            alt = variations[channel][name.replace("__1up","__1down")]
            if nuis>=0:
                add1.y+=nom.y*var.y*abs(nuis)
                add2.y+=nom.y*alt.y*abs(nuis)
            else:
                add2.y+=nom.y*var.y*abs(nuis)
                add1.y+=nom.y*alt.y*abs(nuis)
        else: # use symmetrised uncert
            if 0 and "smear" in name.lower():
                add1.y+=nom.y*var.y*abs(nuis)
                add2.y+=nom.y*var.y*abs(nuis)
            else:
                add1.y+=nom.y*var.y*nuis
                add2.y-=nom.y*var.y*nuis
    # plot nominal and addition
    # nom.y+=add1.y
    if plotOn:
        path = "plots/toy-{0}-{1}-{1}.png".format(channel,toyCount,mode)
        # plotter.plotToys(nom,add1,add2,channel=channel,path=path)
    # add variation with nominal shape
    add1.y+=nom.y
    add2.y+=nom.y
    # assert not (add1.y<0).any() and not (add2.y<0).any()
    add1.y[add1.y<0]=0
    add2.y[add2.y<0]=0
    # normalize to nominal
    add1.y*=sum(nom.y)/sum(add1.y)
    add2.y*=sum(nom.y)/sum(add2.y)
    # set names
    add1.name = "{0}_toy_{1}".format(channel,toyCount)
    add2.name = "{0}_toy_{1}".format(channel,toyCount+1)
    toyCount+=2
    return add1,add2


#Unzip pickle
import zipfile
with zipfile.ZipFile('PyStrap_Modules/relUncerts-2.pickle.zip', 'r') as zip_ref:
    zip_ref.extractall('PyStrap_Modules/')


#Load pickle in python3 format using latin1 encoding
fname = 'PyStrap_Modules/relUncerts-2.pickle'
with open(fname, 'rb') as f:
    histDict = pickle.load(f, encoding='latin1')

# iPath = "pickles/relUncerts.pickle"
iPath = "PyStrap_Modules/relUncerts-2.pickle"
# oPath = "pickles/rootOut.root"
oPath = "rootOut.root"
##########print (yellow("Loading variations..."))
# variations = pickle.load(open(iPath))
#Load pickle in python3 format using latin1 encoding
fname = 'PyStrap_Modules/relUncerts-2.pickle'
with open(fname, 'rb') as f:
    variations = pickle.load(f, encoding='latin1')

plotOn = 0

try:
    channel = sys.argv[1]
    nToys = int(sys.argv[2])
    mode = sys.argv[3]
    oPath = sys.argv[4]
    print (yellow("Toys: CLI mode"))
    print (channel)
    print (nToys)
    print (oPath)
except:
    #######print (red("Toys: Manual mode"))
    channel="ee"
    # channel="mm"
    mode = "all"
    nToys=1000
    oPath = "toys_allUncertainties_Sig.root"
    plotOn = 1
    os.popen("rm "+oPath)

#####if plotOn: os.popen("rm plots/*")
oFile = ROOT.TFile.Open(oPath,"RECREATE")

for i in range(nToys):
    print ("Throwing toy {0} of {1}".format(i,nToys))
    toy1,toy2 = throwToy(channel,mode=mode)
    # save toys
    r1 = toy1.th1()
    r2 = toy2.th1()
    r1.Write()
    r2.Write()

oFile.Write()
oFile.Close()


#####Will create sigma for each and then use nuisnace parameter

nuis = np.random.normal()
while abs(nuis)>1:nuis = np.random.normal()
