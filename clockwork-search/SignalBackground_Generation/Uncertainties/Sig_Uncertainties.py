#This script is to make nice plots of signal uncertainities

#Import standard
import sys
sys.path = ["PyStrap_Modules"] + sys.path
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array


import copy, glob
import plotter
import pickle
import hist 

# import rootpy
# from rootpy.plotting import Hist

import root_numpy as rn 
from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes


import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  

#import ATLAS MPL Style
import mplhep as hep
# plt.style.use(hep.style.ATLAS)
plt.style.use(hep.style.ROOT)

##################################################
# This script generates a pickle file with
# (var-nom)/nom histograms
# For each signal variation
##################################################


#Resolution Uncertianites

#Input directory 
indir = "/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Inputs/"

### get the Nominal background model
fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
fBkg = fbkgfinal.Get("bkgfit")

############################################################

### get the resolution with nominal parameters
fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
fRes = fPeterRes.Get("relResol_dedicatedTF")
sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
for n in range(0,fRes.GetNpar()):
	print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
	sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
fRes.SetLineColor(ROOT.kRed)

### get the resolution with parameters shifted from resolution UP uncertainty 
fPeterRes_SystUP = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
fRes_SystUP = fPeterRes_SystUP.Get("relResol_dedicatedTF")
sfRes_SystUP = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
fRes_SystUP.SetParameter(5, 0.0168464)
fRes_SystUP.SetParameter(6, 0.244998)
fRes_SystUP.SetParameter(7, 0.607754)
fRes_SystUP.SetParameter(8, 0.00968273)
fRes_SystUP.SetParameter(9, 0.110266)
fRes_SystUP.SetParameter(10, 1.0617)
for n in range(0,fRes_SystUP.GetNpar()):
	print (fRes_SystUP.GetParName(n)+": "+str(fRes_SystUP.GetParameter(n)))
	sfRes_SystUP = sfRes_SystUP.replace("["+str(n)+"]",str(fRes_SystUP.GetParameter(n)))
fRes_SystUP.SetLineColor(ROOT.kRed)


### get the resolution with parameters shifted from resolution DOWN uncertainty 
fPeterRes_SystDOWN = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
fRes_SystDOWN = fPeterRes_SystDOWN.Get("relResol_dedicatedTF")
sfRes_SystDOWN = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
fRes_SystDOWN.SetParameter(5, 0.011107)
fRes_SystDOWN.SetParameter(6, 0.22346)
fRes_SystDOWN.SetParameter(7, 1.92675)
fRes_SystDOWN.SetParameter(8, 0.00337693)
fRes_SystDOWN.SetParameter(9, 0.112002)
fRes_SystDOWN.SetParameter(10, 1.2219)
for n in range(0,fRes_SystDOWN.GetNpar()):
	print (fRes_SystDOWN.GetParName(n)+": "+str(fRes_SystDOWN.GetParameter(n)))
	sfRes_SystDOWN = sfRes_SystDOWN.replace("["+str(n)+"]",str(fRes_SystDOWN.GetParameter(n)))
fRes_SystDOWN.SetLineColor(ROOT.kRed)

#Generate signal with no random fluctuations
def SigFixed(k, M5, xmin, xmax):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins
  
  htemp_fRes = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp_fRes.SetLineColor(ROOT.kBlack)
  htemp_fRes.SetMarkerColor(ROOT.kBlack)
  htemp_fRes.SetMarkerStyle(20)
  htemp_fRes.SetMinimum(1e-2)
  fit = fRes.Clone("fee")
  hfit = htemp_fRes.Clone("hfit_ee")
  hfit.Reset()
  listfRes = []
  for i in range(1+xmin,hfit.GetNbinsX()+131):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     listfRes.append(y*100)
     hfit.SetBinContent(i,y)

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS, listfRes


def SigFixed_Resolution_SystematicUP(k, M5, xmin, xmax):

  ### get the resolution with parameters shifted from resolution UP uncertainty 
  fPeterRes_SystUP = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes_SystUP = fPeterRes_SystUP.Get("relResol_dedicatedTF")
  sfRes_SystUP = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  fRes_SystUP.SetParameter(5, 0.0168464)
  fRes_SystUP.SetParameter(6, 0.244998)
  fRes_SystUP.SetParameter(7, 0.607754)
  fRes_SystUP.SetParameter(8, 0.00968273)
  fRes_SystUP.SetParameter(9, 0.110266)
  fRes_SystUP.SetParameter(10, 1.0617)
  for n in range(0,fRes_SystUP.GetNpar()):
     print (fRes_SystUP.GetParName(n)+": "+str(fRes_SystUP.GetParameter(n)))
     sfRes_SystUP = sfRes_SystUP.replace("["+str(n)+"]",str(fRes_SystUP.GetParameter(n)))
  fRes_SystUP.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp_fRes = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp_fRes.SetLineColor(ROOT.kBlack)
  htemp_fRes.SetMarkerColor(ROOT.kBlack)
  htemp_fRes.SetMarkerStyle(20)
  htemp_fRes.SetMinimum(1e-2)
  fit = fRes_SystUP.Clone("fee")
  hfit = htemp_fRes.Clone("hfit_ee")
  hfit.Reset()
  listfRes = []
  for i in range(1+xmin,hfit.GetNbinsX()+131):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     listfRes.append(y*100)
     hfit.SetBinContent(i,y)

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes_SystUP.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS, listfRes

def SigFixed_Resolution_SystematicDOWN(k, M5, xmin, xmax):

  ### get the resolution with parameters shifted from resolution DOWN uncertainty 
  fPeterRes_SystDOWN = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes_SystDOWN = fPeterRes_SystDOWN.Get("relResol_dedicatedTF")
  sfRes_SystDOWN = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  fRes_SystDOWN.SetParameter(5, 0.011107)
  fRes_SystDOWN.SetParameter(6, 0.22346)
  fRes_SystDOWN.SetParameter(7, 1.92675)
  fRes_SystDOWN.SetParameter(8, 0.00337693)
  fRes_SystDOWN.SetParameter(9, 0.112002)
  fRes_SystDOWN.SetParameter(10, 1.2219)
  for n in range(0,fRes_SystDOWN.GetNpar()):
     print (fRes_SystDOWN.GetParName(n)+": "+str(fRes_SystDOWN.GetParameter(n)))
     sfRes_SystDOWN = sfRes_SystDOWN.replace("["+str(n)+"]",str(fRes_SystDOWN.GetParameter(n)))
  fRes_SystDOWN.SetLineColor(ROOT.kRed)


  N = int(xmax-xmin) ## 1 GeV bins

  htemp_fRes = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp_fRes.SetLineColor(ROOT.kBlack)
  htemp_fRes.SetMarkerColor(ROOT.kBlack)
  htemp_fRes.SetMarkerStyle(20)
  htemp_fRes.SetMinimum(1e-2)
  fit = fRes_SystDOWN.Clone("fee")
  hfit = htemp_fRes.Clone("hfit_ee")
  hfit.Reset()
  listfRes = []
  for i in range(1+xmin,hfit.GetNbinsX()+131):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     listfRes.append(y)
     hfit.SetBinContent(i,y)

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if(M>M5): break
     sM = str(M)
     sW = str(M*fRes_SystDOWN.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS, listfRes


# def RatioMaker(listNumnerator, listDenominator):

#   RatioArray =[]

#   for i in range(0, len(listDenominator)):
#     if listDenominator[i] != 0:
#       z = listNumnerator[i]/listDenominator[i]
#     else:
#       z =0
#     RatioArray.append(z)
#   return RatioArray

# def DifferenceMaker(listFit, listToy):

#   ResidualArray =[]

#   for i in range(0, len(listToy)):
#     if listToy[i] != 0:
#       z = listToy[i] - listFit[i]
#     else:
#       z =0
#     ResidualArray.append(z)
#   return ResidualArray

#Acceptance x Efficiency 

### get the acceptance*efficiency
fPeterAcc = ROOT.TFile(indir+"ee_accEffFunction.root","READ")
fAcc = fPeterAcc.Get("accEff_TF1")
sfAcc = "[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)"
for n in range(0,fAcc.GetNpar()):
   print (fAcc.GetParName(n)+": "+str(fAcc.GetParameter(n)))
   sfAcc = str(sfAcc.replace("["+str(n)+"]",str(fAcc.GetParameter(n))))
fAcc.SetLineColor(ROOT.kBlack)


#Make Signal Models with relavant uncertainties

# Nbins = 5870 #2470
# k = 500
# M5 = 6000
# xmin = 130
# xmax = 6000

# massList = np.linspace(xmin, xmax, Nbins)


# Sig_Resolution_SystematicDOWN, Sig_Resolution_SystematicDOWN_listfRes = SigFixed_Resolution_SystematicDOWN(k, M5, xmin, xmax)
# Sig_Resolution_SystematicUP, Sig_Resolution_SystematicUP_listfRes = SigFixed_Resolution_SystematicUP(k, M5, xmin, xmax)
# SigFixed_Nominal, SigFixed_Nominal_listfRes = SigFixed(k, M5, xmin, xmax)

# UPtest = RatioMaker(SigFixed_Nominal_listfRes, Sig_Resolution_SystematicUP_listfRes)
# DOWNtest = RatioMaker(SigFixed_Nominal_listfRes, Sig_Resolution_SystematicDOWN_listfRes)

# ## Plotting of uncertainies and the effect on the analytical signal

# C = ['darkorange', 'steelblue', 'firebrick', 'purple']

# # Figure
# f1, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, SigFixed_Nominal_listfRes, 'black', linestyle='dashed', lw=4, label=r"SigFixed_Nominal")
# ax.plot(massList, Sig_Resolution_SystematicDOWN_listfRes, 'darkorange', lw=6, label=r"Sig_Resolution_SystematicDOWN")
# ax.plot(massList, Sig_Resolution_SystematicUP_listfRes, 'red', lw=6, label=r"Sig_Resolution_SystematicUP")
# plt.fill_between(massList, Sig_Resolution_SystematicDOWN_listfRes, Sig_Resolution_SystematicUP_listfRes, facecolor='blue', alpha=0.5, label=r"Systematic Band")

# # Set limits and labels
# ax.set_xlim(0,6000)
# ax.set_ylim(0,3)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Relative Mass Resoultuion [%]', ha='right', y=1.0)

# # Legend
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()

# # leg.set_title('How to tell you are using the right font:')
# # leg._legend_box.align = "left" # Align legend title


# # Apply CMS labels and ticks
# plt.margins(0)
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # ax.semilogy()
# # ax.semilogy()
# f1.savefig('Example1.png')

# # Figure
# f2, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, UPtest-1, 'black', lw=4, label=r"Sig_Resolution_SystematicUP")
# ax.plot(massList, DOWNtest-1, 'darkorange', lw=6, label=r"Sig_Resolution_SystematicDOWN")
# # ax.plot(massList, Sig_Resolution_SystematicUP_listfRes, 'red', lw=6, label=r"Sig_Resolution_SystematicUP")
# # plt.fill_between(massList, Sig_Resolution_SystematicDOWN_listfRes, Sig_Resolution_SystematicUP_listfRes, facecolor='blue', alpha=0.5, label=r"Systematic Band")

# # Set limits and labels
# ax.set_xlim(130,6000)
# ax.set_ylim(0,3)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Relative Mass Resoultuion [%]', ha='right', y=1.0)

# # Legend
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()

# # leg.set_title('How to tell you are using the right font:')
# # leg._legend_box.align = "left" # Align legend title


# # Apply CMS labels and ticks
# plt.margins(0)
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # ax.semilogy()
# ax.semilogx()
# f2.savefig('Example2.png')



def loadHisto(path,name,rebin=1,scale=1):
    print (yellow("Load",path, name))
    f = TFile.Open(path)
    h = f.Get(name)
    # h.Smooth()
    if scale!=1:
        print ("Scaling",scale)
        h.Scale(scale)
        h.Rebin(int(rebin))
    return hist(h)

if __name__=="__main__":

    # print table of systematics
    release="21"
    nominalName="signlTemplate"
    oPath = "Uncertainty_Pickles/relUncerts_signal.pickle"

    relUncerts = defaultdict(dict)
    # for channel in ["ee"]:
    # for channel in ["mm"]:
    for channel in ["ee"]:
        names = []
        names+= dictionary.pdfNames[release]
        names+= dictionary.bkgTopNames[channel]
        names+= dictionary.bkgDetNames[channel]

        names+= dictionary.SigFitNames[channel]
        names+= dictionary.SigDetNames[channel]

        # names+= dictionary.smearedNames["inv"]
        # names+= dictionary.smearedNames["newPSmear"]
        # names = names[0:2]
        # names = ["m_uu_pstOR_MUON_SAGITTA_RESBIAS__1up"]
        names = filter(lambda n: n not in ignore,names)

        hPath = dictionary.data[release][channel][nominalName].split(":")[0]
        hName = dictionary.data[release][channel][nominalName].split(":")[1]
        nom = loadHisto(hPath,hName)
        nom1GeV = loadHisto(hPath,hName,rebin=10)

        # experimental nominal should use different shape
        # trigger systematic is approximation
        expNomName = nomDetNames[channel]
        hPath = dictionary.data[release][channel][expNomName].split(":")[0]
        hName = dictionary.data[release][channel][expNomName].split(":")[1]
        expNom = loadHisto(hPath,hName)
        expNom.adjustBinning(nom)
        norm = sum(nom.y)/sum(expNom.y)
        expNom.y*=norm
        # expNom.smooth()

        # smeared dist
        print red("Only works with mm channel")
        smearNomName = "m_uu_noInvSmear"
        smearNomName = "m_uu_psmear00"
        hPath = dictionary.data[release]["mm"][smearNomName].split(":")[0]
        hName = dictionary.data[release]["mm"][smearNomName].split(":")[1]
        smearNom = loadHisto(hPath,hName)
        smearNom.adjustBinning(nom)
        norm = sum(nom.y)/sum(smearNom.y)
        smearNom.y*=norm


        print channel
        for iName,name in enumerate(names):
            # if iName>1: break
            # if "ALPHA" not in name: continue
            # if "fakes" not in name: continue
            # if "EL_EFF_Iso_TOTAL" not in name: continue
            if "eeOnly" in name and channel!="ee": continue
            print red(release,channel,name)
            print dictionary.data[release][channel][name]
            hPath = dictionary.data[release][channel][name].split(":")[0]
            hName = dictionary.data[release][channel][name].split(":")[1]
            rebin=1
            print green(release,channel)
            print green(hPath,name)
            var = loadHisto(hPath,hName,rebin=rebin)

            # adjustBinning out experimental uncerts
            needsSmooth= len(var.x)<=6000
            if needsSmooth:
                var.adjustBinning(nom)
                target = expNom
            else:
                target = nom
            if "smear" in name.lower(): 
                print red("Using smeared nominal")
                target = smearNom

            # diagnosis plot
            plotOn=0
            if plotOn:
                path = "plots/var-{0}.png".format(name)
                plotter.variationsPlot2(nom.x,nom.y,var.x,var.y,name=name,path=path,channel=channel)
                quit()


            # print red(len(var.x),len(nom.x))
            # continue

            # check binning identical
            assert (np.abs(target.x-var.x)<0.1).all()

            # normalize variation to nominal
            norm = sum(nom.y)/sum(var.y)
            var.y*=norm

            # take the relative difference to nominal
            var.y = (var.y-target.y)/target.y

            # smooth bins by average neighbors
            # if needsSmooth:
            var.smooth()

            # configure rest of var
            # var.th1=None
            var.name+="--relVarToNom"

            relUncerts[channel][name]=var
        relUncerts[channel]["nominal"]=copy.deepcopy(nom)


#Save varations to pickle
f = open(oPath,"w")
pickle.dump(relUncerts,f)
f.close()

# i=0
# for channel in ["ee","mm"]:
#     for name in variations[channel].keys():
#         # if "RESBIAS" not in name: continue
#         x = variations[channel][name].x
#         y = variations[channel][name].y
#         path = "plots/{0}-{1}.png".format(name,channel).replace("_","-")
#         plotter.variationsPlot(x,y,channel=channel,name=name,path=path)
#         i+=1

