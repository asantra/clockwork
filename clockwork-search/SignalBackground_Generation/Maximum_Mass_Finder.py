
#Import standard
import sys, time
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array
from parton import mkPDF

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes

#Disable any warnings
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  

#Set random generator for Briet-Wigner shape used in signals
random = ROOT.TRandom3(0)

############################################################################################################

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/SignalBackground_Generation/"


sys.path.append(os.path.abspath('Custom_Modules/'))
from Custom_Modules import Basic, Background, Couplings, Graviton, KKcascade, Luminosity, PDFcalc, Smearing
import SigBkg_Functions as SB

import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()


if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775

if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 

eventType = "float"

if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins, eventType)
if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins, eventType)

x0_15pecernt_List = []
x0_10pecernt_List = []
x0_5pecernt_List = []
x0_1pecernt_List = []
for i in range(1,100):
	data = SB.BkgPoissonToy(BkgFix, 1)
	x0_15pecernt, x0_10pecernt, x0_5pecernt, x0_1pecernt = SB.Maximum_Mass_Finder(xmin, data, nBins, xmin, xmax, nBins, argus.b)
	x0_15pecernt_List.append(x0_15pecernt)
	x0_10pecernt_List.append(x0_10pecernt)
	x0_5pecernt_List.append(x0_5pecernt)
	x0_1pecernt_List.append(x0_1pecernt)

print("Average x0_15pecernt from Toys",np.average(x0_15pecernt_List))
print("Average x0_10pecernt from Toys",np.average(x0_10pecernt_List))
print("Average x0_5pecernt from Toys",np.average(x0_5pecernt_List))
print("Average x0_1pecernt from Toys",np.average(x0_1pecernt_List))




