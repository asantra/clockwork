############
#   SBLD   #
############

### Explanations ###
# - Calculate the signal associated to a generic clockwork graviton model, backgrounds.

#Import standard
import sys, time
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array
from parton import mkPDF

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes

#Disable any warnings
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  

#Set random generator for Briet-Wigner shape used in signals
random = ROOT.TRandom3(10)

############################################################################################################

### locate the clokwork_home
Clockwork_Home = os.getenv("HOMELOCATION")
Directory = Clockwork_Home+"/clockwork-search/SignalBackground_Generation/"

sys.path.append(os.path.abspath('Custom_Modules/'))
from Custom_Modules import Basic, Background, Couplings, Graviton, KKcascade, Luminosity, PDFcalc, Smearing


#Input directories
indir_ee = Directory+"Inputs_ee/"
indir_yy = Directory+"Inputs_yy/"

############################################################
############################################################
################### Dielectron Inputs ######################
############################################################
############################################################

## get the nominal acceptance*efficiency
fPeterAcc = ROOT.TFile(indir_ee+"ee_accEffFunction.root","READ")
fAcc_ee = fPeterAcc.Get("accEff_TF1")
sfAcc_ee = "[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)"
for n in range(0,fAcc_ee.GetNpar()):
   print (fAcc_ee.GetParName(n)+": "+str(fAcc_ee.GetParameter(n)))
   sfAcc_ee = str(sfAcc_ee.replace("["+str(n)+"]",str(fAcc_ee.GetParameter(n))))
fAcc_ee.SetLineColor(ROOT.kBlack)

#ee_ID efficiency systematic uncertainties function


### get the Nominal Dielectron Bkg  model
fbkgfinal_ee = ROOT.TFile(indir_ee+"finalbkg.root","READ")
fBkg_ee = fbkgfinal_ee.Get("bkgfit")


Nbins_ee = 4800 #2470
xmin_ee = 0 #230
xmax_ee = 4800 #2700

Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin_ee, xmax_ee)
Acc_ee.SetParameter(0,0.79366)
Acc_ee.SetParameter(1,-234.118)
Acc_ee.SetParameter(2,61575.1)
Acc_ee.SetParameter(3,-1.046e7)
Acc_ee.SetParameter(4,9.63491e8)
Acc_ee.SetParameter(5,-3.67989e10)
Acc_ee.SetParameter(6,-9.10565e-6)
Acc_ee.SetParameter(7,4.221e-10)



#Define mass binning
massList_ee = np.linspace(xmin_ee, xmax_ee, Nbins_ee)


#ee_ID efficiency systematic uncertainties function
sfAcc_ee_IDefficiency = ROOT.TF1("sfAcc_ee_IDefficiency","[0]+[1]*TMath::Exp([2]*(x))+[3]*log(x)+[4]*TMath::TanH([5]*(x-[6]))",xmin_ee, xmax_ee)
sfAcc_ee_IDefficiency.SetParameter(0,0.200345)
sfAcc_ee_IDefficiency.SetParameter(1,-0.219756)
sfAcc_ee_IDefficiency.SetParameter(2,-0.000110541)
sfAcc_ee_IDefficiency.SetParameter(3,0.00922559)
sfAcc_ee_IDefficiency.SetParameter(4,0.0169377)
sfAcc_ee_IDefficiency.SetParameter(5,0.00632574)
sfAcc_ee_IDefficiency.SetParameter(6,530.715)

#sfAcc_ee_Iso_efficiency systematic uncertainties function
sfAcc_ee_Iso_efficiency = ROOT.TF1("sfAcc_ee_Iso_efficiency","[0]+[1]*TMath::TanH([2]*(x-[3]))+[4]*TMath::TanH([5]*(x-[6]))+[7]*log(x)",xmin_ee, xmax_ee)
sfAcc_ee_Iso_efficiency.SetParameter(0,0.00897086)
sfAcc_ee_Iso_efficiency.SetParameter(1,0.018128)
sfAcc_ee_Iso_efficiency.SetParameter(2,-0.000801275)
sfAcc_ee_Iso_efficiency.SetParameter(3,-262.667)
sfAcc_ee_Iso_efficiency.SetParameter(4,0.0130715)
sfAcc_ee_Iso_efficiency.SetParameter(5,0.00145374)
sfAcc_ee_Iso_efficiency.SetParameter(6,453.596)
sfAcc_ee_Iso_efficiency.SetParameter(7,.000817472)

#Generate example of signal model using full Resonant TF shape instead of Guassian
#First define the individual components

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin_ee, xmax_ee)
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)

sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)

alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin_ee, xmax_ee)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)

#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin_ee, xmax_ee)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)

sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)

#K-Parameters
k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin_ee, xmax_ee)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)


########################################################################
####################### TF Variation Shapes ############################
########################################################################

#Up-variation Crystal Ball Parameters

#energy scale
mu_CB_UPvar = ROOT.TF1("mu_CB_UPvar","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin_ee, xmax_ee)
mu_CB_UPvar.SetParameter(0, 0.143853)
mu_CB_UPvar.SetParameter(1, -0.544555)
mu_CB_UPvar.SetParameter(2, -0.00897897)
mu_CB_UPvar.SetParameter(3, 0.000000211052)

#energy resolution
sigma_CB_UPvar = ROOT.TF1("sigma_CB_UPvar","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_CB_UPvar.SetParameter(0, 0.0168464)
sigma_CB_UPvar.SetParameter(1, 0.244998)
sigma_CB_UPvar.SetParameter(2, 0.607754)

#Up-variation Gaussian

#energy scale
mu_G_UPvar = ROOT.TF1("mu_G_UPvar","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)",xmin_ee, xmax_ee)
mu_G_UPvar.SetParameter(0, 0.00437644)
mu_G_UPvar.SetParameter(1, -0.693393)
mu_G_UPvar.SetParameter(2, -0.00000124588)
mu_G_UPvar.SetParameter(3, 0.00000994365)
mu_G_UPvar.SetParameter(4, 3.85433)
mu_G_UPvar.SetParameter(5, 0.0000000000681002)

#energy resolution
sigma_G_UPvar = ROOT.TF1("sigma_G_UPvar","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_G_UPvar.SetParameter(0, 0.00968273)
sigma_G_UPvar.SetParameter(1, 0.110266)
sigma_G_UPvar.SetParameter(2, 1.0617)

########################################################################

#Down-variation Crystal Ball Parameters

#energy scale
mu_CB_DOWNvar = ROOT.TF1("mu_CB_DOWNvar","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)",xmin_ee, xmax_ee)
mu_CB_DOWNvar.SetParameter(0, 0.125578)
mu_CB_DOWNvar.SetParameter(1, -0.350447)
mu_CB_DOWNvar.SetParameter(2, -0.0144995)
mu_CB_DOWNvar.SetParameter(3, 0.00000373014)

#energy resolution
sigma_CB_DOWNvar = ROOT.TF1("sigma_CB_DOWNvar","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_CB_DOWNvar.SetParameter(0, 0.011107)
sigma_CB_DOWNvar.SetParameter(1, 0.22346)
sigma_CB_DOWNvar.SetParameter(2, 1.92675)

#DOWN-variation Gaussian

#energy scale
mu_G_DOWNvar = ROOT.TF1("mu_G_DOWNvar","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)",xmin_ee, xmax_ee)
mu_G_DOWNvar.SetParameter(0, -0.00743235)
mu_G_DOWNvar.SetParameter(1, 0.650265)
mu_G_DOWNvar.SetParameter(2, 0.000000574646)
mu_G_DOWNvar.SetParameter(3, -0.00000498561)
mu_G_DOWNvar.SetParameter(4, 28.7656)
mu_G_DOWNvar.SetParameter(5, -0.0000000000452119)

#energy resolution
sigma_G_DOWNvar = ROOT.TF1("sigma_G_DOWNvar","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin_ee, xmax_ee)
sigma_G_DOWNvar.SetParameter(0, 0.00337693)
sigma_G_DOWNvar.SetParameter(1, 0.112002)
sigma_G_DOWNvar.SetParameter(2, 1.2219)


############################################################
################### Diphoton Inputs ########################
############################################################


#Define binning, xmin and xmax for the yy inputs
Nbins_yy = 4850 #2470
xmin_yy = 150 #230
xmax_yy = 5000 #2700

### get the Nominal background model from the High-Mass yy analysis
fbkgfinal_yy = ROOT.TFile(indir_yy+"finalbkg_yy.root","READ")
fBkg_yy = fbkgfinal_yy.Get("bkgfit")


sigmagrav = ROOT.TF1("sigmagrav","1.460+0.00566*x")
deltamxgrav = ROOT.TF1("deltamxgrav","0.397 - 0.000907*x")
alphalow_fn = ROOT.TF1("alphalow_fn","1.316 - 0.00005043*x")
alphahigh_fn = ROOT.TF1("alphahigh_fn","1.672 + 0.00001005*x")
nlow_constant = 19.7
nhigh_constant = 12.1

#Let us define the double sided cyrstal ball function from the High-Mass Diphoton analysis
def double_sided_CB_mass(x,par):
  '''
  Some quick comments. We'll use the following structure for the different parameters
  par[0] = Normalization
  par[1] = mass
  '''
  # Sigma Double-Sided Crystal Ball
  sigmagrav = 1.460+0.00566*par[1]
  deltamxgrav = 0.397 - 0.000907*par[1]
  alphalow = 1.316 - 0.00005043*par[1]
  alphahigh = 1.672 + 0.00001005*par[1] 
  nlow = 19.7
  nhigh = 12.1
  t = (x[0] - (par[1]+deltamxgrav))/sigmagrav
  y = 0
  if(t < -alphalow):
    numerator = math.exp(-(alphalow*alphalow)/2.)
    denominator = ROOT.TMath.Power((alphalow/(nlow))*((nlow/alphalow) - alphalow - t),nlow)
    y = par[0] * (numerator/denominator)
  elif(t > alphahigh):
    numerator = math.exp(-(alphahigh*alphahigh)/2.)
    denominator = ROOT.TMath.Power((alphahigh/(nhigh))*((nhigh/alphahigh) - alphahigh + t),nhigh)
    y = par[0] * (numerator/denominator)
  else:
    y = par[0] * math.exp(-(t*t)/2.)
  return y

#Basic resoultion function to do Gaussian smearing for first testing
fRes_yy = ROOT.TF1("sigma_dscb_grav","1.460+0.00566*x",xmin_yy,xmax_yy)


############################################################
############################################################
################### Dielectron Signal Model ################
############################################################
############################################################


#Generate signal with no random fluctuations
def SigFixed_ee_withCB(k, M5, xmin, xmax, nBins):

### get the resolution with nominal parameters

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  # fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",nBins,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig")
  kR = str(10)
  scale = str(8300) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc_ee)+")"
  sResonances = ""  
  for n in range(1,100):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     if (M5>k) and (M>M5): break
     #Gaussain
     mu_Gauss = str(mu_G.Eval(M)*M+M)
     sigma_Guass = str(sigma_G.Eval(M)*M)
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
     mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  # for i in range(1,N+1):
  for i in range(0, len(massList) - 1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float":    
      listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
        listS.append(y)
  return listS

#Generate signal with no random fluctuations - just relative resolution using Gauissian from Resonant Search
def SigFixed_ee_GuassianOnly(k, M5, xmin, xmax, nBins):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(6.1/5)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc_ee)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if (M5>k) and (M>M5): break
     sM = str(M)
     sW = str(M*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  # for i in range(1,N+1):
  for i in range(0, len(massList) - 1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float":    
      listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
        listS.append(y)
  return listS

#Generate signal with no random fluctuations
def SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins):

  Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin, xmax)
  Acc_ee.SetParameter(0,0.79366)
  Acc_ee.SetParameter(1,-234.118)
  Acc_ee.SetParameter(2,61575.1)
  Acc_ee.SetParameter(3,-1.046e7)
  Acc_ee.SetParameter(4,9.63491e8)
  Acc_ee.SetParameter(5,-3.67989e10)
  Acc_ee.SetParameter(6,-9.10565e-6)
  Acc_ee.SetParameter(7,4.221e-10)

  def Smearing(M,n,m, Gamma):
    if m > xmin:
      scale = str(1)
      sClockwork = "((RESONANCES)*("+(scale)+")"
      sResonances = ""  
      sBreitWigner = "((BREITWIGNER)*("+(scale)+")"
      sWidth = ""
      #Gaussain
      mu_Gauss = str(mu_G.Eval(M)*M+M)
      sigma_Guass = str(sigma_G.Eval(M)*M)
      #Crystal_Ball
      alpha_CrystalBall = str(alpha_CB)
      n_CrystalBall = str(n_CB.Eval(M))
      sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
      mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
      #k
      k_p = str(k_param.Eval(M))
      #Breit-Wigner
      BW_Gamma = str(Gamma)
      M_truth = str(M) #"+M_truth+"
      if(n==1): 
        # sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sResonances = "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
        # sWidth = "(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+",0)))"
        sWidth = "(TMath::BreitWigner(x,0,"+BW_Gamma+")))"
      else:     
        # sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
        # sWidth += "+(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+",0)))"
        sWidth += "+(TMath::BreitWigner(x,0,"+BW_Gamma+")))"
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
      # y  = (((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5) Wrong!!! - Double counting, mixing shapes
      y  = fClockwork.Eval(m) # Assuming in convolution, just get TF because of the very small width, effectively delta function
    else:
      y = 0 
    return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information MSTW2008lo68cl NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  print("KKlist")
  print(massList)
  print("KKlist length")
  print(len(massList))
  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRee(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    BW         = 0
    mass       = round(massList[i],1)
    print("mass")
    print(mass)
    deltamass  = massList[i + 1] - massList[i]

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      fAcc_ee = Acc_ee.Eval(listMassKK[particle])
      if fAcc_ee < 0:
        fAcc_ee = 0
      
      '''
      print("listCS[particle]             : ", listCS[particle])
      print("Smearing(listMassKK[particle]: ", Smearing(listMassKK[particle],particle,mass,listGamma[particle]))
      print("listBRee[particle            : ", listBRee[particle])
      print("Intlum                       : ", Intlum)
      print("fAcc_ee                      : ", fAcc_ee)
      '''
      
      total += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle])*listBRee[particle]*Intlum*fAcc_ee
      # BW = total*random.BreitWigner(listMassKK[particle],listGamma[particle])
    #Append to lists
    print("deltamass: ", deltamass)
    listS.append(total*deltamass)
    print("S")
    print(total*deltamass)
    listCSplot.append(total/Intlum)

  print("total")
  print(total) 

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

#Generate signal with no random fluctuations
def SigFix_Theory_Dielectron_NewConvolution(k, M5, xmin, xmax, nBins):

  Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin, xmax)
  Acc_ee.SetParameter(0,0.79366)
  Acc_ee.SetParameter(1,-234.118)
  Acc_ee.SetParameter(2,61575.1)
  Acc_ee.SetParameter(3,-1.046e7)
  Acc_ee.SetParameter(4,9.63491e8)
  Acc_ee.SetParameter(5,-3.67989e10)
  Acc_ee.SetParameter(6,-9.10565e-6)
  Acc_ee.SetParameter(7,4.221e-10)


        #   f_conv = TF1Convolution("expo", "gaus", -10, 10, kTRUE)
        # f_conv.SetRange(-2, 2)
        # f_conv.SetNofPointsFFT(1000)
        # print(f_conv.GetNpar())
        # fitfunction_conv = TF1("fitfunction_conv", f_conv, -2, 2, f_conv.GetNpar())

  # def Convolution(M,n,m,Gamma):
  #  #      // use directly the functionin ROOT::MATH note that the parameters definition is different is (alpha, n sigma, mu)
  #  # 24    auto f2 = new TF1("f2","ROOT::Math::crystalball_function(x, 2, 1, 1, 0)",-5,5);
  #   # f_conv = ROOT.TF1Convolution("[0]*TMath::BreitWigner(x,0,[1])","(([0]*ROOT::Math::crystalball_function(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax, ROOT.kTRUE)
  #   # f_conv = ROOT.TF1Convolution("[0]*TMath::BreitWigner(x,0,[1])","(([2]*ROOT::Math::crystalball_pdf(x, [3], [4],[5],[6]))+((1-[2])*TMath::Gaus(x,[7],[8],1)))",xmin, xmax, ROOT.kTRUE)
  #   f_conv = ROOT.TF1Convolution("(([2]*ROOT::Math::crystalball_pdf(x, [3], [4],[5],[6]))+((1-[2])*TMath::Gaus(x,[7],[8],1)))","[0]*ROOT::Math::breitwigner_pdf(x,[1],0)",xmin, xmax, ROOT.kFALSE)
  #   f_conv.SetRange(xmin,xmax)
  #   f_conv.SetNofPointsFFT(10000)
  #   # f_conv.Update()
  #   print(f_conv.GetNpar())
  #   fitfunction_conv = ROOT.TF1("fitfunction_conv",f_conv, 130, 6000,f_conv.GetNpar())
  #   # fitfunction_conv.SetParameter(0, CS)
  #   # fitfunction_conv.SetParameter(1, Gamma)
  #   fitfunction_conv.FixParameter(0, 1)
  #   fitfunction_conv.FixParameter(1, Gamma)
  #   fitfunction_conv.FixParameter(2,k_param.Eval(M))
  #   fitfunction_conv.FixParameter(3, alpha_CB)
  #   fitfunction_conv.FixParameter(4, n_CB.Eval(M))
  #   fitfunction_conv.FixParameter(5, sigma_CB.Eval(M))
  #   fitfunction_conv.FixParameter(6, mu_CB.Eval(M))
  #   fitfunction_conv.FixParameter(7, mu_G.Eval(M))
  #   fitfunction_conv.FixParameter(8, sigma_G.Eval(M))
  #   # Convolution = 0
  #   Convolution = fitfunction_conv.Eval(m)
  #   # if(n==1):
  #   #   Convolution = fitfunction_conv.Eval(m)
  #   # else:
  #   #   Convolution += fitfunction_conv.Eval(m)
  #   print("Convolution",Convolution)
  #   return Convolution

  # def Convolution(M,n,m, Gamma):
  #   if m > 130:
  #     scale = str(1)
  #     sClockwork = "(RESONANCES)*("+(scale)+")"
  #     sResonances = ""  
  #     sBreitWigner = "((BREITWIGNER)*("+(scale)+")"
  #     sWidth = ""
  #     if(n==1): 
  #       sResonances = "(([2]*ROOT::Math::crystalball_function(x, [3], [4],[5],[6]))+((1-[2])*ROOT::Math::gaussian_pdf(x,[8],[7])))"
  #       # sResonances += "(([2]*ROOT::Math::crystalball_pdf(x, [3], [4],[5],[6]))+((1-[2])*TMath::Gaus(x,[7],[8],1)))"
  #       # sResonances = "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
  #       sWidth = "(ROOT::Math::breitwigner_pdf(x,[1],[0])))"
  #       # sWidth = "(TMath::BreitWigner(x,[0],[1])))"
  #     else:   
  #       # sResonances += "+"+fitfunction_conv+""  
  #       sResonances += "+(([2]*ROOT::Math::crystalball_pdf(x, [3], [4],[5],[6]))+((1-[2])*ROOT::Math::gaussian_pdf(x,[8],[7])))"
  #       # sResonances += "+(([2]*ROOT::Math::crystalball_function(x, [3], [4],[5],[6]))+((1-[2])*TMath::Gaus(x,[7],[8],1)))"
  #       # sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
  #       sWidth += "+(ROOT::Math::breitwigner_pdf(x,[1],[0])))"
  #       # sWidth += "+(TMath::BreitWigner(x,[0],[1])))"
  #     sClockwork = sClockwork.replace("RESONANCES",sResonances)
  #     sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
  #     fClockwork = ROOT.TF1("fClockwork",sClockwork,130,6000)
  #     fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,130,6000)
  #     f_conv = ROOT.TF1Convolution("fBreitWigner","fClockwork",xmin, xmax, ROOT.kFALSE)
  #     f_conv.SetRange(xmin,xmax)
  #     f_conv.SetNofPointsFFT(100000)
  #     f_conv.Update()
  #     # print(f_conv.GetNpar())
  #     fitfunction_conv = ROOT.TF1("fitfunction_conv",f_conv, xmin, xmax,f_conv.GetNpar())
  #     # y  = (((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5)
  #     fitfunction_conv.FixParameter(0, M)
  #     fitfunction_conv.FixParameter(1, Gamma)
  #     fitfunction_conv.FixParameter(2,k_param.Eval(M))
  #     fitfunction_conv.FixParameter(3, alpha_CB)
  #     fitfunction_conv.FixParameter(4, n_CB.Eval(M))
  #     fitfunction_conv.FixParameter(5, sigma_CB.Eval(M))
  #     fitfunction_conv.FixParameter(6, mu_CB.Eval(M))
  #     fitfunction_conv.FixParameter(7, mu_G.Eval(M))
  #     fitfunction_conv.FixParameter(8, sigma_G.Eval(M))
  #     y  = fitfunction_conv.Eval(m)
  #     # y = np.convolve(BW,TF, 'valid')
  #   else:
  #     y = 0 
  #   return y

  def Convolution(M,n,m, Gamma):
    if m > 130:
      scale = str(1)
      sClockwork = "(RESONANCES)*("+(scale)+")"
      sResonances = ""  
      sBreitWigner = "((BREITWIGNER)*("+(scale)+")"
      sWidth = ""
      y = 0
      if(n==1): 
        sResonances = "(([2]*ROOT::Math::crystalball_function(x, [3], [4],[5],[6]))+((1-[2])*ROOT::Math::gaussian_pdf(x,[8],[7])))"
        sWidth = "(ROOT::Math::breitwigner_pdf(x,[1],[0])))"
        sClockwork = sClockwork.replace("RESONANCES",sResonances)
        sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
        fClockwork = ROOT.TF1("fClockwork",sClockwork,130,6000)
        fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,130,6000)
        f_conv = ROOT.TF1Convolution("fBreitWigner","fClockwork",xmin, xmax, ROOT.kTRUE)
        f_conv.SetRange(xmin,xmax)
        f_conv.SetNofPointsFFT(100000)
        fitfunction_conv = ROOT.TF1("fitfunction_conv",f_conv, xmin, xmax,f_conv.GetNpar())
        # y  = (((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5)
        fitfunction_conv.FixParameter(0, M)
        fitfunction_conv.FixParameter(1, Gamma)
        fitfunction_conv.FixParameter(2,k_param.Eval(M))
        fitfunction_conv.FixParameter(3, alpha_CB)
        fitfunction_conv.FixParameter(4, n_CB.Eval(M))
        fitfunction_conv.FixParameter(5, sigma_CB.Eval(M))
        fitfunction_conv.FixParameter(6, mu_CB.Eval(M))
        fitfunction_conv.FixParameter(7, mu_G.Eval(M))
        fitfunction_conv.FixParameter(8, sigma_G.Eval(M))
        y  = fitfunction_conv.Eval(m)
      else:   
        sResonances = "(([2]*ROOT::Math::crystalball_pdf(x, [3], [4],[5],[6]))+((1-[2])*ROOT::Math::gaussian_pdf(x,[8],[7])))"
        sWidth = "(ROOT::Math::breitwigner_pdf(x,[1],[0])))"
        sClockwork = sClockwork.replace("RESONANCES",sResonances)
        sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
        fClockwork = ROOT.TF1("fClockwork",sClockwork,130,6000)
        fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,130,6000)
        f_conv = ROOT.TF1Convolution("fBreitWigner","fClockwork",xmin, xmax, ROOT.kTRUE)
        f_conv.SetRange(xmin,xmax)
        f_conv.SetNofPointsFFT(10000)
        fitfunction_conv = ROOT.TF1("fitfunction_conv",f_conv, xmin, xmax,f_conv.GetNpar())
        # y  = (((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5)
        fitfunction_conv.FixParameter(0, M)
        fitfunction_conv.FixParameter(1, Gamma)
        fitfunction_conv.FixParameter(2,k_param.Eval(M))
        fitfunction_conv.FixParameter(3, alpha_CB)
        fitfunction_conv.FixParameter(4, n_CB.Eval(M))
        fitfunction_conv.FixParameter(5, sigma_CB.Eval(M))
        fitfunction_conv.FixParameter(6, mu_CB.Eval(M))
        fitfunction_conv.FixParameter(7, mu_G.Eval(M))
        fitfunction_conv.FixParameter(8, sigma_G.Eval(M))
        y  += fitfunction_conv.Eval(m)
    else:
      y = 0 
    return y


  # def Convolution(M,n,m, Gamma):
  #   TF_DYee =  ROOT.TF1("TF_DYee","(([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  #   BW_ee = ROOT.TF1("BW_ee","TMath::BreitWigner(x,0,[0])",xmin, xmax)
  #   BW_ee.SetParameter(0, Gamma)
  #   TF_DYee.SetParameter(0,k_param.Eval(M))
  #   TF_DYee.FixParameter(1, alpha_CB)
  #   TF_DYee.SetParameter(2, n_CB.Eval(M))
  #   TF_DYee.SetParameter(3, sigma_CB.Eval(M))
  #   TF_DYee.SetParameter(4, mu_CB.Eval(M))
  #   TF_DYee.SetParameter(5, mu_G.Eval(M))
  #   TF_DYee.SetParameter(6, sigma_G.Eval(M))
  #   y = 0
  #   if m > k:
  #     TF = TF_DYee.Eval(m)
  #     BW = BW_ee.Eval(m)
  #     if(n==1): 
  #       y = np.convolve(BW,TF, 'valid')
  #     else: 
  #       y += np.convolve(BW,TF, 'valid')
  #   else:
  #     y = 0 
  #   return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information MSTW2008lo68cl NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  print("KKlist")
  print(massList)
  print("KKlist length")
  print(len(massList))
  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]
  CS_Sum    =     0
  listCS_Truth  = [0]
  TruthEvents   = 100000

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRee(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  CS_Sum = sum(listCS)
  print("CSSum",CS_Sum)

  for n in range(1, len(listMassKK)):
    listCS_Truth.append((Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC)/CS_Sum)*TruthEvents)
    print("listCS_Truth",listCS_Truth)

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []


  for i in range(0, len(massList) - 1):

      #Initialize variables
      total      = 0
      BW         = 0
      mass       = round(massList[i],1)
      print("mass")
      print(mass)
      deltamass  = massList[i + 1] - massList[i]

      #Sum over particles
      for particle in range(1, len(listMassKK)):
        fAcc_ee = Acc_ee.Eval(listMassKK[particle])
        if fAcc_ee < 0:
          fAcc_ee = 0
        total +=   listCS[particle]*Convolution(listMassKK[particle],particle,mass,listGamma[particle])*listBRee[particle]*Intlum*fAcc_ee
          # total += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle])*listBRee[particle]*Intlum*fAcc_ee
      #Append to lists
      listS.append(total*deltamass)
      print("S")
      print(total*deltamass)
      listCSplot.append(total/Intlum)

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

#Generate truth ee signal with no random fluctuations
def SigFix_Theory_Dielectron_TruthShape(k, M5, xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  print("KKlist")
  print(massList)
  print("KKlist length")
  print(len(massList))
  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections but for turth distibution use total crosss ection 
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]
  CS_Sum    =     0
  listCS_Truth  = [0]
  TruthEvents   = 100000

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRee(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  CS_Sum = sum(listCS)
  print("CSSum",CS_Sum)

  for n in range(1, len(listMassKK)):
    listCS_Truth.append((Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC)/CS_Sum)*TruthEvents)
    print("listCS_Truth",listCS_Truth)
  #Write properties
  # if Couplings.writeProp:

  #   ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

  #   ftemp.write("Summary of the graviton properties \n")
  #   ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

  #   for n in range(0, len(listMassKK)):
  #     ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

  #   ftemp.close()

  #Apply smearing
  listS      = []
  listBW = []
  listCSplot = []

  for i in range(0, len(massList) - 1):
    #Initialize variables 
    total      = 0
    mass       = round(massList[i],2) 
    deltamass  = massList[i + 1] - massList[i]
    print("mass")
    print(mass)
    for particle in range(1, len(listMassKK)):
      TruthMass = round(listMassKK[particle],1)
      # TruthMass = int(listMassKK[particle])
      if mass == TruthMass:
        total = np.random.poisson(random.BreitWigner(listCS_Truth[particle],listGamma[particle]))
    listS.append(total)
    listCSplot.append(total/Intlum)
    print("#####Signal#####")
    print(total)
  

  #Return
  return listS

############################################################
############################################################
################### High-Mass Diphoton Signal Model ########
############################################################
############################################################

#Generate signal with no random fluctuations
def SigFixed_yy_DoubleCB(k, M5, xmin, xmax, nBins):


    #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()

  # fit_DSCB_mass = ROOT.TF1('fit_DSCB_mass', double_sided_CB_mass, 150, 5000, 4850)
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm= str(1)
  sClockwork = "("+(scale)+"*"+(Norm)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc_yy)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if (M5>k) and (M>M5): break
     sM = str(M)
     sW = str(fRes_yy.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
        sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     
        sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  # for i in range(1,N+1):
  for i in range(0, len(massList) - 1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  return listS

#Generate signal with no random fluctuations
def SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins):

  #Basic resoultion function to do Gaussian smearing for first testing
  # fRes_yy = ROOT.TF1("sigma_dscb_grav","1.460+0.00566*x",xmin,xmax)
  fit_DSCB_mass = ROOT.TF1('fit_DSCB_mass', double_sided_CB_mass, xmin, xmax,  nBins)
  # sfAcc_yy = ROOT.TF1("sfAcc_yy","(-6.514 - 0.329*exp(-1.84*(x/1000)) + 6.996*exp(0.0012*(x/1000)))", xmin, xmax,  nBins)
  sfAcc_yy = ROOT.TF1("sfAcc_yy","0.541-0.268*exp(-0.000878*x)", xmin, xmax,  nBins)
  # #Guassian Intergal
  # def gaussianIntegral(tmin, tmax):
  #   return sqrt(TMath::TwoPi())*(ROOT::Math::gaussian_cdf(tmax) - ROOT::Math::gaussian_cdf(tmin))
  # #powerLawIntegral
  # def powerLawIntegral(tmin, tmax):
  #   a = exp(-0.5*alpha*alpha)
  #   b = n/alpha - alpha
  #   return a/(1 - n)*( (b - tmin)/(TMath::Power(alpha/n*(b - tmin), n)) - (b - tmax)/(TMath::Power(alpha/n*(b - tmax), n)) )

  #Define resolution with crystal ball + gaussian from the dielectron analysis
  def Resolution_yy(M,n,m, Gamma):
    # fit_DSCB_mass = ROOT.TF1('fit_DSCB_mass', double_sided_CB_mass, xmin_yy, xmax_yy,  Nbins_yy)
    scale = str(1)
    sResonances = 0
    #Breit-Wigner
    BW_Gamma = str(Gamma)
    M_truth = str(M)
    sBreitWigner = "BREITWIGNER"
    sWidth = ""
    #Need to evalaute the normalisation for the Diphoton for each signal
    N = 1
    fit_DSCB_mass.SetParameter(0, N)
    fit_DSCB_mass.FixParameter(1, M)
    ######
    # Diphoton_CB = fit_DSCB_mass.Eval(M)
    if(n==1): 
      sResonances = fit_DSCB_mass.Eval(m)
      # sWidth = "(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+","+M_truth+"))"
      # sWidth = "(TMath::BreitWigner(x,0,"+BW_Gamma+"))"
    else:     
      sResonances += fit_DSCB_mass.Eval(m)
      # sWidth += "(ROOT::Math::breitwigner_pdf(x,"+BW_Gamma+","+M_truth+"))"
      # sWidth += "(TMath::BreitWigner(x,0,"+BW_Gamma+"))"
    # sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
    # fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)

    # return (sResonances**2 + fBreitWigner.Eval(m)**2)**0.5
    return sResonances



  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')

  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1

  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRgammagamma      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRgammagamma.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/HighMass_GravitonProperties.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRgammagamma[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    mass       = round(massList[i],2)
    deltamass  = massList[i + 1] - massList[i]
    

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      AccEff = sfAcc_yy.Eval(listMassKK[particle])
      if AccEff < 0:
        AccEff = 0
      #First try Ae = - need to have truth mass in TeV
      total += listCS[particle]*Resolution_yy(listMassKK[particle], particle, mass,listGamma[particle])*listBRgammagamma[particle]*Intlum*AccEff

    #Append to lists
    print("#####Signal#####")
    print(total*deltamass)
    print("#####mass#####")
    print(mass)
    listS.append(total*deltamass*(2.5/48.1))
    # listS.append(total*deltamass)
    listCSplot.append(total/Intlum)

  # #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

def SigFix_Theory_HighMass_Diphoton_TruthShape(k, M5, xmin, xmax, nBins):
  #Define resolution with crystal ball + gaussian from the dielectron analysis

  def BreitWigner(M,m,Gamma):
    sBreitWigner = "(BREITWIGNER)"
    sWidth = ""
    BW_Gamma = str(Gamma)
    M_truth = str(M)
    # sWidth = "(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+"))"
    sWidth = "(ROOT::Math::breitwigner_pdf(x,"+M_truth+","+BW_Gamma+"))"
    # else:     
    #   sWidth += "+(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+"))"
    sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
    fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
    y  = fBreitWigner.Eval(m)
    return y



  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information MSTW2008lo68cl NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  print("KKlist")
  print(massList)
  print("KKlist length")
  print(len(massList))
  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(int(mtemp))
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]


  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRgammagamma.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):
    #Initialize variables 
    total      = 0
    mass       = massList[i]
    deltamass  = massList[i + 1] - massList[i]
    print("mass")
    print(mass)
    for particle in range(1, len(listMassKK)):
      TruthMass = listMassKK[particle]
      if mass == TruthMass:
        print("TruthMass")
        print(TruthMass)
        total = listCS[particle]*Intlum*BreitWigner(listMassKK[particle],mass,listGamma[particle])
    print("#####Signal#####")
    print(total*deltamass)
    listS.append(total*10)
    listCSplot.append(total/Intlum)


  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

#Generate signal with no random fluctuations
def SigFix_Theory_HighMass_Diphoton_GaussSmeared(k, M5, xmin, xmax, nBins):

  #Basic resoultion function to do Gaussian smearing for first testing
  fRes_yy = ROOT.TF1("sigma_dscb_grav","1.460+0.00566*x",xmin,xmax)
  # fit_DSCB_mass = ROOT.TF1('fit_DSCB_mass', double_sided_CB_mass, xmin, xmax,  nBins)
  # sfAcc_yy = ROOT.TF1("sfAcc_yy","(-6.514 - 0.329*exp(-1.84*(x/1000)) + 6.996*exp(0.0012*(x/1000)))", xmin, xmax,  nBins)
  sfAcc_yy = ROOT.TF1("sfAcc_yy","0.541-0.268*exp(-0.000878*x)", xmin, xmax,  nBins)


  #Define resolution with crystal ball + gaussian from the dielectron analysis
  def Resolution(M,n,m, Gamma):
    if m > k:
      # fit_DSCB_mass = ROOT.TF1('fit_DSCB_mass', double_sided_CB_mass, xmin_yy, xmax_yy,  Nbins_yy)
      scale = str(1)
      sClockwork = "(RESONANCES)*("+(scale)+")"
      sResonances = ""
      ######
      sM = str(M)
      sW = str(fRes_yy.Eval(M))
      if(n==1):
        sResonances += "TMath::Gaus(x,"+sM+","+sW+",1)"
      else:
        sResonances += "+TMath::Gaus(x,"+sM+","+sW+",1)"
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      y  = fClockwork.Eval(m)
    else:
      y = 0 
    return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')

  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1

  # while mtemp < massList[-1]:
  while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRgammagamma      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRgammagamma.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/HighMass_GravitonProperties.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRgammagamma[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    mass       = round(massList[i],2)
    deltamass  = massList[i + 1] - massList[i]
    

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      AccEff = sfAcc_yy.Eval(listMassKK[particle])
      if AccEff < 0:
        AccEff = 0
      total += listCS[particle]*Resolution(listMassKK[particle], particle, mass,listGamma[particle])*listBRgammagamma[particle]*Intlum*AccEff

    #Append to lists
    print("#####Signal#####")
    print(total*deltamass)
    print("#####mass#####")
    print(mass)
    # listS.append(total*deltamass*(2.5/48.1))
    listS.append(total*deltamass)
    listCSplot.append(total/Intlum)

  # #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

############################################################  
############################################################
################### 36.1 fb-1 Diphoton Signal Model ########
############################################################
############################################################


#Generate signal with no random fluctuations
def SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins):

  #Parameter
  SLHC   = Couplings.SLHC
  Eff    = Couplings.Eff
  Intlum = 36.7

  #Load pdf information
  pdf = mkPDF('MSTW2008lo68cl', 0, pdfdir=Directory+'Custom_Modules')

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1

  while mtemp < massList[-1]:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRPhotons = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRPhotons.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDiphoton36fb.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRPhotons[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    mass       = massList[i]
    deltamass  = massList[i + 1] - massList[i]

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      total += listCS[particle]*Smearing.shape(mass, listMassKK[particle], listGamma[particle])*listBRPhotons[particle]*Eff*Intlum
    #Append to lists
    listS.append(total*deltamass)
    listCSplot.append(total/Intlum)

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

############################################################
############################################################
################### Perfect Oscillating Signal  ############
############################################################
############################################################

#Generate signal with no random fluctuations
def SigFix_Generic(fundamental_freq, xmin, xmax, nBins):

  Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin, xmax)
  Acc_ee.SetParameter(0,0.79366)
  Acc_ee.SetParameter(1,-234.118)
  Acc_ee.SetParameter(2,61575.1)
  Acc_ee.SetParameter(3,-1.046e7)
  Acc_ee.SetParameter(4,9.63491e8)
  Acc_ee.SetParameter(5,-3.67989e10)
  Acc_ee.SetParameter(6,-9.10565e-6)
  Acc_ee.SetParameter(7,4.221e-10)


  def Smearing(m):
    if m > xmin:
      scale = str(1)
      sClockwork = "((RESONANCES)*("+(scale)+")"
      sResonances = ""  
      #Gaussain
      mu_Gauss = str(mu_G.Eval(m)*m+m)
      sigma_Guass = str(sigma_G.Eval(m)*m)
      #Crystal_Ball
      alpha_CrystalBall = str(alpha_CB)
      n_CrystalBall = str(n_CB.Eval(m))
      sigma_CrystalBall = str(sigma_CB.Eval(m)*m)
      mu_CrystalBall = str(mu_CB.Eval(m)*m+m)
      #k
      k_p = str(k_param.Eval(m)) 
      sResonances = "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*ROOT::Math::gaussian_pdf(x,"+sigma_Guass+","+mu_Gauss+")))"
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      y  = fClockwork.Eval(m)
    else:
      y = 0 
    return y

  listMassKK = [0]
  mtemp      = 0
  nint = 1
  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  while mtemp < massList[-1]:
    mtemp = massList[nint] + 0.5
    listMassKK.append(mtemp)
    nint += 1

  #Parameter
  Intlum = 139
  

  #Apply smearing
  listS      = []
  # Signal = []

  for i in range(0, len(massList)-1):

    #Initialize variables
    total      = 0
    BW         = 0
    mass       = massList[i] + 0.5
    print("mass")
    print(mass)
    deltamass  = massList[i + 1] - massList[i]

    fAcc_ee = Acc_ee.Eval(mass)
    frequencies = fundamental_freq

    Signal = 1 + np.cos((2*np.pi)*mass*frequencies)
    # Signal = np.cos((mass))
    print("Signal",Signal)

    
    if fAcc_ee < 0:
      fAcc_ee = 0
    total = Signal#*Smearing(mass)

      #Append to lists
    listS.append(total*deltamass)
    print("S")
    print(total*deltamass)



  #Return
  return listS




############################################################
############################################################
################### MC Signal ##############################
############################################################
############################################################

def MC_Signal_Dielectron_Reconstructed(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Dielectron_MCSamples/DAOD_M6k0.5ee.root","READ")
  fMCSig = fbkgfinal.Get("mass")
  # fMCSig.Rebin(10)
  # fMCSig.Scale(0.001)
  Ae = 0.594
  L = 139.0
  BR = 0.0203
  #500
  N = 1.16019e6
  xsec = 358.72
  #Weight
  Wgt = ((BR*xsec/N))*L
  # fMCSig.Scale(Wgt)
  fAcc_test = ROOT.TF1("fAcc_test",sfAcc_ee,xmin,xmax)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-10)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(225, len(massList)+225):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y*Wgt*1000)
     hfit.SetBinContent(i,(y))
    # print (len(listBkg))
  return listSig


def MC_Signal_Dielectron_TFApplied(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Dielectron_MCSamples/M6k0.5eemcr.root","READ")
  fMCSig = fbkgfinal.Get("Mr")
  # fMCSig.Rebin(10)
  # fMCSig.Scale(0.001)
  Ae = 0.594
  L = 139.0
  BR = 0.0203
  #500
  N = 95581.4
  xsec = 357.06
  #Weight
  Wgt = ((BR*xsec/N))*L
  # fMCSig.Scale(Wgt)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-10)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(0, len(massList)):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y*Wgt)
     hfit.SetBinContent(i,(y*Wgt))
    # print (len(listBkg))
  return listSig

def MC_Signal_Diphoton_TFApplied(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Diphoton_MCSamples/M6k0.5yymcr.root","READ")
  fMCSig = fbkgfinal.Get("Mr")
  # fMCSig.Rebin(10)
  # fMCSig.Scale(0.001)
  L = 139.0
  BR = 0.0407
  #500
  N = 1.79718e6
  xsec = 357.06
  #Weight
  Wgt = ((BR*xsec)/N)*L


  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(0, len(massList)):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y*Wgt)
     hfit.SetBinContent(i,(y*Wgt))
    # print (len(listBkg))
  return listSig

def MCTruth_Signal_Dielectron(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Dielectron_MCSamples/M6k0.5mc.root","READ")
  fMCSig = fbkgfinal.Get("mass")
  # fMCSig.Rebin(10)
  # Normalization = 12824.33035818426/100000
  # fMCSig.Scale(Normalization)
  # fMCSig.Scale(0.001)
  # Ae = 0.428


  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-5)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(0, len(massList)):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y)
     hfit.SetBinContent(i,(y))
    # print (len(listBkg))
  return listSig

def MCTruth_Signal_Diphoton(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Diphoton_MCSamples/M6k0.5yymc.root","READ")
  fMCSig = fbkgfinal.Get("mass")
  fMCSig.Rebin(10)
  fMCSig.Scale(0.001)
  L = 139.0
  BR = 0.0407
  #500
  N = 1.79895e6
  xsec = 358.72
  #Weight
  Wgt = ((BR*xsec)/N)*L


  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(225, len(massList) + 225):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y)
     hfit.SetBinContent(i,(y*Wgt))
    # print (len(listBkg))
  return listSig

def MC_Signal_Diphoton_Reconstructed(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(Directory+"MCSignal_Samples/Diphoton_MCSamples/Myy_histograms.root","READ")
  fMCSig = fbkgfinal.Get("m_yy_k0500")
  # fMCSig.Rebin(10)
  # fMCSig.Scale(0.001)
  Ae = 0.594
  L = 139.0
  BR = 0.0203
  #500
  N = 1.16019e6
  xsec = 358.72
  #Weight
  Wgt = ((BR*xsec/N))*L
  # fMCSig.Scale(Wgt)
  fAcc_test = ROOT.TF1("fAcc_test",sfAcc_ee,xmin,xmax)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-10)
  MCSig = fMCSig.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listSig = []
  for i in range(xmin-150, xmax-150):
     x = hfit.GetBinCenter(i)
     # if(x<MCSig.GetXmin()): continue
     y = MCSig.GetBinContent(i)
     listSig.append(y)
     hfit.SetBinContent(i,(y))
    # print (len(listBkg))
  return listSig

############################################################
############################################################
####### Dielectron Signal Model with Uncertainties #########
############################################################
############################################################

#Generate signal with no random fluctuations
def SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, Uncertainty):



  Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin, xmax)
  Acc_ee.SetParameter(0,0.79366)
  Acc_ee.SetParameter(1,-234.118)
  Acc_ee.SetParameter(2,61575.1)
  Acc_ee.SetParameter(3,-1.046e7)
  Acc_ee.SetParameter(4,9.63491e8)
  Acc_ee.SetParameter(5,-3.67989e10)
  Acc_ee.SetParameter(6,-9.10565e-6)
  Acc_ee.SetParameter(7,4.221e-10)

  def Smearing(M,n,m,Gamma,Uncertainty):
    if m > xmin:
      scale = str(1)
      # Acc_ee = str(sfAcc_ee) 
      sClockwork = "(RESONANCES*("+(scale)+")"
      sResonances = ""  
      sBreitWigner = "(BREITWIGNER*("+(scale)+")"
      sWidth = ""
      if Uncertainty == "Nominal":
        sClockwork = "((RESONANCES)*("+(scale)+")"
        sResonances = ""  
        sBreitWigner = "((BREITWIGNER)*("+(scale)+")"
        sWidth = ""
      if (Uncertainty == "Nominal"):
        mu_Gauss = str(mu_G.Eval(M)*M+M)
        sigma_Guass = str(sigma_G.Eval(M)*M)
        sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
        mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
      if Uncertainty == "EnergyScale_DOWN":
        sigma_Guass = str(sigma_G.Eval(M)*M)
        sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
        mu_Gauss = str(mu_G_DOWNvar.Eval(M)*M+M)
        mu_CrystalBall = str(mu_CB_DOWNvar.Eval(M)*M+M)
      if Uncertainty == "EnergyScale_UP":
        sigma_Guass = str(sigma_G.Eval(M)*M)
        sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
        mu_Gauss = str(mu_G_UPvar.Eval(M)*M+M)
        mu_CrystalBall = str(mu_CB_UPvar.Eval(M)*M+M)
      if Uncertainty == "EnergyResolution_UP":
        sigma_Guass = str(sigma_G_UPvar.Eval(M)*M)
        sigma_CrystalBall = str(sigma_CB_UPvar.Eval(M)*M)
        mu_Gauss = str(mu_G.Eval(M)*M+M)
        mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
      if Uncertainty == "EnergyResolution_DOWN":   
        sigma_Guass = str(sigma_G_DOWNvar.Eval(M)*M)
        sigma_CrystalBall = str(sigma_CB_DOWNvar.Eval(M)*M)
        mu_Gauss = str(mu_G.Eval(M)*M+M)
        mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
      #Crystal_Ball
      alpha_CrystalBall = str(alpha_CB)
      n_CrystalBall = str(n_CB.Eval(M))
      #k
      k_p = str(k_param.Eval(M))
      #Breit-Wigner
      BW_Gamma = str(Gamma)
      M_truth = str(M)
      if(n==1): 
        sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sWidth += "(ROOT::Math::breitwigner_pdf(x,"+M_truth+","+BW_Gamma+")))"
      else:     
        sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
        sWidth += "+(ROOT::Math::breitwigner_pdf(x,"+M_truth+","+BW_Gamma+")))"    
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
      # y  = ((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5
      y  = fClockwork.Eval(m)
    else:
      y = 0 
    return y


  def BreitWigner(M,m,n,Gamma):
    sBreitWigner = "(BREITWIGNER)"
    sWidth = ""
    BW_Gamma = str(Gamma*M)
    M_truth = str(M*M+M)
    if(n==1): 
      sWidth += "(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+"))"
    else:     
      sWidth += "+(TMath::BreitWigner(x,"+M_truth+","+BW_Gamma+"))"
    sBreitWigner = sBreitWigner.replace("BREITWIGNER",sWidth)
    fBreitWigner = ROOT.TF1("fBreitWigner",sBreitWigner,xmin,xmax)
    y  = fBreitWigner.Eval(m)
    return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information MSTW2008lo68cl NNPDF23_lo_as_0130_qed
  pdf = mkPDF('NNPDF23_lo_as_0130_qed', 0, pdfdir=Directory+'Custom_Modules')
  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1
  while mtemp < massList[-1]:
  # while nint < 107:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRee(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open(Directory+"Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  # listS_Acc_ee_IDefficiency      = []
  # listS_Acc_ee_Iso_efficiency     = []
  listS_EnergyResolution_DOWN     = []
  listS_EnergyResolution_UP      = []
  listS_EnergyScale_UP      = []
  listS_EnergyScale_DOWN      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    total_EnergyResolution_DOWN      = 0
    total_EnergyResolution_UP       = 0
    total_EnergyScale_UP      = 0
    total_EnergyScale_DOWN      = 0
    BW         = 0
    mass       = massList[i]
    print("mass")
    print(mass)
    deltamass  = massList[i + 1] - massList[i]

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      fAcc_ee = Acc_ee.Eval(listMassKK[particle])
      if Uncertainty == "Nominal":
        total += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"Nominal")*listBRee[particle]*Intlum*fAcc_ee
      sWidth = ""
      if Uncertainty == "All":
        total += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"Nominal")*listBRee[particle]*Intlum*fAcc_ee
        total_EnergyResolution_DOWN += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"EnergyResolution_DOWN")*listBRee[particle]*Intlum*fAcc_ee
        total_EnergyResolution_UP += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"EnergyResolution_UP")*listBRee[particle]*Intlum*fAcc_ee
        total_EnergyScale_UP += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"EnergyScale_UP")*listBRee[particle]*Intlum*fAcc_ee
        total_EnergyScale_DOWN += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"EnergyScale_DOWN")*listBRee[particle]*Intlum*fAcc_ee
    
    listS.append(total*deltamass)
    listS_EnergyResolution_DOWN.append(total_EnergyResolution_DOWN*deltamass)
    listS_EnergyResolution_UP.append(total_EnergyResolution_UP*deltamass)
    listS_EnergyScale_UP.append(total_EnergyScale_UP*deltamass)
    listS_EnergyScale_DOWN.append(total_EnergyScale_DOWN*deltamass)

    # print("S")
    # print(total*deltamass)
    listCSplot.append(total/Intlum)

  print("total")
  print(total) 

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig(Directory+'Plots/Signal.pdf')
    plt.clf()

  #Return
  return listS, listS_EnergyResolution_DOWN, listS_EnergyResolution_UP, listS_EnergyScale_DOWN, listS_EnergyScale_UP, lum


#Generate signal with no random fluctuations
def SigFix_Generic_Uncertainties(fundamental_freq, xmin, xmax, nBins, Uncertainty):


  Acc_ee = ROOT.TF1("Acc_ee","[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)",xmin, xmax)
  Acc_ee.SetParameter(0,0.79366)
  Acc_ee.SetParameter(1,-234.118)
  Acc_ee.SetParameter(2,61575.1)
  Acc_ee.SetParameter(3,-1.046e7)
  Acc_ee.SetParameter(4,9.63491e8)
  Acc_ee.SetParameter(5,-3.67989e10)
  Acc_ee.SetParameter(6,-9.10565e-6)
  Acc_ee.SetParameter(7,4.221e-10)

  def Smearing(m,Uncertainty):
    if m > xmin:
      scale = str(1)
      # Acc_ee = str(sfAcc_ee) 
      sClockwork = "(RESONANCES*("+(scale)+")"
      sResonances = ""  
      if Uncertainty == "Nominal":
        sClockwork = "((RESONANCES)*("+(scale)+")"
        sResonances = ""  
      if (Uncertainty == "Nominal"):
        mu_Gauss = str(mu_G.Eval(m)*m+m)
        sigma_Guass = str(sigma_G.Eval(m)*m)
        sigma_CrystalBall = str(sigma_CB.Eval(m)*m)
        mu_CrystalBall = str(mu_CB.Eval(m)*m+m)
      if Uncertainty == "EnergyScale_DOWN":
        sigma_Guass = str(sigma_G.Eval(m)*m)
        sigma_CrystalBall = str(sigma_CB.Eval(m)*m)
        mu_Gauss = str(mu_G_DOWNvar.Eval(m)*m+m)
        mu_CrystalBall = str(mu_CB_DOWNvar.Eval(m)*m+m)
      if Uncertainty == "EnergyScale_UP":
        sigma_Guass = str(sigma_G.Eval(m)*m)
        sigma_CrystalBall = str(sigma_CB.Eval(m)*m)
        mu_Gauss = str(mu_G_UPvar.Eval(m)*m+m)
        mu_CrystalBall = str(mu_CB_UPvar.Eval(m)*m+m)
      if Uncertainty == "EnergyResolution_UP":
        sigma_Guass = str(sigma_G_UPvar.Eval(m)*m)
        sigma_CrystalBall = str(sigma_CB_UPvar.Eval(m)*m)
        mu_Gauss = str(mu_G.Eval(m)*m+m)
        mu_CrystalBall = str(mu_CB.Eval(m)*m+m)
      if Uncertainty == "EnergyResolution_DOWN":   
        sigma_Guass = str(sigma_G_DOWNvar.Eval(m)*m)
        sigma_CrystalBall = str(sigma_CB_DOWNvar.Eval(m)*m)
        mu_Gauss = str(mu_G.Eval(m)*m+m)
        mu_CrystalBall = str(mu_CB.Eval(m)*m+m)
      #Crystal_Ball
      alpha_CrystalBall = str(alpha_CB)
      n_CrystalBall = str(n_CB.Eval(m))
      #k
      k_p = str(k_param.Eval(m))
      sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      sClockwork = sClockwork.replace("RESONANCES",sResonances)
      fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
      # y  = ((fClockwork.Eval(m)**2) + (fBreitWigner.Eval(m)**2))**0.5
      y  = fClockwork.Eval(m)
    else:
      y = 0 
    return y


  #Define massList
  massList = np.linspace(xmin, xmax, nBins+1)

  #Apply smearing
  listS      = []
  # listS_Acc_ee_IDefficiency      = []
  # listS_Acc_ee_Iso_efficiency     = []
  listS_EnergyResolution_DOWN     = []
  listS_EnergyResolution_UP      = []
  listS_EnergyScale_UP      = []
  listS_EnergyScale_DOWN      = []


  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    total_EnergyResolution_DOWN      = 0
    total_EnergyResolution_UP       = 0
    total_EnergyScale_UP      = 0
    total_EnergyScale_DOWN      = 0
    mass       = massList[i] + 0.5
    print("mass")
    print(mass)
    deltamass  = massList[i + 1] - massList[i]
    # Eff = fAcc.Eval(mass)

    fAcc_ee = Acc_ee.Eval(mass)
    frequencies = fundamental_freq

    Signal = 1 + np.cos((2*np.pi)*mass*frequencies)
    # Signal = np.cos((mass))
    print("Signal",Signal)

    if Uncertainty == "Nominal":
      total += listCS[particle]*Smearing(listMassKK[particle],particle,mass,listGamma[particle],"Nominal")*listBRee[particle]*Intlum

    if Uncertainty == "All":
      total += Signal*Smearing(mass,"Nominal")
      total_EnergyResolution_DOWN += Signal*Smearing(mass,"EnergyResolution_DOWN")
      total_EnergyResolution_UP += Signal*Smearing(mass,"EnergyResolution_UP")
      total_EnergyScale_UP += Signal*Smearing(mass,"EnergyScale_UP")
      total_EnergyScale_DOWN += Signal*Smearing(mass,"EnergyScale_DOWN")
    
    listS.append(total*deltamass)
    listS_EnergyResolution_DOWN.append(total_EnergyResolution_DOWN*deltamass)
    listS_EnergyResolution_UP.append(total_EnergyResolution_UP*deltamass)
    listS_EnergyScale_UP.append(total_EnergyScale_UP*deltamass)
    listS_EnergyScale_DOWN.append(total_EnergyScale_DOWN*deltamass)


  print("total")
  print(total) 



  #Return
  return listS, listS_EnergyResolution_DOWN, listS_EnergyResolution_UP, listS_EnergyScale_DOWN, listS_EnergyScale_UP

############################################################
################### Signal Uncertainty Processing ##########
############################################################

  #Attempt to make a function that takes in k and M5 values and gives a toy of the ensemble emthod of evaluating uncertainties


def DifferenceMaker(listFit, listToy):

  ResidualArray =[]

  for i in range(0, len(listToy)):
    if listToy[i] != 0:
      z = listToy[i] - listFit[i]
    else:
      z =0
    ResidualArray.append(z)
  return ResidualArray



def Sig_ee_Uncertainty_Toy(Nominal,listSRes_DOWN,listSRes_UP, listSscale_DOWN, listSscale_UP, Bkg, xmin, xmax,i,SearchType):


  # Place list of iosalted variations
  # if Nominal
  if Nominal > listSRes_UP:
    Res_SystUP_diff = DifferenceMaker(listSRes_UP, Nominal)
  if listSRes_UP > Nominal:
    Res_SystUP_diff = DifferenceMaker(Nominal, listSRes_UP)
  if Nominal > listSscale_UP:
    Scale_SystUP_diff = DifferenceMaker(listSscale_UP, Nominal)
  if listSscale_UP>Nominal:
    Scale_SystUP_diff = DifferenceMaker(Nominal, listSscale_UP)
  if Nominal>listSRes_DOWN:
    Res_SystDOWN_diff = DifferenceMaker(listSRes_DOWN, Nominal)
  if listSRes_DOWN>Nominal:
    Res_SystDOWN_diff = DifferenceMaker(Nominal, listSRes_DOWN)
  if Nominal>listSscale_DOWN:
    Scale_SystDOWN_diff = DifferenceMaker(listSscale_DOWN, Nominal)
  if listSscale_DOWN>Nominal:
    Scale_SystDOWN_diff = DifferenceMaker(Nominal, listSscale_DOWN)
  
  # print (Res_SystDOWN_diff)
  def Uncert_diff(diffUP,diffDOWN):

    nuis1 = np.random.normal(0,1,1)
    while abs(nuis1)>1:nuis1 = np.random.normal()
    nuis2 = np.random.normal(0,1,1)
    while abs(nuis2)>1:nuis2 = np.random.normal()

    for x in diffUP,diffDOWN:
      U1 = np.asarray(diffUP)*abs(nuis1)
      U2 =  np.asarray(diffDOWN)*abs(nuis2)

    return U1,U2


# for i in range(nTests):
  UP_Res,DOWN_Res = Uncert_diff(Res_SystUP_diff,Res_SystDOWN_diff)
  UP_Scale,DOWN_Scale = Uncert_diff(Scale_SystUP_diff,Scale_SystDOWN_diff)
  toy1 = Nominal - (UP_Scale + UP_Res)
  toy2 = Nominal + (DOWN_Res + DOWN_Scale)


  if (i % 2 == 0): 
    return abs(toy2)
  else:
    return abs(toy1)  

############################################################
############################################################
################### Backgorund Models ######################
############################################################
############################################################

#Get background with no random fluctuations
def Dielectron_Bkg(xmin, xmax, nBins):

  fbkgfinal = ROOT.TFile(indir_ee+"finalbkg.root","READ")
  fBkg = fbkgfinal.Get("bkgfit")
  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listBkg = []
  
  for i in range(0, len(massList)):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     listBkg.append(y)
     hfit.SetBinContent(i,y)

  return listBkg


#Get background with no random fluctuations
def HighMassDiphoton_Bkg(xmin, xmax, nBins):

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  fbkgfinal = ROOT.TFile(indir_yy+"finalbkg_yy.root","READ")
  fBkg = fbkgfinal.Get("bkgfit")
  
  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listBkg = []
  for i in range(0, len(massList)+1):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x) 
     listBkg.append(y)
     hfit.SetBinContent(i,y)
  # print (len(listBkg))
  listBkg_rounded = np.floor(listBkg)
  return listBkg



#Generate background with no random fluctuations
def BkgFix_Diphoton36fb_Theory(massList):

  massBGlist = []
  BGlist     = []
  listB      = []

  with open(Directory+'Background_36.1/Background.txt') as f:
    for line in f:
      data = line.split()
      massBGlist.append(float(data[0]))
      BGlist.append(float(data[1]))

  for i in range(0, len(massList) - 1):
    mass      = massList[i]
    deltamass = massList[i + 1] - massList[i] 
    listB.append(Background.BackgroundCal(massBGlist, BGlist, mass)/20*deltamass)

  return listB


############################################################
################### Backgorund Uncertainty Models ##########
############################################################





def BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, ToySystNumber):


  fbkgfinal = ROOT.TFile(Directory+"Uncertainties/toys_allUncertainties.root","READ")
  
  Bkg = fbkgfinal.Get("ee_toy_"+str(ToySystNumber))
  Bkg.Rebin(10)
  Bkg.Sumw2()
  # fBkg.GetXaxis().SetRangeUser(225,6000)
  # N = (int(xmax-xmin) ## 1 GeV bins

  bkgfit = ROOT.TF1("bkgfit","[0] * [2]/((x-[1])^2 + [2]^2) * (1-(x/13000)^[3])^[4] * (x/13000)^([5]+[6]*log(x/13000)+[7]*log(x/13000)^2+[8]*log(x/13000)^3)",225,xmax)
  
  bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 1)
  # bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 178000)
  bkgfit.SetParName(1,"m0") 
  bkgfit.FixParameter(1, +91.1876) 
  bkgfit.SetParName(2,"w0"); 
  bkgfit.FixParameter(2, +2.4952) 
  bkgfit.SetParName(3,"c");  
  bkgfit.FixParameter(3, +1)  
  bkgfit.SetParName(4,"b");  
  bkgfit.SetParameter(4, +1.5) 
  bkgfit.SetParName(5,"p0") 
  bkgfit.SetParameter(5, -12.38) 
  bkgfit.SetParName(6,"p1") 
  bkgfit.SetParameter(6, -4.29) 
  bkgfit.SetParName(7,"p2")
  bkgfit.SetParameter(7, -0.919)
  bkgfit.SetParName(8,"p3")
  bkgfit.SetParameter(8, -0.0845)
  bkgfitI = bkgfit.Integral(xmin,xmax)

  f = ROOT.TFile(Directory+"Inputs_ee/mInv_spectrum_combined_ll_rel21_fullRun2.root","READ")
  data = f.Get("mInv_spectrum_201518_ee_1GeV")
  data.SetMinimum(5.e-5)
  dataN = 0
  xup = -1
  xxx = -1
  isfound = False
  # integral0 = data.Integral(1,data.GetNbinsX())
  for b in range(data.GetNbinsX()):
     x = data.GetBinCenter(b)
     if(x<xmin): continue
     if(x>xmax): break
     integralx = data.Integral(b,data.GetNbinsX())
     relerr    = (1/math.sqrt(integralx))*100 if(integralx>0) else 100
     dataN += data.GetBinContent(b)
     if(relerr>=10 and not isfound):
        xxx = x
        isfound = True
  normfactor = dataN/bkgfitI
  print ("normfactor:",normfactor)
  bkgfit.FixParameter(0,normfactor)
  bkgfit.RejectPoint(True)
  bkgfit.SetNumberFitPoints(nBins)
  bkgfit.SetNpx(100000)
  Bkg.Fit("bkgfit","RM","0", 225, xmax)
  # if ToySystNumber == 1:
  #   Bkg.Fit("bkgfit","RM","0", xmin, xmax)
  #Define massList
  toyList = []
  massList = np.linspace(xmin, xmax, nBins)
  htemp = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(5e-5)
  toyHist = Bkg.Clone("fee")
  for i in range(xmin, xmax+1):
    x = toyHist.GetBinCenter(i)
    if(x<xmin): continue
    if(x>xmax): break
    y = bkgfit.Eval(x)

    toyList.append(y)



  # Bkg.Reset()
  return toyList



############################################################################################################
############################################################################################################
#################################### Other functions - Stat toys ###########################################
############################################################################################################
############################################################################################################

#Generate random fluctuations in function using numpy Poission with random generator for given lamda
def BkgPoissonToy(listBkgFixed, aBkg):

  listBkgrandom = []

  for i in range(0, len(listBkgFixed)):
  
    listBkgrandom.append(np.random.poisson(lam = aBkg*listBkgFixed[i]))

  return listBkgrandom


#Generate random fluctuations in function using numpy Poission with random generator for given lamda
def SigBkgPoissonToy(listBkgFixed, listSigFixed, aBkg, aSig):

  listBkgSigrandom = []

  for i in range(0, len(listSigFixed)):
  
    listBkgSigrandom.append(np.random.poisson(lam = aBkg*listBkgFixed[i] + aSig*listSigFixed[i]))

  return listBkgSigrandom

#Combination of nominal signal and background with no variations

def SigBkgFixed(listBkgFixed, listSigFixed):

  listBkgSigFixed = []
  print("listBkgFixed")
  print(len(listBkgFixed))
  print("listSigFixed")
  print(len(listSigFixed))
  for i in range(0, len(listSigFixed)):
    listBkgSigFixed.append(listBkgFixed[i] + listSigFixed[i])

  return listBkgSigFixed


############################################################################################################
############################################################################################################




# #Decide end of window by looking the data - both analyses are unblinded but still use Bkg
def Maximum_Mass_Finder(xstart, data, Intervals, xmin, xmax, nBins,BackgroundType):
 #Define massList
  if (BackgroundType == "Dielectron_Bkg"):
    massList = np.linspace(xmin, xmax, nBins)
  if (BackgroundType == "HighMass_Diphoton_Bkg"):
    massList = np.linspace(xmin, xmax, nBins-1)
  hdata = ROOT.TH1D("hdata","",nBins,xmin,xmax)
  hdata.SetMinimum(1e-5)
  for i in range(0, len(massList)):
    print(i)
    y = data[i]
    hdata.SetBinContent(i,y)
  x0_Error_15percent = []
  x0_Error_10percent = []
  x0_Error_5percent = []
  x0_Error_1percent = []
  dataN = 0
  xup = -1
  xxx = -1
  isfound = False
  # integral0 = data.Integral(1,data.GetNbinsX())
  x0List = np.linspace(xstart,xmax,Intervals)
  for b in range(hdata.GetNbinsX()):
    x = hdata.GetBinCenter(b)
    if(x<xmin): continue
    if(x>xmax): break
    # if(x<xstart): continue
    print("b",b)
    print("x",x)
    if (BackgroundType == "Dielectron_Bkg"):
      integralx = hdata.Integral(b,hdata.GetNbinsX())
    if (BackgroundType == "HighMass_Diphoton_Bkg"):
      integralx = hdata.Integral(b,hdata.GetNbinsX())
    relerr    = (1/math.sqrt(integralx))*100 if(integralx>0) else 100
    print("b,relerr",b,relerr)
    if (relerr > 15):
      print("relerr",relerr)
      x0_Error_15percent.append(x)
    if (relerr > 10):
      print("relerr",relerr)
      x0_Error_10percent.append(x)
    if (relerr > 5):
      print("relerr",relerr)
      x0_Error_5percent.append(x)
    if (relerr > 1):
      print("relerr",relerr)
      x0_Error_1percent.append(x)
      
  return x0_Error_15percent[0], x0_Error_10percent[0],x0_Error_5percent[0], x0_Error_1percent[0]







#Other functions that can be used fot ratios or residuals etc


def RatioMaker(listNumnerator, listDenominator):

  RatioArray =[]

  for i in range(0, len(listDenominator)):
    if listDenominator[i] != 0:
      z = listNumnerator[i]/listDenominator[i]
    else:
      z =0
    RatioArray.append(z)
  return RatioArray

def DifferenceMaker(listFit, listToy):

  ResidualArray =[]

  for i in range(0, len(listToy)):
    if listToy[i] != 0:
      z = listToy[i] - listFit[i]
    else:
      z =0
    ResidualArray.append(z)
  return ResidualArray



#Difference between the Bkg Fit and toy

def BkgFitToyDifference(listBkgFixed, Toy):
  listBkgDiff = []

  for i in range(0, len(Toy)):
    listBkgDiff.append(Toy[i] - listBkgFixed[i])

  return listBkgDiff

############################################################################################################
############################################################################################################
################################ RooFit Functions ##########################################################
############################################################################################################
############################################################################################################

#Generate random fluctatuons using RooFit Generate
def SigBkgRooFitToy(listBkgFixed, listSigFixed, xmin, xmax, nBins, aBkg, aSig):

  massList = np.linspace(xmin, xmax, nBins)

  if (aBkg == 1) and (aSig == 1):
    hbkg = ROOT.TH1D("htemp_ee","",nBins,xmin,xmax)
    hbkg.SetMinimum(1e-2)
    for i in range(0, len(massList) - 1):
      x = hbkg.GetBinCenter(i)
      # if(x<hbkg.GetXmin()): continue
      y = listBkgFixed[i]
      hbkg.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig","",nBins,xmin,xmax)
    hsig.SetMinimum(1e-2)
    for i in range(10, len(massList) - 1):
      x = hsig.GetBinCenter(i)
      # if(x<hsig.GetXmin()): continue
      y = listSigFixed[i]
      hsig.SetBinContent(i,y)
    
    htemp = hbkg.Clone("htemp")
    htemp.Add(hsig,1)

  if (aBkg == 1) and (aSig == 0):

    htemp = ROOT.TH1D("htemp","",nBins,xmin,xmax)
    htemp.SetMinimum(1e-2)
    for i in range(0, len(massList) - 1):
      x = htemp.GetBinCenter(i)
      # if(x<hbkg.GetXmin()): continue
      y = listBkgFixed[i]
      htemp.SetBinContent(i,y)

  if (aBkg == 0) and (aSig == 1):
    htemp = ROOT.TH1D("htemp","",nBins,xmin,xmax)
    htemp.SetMinimum(1e-2)
    for i in range(10, len(massList) - 1):
      x = htemp.GetBinCenter(i)
      # if(x<hsig.GetXmin()): continue
      y = listSigFixed[i]
      htemp.SetBinContent(i,y)

  x = ROOT.RooRealVar("x","x",htemp.GetXaxis().GetXmin(),htemp.GetXaxis().GetXmax())
  x.setBins(htemp.GetNbinsX())
  rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),htemp)
  histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
  n = int(rooHist.sumEntries())
  print ("Going to sample n=%g events" % (n))
  toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
  toy.Print()
## now dump the toy into the new hist:
  toyHist = htemp.Clone("mass_")
  toyHist.Reset()
  entries = toy.numEntries()
  print ("rooHist.sumEntries()=",rooHist.sumEntries())
  for i in range(entries):
    event = toy.get(i)
    var = event.find("x");
    val = var.getVal()
    toyHist.Fill(val)
    toyHist.SetMarkerStyle(20)
    toyHist.SetMarkerSize(1)
    toyHist.SetMarkerColor(ROOT.kBlack)
     # difHist = toyHist.Clone("diff_"+newname)
     # difHist.Add(bkgfithist,-1.)
  # fit = fBkg.Clone("fee")
  toyList = []
  for i in range(1,toyHist.GetNbinsX()+1):
    x = toyHist.GetBinLowEdge(2)
    if(x<toyHist.GetXaxis().GetXmin()): continue
    # y = fit.Eval(x)
    y = toyHist.GetBinContent(i)
    toyList.append(y)

  return toyList


def Fitting_SignalInjection_MCBkg(k, M5, xmin, xmax, ToyType , aSig):



  if (ToyType == "Bkg_Only"):

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
      # xmin = fBkg.GetXmin()
      # xmax = fBkg.GetXmax()
    N = int(xmax-xmin) ## 1 GeV bins

    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    listB = []
    listX =[]
    # for i in range(1,hfit.GetNbinsX()+1):
    for i in range(131,hfit.GetNbinsX()+131):
    # for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # listX.append(x)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       # listB.append(y)
       hfit.SetBinContent(i,y)

    x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
    x.setBins(hfit.GetNbinsX())
    print (hfit.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
      toyHist.SetMarkerStyle(20)
      toyHist.SetMarkerSize(1)
      toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    fit = fBkg.Clone("fee")
    toyList = [] 
    toyListBinError = []
    # for i in range(1,toyHist.GetNbinsX()+1):
    for i in range(131, toyHist.GetNbinsX()+131):
    # for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      y = toyHist.GetBinContent(i)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListBinError.append(z)
    # save to csv file
    # np.savetxt('bkg_RooHistToy_BinValues.csv', toyList, delimiter=',')  
    # np.savetxt('bkg_RooHistToy_BinError.csv', toyListBinError, delimiter=',')  
    # save hist to ROOT file


  if (ToyType == "bkgsig"):

    ### get the resolution with nominal parameters
    fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
    fRes = fPeterRes.Get("relResol_dedicatedTF")
    sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
    for n in range(0,fRes.GetNpar()):
      print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
      sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
    fRes.SetLineColor(ROOT.kRed)

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    # for i in range(1,hfit.GetNbinsX()+1):
    for i in range(131, hfit.GetNbinsX()+131):
    # for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    rooHist_Sig = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    histPdf_Sig = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist_Sig)
    n = int(rooHist.sumEntries())
    n_Sig = int(rooHist_Sig.sumEntries())
    # print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy_Sig = histPdf_Sig.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n_Sig),ROOT.RooFit.AutoBinned(ROOT.kFALSE))

    # toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    toyHist_Sig = hsig.Clone("mass_")
    toyHist_Sig.Reset()
    entries_Sig = toy_Sig.numEntries()
    print ("rooHist.sumEntries()=",rooHist_Sig.sumEntries())
    for i in range(entries_Sig):
      event_Sig = toy_Sig.get(i)
      var_Sig = event_Sig.find("x");
      val_Sig = var_Sig.getVal()
      toyHist_Sig.Fill(val_Sig)
    toyHist.Add(toyHist_Sig,1)

    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    toyListBinError = []
    # for i in range(1,toyHist.GetNbinsX()+1):
    for i in range(131, toyHist.GetNbinsX()+131):
    # for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # print (x)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListBinError.append(z) 
    # # save hist to ROOT file
    # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/bkgsig_Toy.root","RECREATE")
    # fOut.cd()
    # toyHist.Write()
    # fOut.Write()
    # fOut.Close()

  N_MC = int(xmax-xmin) ## 1 GeV bins
  bkgfit_MC = ROOT.TF1("bkgfit_MC","[0] * [2]/((x-[1])^2 + [2]^2) * (1-(x/13000)^[3])^[4] * (x/13000)^([5]+[6]*log(x/13000)+[7]*log(x/13000)^2+[8]*log(x/13000)^3)",xmin,xmax)
  bkgfit_MC.SetParName(0,"N");  bkgfit_MC.SetParameter(0, 1)
  # bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 178000) #FixParameter above?
  bkgfit_MC.SetParName(1,"m0"); bkgfit_MC.SetParameter(1, +91.1876); bkgfit_MC.SetParLimits(1, 0,+150)
  bkgfit_MC.SetParName(2,"w0"); bkgfit_MC.SetParameter(2, +2.4952); bkgfit_MC.SetParLimits(2, -10,15)
  bkgfit_MC.SetParName(3,"c");  bkgfit_MC.SetParameter(3, +1) ; bkgfit_MC.SetParLimits(3, -11,10)
  bkgfit_MC.SetParName(4,"b");  bkgfit_MC.SetParameter(4, +1.5); bkgfit_MC.SetParLimits(4, -10,15)
  bkgfit_MC.SetParName(5,"p0"); bkgfit_MC.SetParameter(5, -12.38); bkgfit_MC.SetParLimits(5, -100,110)
  bkgfit_MC.SetParName(6,"p1"); bkgfit_MC.SetParameter(6, -4.29); bkgfit_MC.SetParLimits(6, -100,150)
  bkgfit_MC.SetParName(7,"p2"); bkgfit_MC.SetParameter(7, -0.919); bkgfit_MC.SetParLimits(7, -10,15)
  bkgfit_MC.SetParName(8,"p3"); bkgfit_MC.SetParameter(8, -0.0845); bkgfit_MC.SetParLimits(8, -10,15)
  bkgfit_MC_I = bkgfit_MC.Integral(xmin,xmax)
  print ("MC Integral:",bkgfit_MC_I)


  n = int(xmax-xmin) ## 1 GeV bins
  htemp = ROOT.TH1D("htemp_ee","",n,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(5e-5)
  MC_N = 0
  xup = -1
  xxx = -1
  isfound = False
  print (MC.GetNbinsX())
  for b in range(toyHist.GetNbinsX()):
    x = toyHist.GetBinCenter(b)
    integralx = toyHist.Integral(b,toyHist.GetNbinsX())
    MC_N += toyHist.GetBinContent(b)
    # htemp.SetBinContent(b-130,MC_N_Sig_perBin)
 
  
  normfactor_MC = MC_N/bkgfit_MC_I

  print ("MC normfactor:",normfactor_MC)
  bkgfit_MC.FixParameter(0,normfactor_MC)
  toyHist.Fit("bkgfit_MC","R", "0", 130, 6000)
  FittedToyList =[]
  bkghist_MC = toyHist.Clone("bkghist")
  bkghist_MC.Reset()
  # for b in range(1, toyHist.GetNbinsX()+1):
  for b in range(131, toyHist.GetNbinsX()+131):
  # for b in range(261, toyHist.GetNbinsX()+261):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    y = bkgfit_MC.Eval(x)
    FittedToyList.append(y)
    bkghist_MC.SetBinContent(b,y)


  return FittedToyList, toyList, toyListBinError



def MC_Fitting_PoissonToy(k, M5, xmin, xmax, ToyType , aSig):

  BkgFix = BkgFixed(130, 6000)
  htemp = ROOT.TH1D("htemp_ee","",5870,130,6000)
  Bkgroot = rn.array2hist(BkgFix,htemp,errors=None)

  if (ToyType == "Bkg_Only"):

    #need something here to convert to numpy to root files

    n = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    listB = []
    listX =[]
    # for i in range(131,hfit.GetNbinsX()+131):
    for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # listX.append(x)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       # listB.append(y)
       hfit.SetBinContent(i,y)

    x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
    x.setBins(hfit.GetNbinsX())
    print (hfit.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
      toyHist.SetMarkerStyle(20)
      toyHist.SetMarkerSize(1)
      toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    fit = fBkg.Clone("fee")
    toyList = [] 
    toyListError = []
    # for i in range(131, toyHist.GetNbinsX()+131):
    for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      y = toyHist.GetBinContent(i)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListError.append(z)



  if (ToyType == "bkgsig"):

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    # for i in range(131, hfit.GetNbinsX()+131):
    for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfitsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfitsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    toyListError = []
    # for i in range(131, toyHist.GetNbinsX()+131):
    for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # print (x)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListError.append(z)

  N_MC = int(xmax-xmin) ## 1 GeV bins
  bkgfit_MC = ROOT.TF1("bkgfit_MC","[0] * [2]/((x-[1])^2 + [2]^2) * (1-(x/13000)^[3])^[4] * (x/13000)^([5]+[6]*log(x/13000)+[7]*log(x/13000)^2+[8]*log(x/13000)^3)",xmin,xmax)
  bkgfit_MC.SetParName(0,"N");  bkgfit_MC.SetParameter(0, 1)
  # bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 178000) #FixParameter above?
  bkgfit_MC.SetParName(1,"m0"); bkgfit_MC.SetParameter(1, +91.1876); bkgfit_MC.SetParLimits(1, 0,+150)
  bkgfit_MC.SetParName(2,"w0"); bkgfit_MC.SetParameter(2, +2.4952); bkgfit_MC.SetParLimits(2, -10,15)
  bkgfit_MC.SetParName(3,"c");  bkgfit_MC.SetParameter(3, +1) ; bkgfit_MC.SetParLimits(3, -11,10)
  bkgfit_MC.SetParName(4,"b");  bkgfit_MC.SetParameter(4, +1.5); bkgfit_MC.SetParLimits(4, -10,15)
  bkgfit_MC.SetParName(5,"p0"); bkgfit_MC.SetParameter(5, -12.38); bkgfit_MC.SetParLimits(5, -100,110)
  bkgfit_MC.SetParName(6,"p1"); bkgfit_MC.SetParameter(6, -4.29); bkgfit_MC.SetParLimits(6, -100,150)
  bkgfit_MC.SetParName(7,"p2"); bkgfit_MC.SetParameter(7, -0.919); bkgfit_MC.SetParLimits(7, -10,15)
  bkgfit_MC.SetParName(8,"p3"); bkgfit_MC.SetParameter(8, -0.0845); bkgfit_MC.SetParLimits(8, -10,15)

  bkgfit_MC_I = bkgfit_MC.Integral(xmin,xmax)
  print ("MC Integral:",bkgfit_MC_I)


  n = int(xmax-xmin) ## 1 GeV bins
  htemp = ROOT.TH1D("htemp_ee","",n,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(5e-5)
  MC_N = 0
  xup = -1
  xxx = -1
  isfound = False
  print (MC.GetNbinsX())
  for b in range(toyHist.GetNbinsX()):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    # print (x)
    # if(x<xmin): continue
    # if(x>xmax): break
    integralx = toyHist.Integral(b,toyHist.GetNbinsX())
    MC_N += toyHist.GetBinContent(b)
    # htemp.SetBinContent(b-130,MC_N_Sig_perBin)
 
      

  normfactor_MC = MC_N/bkgfit_MC_I

  print ("MC normfactor:",normfactor_MC)
  # Htemp = Bkghist_MCMaker_Sig_Prefit_WithSignal(bkgfit, 1, ROOT.kYellow+1)
  # bkgfit_MC.SetParameter(0,normfactor_MC_Sig)
  bkgfit_MC.FixParameter(0,normfactor_MC)
  toyHist.Fit("bkgfit_MC","R", "0", 130, 1700)
  # bkgfit_MC_Sig.Write("bkgsig_MC1")
  # InputToy_MC_FitResult = Bkghist_MCMaker_Sig(bkgfit_MC_Sig, ROOT.kOrange)
  # FittedToyList =[]
  # # hist_MC = toyHist.Clone("hist")
  # # hist_MC.Reset()
  # for b in range(toyHist.GetNbinsX()):
  #    x = toyHist.GetBinCenter(b)
  #    # print (x)
  #    # if(x<xmin): continue
  #    # if(x>xmax): break
  #    y = bkgfit_MC.Eval(x)
  #    FittedToyList.append(y)
  #    toyHist.SetBinContent(b,y)
  #    toyHist.SetLineColor(ROOT.kRed)

  FittedToyList =[]
  bkghist_MC = toyHist.Clone("bkghist")
  bkghist_MC.Reset()
  # for b in range(131, toyHist.GetNbinsX()+131):
  for b in range(261, toyHist.GetNbinsX()+261):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    y = bkgfit_Sig.Eval(x)
    FittedToyList.append(y)
    bkghist_MC.SetBinContent(b,y)

  # MC_FitResult = Bkghist_MCMaker_Sig(toyHist, bkgfit_MC)

  return MC_FitResult, toyList, toyListError






############################################################################################################
############################################################################################################
############################################################################################################

