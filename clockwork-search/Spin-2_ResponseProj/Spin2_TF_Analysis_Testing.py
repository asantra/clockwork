#!/usr/bin/env python
import os, sys
import ROOT
# from ROOT import *
import math
import array
from array import *
# import ROOT
import numpy as np
#Standard
import matplotlib
matplotlib.use("Agg")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)


# Generate example of signal model using full Resonant TF shape instead of Guassian, then lets use it to plot a peak og spin2/1 central mass points
#First define the individual components

Nbins = 200 #2470
xmin = -0.2 #230
xmax = 0.1 #2700

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)



sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)


alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin, xmax)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)


#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
# mu_G = ROOT.TF1("mu_G","[0]+[1]*(x^-1) + [2]*x+[3]*log(x^3)+[4]*((x^2)^-1)+[5]*(x^2)", xmin, xmax)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)


sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)


#K-Parameters

k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin, xmax)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)
 
########################################################################
########################################################################
########################################################################

#Up-variation Crystal Ball Parameters

#energy scale
mu_CB_UPvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB_UPvar.SetParameter(0, 0.143853)
mu_CB_UPvar.SetParameter(1, -0.544555)
mu_CB_UPvar.SetParameter(2, -0.00897897)
mu_CB_UPvar.SetParameter(3, 0.000000211052)
# mu_CB_UPvartest = rn.evaluate(mu_CB_UPvar,massList)

#energy resolution
sigma_CB_UPvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_UPvar.SetParameter(0, 0.0168464)
sigma_CB_UPvar.SetParameter(1, 0.244998)
sigma_CB_UPvar.SetParameter(2, 0.607754)
# sigma_CB_UPvartest = rn.evaluate(sigma_CB,massList)

#Up-variation Gaussian

#energy scale
mu_G_UPvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
mu_G_UPvar.SetParameter(0, 0.00437644)
mu_G_UPvar.SetParameter(1, -0.693393)
mu_G_UPvar.SetParameter(2, -0.00000124588)
mu_G_UPvar.SetParameter(3, 0.00000994365)
mu_G_UPvar.SetParameter(4, 3.85433)
mu_G_UPvar.SetParameter(5, 0.0000000000681002)
# mu_G_UPvartest = rn.evaluate(mu_G_UPvar,massList)

#energy resolution
sigma_G_UPvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_UPvar.SetParameter(0, 0.00968273)
sigma_G_UPvar.SetParameter(1, 0.110266)
sigma_G_UPvar.SetParameter(2, 1.0617)
# sigma_G_UPvartest = rn.evaluate(sigma_G_UPvar,massList)

########################################################################

#Down-variation Crystal Ball Parameters

#energy scale
mu_CB_DOWNvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB_DOWNvar.SetParameter(0, 0.125578)
mu_CB_DOWNvar.SetParameter(1, -0.350447)
mu_CB_DOWNvar.SetParameter(2, -0.0144995)
mu_CB_DOWNvar.SetParameter(3, 0.00000373014)
# mu_CB__DOWNvartest = rn.evaluate(mu_CB_DOWNvar,massList)

#energy resolution
sigma_CB_DOWNvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_DOWNvar.SetParameter(0, 0.011107)
sigma_CB_DOWNvar.SetParameter(1, 0.22346)
sigma_CB_DOWNvar.SetParameter(2, 1.92675)
# sigma_CB_DOWNvartest = rn.evaluate(sigma_CB_DOWNvar,massList)

#DOWN-variation Gaussian

#energy scale
mu_G_DOWNvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
mu_G_DOWNvar.SetParameter(0, -0.00743235)
mu_G_DOWNvar.SetParameter(1, 0.650265)
mu_G_DOWNvar.SetParameter(2, 0.000000574646)
mu_G_DOWNvar.SetParameter(3, -0.00000498561)
mu_G_DOWNvar.SetParameter(4, 28.7656)
mu_G_DOWNvar.SetParameter(5, -0.0000000000452119)
# mu_G_DOWNvartest = rn.evaluate(mu_G_DOWNvar,massList)

#energy resolution
sigma_G_DOWNvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_DOWNvar.SetParameter(0, 0.00337693)
sigma_G_DOWNvar.SetParameter(1, 0.112002)
sigma_G_DOWNvar.SetParameter(2, 1.2219)
# sigma_G_DOWNvartest = rn.evaluate(sigma_G_DOWNvar,massList)

def TF_DYee_Nominal(M):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee750","(([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.SetParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.SetParameter(2, n_CB.Eval(M))
  TF_DYee.SetParameter(3, sigma_CB.Eval(M))
  TF_DYee.SetParameter(4, mu_CB.Eval(M))
  TF_DYee.SetParameter(5, mu_G.Eval(M))
  TF_DYee.SetParameter(6, sigma_G.Eval(M))
  # TF_DYee.Clear()
  return TF_DYee

def TF_DYee_DOWNVariation(M):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee750","([7]*([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.FixParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.FixParameter(2, n_CB.Eval(M))
  TF_DYee.FixParameter(3, sigma_CB_DOWNvar.Eval(M))
  TF_DYee.FixParameter(4, mu_CB_DOWNvar.Eval(M))
  TF_DYee.FixParameter(5, mu_G_DOWNvar.Eval(M))
  TF_DYee.FixParameter(6, sigma_G_DOWNvar.Eval(M))
  # TF_DYee.SetParameter(7, N)
  # /TF_DYee.SetParLimits(7,0,1)
  # TF_DYee.Clear()
  return TF_DYee


def TF_DYee_UPVariation(M):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee750","([7]*([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.FixParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.FixParameter(2, n_CB.Eval(M))
  TF_DYee.FixParameter(3, sigma_CB_UPvar.Eval(M))
  TF_DYee.FixParameter(4, mu_CB_UPvar.Eval(M))
  TF_DYee.FixParameter(5, mu_G_UPvar.Eval(M))
  TF_DYee.FixParameter(6, sigma_G_UPvar.Eval(M))
  # TF_DYee.SetParameter(7, N)
  # /TF_DYee.SetParLimits(7,0,1)
  # TF_DYee.Clear()
  return TF_DYee

def TF_DYee(M,N):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee","([7]*([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.FixParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.FixParameter(2, n_CB.Eval(M))
  TF_DYee.FixParameter(3, sigma_CB.Eval(M))
  TF_DYee.FixParameter(4, mu_CB.Eval(M))
  TF_DYee.FixParameter(5, mu_G.Eval(M))
  TF_DYee.FixParameter(6, sigma_G.Eval(M))
  TF_DYee.SetParameter(7, N)
  # /TF_DYee.SetParLimits(7,0,1)
  # TF_DYee.Clear()
  return TF_DYee


def TF_DYeeG(M,N):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee750","([7]*([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.SetParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.SetParameter(2, n_CB.Eval(M))
  TF_DYee.SetParameter(3, sigma_CB.Eval(M))
  TF_DYee.SetParameter(4, mu_CB.Eval(M))
  TF_DYee.SetParameter(5, mu_G.Eval(M))
  TF_DYee.SetParameter(6, sigma_G.Eval(M))
  TF_DYee.SetParameter(7, N)
  # TF_DYee.SetParLimits(7,0,1)
  # TF_DYee.Clear()
  return TF_DYee

# def TF_DYee_NormSum(M):
#   Guass =  ROOT.TF1("Guass","TMath::Gaus(x,[0],[1],1)",xmin, xmax)
#   CB = ROOT.TF1("CB","ROOT::Math::crystalball_pdf(x, [0], [1],[2],[3])",xmin, xmax)
#   k_1 =  k_param.Eval(M)
#   # k_2 =  (1-k_param.Eval(M))
#   # CB.FixParameter(0, alpha_CB)
#   # CB.FixParameter(1, n_CB.Eval(M))
#   # CB.FixParameter(2, sigma_CB.Eval(M))
#   # CB.FixParameter(3, mu_CB.Eval(M))
#   # Guass.FixParameter(0, mu_G.Eval(M))
#   # Guass.FixParameter(1, sigma_G.Eval(M))
#   CB.SetParName(0,"alpha_CB")
#   CB.SetParName(1,"n_CB")
#   CB.SetParName(2,"sigma_CB")
#   CB.SetParName(3,"mu_CB")
#   Guass.SetParName(0,"mu_G")
#   Guass.SetParName(1,"sigma_G")
#   TF_DYeeNorm =  ROOT.TF1NormSum(CB,Guass,k_1,(1-k_1),0.0015)
#   TF_DYee =  ROOT.TF1("TF_DYee",TF_DYeeNorm,xmin, xmax,TF_DYeeNorm.GetNpar())
#   pars = TF_DYeeNorm.GetParameters()
#   TF_DYee.SetParameters( pars.data() )
#   TF_DYee.SetParName(0,"k_1")
#   # TF_DYee.SetParName(1,"k_2")
#   for i in range(1,TF_DYee.GetNpar()):
#     TF_DYee.SetParName(i,TF_DYeeNorm.GetParName(i) )
#   for i in range(0,TF_DYee.GetNpar()):
#     print(TF_DYee.GetParName(i),TF_DYee.GetParameter(i))
#   TF_DYee.FixParameter(2, alpha_CB)
#   TF_DYee.FixParameter(3, n_CB.Eval(M))
#   TF_DYee.FixParameter(4, sigma_CB.Eval(M))
#   TF_DYee.FixParameter(5, mu_CB.Eval(M))
#   TF_DYee.FixParameter(6, mu_G.Eval(M))
#   TF_DYee.FixParameter(7, sigma_G.Eval(M))

#   return TF_DYee

# def TF_Gee_NormSum(M):
#   Guass =  ROOT.TF1("Guass","TMath::Gaus(x,[0],[1],1)",xmin, xmax)
#   CB = ROOT.TF1("CB","ROOT::Math::crystalball_pdf(x, [0], [1],[2],[3])",xmin, xmax)
#   k_1 =  k_param.Eval(M)
#   # k_2 =  (1-k_param.Eval(M))
#   # CB.FixParameter(0, alpha_CB)
#   # CB.SetParameter(1, n_CB.Eval(M))
#   # CB.SetParameter(2, sigma_CB.Eval(M))
#   # CB.SetParameter(3, mu_CB.Eval(M))
#   # Guass.SetParameter(0, mu_G.Eval(M))
#   # Guass.SetParameter(1, sigma_G.Eval(M))
#   CB.SetParName(0,"alpha_CB")
#   CB.SetParName(1,"n_CB")
#   CB.SetParName(2,"sigma_CB")
#   CB.SetParName(3,"mu_CB")
#   Guass.SetParName(0,"mu_G")
#   Guass.SetParName(1,"sigma_G")
#   TF_DYeeNorm =  ROOT.TF1NormSum(CB,Guass,k_1,(1-k_1),1)
#   TF_DYee =  ROOT.TF1("TF_DYee",TF_DYeeNorm,xmin, xmax,TF_DYeeNorm.GetNpar())
#   pars = TF_DYeeNorm.GetParameters()
#   TF_DYee.SetParameters( pars.data() )
#   TF_DYee.SetParName(0,"k_1")
#   # TF_DYee.SetParName(1,"k_2")
#   for i in range(1,TF_DYee.GetNpar()):
#     TF_DYee.SetParName(i,TF_DYeeNorm.GetParName(i) )
#   for i in range(0,TF_DYee.GetNpar()):
#     print(TF_DYee.GetParName(i),TF_DYee.GetParameter(i))
#   TF_DYee.FixParameter(2, alpha_CB)
#   TF_DYee.FixParameter(3, n_CB.Eval(M))
#   TF_DYee.FixParameter(4, sigma_CB.Eval(M))
#   TF_DYee.FixParameter(5, mu_CB.Eval(M))
#   TF_DYee.FixParameter(6, mu_G.Eval(M))
#   TF_DYee.FixParameter(7, sigma_G.Eval(M))
#   return TF_DYee
  
def ConfidenceIntervals(hist,tf1,colour,opacity):
  hint = ROOT.TH1D("hint",  ";m_{ee} [GeV];Events",200, xmin, xmax)
  ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(hint)
  hint.SetStats(False)
  hint.SetFillColorAlpha(colour, opacity)
  # hint.SetFillColor(colour)
  return hint


# def TF_DYee_Custom(InputHisto,xmin,xmax,TF_DYee_Nominal,M):
#   # TF_DYee.Clear()
#   TF_DYee =  ROOT.TF1("TF_DYee750","(([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
#   TF_DYee.SetParameter(0,k_param.Eval(M))
#   TF_DYee.FixParameter(1, alpha_CB)
#   TF_DYee.SetParameter(2, n_CB.Eval(M))
#   TF_DYee.SetParameter(3, sigma_CB.Eval(M))
#   TF_DYee.SetParameter(4, mu_CB.Eval(M))
#   TF_DYee.SetParameter(5, mu_G.Eval(M))
#   TF_DYee.SetParameter(6, sigma_G.Eval(M))
#   opt = ROOT.Fit.DataOptions()
#   opt.fIntegral = True 
#   range = ROOT.Fit.DataRange(xmin,xmax)
#   data = ROOT.Fit.BinData(opt,range)
#   # InputHisto = ROOT.TH1D("InputHisto","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, xmin, xmax)
#   ROOT.Fit.FillData(data,InputHisto,TF_DYee)
#   fitFunction = ROOT.Math.WrappedMultiTF1(TF_DYee)
#   fitter = ROOT.Fit.Fitter()
#   fitter.SetFunction(fitFunction)
#   # fitter.SetFunction(fitFunction, False)
#   # opt.ROOT.Math.MinimizerOptions() 
#   # opt.Print()
#   result = fitter.Fit(data)
#   test = fitter.Result()
#   test2 = ROOT.Fit.FitResult(test)

#   return test2


#################################################
########################################################################

#Generate signal with no random fluctuations
def Smearing(M):
  scale = str(1)
  sClockwork = "("+(scale)+" * (RESONANCES) )"
  sResonances = ""  
  #Gaussain
  mu_Gauss = str(mu_G.Eval(M))
  sigma_Guass = str(sigma_G.Eval(M))
  #Crystal_Ball
  alpha_CrystalBall = str(alpha_CB)
  n_CrystalBall = str(n_CB.Eval(M))
  sigma_CrystalBall = str(sigma_CB.Eval(M))
  mu_CrystalBall = str(mu_CB.Eval(M))
  # #k
  k_p = str(k_param.Eval(M))

  sResonances = "(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
  print (f"Tower Formula n==1:{sResonances}")
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,-0.20,0.1)
  hClockwork = ROOT.TH1F("hTF"+str(M)+"","",200,-0.2,0.1)
  # hClockwork_f = ROOT.TH1F("hClockwork","hClockwork",Nbins,-0.2,0.1)
  y  = fClockwork.Eval(M)

  for i in range(1,200+1):
     x = hClockwork.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     # hClockwork.SetBinContent(i,y)
     hClockwork.SetBinContent(i,y)

  return hClockwork, fClockwork


########################################################################
########################################################################


def GetLogBinning(nbins,xmin,xmax):
   logmin  = math.log10(xmin)
   logmax  = math.log10(xmax)
   logbinwidth = (logmax-logmin)/nbins
   # Bin edges in GeV
   xbins = [xmin,] #the lowest edge first
   for i in range(1,nbins+1):
      xbins.append( ROOT.TMath.Power( 10,(logmin + i*logbinwidth) ) )
   arrxbins = array("d", xbins)
   return nbins, arrxbins

nbins_mee_log40, bins_mee_log40 = GetLogBinning(100,130,6000)# 40,130,3130 default

def GetXsecWeight(event,sample):
   if(event.xsec>0):
      return 1./(event.Nevt/(event.xsec*1e6)/event.geneff)
   else:
      # print ("ERROR: event.xsec=%g for sample=%s" % (event.xsec,sample))
      # print ("       quitting!")
      # quit()
    return 1
# pass_preor_ee to pass_only_preor_ee everywhere
def GetLeptonSfWeight(event,channel):
   SF = 1.
   if("ee" in channel and event.pass_only_preor_ee==1): SF = event.el_preor_SF[0]*event.el_preor_SF[1]
   if("uu" in channel and event.pass_only_preor_uu==1): SF = event.mu_preor_SF[0]*event.mu_preor_SF[1]
   if("eu" in channel and event.pass_only_preor_eu==1): SF = event.el_preor_SF[0]*event.mu_preor_SF[0]
   if(SF<=0):
      #print "WARNING (GetLeptonSfWeight): SF=0 in channel=",channel
      SF = 1
   return SF

def GetJetSfWeight(event,channel):
   SF = 1.
   if("0b" in channel or "1b" in channel or "2b" in channel): SF = event.btag_signal_jets_SF
   if(SF<=0):
      #print "WARNING (GetJetSfWeight): SF=0 in channel=",channel
      SF = 1
   return SF

def GetTriggerSfWeight(event,channel):
   SF = 1.
   if("ee" in channel and event.pass_only_preor_ee==1): SF = event.trig_ee_SF
   if("uu" in channel and event.pass_only_preor_uu==1): SF = event.trig_uu_SF
   if("eu" in channel and event.pass_only_preor_eu==1): SF = event.trig_eu_SF
   if(SF<=0):
      #print "WARNING (GetTriggerSfWeight): SF=0 in channel=",channel
      SF = 1.
   return SF


def GetWeight(event,channel,sample):
   if(sample=="data"): return 1
   wgt_mc = event.mconly_weight
   wgt_pu = event.pu_weight/event.mconly_weight  ## pu_weight is multiplied by mconly_weight in ST :S
   wgt_kf = event.kF_weight
   wgt_xs = GetXsecWeight(event,sample)
   wgt_lsf = GetLeptonSfWeight(event,channel)
   wgt_jsf = GetJetSfWeight(event,channel)
   wgt_tsf = GetTriggerSfWeight(event,channel)
   wgt = 1.
   wgt *= wgt_xs
   wgt *= wgt_kf
   #wgt *= wgt_mc
   wgt *= wgt_lsf
   #wgt *= wgt_jsf
   wgt *= wgt_tsf
   if(wgt_pu>1.e1): wgt /= wgt_pu ### TODO --> tepmporary hack!!!
   return wgt

wgt = GetWeight

#Define all the samples:

sample = "305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750""305551.Pythia8EvtGen_A14NNPDF23LO_Gee_02_750""305552.Pythia8EvtGen_A14NNPDF23LO_Gee_03_750""305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000""305554.Pythia8EvtGen_A14NNPDF23LO_Gee_02_1000""305555.Pythia8EvtGen_A14NNPDF23LO_Gee_03_1000""305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000""305557.Pythia8EvtGen_A14NNPDF23LO_Gee_02_2000""305558.Pythia8EvtGen_A14NNPDF23LO_Gee_03_2000""305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000""305560.Pythia8EvtGen_A14NNPDF23LO_Gee_02_3000""305561.Pythia8EvtGen_A14NNPDF23LO_Gee_03_3000""305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000""305563.Pythia8EvtGen_A14NNPDF23LO_Gee_02_4000""305564.Pythia8EvtGen_A14NNPDF23LO_Gee_03_4000""305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000""305566.Pythia8EvtGen_A14NNPDF23LO_Gee_02_5000""305567.Pythia8EvtGen_A14NNPDF23LO_Gee_03_5000"
sample_Z = "301215.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000""301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000""301217.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi4000""301218.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi5000"

#mc16a Graviton_ee samples
chain_750_1 = ROOT.TChain("nominal")
chain_1000_1 = ROOT.TChain("nominal")
chain_2000_1 = ROOT.TChain("nominal")
chain_3000_1 = ROOT.TChain("nominal")
chain_4000_1 = ROOT.TChain("nominal")
chain_5000_1 = ROOT.TChain("nominal")

# # Gee MC chain
#750 GeV Mass Point for all periods and K/M =1
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
#1000 GeV
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
#2000 GeV
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
#3000 GeV
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
#4000 GeV
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
#5000 GeV
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")


#Make ROOT file containing relevant histos for either MC or Data

G_ee_Response = ROOT.TFile.Open("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Response_plots/TestTFSpin2.root","RECREATE")


#Make histograms for all mass points

G_ee_750_Truth_1 = ROOT.TH1D("G_ee_750_Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_750_Reco_1 = ROOT.TH1D("G_ee_750_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_750_Response_1 = ROOT.TH1D("G_ee_750_Response_1",";m_{ee} [GeV];Events",200, -0.2, 0.1)

G_ee_1000_Truth_1 = ROOT.TH1D("G_ee_1000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_1000_Reco_1 = ROOT.TH1D("G_ee_1000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_1000_Response_1 = ROOT.TH1D("G_ee_1000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_2000_Truth_1 = ROOT.TH1D("G_ee_2000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_2000_Reco_1 = ROOT.TH1D("G_ee_2000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_2000_Response_1 = ROOT.TH1D("G_ee_2000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_3000_Truth_1 = ROOT.TH1D("G_ee_3000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_3000_Reco_1 = ROOT.TH1D("G_ee_3000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_3000_Response_1 = ROOT.TH1D("G_ee_3000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_4000_Truth_1 = ROOT.TH1D("G_ee_4000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_4000_Reco_1 = ROOT.TH1D("G_ee_4000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_4000_Response_1 = ROOT.TH1D("G_ee_4000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_5000_Truth_1 = ROOT.TH1D("G_ee_5000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_5000_Reco_1 = ROOT.TH1D("G_ee_5000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_5000_Response_1 = ROOT.TH1D("G_ee_5000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

#make event loop function to process each chainlater as it is called

def Response_EventLoop(chain,truth_hist,reco_hist,response_hist,samples):
  n=1
  for event in chain:  #t for Single File
    # wgtee = GetWeight(event,"ee")
    if event.pass_only_ee:
      wgtee = GetWeight(event,"ee",samples)
      # print(wgtee)
      truth_hist.Fill(event.truth_Z_forTF_m/1000,wgtee)
      reco_hist.Fill(event.m_ee,wgtee)
      response_hist.Fill((event.m_ee - (event.truth_Z_forTF_m/1000))/(event.truth_Z_forTF_m/1000),wgtee)
      n+=1
  return truth_hist,reco_hist,response_hist

def Response_EventLoop_ResponseOnly(chain,response_hist,samples):
  n=1
  for event in chain:  #t for Single File
    # wgtee = GetWeight(event,"ee")
    if event.pass_only_ee:
      wgtee = GetWeight(event,"ee",samples)
      # print(wgtee)
      # truth_hist.Fill(event.truth_Z_forTF_m/1000,wgtee)
      # reco_hist.Fill(event.m_ee,wgtee)
      response_hist.Fill((event.m_ee - (event.truth_Z_forTF_m/1000))/(event.truth_Z_forTF_m/1000),wgtee)
      n+=1
  return response_hist

#Use event loop function to get response for all mass points
G_ee_750_truth_hist, G_ee_750_reco_hist, G_ee_750_response_hist = Response_EventLoop(chain_750_1, G_ee_750_Truth_1, G_ee_750_Reco_1, G_ee_750_Response_1, sample)
G_ee_1000_truth_hist, G_ee_1000_reco_hist, G_ee_1000_response_hist = Response_EventLoop(chain_1000_1, G_ee_1000_Truth_1, G_ee_1000_Reco_1, G_ee_1000_Response_1, sample)
G_ee_2000_truth_hist, G_ee_2000_reco_hist, G_ee_2000_response_hist = Response_EventLoop(chain_2000_1, G_ee_2000_Truth_1, G_ee_2000_Reco_1, G_ee_2000_Response_1, sample)
G_ee_3000_truth_hist, G_ee_3000_reco_hist, G_ee_3000_response_hist = Response_EventLoop(chain_3000_1, G_ee_3000_Truth_1, G_ee_3000_Reco_1, G_ee_3000_Response_1, sample)
G_ee_4000_truth_hist, G_ee_4000_reco_hist, G_ee_4000_response_hist = Response_EventLoop(chain_4000_1, G_ee_4000_Truth_1, G_ee_4000_Reco_1, G_ee_4000_Response_1, sample)
G_ee_5000_truth_hist, G_ee_5000_reco_hist, G_ee_5000_response_hist = Response_EventLoop(chain_5000_1, G_ee_5000_Truth_1, G_ee_5000_Reco_1, G_ee_5000_Response_1, sample)

#Let us use the TFs to evalute the function at the G_ee Mass points
hTF750, fTF750 = Smearing(750)
hTF1000, fTF1000 = Smearing(1000)
hTF2000, fTF2000 = Smearing(2000)
hTF3000, fTF3000 = Smearing(3000)
hTF4000, fTF4000 = Smearing(4000)
hTF5000, fTF5000 = Smearing(5000)

#Ensure sum of weights sqaured is used
G_ee_750_response_hist.Sumw2()
G_ee_1000_response_hist.Sumw2()
G_ee_2000_response_hist.Sumw2()
G_ee_3000_response_hist.Sumw2() 
G_ee_4000_response_hist.Sumw2()
G_ee_5000_response_hist.Sumw2() 
hTF750.Sumw2()
hTF1000.Sumw2()
hTF2000.Sumw2()
hTF3000.Sumw2() 
hTF4000.Sumw2()
hTF5000.Sumw2() 

#Consrtruct all the fits and make tuple to hold Gee Fits

Gee_Fits_list = []

#Gee 750 fit
TF_DYee750G = TF_DYeeG(750,1)
# TF_DYee750G = TF_Gee_NormSum(750)
TF_DYee750G.SetNormalized(True)
TF_DYee750G.RejectPoint(True)
TF_DYee750G.SetNumberFitPoints(20000)
TF_DYee750G.SetNpx(10000)
TF_DYee750G.SetLineColor(6)
#TF 750 fit
TF_DYee750 = TF_DYee(750,0.0015)
TF_DYee750.SetNormalized(True)
TF_DYee750.RejectPoint(True)
TF_DYee750.SetNumberFitPoints(20000)
TF_DYee750.SetNpx(10000)
TF_DYee750.SetLineColor(7)
#Gee 1000 fit
TF_DYee1000G = TF_DYeeG(1000,1)
TF_DYee1000G.SetNormalized(True)
TF_DYee1000G.RejectPoint(True)
TF_DYee1000G.SetNumberFitPoints(20000)
TF_DYee1000G.SetNpx(10000)
TF_DYee1000G.SetLineColor(6)
#TF 1000 fit
TF_DYee1000 = TF_DYee(1000,1.4048)
TF_DYee1000.SetNormalized(True)
TF_DYee1000.RejectPoint(True)
TF_DYee1000.SetNumberFitPoints(20000)
TF_DYee1000.SetNpx(10000)
TF_DYee1000.SetLineColor(7)
#Gee 2000 fit
TF_DYee2000G = TF_DYeeG(2000,1.24644)
TF_DYee2000G.SetNormalized(True)
TF_DYee2000G.RejectPoint(True)
TF_DYee2000G.SetNumberFitPoints(20000)
TF_DYee2000G.SetNpx(10000)
TF_DYee2000G.SetLineColor(6)
#TF 2000 fit
TF_DYee2000 = TF_DYee(2000,1.00001)
TF_DYee2000.SetNormalized(True)
TF_DYee2000.RejectPoint(True)
TF_DYee2000.SetNumberFitPoints(20000)
TF_DYee2000.SetNpx(10000)
TF_DYee2000.SetLineColor(7)

#Gee 3000 fit
TF_DYee3000G = TF_DYeeG(3000,1.195)
TF_DYee3000G.SetNormalized(True)
TF_DYee3000G.RejectPoint(True)
TF_DYee3000G.SetNumberFitPoints(200000)
TF_DYee3000G.SetNpx(100000)
TF_DYee3000G.SetLineColor(6)
#TF 3000 fit
TF_DYee3000 = TF_DYee(3000,0.999999)
TF_DYee3000.SetNormalized(True)
TF_DYee3000.RejectPoint(True)
TF_DYee3000.SetNumberFitPoints(20000)
TF_DYee3000.SetNpx(10000)
TF_DYee3000.SetLineColor(7)

#Gee 4000 fit
TF_DYee4000G = TF_DYeeG(4000,1.31323)
TF_DYee4000G.SetNormalized(True)
TF_DYee4000G.RejectPoint(True)
TF_DYee4000G.SetNumberFitPoints(20000)
TF_DYee4000G.SetNpx(10000)
TF_DYee4000G.SetLineColor(6)
#TF 4000 fit
TF_DYee4000 = TF_DYee(4000,0.999994)
TF_DYee4000.SetNormalized(True)
TF_DYee4000.RejectPoint(True)
TF_DYee4000.SetNumberFitPoints(20000)
TF_DYee4000.SetNpx(10000)
TF_DYee4000.SetLineColor(7)

#Gee 5000 fit
TF_DYee5000G = TF_DYeeG(5000,1.278)
TF_DYee5000G.SetNormalized(True)
TF_DYee5000G.RejectPoint(True)
TF_DYee5000G.SetNumberFitPoints(20000)
TF_DYee5000G.SetNpx(10000)
TF_DYee5000G.SetLineColor(6)
#TF 5000 fit
TF_DYee5000 = TF_DYee(5000,0.999991)
TF_DYee5000.SetNormalized(True)
TF_DYee5000.RejectPoint(True)
TF_DYee5000.SetNumberFitPoints(20000)
TF_DYee5000.SetNpx(10000)
TF_DYee5000.SetLineColor(7)

Gee_Fits_list.append(TF_DYee750G)
Gee_Fits_list.append(TF_DYee1000G)
Gee_Fits_list.append(TF_DYee2000G)
Gee_Fits_list.append(TF_DYee3000G)
Gee_Fits_list.append(TF_DYee4000G)
Gee_Fits_list.append(TF_DYee5000G)

####Set of fits for drawing non-normalised

#Gee 750 fit
TF_DYee750G_Plotting = TF_DYeeG(750,1)
TF_DYee750G_Plotting.RejectPoint(True)
TF_DYee750G_Plotting.SetNumberFitPoints(20000)
TF_DYee750G_Plotting.SetNpx(10000)
TF_DYee750G_Plotting.SetLineColor(6)
#TF 750 fit
TF_DYee750_Plotting  = TF_DYee(750,0.0015)
TF_DYee750_Plotting.RejectPoint(True)
TF_DYee750_Plotting.SetNumberFitPoints(20000)
TF_DYee750_Plotting.SetNpx(10000)
TF_DYee750_Plotting.SetLineColor(7)
#Gee 1000 fit
TF_DYee1000G_Plotting  = TF_DYeeG(1000,1)
TF_DYee1000G_Plotting.RejectPoint(True)
TF_DYee1000G_Plotting.SetNumberFitPoints(20000)
TF_DYee1000G_Plotting.SetNpx(10000)
TF_DYee1000G_Plotting.SetLineColor(6)
#TF 1000 fit
TF_DYee1000_Plotting  = TF_DYee(1000,1.4048)
TF_DYee1000_Plotting.RejectPoint(True)
TF_DYee1000_Plotting.SetNumberFitPoints(20000)
TF_DYee1000_Plotting.SetNpx(10000)
TF_DYee1000_Plotting.SetLineColor(7)
#Gee 2000 fit
TF_DYee2000G_Plotting  = TF_DYeeG(2000,1.24644)
TF_DYee2000G_Plotting.RejectPoint(True)
TF_DYee2000G_Plotting.SetNumberFitPoints(20000)
TF_DYee2000G_Plotting.SetNpx(10000)
TF_DYee2000G_Plotting.SetLineColor(6)
#TF 2000 fit
TF_DYee2000_Plotting  = TF_DYee(2000,1.00001)
TF_DYee2000_Plotting.RejectPoint(True)
TF_DYee2000_Plotting.SetNumberFitPoints(20000)
TF_DYee2000_Plotting.SetNpx(10000)
TF_DYee2000.SetLineColor(7)

#Gee 3000 fit
TF_DYee3000G_Plotting = TF_DYeeG(3000,1.20595)
TF_DYee3000G_Plotting.RejectPoint(True)
TF_DYee3000G_Plotting.SetNumberFitPoints(200000)
TF_DYee3000G_Plotting.SetNpx(100000)
TF_DYee3000G_Plotting.SetLineColor(6)
#TF 3000 fit
TF_DYee3000_Plotting = TF_DYee(3000,0.999999)
TF_DYee3000_Plotting.RejectPoint(True)
TF_DYee3000_Plotting.SetNumberFitPoints(20000)
TF_DYee3000_Plotting.SetNpx(10000)
TF_DYee3000_Plotting.SetLineColor(7)

#Gee 4000 fit
TF_DYee4000G_Plotting = TF_DYeeG(4000,1.31323)
TF_DYee4000G_Plotting.RejectPoint(True)
TF_DYee4000G_Plotting.SetNumberFitPoints(20000)
TF_DYee4000G_Plotting.SetNpx(10000)
TF_DYee4000G_Plotting.SetLineColor(6)
#TF 4000 fit
TF_DYee4000_Plotting = TF_DYee(4000,0.999994)
TF_DYee4000_Plotting.RejectPoint(True)
TF_DYee4000_Plotting.SetNumberFitPoints(20000)
TF_DYee4000_Plotting.SetNpx(10000)
TF_DYee4000_Plotting.SetLineColor(7)

#Gee 5000 fit
TF_DYee5000G_Plotting = TF_DYeeG(5000,1.30605)
TF_DYee5000G_Plotting.RejectPoint(True)
TF_DYee5000G_Plotting.SetNumberFitPoints(20000)
TF_DYee5000G_Plotting.SetNpx(10000)
TF_DYee5000G_Plotting.SetLineColor(6)
#TF 5000 fit
TF_DYee5000_Plotting = TF_DYee(5000,0.999991)
TF_DYee5000_Plotting.RejectPoint(True)
TF_DYee5000_Plotting.SetNumberFitPoints(20000)
TF_DYee5000_Plotting.SetNpx(10000)
TF_DYee5000_Plotting.SetLineColor(7)

#Use results of fits of TF1 to Gee to extract new 7 parameters 
#Also make the values from the nominal TF to compare
#Make function that reads for one type of parameter creates a python list across central mass values 

Gee_CentralMass_Values = [750,1000,2000,3000,4000,5000]
TF_CentralMass_Values = [130,140,150,160,170,180,190,200,210,220,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,750,1000,2000,3000,4000,5000]


def Parameter_Extraction_GeeFit(Parameter_Gee, G_ee_Masses):

  #Open up list to append the parameter values at each mass point
  ParameterValue_List = []

  for mass in G_ee_Masses:
    ParameterValue_List.append(mass.GetParameter(Parameter_Gee))

  return ParameterValue_List

def Parameter_Extraction_TF_Fit(Parameter_Gee, TF_DYee_Nominal, TF_CentralMass_Values):

  #Open up list to append the parameter values at each mass point
  ParameterValue_List = []

  for mass in TF_CentralMass_Values:
    TF = TF_DYee_Nominal(mass)
    ParameterValue_List.append(TF.GetParameter(Parameter_Gee))
    TF.Clear()
  return ParameterValue_List  

def Parameter_Extraction_TF_Fit_UPVariation(Parameter_Gee, TF_DYee_UPVariation, TF_CentralMass_Values):

  #Open up list to append the parameter values at each mass point
  ParameterValue_List = []

  for mass in TF_CentralMass_Values:
    TF = TF_DYee_UPVariation(mass)
    ParameterValue_List.append(TF.GetParameter(Parameter_Gee))
    TF.Clear()
  return ParameterValue_List  

def Parameter_Extraction_TF_Fit_DOWNVariation(Parameter_Gee, TF_DYee_DOWNVariation, TF_CentralMass_Values):

  #Open up list to append the parameter values at each mass point
  ParameterValue_List = []

  for mass in TF_CentralMass_Values:
    TF = TF_DYee_DOWNVariation(mass)
    ParameterValue_List.append(TF.GetParameter(Parameter_Gee))
    TF.Clear()
  return ParameterValue_List  



#Plotting Responses and fits


#Normalised Fit for parameter extraction 

cnv_Response750 = ROOT.TCanvas("c","",800,600)
G_ee_750_response_hist.SetLineColor(1)
G_ee_750_response_hist.SetMarkerColor(1)
# G_ee_750_response_hist.GetYaxis().SetRangeUser(0, 0.09)
# G_ee_750_response_hist.Scale(1/G_ee_750_response_hist.Integral())
# G_ee_750_response_hist.Scale(1., "width")
G_ee_750_response_hist.Scale(hTF750.Integral()/G_ee_750_response_hist.Integral())
# G_ee_750_response_hist.Draw()
# G_ee_750_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_750_response_hist.Fit(TF_DYee750G,"R","0",-0.2,0.1)
# G_ee_750_response_hist.Scale(1/G_ee_750_response_hist.Integral())
hTF750.SetLineColor(2)
# hTF750.Draw("e2" "l") 
# hTF750.Draw("l hist same")
hTF750.Scale(1/hTF750.Integral())
# hTF750.Scale(1., "width")
# hTF750.GetYaxis().SetRangeUser(0, 0.09)
# hTF750.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF750.Fit(TF_DYee750,"R","0",-0.2,0.1)
# hTF750.Fit(TF_DYee750,"R","",-0.1,0.1)
# test = ConfidenceIntervals(hTF750,TF_DYee750,ROOT.kBlue,0.4)
# hTF750.Draw("e2" "l") 
# hTF750.Draw("l hist same")
# G_ee_750_response_hist.Draw("same")
# test1.Draw("e3 same")
# test.Draw("e3 same")
TF_DYee750.Draw()
TF_DYee750G.Draw("same")
# hTF750.Draw("e2" "l") 
# hTF750.Draw("l hist same")
G_ee_750_response_hist.Draw("same")
print (G_ee_750_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_750_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
legend.AddEntry(G_ee_750_response_hist,"G_ee_750_Response_1")
# legend.AddEntry(hTF750,"DYee TF")
legend.AddEntry(TF_DYee750,"TF fit")
legend.AddEntry(TF_DYee750G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response750.SaveAs("Response_plots/Response750.pdf")


#Normal fit of Gee Response vs TF for visual comparson










cnv_Response1000 = ROOT.TCanvas("c","",800,600)
G_ee_1000_response_hist.SetLineColor(1)
G_ee_1000_response_hist.SetMarkerColor(1)
G_ee_1000_response_hist.Scale(hTF1000.Integral()/G_ee_1000_response_hist.Integral())
# G_ee_1000_response_hist.GetYaxis().SetRangeUser(0, 0.09)
G_ee_1000_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_1000_response_hist.Fit(TF_DYee1000G,"R","",-0.2,0.1)
# test1 = ConfidenceIntervals(G_ee_1000_response_hist,TF_DYee1000G,ROOT.kRed,0.4)
# G_ee_1000_response_hist.Fit(TF_DYee1000G,"R","",-0.2,0.1)
# TF_DYee1000G.Clear()
hTF1000.SetLineColor(2)
# hTF1000.Scale(1/hTF1000.Integral())
# hTF1000.GetYaxis().SetRangeUser(0, 0.09)
hTF1000.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF1000.Fit(TF_DYee1000,"R","",-0.2,0.1)
# test = ConfidenceIntervals(hTF1000,TF_DYee1000,ROOT.kBlue,0.4)
# # hTF1000.Fit(TF_DYee1000,"R","",-0.1,0.1)
# hTF1000.Draw("e2" "l") 
# hTF1000.Draw("l hist same")
# G_ee_1000_response_hist.Draw("same")
# test1.Draw("e3 same")
# test.Draw("e3 same")
TF_DYee1000.Draw()
TF_DYee1000G.Draw("same")
# hTF1000.Draw("e2" "l") 
# hTF1000.Draw("l hist same")
G_ee_750_response_hist.Draw("same")
print (G_ee_1000_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_1000_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
legend.AddEntry(G_ee_1000_response_hist,"G_ee_1000_Response_1")
# legend.AddEntry(hTF1000,"DYee TF")
legend.AddEntry(TF_DYee1000,"TF fit")
legend.AddEntry(TF_DYee1000G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response1000.SaveAs("Response_plots/Response1000.pdf")

cnv_Response2000 = ROOT.TCanvas("c","",800,600)
G_ee_2000_response_hist.SetLineColor(1)
G_ee_2000_response_hist.SetMarkerColor(1)
G_ee_2000_response_hist.Scale(hTF2000.Integral()/G_ee_2000_response_hist.Integral())
# G_ee_2000_response_hist.GetYaxis().SetRangeUser(0, 0.09)
G_ee_2000_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_2000_response_hist.Fit(TF_DYee2000G,"RM","",-0.2,0.1)
# test1 = ConfidenceIntervals(G_ee_2000_response_hist,TF_DYee2000G,ROOT.kRed,0.4)
# G_ee_2000_response_hist.Fit(TF_DYee2000G,"R","",-0.2,0.1)
# TF_DYee2000G.Clear()
hTF2000.SetLineColor(2)
# hTF2000.Scale(1/hTF2000.Integral())
# hTF2000.GetYaxis().SetRangeUser(0, 0.09)
hTF2000.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF2000.Fit(TF_DYee2000,"RM","",-0.2,0.1)
# test = ConfidenceIntervals(hTF2000,TF_DYee2000,ROOT.kBlue,0.4)
# hTF2000.Fit(TF_DYee2000,"R","",-0.15,0.1)
# hTF2000.Draw("e2" "l") 
# hTF2000.Draw("l hist same")
G_ee_2000_response_hist.Draw()
# test1.Draw("e3 same")
# test.Draw("e3 same")
# TF_DYee2000G.Draw("same")
TF_DYee2000.Draw("same")
TF_DYee2000G.Draw("same")
print (G_ee_2000_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_2000_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
legend.AddEntry(G_ee_2000_response_hist,"G_ee_2000_Response_1")
# legend.AddEntry(hTF2000,"DYee TF")
legend.AddEntry(TF_DYee2000,"TF fit")
legend.AddEntry(TF_DYee2000G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response2000.SaveAs("Response_plots/Response2000.pdf")

cnv_Response3000 = ROOT.TCanvas("c","",800,600)
G_ee_3000_response_hist.SetLineColor(1)
G_ee_3000_response_hist.SetMarkerColor(1)
G_ee_3000_response_hist.Scale(hTF3000.Integral()/G_ee_3000_response_hist.Integral())
# G_ee_3000_response_hist.GetYaxis().SetRangeUser(0, 0.09)
G_ee_3000_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_3000_response_hist.Fit(TF_DYee3000G,"R","",-0.2,0.1)
# test1 = ConfidenceIntervals(G_ee_3000_response_hist,TF_DYee3000G,ROOT.kRed,0.4)
# G_ee_3000_response_hist.Fit(TF_DYee3000G,"R","",-0.2,0.1)
# TF_DYee3000G.Clear()
hTF3000.SetLineColor(2)
# hTF3000.Scale(1/hTF3000.Integral())
# hTF3000.GetYaxis().SetRangeUser(0, 0.09)
hTF3000.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF3000.Fit(TF_DYee3000,"R","",-0.2,0.1)
# test = ConfidenceIntervals(hTF3000,TF_DYee3000,ROOT.kBlue,0.4)
# # hTF3000.Fit(TF_DYee3000,"R","",-0.1,0.1)
# hTF3000.Draw("e2" "l") 
# hTF3000.Draw("l hist same")
G_ee_3000_response_hist.Draw()
# test1.Draw("e3 same")
# test.Draw("e3 same")
# TF_DYee3000G.Draw("same")
TF_DYee3000.Draw("same")
TF_DYee3000G.Draw("same")
print (G_ee_3000_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_3000_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
# legend.AddEntry(G_ee_3000_response_hist,"G_ee_3000_Response_1")
# legend.AddEntry(hTF3000,"DYee TF")
legend.AddEntry(TF_DYee3000,"TF fit")
legend.AddEntry(TF_DYee3000G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response3000.SaveAs("Response_plots/Response3000.pdf")

cnv_Response4000 = ROOT.TCanvas("c","",800,600)
G_ee_4000_response_hist.SetLineColor(1)
G_ee_4000_response_hist.SetMarkerColor(1)
G_ee_4000_response_hist.Scale(hTF4000.Integral()/G_ee_4000_response_hist.Integral())
# G_ee_4000_response_hist.GetYaxis().SetRangeUser(0, 0.09)
G_ee_4000_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_4000_response_hist.Fit(TF_DYee4000G,"RM","",-0.2,0.1)
# test1 = ConfidenceIntervals(G_ee_4000_response_hist,TF_DYee4000G,ROOT.kRed,0.4)
# G_ee_4000_response_hist.Fit(TF_DYee4000G,"R","",-0.2,0.1)
# TF_DYee4000G.Clear()
hTF4000.SetLineColor(2)
# hTF4000.Scale(1/hTF4000.Integral())
# hTF4000.GetYaxis().SetRangeUser(0, 0.09)
hTF4000.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF4000.Fit(TF_DYee4000,"RM","",-0.2,0.1)
# test = ConfidenceIntervals(hTF4000,TF_DYee4000,ROOT.kBlue,0.4)
# hTF4000.Fit(TF_DYee4000,"R","",-0.1,0.1)
# hTF4000.Draw("e2" "l") 
# hTF4000.Draw("l hist same")
G_ee_4000_response_hist.Draw()
# test1.Draw("e3 same")
# test.Draw("e3 same")
# TF_DYee4000G.Draw("same")
TF_DYee4000.Draw("same")
TF_DYee4000G.Draw("same")
print (G_ee_4000_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_4000_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
# legend.AddEntry(G_ee_4000_response_hist,"G_ee_4000_Response_1")
# legend.AddEntry(hTF4000,"DYee TF")
legend.AddEntry(TF_DYee4000,"TF fit")
legend.AddEntry(TF_DYee4000G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response4000.SaveAs("Response_plots/Response4000.pdf")

cnv_Response5000 = ROOT.TCanvas("c","",800,600)
G_ee_5000_response_hist.SetLineColor(1)
G_ee_5000_response_hist.SetMarkerColor(1)
G_ee_5000_response_hist.Scale(hTF5000.Integral()/G_ee_5000_response_hist.Integral())
# G_ee_5000_response_hist.GetYaxis().SetRangeUser(0, 0.09)
G_ee_5000_response_hist.GetXaxis().SetRangeUser(-0.2, 0.1)
G_ee_5000_response_hist.Fit(TF_DYee5000G,"R","",-0.2,0.1)
# test1 = ConfidenceIntervals(G_ee_5000_response_hist,TF_DYee5000G,ROOT.kRed,0.4)
# G_ee_5000_response_hist.Fit(TF_DYee5000G,"R","",-0.2,0.1)
# TF_DYee5000G.Clear()
hTF5000.SetLineColor(2)
# hTF5000.Scale(1/hTF5000.Integral())
# hTF5000.GetYaxis().SetRangeUser(0, 0.09)
hTF5000.GetXaxis().SetRangeUser(-0.2, 0.1)
hTF5000.Fit(TF_DYee5000,"R","",-0.2,0.1)
# test = ConfidenceIntervals(hTF5000,TF_DYee5000,ROOT.kBlue,0.4)
# hTF5000.Fit(TF_DYee5000,"R","",-0.1,0.1)
# hTF5000.Draw("e2" "l") 
# hTF5000.Draw("l hist same")
G_ee_5000_response_hist.Draw()
# test1.Draw("e3 same")
# test.Draw("e3 same")
# TF_DYee5000G.Draw("same")
TF_DYee5000.Draw("same")
TF_DYee5000G.Draw("same")
print (G_ee_5000_response_hist.Integral())
# print (hClockwork.Integral())
G_ee_5000_response_hist.GetXaxis().SetLabelSize(0.03)
legend = ROOT.TLegend(0.15,0.7,0.4,0.90)
# legend.AddEntry(G_ee_5000_response_hist,"G_ee_5000_Response_1")
# legend.AddEntry(hTF5000,"DYee TF")
legend.AddEntry(TF_DYee5000,"TF fit")
legend.AddEntry(TF_DYee5000G,"Gee Fit")
# legend.AddEntry(test,"95% C.I TF fit")
# legend.AddEntry(test1,"95% C.I Gee fit")
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.Draw()
cnv_Response5000.SaveAs("Response_plots/Response5000.pdf")

#Plot new TF fitted to Gee paramters over the TF/Gee mass range

mu_CB_TF = Parameter_Extraction_TF_Fit(4,TF_DYee_Nominal,TF_CentralMass_Values)
sigma_CB_TF = Parameter_Extraction_TF_Fit(3,TF_DYee_Nominal,TF_CentralMass_Values)
n_CB_TF = Parameter_Extraction_TF_Fit(2,TF_DYee_Nominal,TF_CentralMass_Values)
mu_G_TF = Parameter_Extraction_TF_Fit(5,TF_DYee_Nominal,TF_CentralMass_Values)
sigma_G_TF = Parameter_Extraction_TF_Fit(6,TF_DYee_Nominal,TF_CentralMass_Values)
k_param_TF = Parameter_Extraction_TF_Fit(0,TF_DYee_Nominal,TF_CentralMass_Values)

mu_CB_TF_UPVariation = Parameter_Extraction_TF_Fit_UPVariation(4,TF_DYee_UPVariation,TF_CentralMass_Values)
sigma_CB_TF_UPVariation = Parameter_Extraction_TF_Fit_UPVariation(3,TF_DYee_UPVariation,TF_CentralMass_Values)
mu_G_TF_UPVariation = Parameter_Extraction_TF_Fit_UPVariation(5,TF_DYee_UPVariation,TF_CentralMass_Values)
sigma_G_TF_UPVariation = Parameter_Extraction_TF_Fit_UPVariation(6,TF_DYee_UPVariation,TF_CentralMass_Values)

mu_CB_TF_DOWNVariation = Parameter_Extraction_TF_Fit_DOWNVariation(4,TF_DYee_DOWNVariation,TF_CentralMass_Values)
sigma_CB_TF_DOWNVariation = Parameter_Extraction_TF_Fit_DOWNVariation(3,TF_DYee_DOWNVariation,TF_CentralMass_Values)
mu_G_TF_DOWNVariation = Parameter_Extraction_TF_Fit_DOWNVariation(5,TF_DYee_DOWNVariation,TF_CentralMass_Values)
sigma_G_TF_DOWNVariation = Parameter_Extraction_TF_Fit_DOWNVariation(6,TF_DYee_DOWNVariation,TF_CentralMass_Values)


mu_CB_Gee = Parameter_Extraction_GeeFit(4,Gee_Fits_list)
sigma_CB_Gee = Parameter_Extraction_GeeFit(3,Gee_Fits_list)
n_CB_Gee = Parameter_Extraction_GeeFit(2,Gee_Fits_list)
mu_G_Gee = Parameter_Extraction_GeeFit(5,Gee_Fits_list)
sigma_G_Gee = Parameter_Extraction_GeeFit(6,Gee_Fits_list)
k_param_Gee = Parameter_Extraction_GeeFit(0,Gee_Fits_list)


fig_mu_CB, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,mu_CB_TF, 'r',linewidth=3, label=r"mu_CB_TF")
plt.plot(TF_CentralMass_Values,mu_CB_TF_DOWNVariation, 'g',linewidth=3, label=r"mu_CB_TF_DOWNVariation")
plt.plot(TF_CentralMass_Values,mu_CB_TF_UPVariation, 'g',linewidth=3, label=r"mu_CB_TF_UPVariation")
plt.plot(Gee_CentralMass_Values,mu_CB_Gee, 'b',linewidth=3, label=r"mu_CB_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('mu_CB', ha='right', y=1.0)
plt.margins(0)
plt.legend()
# plt.ylim(top=0)
# plt.ylim(bottom=-0.012)
plt.ylim(top=0.015)
plt.ylim(bottom=-0.035)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_mu_CB.savefig("Response_plots/fig_mu_CB")

fig_sigma_CB_Gee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,sigma_CB_TF, 'r',linewidth=3, label=r"sigma_CB_TF")
plt.plot(TF_CentralMass_Values,sigma_CB_TF_DOWNVariation, 'g',linewidth=3, label=r"sigma_CB_TF_DOWNVariation")
plt.plot(TF_CentralMass_Values,sigma_CB_TF_UPVariation, 'g',linewidth=3, label=r"sigma_CB_TF_UPVariation")
plt.plot(Gee_CentralMass_Values,sigma_CB_Gee, 'b',linewidth=3, label=r"sigma_CB_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('sigma_CB', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(top=0.03)
plt.ylim(bottom=0)
# plt.ylim(top=0.23)
# plt.ylim(bottom=-0.33)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_sigma_CB_Gee.savefig("Response_plots/fig_sigma_CB_Gee")

fig_n_CB_Gee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,n_CB_TF, 'r',linewidth=3, label=r"n_CB_TF")
plt.plot(Gee_CentralMass_Values,n_CB_Gee, 'b',linewidth=3, label=r"n_CB_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('n_CB', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(top=1.7)
plt.ylim(bottom=0)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_n_CB_Gee.savefig("Response_plots/fig_n_CB_Gee")

fig_mu_G_Gee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,mu_G_TF, 'r',linewidth=3, label=r"mu_G_TF")
plt.plot(TF_CentralMass_Values,mu_G_TF_DOWNVariation, 'g',linewidth=3, label=r"mu_G_TF_DOWNVariation")
plt.plot(TF_CentralMass_Values,mu_G_TF_UPVariation, 'g',linewidth=3, label=r"mu_G_TF_UPVariation")
plt.plot(Gee_CentralMass_Values,mu_G_Gee, 'b',linewidth=3, label=r"mu_G_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('mu_G', ha='right', y=1.0)
plt.margins(0)
plt.legend()
# plt.ylim(top=0)
# plt.ylim(bottom=-0.0025)
plt.ylim(top=0.015)
plt.ylim(bottom=-0.015)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_mu_G_Gee.savefig("Response_plots/fig_mu_G_Gee")

fig_sigma_G_Gee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,sigma_G_TF, 'r',linewidth=3, label=r"sigma_G_TF")
plt.plot(TF_CentralMass_Values,sigma_G_TF_DOWNVariation, 'g',linewidth=3, label=r"sigma_G_TF_DOWNVariation")
plt.plot(TF_CentralMass_Values,sigma_G_TF_UPVariation, 'g',linewidth=3, label=r"sigma_G_TF_UPVariation")
plt.plot(Gee_CentralMass_Values,sigma_G_Gee, 'b',linewidth=3, label=r"sigma_G_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('sigma_G', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(top=0.015)
plt.ylim(bottom=0)
# plt.ylim(top=0.7)
# plt.ylim(bottom=-0.5)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_sigma_G_Gee.savefig("Response_plots/fig_sigma_G_Gee")

fig_k_param_Gee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(TF_CentralMass_Values,k_param_TF, 'r',linewidth=3, label=r"k_param_TF")
plt.plot(Gee_CentralMass_Values,k_param_Gee, 'b',linewidth=3, label=r"k_param_Gee")
plt.xlabel(r'm_ee [GeV]}$', ha='right', x=1.0)
plt.ylabel('k_param', ha='right', y=1.0)
plt.margins(0)
plt.legend()
plt.ylim(top=0.45)
plt.ylim(bottom=0)
plt.xlim(130,6000)
# plt.yscale('log')
plt.xscale('log')
fig_k_param_Gee.savefig("Response_plots/fig_k_param_Gee")




G_ee_Response.Close()
