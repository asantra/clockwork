#!/usr/bin/env python
import os, sys
import ROOT
from ROOT import RooFit
# from ROOT import *
import math
import array
from array import *
# import ROOT
import numpy as np
#Standard
import matplotlib
matplotlib.use("Agg")
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)

# Generate example of signal model using full Resonant TF shape instead of Guassian, then lets use it to plot a peak og spin2/1 central mass points
#First define the individual components

Nbins = 200 #2470
xmin = -0.2 #230
xmax = 0.1 #2700

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)



sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)


alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin, xmax)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)


#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
# mu_G = ROOT.TF1("mu_G","[0]+[1]*(x^-1) + [2]*x+[3]*log(x^3)+[4]*((x^2)^-1)+[5]*(x^2)", xmin, xmax)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)


sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)


#K-Parameters

k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin, xmax)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)
 
########################################################################
########################################################################
########################################################################

#Up-variation Crystal Ball Parameters

#energy scale
mu_CB_UPvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB_UPvar.SetParameter(0, 0.143853)
mu_CB_UPvar.SetParameter(1, -0.544555)
mu_CB_UPvar.SetParameter(2, -0.00897897)
mu_CB_UPvar.SetParameter(3, 0.000000211052)
# mu_CB_UPvartest = rn.evaluate(mu_CB_UPvar,massList)

#energy resolution
sigma_CB_UPvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_UPvar.SetParameter(0, 0.0168464)
sigma_CB_UPvar.SetParameter(1, 0.244998)
sigma_CB_UPvar.SetParameter(2, 0.607754)
# sigma_CB_UPvartest = rn.evaluate(sigma_CB,massList)

#Up-variation Gaussian

#energy scale
mu_G_UPvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
mu_G_UPvar.SetParameter(0, 0.00437644)
mu_G_UPvar.SetParameter(1, -0.693393)
mu_G_UPvar.SetParameter(2, -0.00000124588)
mu_G_UPvar.SetParameter(3, 0.00000994365)
mu_G_UPvar.SetParameter(4, 3.85433)
mu_G_UPvar.SetParameter(5, 0.0000000000681002)
# mu_G_UPvartest = rn.evaluate(mu_G_UPvar,massList)

#energy resolution
sigma_G_UPvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_UPvar.SetParameter(0, 0.00968273)
sigma_G_UPvar.SetParameter(1, 0.110266)
sigma_G_UPvar.SetParameter(2, 1.0617)
# sigma_G_UPvartest = rn.evaluate(sigma_G_UPvar,massList)

########################################################################

#Down-variation Crystal Ball Parameters

#energy scale
mu_CB_DOWNvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB_DOWNvar.SetParameter(0, 0.125578)
mu_CB_DOWNvar.SetParameter(1, -0.350447)
mu_CB_DOWNvar.SetParameter(2, -0.0144995)
mu_CB_DOWNvar.SetParameter(3, 0.00000373014)
# mu_CB__DOWNvartest = rn.evaluate(mu_CB_DOWNvar,massList)

#energy resolution
sigma_CB_DOWNvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_DOWNvar.SetParameter(0, 0.011107)
sigma_CB_DOWNvar.SetParameter(1, 0.22346)
sigma_CB_DOWNvar.SetParameter(2, 1.92675)
# sigma_CB_DOWNvartest = rn.evaluate(sigma_CB_DOWNvar,massList)

#DOWN-variation Gaussian

#energy scale
mu_G_DOWNvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
mu_G_DOWNvar.SetParameter(0, -0.00743235)
mu_G_DOWNvar.SetParameter(1, 0.650265)
mu_G_DOWNvar.SetParameter(2, 0.000000574646)
mu_G_DOWNvar.SetParameter(3, -0.00000498561)
mu_G_DOWNvar.SetParameter(4, 28.7656)
mu_G_DOWNvar.SetParameter(5, -0.0000000000452119)
# mu_G_DOWNvartest = rn.evaluate(mu_G_DOWNvar,massList)

#energy resolution
sigma_G_DOWNvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_DOWNvar.SetParameter(0, 0.00337693)
sigma_G_DOWNvar.SetParameter(1, 0.112002)
sigma_G_DOWNvar.SetParameter(2, 1.2219)
# sigma_G_DOWNvartest = rn.evaluate(sigma_G_DOWNvar,massList)

def TF_DYee(M,N):
  # TF_DYee.Clear()
  TF_DYee =  ROOT.TF1("TF_DYee","([7]*([0]*ROOT::Math::crystalball_pdf(x, [1], [2],[3],[4]))+((1-[0])*TMath::Gaus(x,[5],[6],1)))",xmin, xmax)
  TF_DYee.FixParameter(0,k_param.Eval(M))
  TF_DYee.FixParameter(1, alpha_CB)
  TF_DYee.FixParameter(2, n_CB.Eval(M))
  TF_DYee.FixParameter(3, sigma_CB.Eval(M))
  TF_DYee.FixParameter(4, mu_CB.Eval(M))
  TF_DYee.FixParameter(5, mu_G.Eval(M))
  TF_DYee.FixParameter(6, sigma_G.Eval(M))
  TF_DYee.SetParameter(7, N)
  # /TF_DYee.SetParLimits(7,0,1)
  # TF_DYee.Clear()
  return TF_DYee

#################################################
########################################################################

#Generate signal with no random fluctuations
def Smearing(M):
  scale = str(1)
  sClockwork = "("+(scale)+" * (RESONANCES) )"
  sResonances = ""  
  #Gaussain
  mu_Gauss = str(mu_G.Eval(M))
  sigma_Guass = str(sigma_G.Eval(M))
  #Crystal_Ball
  alpha_CrystalBall = str(alpha_CB)
  n_CrystalBall = str(n_CB.Eval(M))
  sigma_CrystalBall = str(sigma_CB.Eval(M))
  mu_CrystalBall = str(mu_CB.Eval(M))
  # #k
  k_p = str(k_param.Eval(M))

  sResonances = "(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
  print (f"Tower Formula n==1:{sResonances}")
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,-0.20,0.1)
  hClockwork = ROOT.TH1F("hTF"+str(M)+"","",200,-0.2,0.1)
  # hClockwork_f = ROOT.TH1F("hClockwork","hClockwork",Nbins,-0.2,0.1)
  y  = fClockwork.Eval(M)

  for i in range(1,200+1):
     x = hClockwork.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     # hClockwork.SetBinContent(i,y)
     hClockwork.SetBinContent(i,y)

  return hClockwork, fClockwork


########################################################################
########################################################################


def GetLogBinning(nbins,xmin,xmax):
   logmin  = math.log10(xmin)
   logmax  = math.log10(xmax)
   logbinwidth = (logmax-logmin)/nbins
   # Bin edges in GeV
   xbins = [xmin,] #the lowest edge first
   for i in range(1,nbins+1):
      xbins.append( ROOT.TMath.Power( 10,(logmin + i*logbinwidth) ) )
   arrxbins = array("d", xbins)
   return nbins, arrxbins

nbins_mee_log40, bins_mee_log40 = GetLogBinning(100,130,6000)# 40,130,3130 default

def GetXsecWeight(event,sample):
   if(event.xsec>0):
      return 1./(event.Nevt/(event.xsec*1e6)/event.geneff)
   else:
      # print ("ERROR: event.xsec=%g for sample=%s" % (event.xsec,sample))
      # print ("       quitting!")
      # quit()
    return 1
# pass_preor_ee to pass_only_preor_ee everywhere
def GetLeptonSfWeight(event,channel):
   SF = 1.
   if("ee" in channel and event.pass_only_preor_ee==1): SF = event.el_preor_SF[0]*event.el_preor_SF[1]
   if("uu" in channel and event.pass_only_preor_uu==1): SF = event.mu_preor_SF[0]*event.mu_preor_SF[1]
   if("eu" in channel and event.pass_only_preor_eu==1): SF = event.el_preor_SF[0]*event.mu_preor_SF[0]
   if(SF<=0):
      #print "WARNING (GetLeptonSfWeight): SF=0 in channel=",channel
      SF = 1
   return SF

def GetJetSfWeight(event,channel):
   SF = 1.
   if("0b" in channel or "1b" in channel or "2b" in channel): SF = event.btag_signal_jets_SF
   if(SF<=0):
      #print "WARNING (GetJetSfWeight): SF=0 in channel=",channel
      SF = 1
   return SF

def GetTriggerSfWeight(event,channel):
   SF = 1.
   if("ee" in channel and event.pass_only_preor_ee==1): SF = event.trig_ee_SF
   if("uu" in channel and event.pass_only_preor_uu==1): SF = event.trig_uu_SF
   if("eu" in channel and event.pass_only_preor_eu==1): SF = event.trig_eu_SF
   if(SF<=0):
      #print "WARNING (GetTriggerSfWeight): SF=0 in channel=",channel
      SF = 1.
   return SF


def GetWeight(event,channel,sample):
   if(sample=="data"): return 1
   wgt_mc = event.mconly_weight
   wgt_pu = event.pu_weight/event.mconly_weight  ## pu_weight is multiplied by mconly_weight in ST :S
   wgt_kf = event.kF_weight
   wgt_xs = GetXsecWeight(event,sample)
   wgt_lsf = GetLeptonSfWeight(event,channel)
   wgt_jsf = GetJetSfWeight(event,channel)
   wgt_tsf = GetTriggerSfWeight(event,channel)
   wgt = 1.
   wgt *= wgt_xs
   wgt *= wgt_kf
   #wgt *= wgt_mc
   wgt *= wgt_lsf
   #wgt *= wgt_jsf
   wgt *= wgt_tsf
   if(wgt_pu>1.e1): wgt /= wgt_pu ### TODO --> tepmporary hack!!!
   return wgt

wgt = GetWeight

#Define all the samples:

sample = "305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750""305551.Pythia8EvtGen_A14NNPDF23LO_Gee_02_750""305552.Pythia8EvtGen_A14NNPDF23LO_Gee_03_750""305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000""305554.Pythia8EvtGen_A14NNPDF23LO_Gee_02_1000""305555.Pythia8EvtGen_A14NNPDF23LO_Gee_03_1000""305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000""305557.Pythia8EvtGen_A14NNPDF23LO_Gee_02_2000""305558.Pythia8EvtGen_A14NNPDF23LO_Gee_03_2000""305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000""305560.Pythia8EvtGen_A14NNPDF23LO_Gee_02_3000""305561.Pythia8EvtGen_A14NNPDF23LO_Gee_03_3000""305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000""305563.Pythia8EvtGen_A14NNPDF23LO_Gee_02_4000""305564.Pythia8EvtGen_A14NNPDF23LO_Gee_03_4000""305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000""305566.Pythia8EvtGen_A14NNPDF23LO_Gee_02_5000""305567.Pythia8EvtGen_A14NNPDF23LO_Gee_03_5000"
sample_Z = "301215.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000""301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000""301217.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi4000""301218.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi5000"

#mc16a Graviton_ee samples
chain_750_1 = ROOT.TChain("nominal")
chain_1000_1 = ROOT.TChain("nominal")
chain_2000_1 = ROOT.TChain("nominal")
chain_3000_1 = ROOT.TChain("nominal")
chain_4000_1 = ROOT.TChain("nominal")
chain_5000_1 = ROOT.TChain("nominal")

# # Gee MC chain
#750 GeV Mass Point for all periods and K/M =1
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
chain_750_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305550.Pythia8EvtGen_A14NNPDF23LO_Gee_01_750_minitrees.root.root")
#1000 GeV
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
chain_1000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305553.Pythia8EvtGen_A14NNPDF23LO_Gee_01_1000_minitrees.root.root")
#2000 GeV
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
chain_2000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305556.Pythia8EvtGen_A14NNPDF23LO_Gee_01_2000_minitrees.root.root")
#3000 GeV
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
chain_3000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305559.Pythia8EvtGen_A14NNPDF23LO_Gee_01_3000_minitrees.root.root")
#4000 GeV
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
chain_4000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305562.Pythia8EvtGen_A14NNPDF23LO_Gee_01_4000_minitrees.root.root")
#5000 GeV
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16a/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16a10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16d/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16d10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")
chain_5000_1.Add("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Spin_Analysis_TREE/mc16_13TeV/mc16e/305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000/data-tinytrees/user.pfalke.SIGNOSKIMmc16e10012019v1.mc16_13TeV.305565.Pythia8EvtGen_A14NNPDF23LO_Gee_01_5000_minitrees.root.root")


#Make ROOT file containing relevant histos for either MC or Data

G_ee_Response = ROOT.TFile.Open("/scratch2/slawlor/Clockwork_Project/clockwork-search/Spin-2_ResponseProj/Response_plots/TestTFSpin2.root","RECREATE")


#Make histograms for all mass points

G_ee_750_Truth_1 = ROOT.TH1D("G_ee_750_Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_750_Reco_1 = ROOT.TH1D("G_ee_750_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_750_Response_1 = ROOT.TH1D("G_ee_750_Response_1",";m_{ee} [GeV];Events",200, -0.2, 0.1)

G_ee_1000_Truth_1 = ROOT.TH1D("G_ee_1000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_1000_Reco_1 = ROOT.TH1D("G_ee_1000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_1000_Response_1 = ROOT.TH1D("G_ee_1000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_2000_Truth_1 = ROOT.TH1D("G_ee_2000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_2000_Reco_1 = ROOT.TH1D("G_ee_2000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_2000_Response_1 = ROOT.TH1D("G_ee_2000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_3000_Truth_1 = ROOT.TH1D("G_ee_3000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_3000_Reco_1 = ROOT.TH1D("G_ee_3000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_3000_Response_1 = ROOT.TH1D("G_ee_3000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_4000_Truth_1 = ROOT.TH1D("G_ee_4000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_4000_Reco_1 = ROOT.TH1D("G_ee_4000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_4000_Response_1 = ROOT.TH1D("G_ee_4000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

G_ee_5000_Truth_1 = ROOT.TH1D("G_ee_5000__Truth_1",  ";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_5000_Reco_1 = ROOT.TH1D("G_ee_5000_TF_Reco_1",";m_{ee} [GeV];Events",Nbins, xmin, xmax)
G_ee_5000_Response_1 = ROOT.TH1D("G_ee_5000_Response_1","; #frac{m_{reco} - m_{truth}}{m_{truth}};Events",200, -0.2, 0.1)

#make event loop function to process each chainlater as it is called

def Response_EventLoop(chain,truth_hist,reco_hist,response_hist,samples):
  n=1
  for event in chain:  #t for Single File
    # wgtee = GetWeight(event,"ee")
    if event.pass_only_ee:
      wgtee = GetWeight(event,"ee",samples)
      # print(wgtee)
      truth_hist.Fill(event.truth_Z_forTF_m/1000,wgtee)
      reco_hist.Fill(event.m_ee,wgtee)
      response_hist.Fill((event.m_ee - (event.truth_Z_forTF_m/1000))/(event.truth_Z_forTF_m/1000),wgtee)
      n+=1
  return truth_hist,reco_hist,response_hist

def Response_EventLoop_ResponseOnly(chain,response_hist,samples):
  n=1
  for event in chain:  #t for Single File
    # wgtee = GetWeight(event,"ee")
    if event.pass_only_ee:
      wgtee = GetWeight(event,"ee",samples)
      # print(wgtee)
      # truth_hist.Fill(event.truth_Z_forTF_m/1000,wgtee)
      # reco_hist.Fill(event.m_ee,wgtee)
      response_hist.Fill((event.m_ee - (event.truth_Z_forTF_m/1000))/(event.truth_Z_forTF_m/1000),wgtee)
      n+=1
  return response_hist

#Use event loop function to get response for all mass points
G_ee_750_truth_hist, G_ee_750_reco_hist, G_ee_750_response_hist = Response_EventLoop(chain_750_1, G_ee_750_Truth_1, G_ee_750_Reco_1, G_ee_750_Response_1, sample)
G_ee_1000_truth_hist, G_ee_1000_reco_hist, G_ee_1000_response_hist = Response_EventLoop(chain_1000_1, G_ee_1000_Truth_1, G_ee_1000_Reco_1, G_ee_1000_Response_1, sample)
G_ee_2000_truth_hist, G_ee_2000_reco_hist, G_ee_2000_response_hist = Response_EventLoop(chain_2000_1, G_ee_2000_Truth_1, G_ee_2000_Reco_1, G_ee_2000_Response_1, sample)
G_ee_3000_truth_hist, G_ee_3000_reco_hist, G_ee_3000_response_hist = Response_EventLoop(chain_3000_1, G_ee_3000_Truth_1, G_ee_3000_Reco_1, G_ee_3000_Response_1, sample)
G_ee_4000_truth_hist, G_ee_4000_reco_hist, G_ee_4000_response_hist = Response_EventLoop(chain_4000_1, G_ee_4000_Truth_1, G_ee_4000_Reco_1, G_ee_4000_Response_1, sample)
G_ee_5000_truth_hist, G_ee_5000_reco_hist, G_ee_5000_response_hist = Response_EventLoop(chain_5000_1, G_ee_5000_Truth_1, G_ee_5000_Reco_1, G_ee_5000_Response_1, sample)

#Let us use the TFs to evalute the function at the G_ee Mass points
hTF750, fTF750 = Smearing(750)
hTF1000, fTF1000 = Smearing(1000)
hTF2000, fTF2000 = Smearing(2000)
hTF3000, fTF3000 = Smearing(3000)
hTF4000, fTF4000 = Smearing(4000)
hTF5000, fTF5000 = Smearing(5000)

#Ensure sum of weights sqaured is used
G_ee_750_response_hist.Sumw2()
G_ee_1000_response_hist.Sumw2()
G_ee_2000_response_hist.Sumw2()
G_ee_3000_response_hist.Sumw2() 
G_ee_4000_response_hist.Sumw2()
G_ee_5000_response_hist.Sumw2() 
hTF750.Sumw2()
hTF1000.Sumw2()
hTF2000.Sumw2()
hTF3000.Sumw2() 
hTF4000.Sumw2()
hTF5000.Sumw2() 

def RooFit_Dielectron_TF(M):
  # Create a ROOT ROOT.TF1 function
  Dielectron_TF = ROOT.TF1("TF_DYee","K1*ROOT::Math::crystalball_pdf(x, alphaCB, nCB,sigmaCB,muCB)+((1-K1)*TMath::Gaus(x,muG,sigmaG,1))",xmin, xmax)

  # Create an observable
  x = ROOT.RooRealVar("x", "x", -0.2, 0.1)
  K1 = ROOT.RooRealVar("K", "K", k_param.Eval(M) ,xmin, xmax)
  alphaCB = ROOT.RooRealVar("alpha_CB", "alpha_CB", 1.59112,xmin, xmax)
  nCB = ROOT.RooRealVar("n_CB", "n_CB", n_CB.Eval(M),xmin, xmax)
  sigmaCB = ROOT.RooRealVar("sigma_CB", "sigma_CB", sigma_CB.Eval(M),xmin, xmax)
  muCB = ROOT.RooRealVar("mu_CB", "mu_CB", mu_CB.Eval(M),xmin, xmax)
  muG = ROOT.RooRealVar("mu_G", "mu_G", mu_G.Eval(M),xmin, xmax)
  sigmaG = ROOT.RooRealVar("sigma_G", "sigma_G", sigma_G.Eval(M),xmin, xmax)

  # Create binding of ROOT.TF1 object to above observable
  rDielectron_TF = ROOT.RooFit.bindFunction(Dielectron_TF, K1, alphaCB,nCB,sigmaCB,muCB, muG,sigmaG,x)
  # Print rfa1 definition
  rDielectron_TF.Print()
  return rDielectron_TF

RooFit_Dielectron_TF_750 = RooFit_Dielectron_TF(750)
# w = ROOT.RooWorkspace()
# w.factory('x[-0.2,0.1]') # Create a RooRealVar in range [0,200]
# x = w.var('x')          # Retrieve the RooRealVar as a python object
# Have to pass the variable as a RooArgSet (set of vector data) because
# in principle we might be reading a multi-dimensional dataset
# inputdata = ROOT.RooDataSet('data','data',G_ee_750_response_hist,ROOT.RooArgSet(x))
inputdata  = ROOT.RooDataHist("data", ROOT.RooArgSet(x), ROOT.RooFit.Import(G_ee_750_response_hist),1)

# # Represent data in dh as pdf in x
# histpdf1 = ROOT.RooHistPdf("histpdf1", "histpdf1", ROOT.RooArgSet(x), G_ee_750_response_hist, 0)

xframe = x.frame() # Generates a new frame for plotting
inputdata.plotOn(xframe)
xframe.Draw()
c.Draw()



xframe = x.frame() # Generates a new frame for plotting
inputdata.plotOn(xframe)

# Now fit the background pdf to the sample data, saving the fit result
fitresult = RooFit_Dielectron_TF_750.fitTo(inputdata,ROOT.RooFit.Save(),ROOT.RooFit.PrintLevel(-1))
fitresult.Print() # Print fit result
# Add the fitted function to the plot
pdf.plotOn(xframe)
xframe.Draw()
c.Draw()