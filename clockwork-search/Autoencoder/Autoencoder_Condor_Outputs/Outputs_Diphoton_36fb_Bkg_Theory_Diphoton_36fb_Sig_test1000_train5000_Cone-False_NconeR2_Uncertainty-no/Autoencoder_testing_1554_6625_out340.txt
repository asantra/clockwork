[7mlsetup              [0m lsetup <tool1> [ <tool2> ...] (see lsetup -h):
[7m lsetup agis        [0m  ATLAS Grid Information System
[7m lsetup asetup      [0m  (or asetup) to setup an Athena release
[7m lsetup atlantis    [0m  Atlantis: event display
[7m lsetup eiclient    [0m  Event Index 
[7m lsetup emi         [0m  EMI: grid middleware user interface 
[7m lsetup ganga       [0m  Ganga: job definition and management client
[7m lsetup lcgenv      [0m  lcgenv: setup tools from cvmfs SFT repository
[7m lsetup panda       [0m  Panda: Production ANd Distributed Analysis
[7m lsetup pod         [0m  Proof-on-Demand (obsolete)
[7m lsetup pyami       [0m  pyAMI: ATLAS Metadata Interface python client
[7m lsetup root        [0m  ROOT data processing framework
[7m lsetup rucio       [0m  distributed data management system client
[7m lsetup views       [0m  Set up a full LCG release
[7m lsetup xcache      [0m  XRootD local proxy cache
[7m lsetup xrootd      [0m  XRootD data access
[7madvancedTools       [0m advanced tools menu
[7mdiagnostics         [0m diagnostic tools menu
[7mhelpMe              [0m more help
[7mprintMenu           [0m show this menu
[7mshowVersions        [0m show versions of installed software

************************************************************************
Requested:  views ... 
 Setting up [4mviews LCG_97python3:x86_64-centos7-gcc8-opt[0m ... 
>>>>>>>>>>>>>>>>>>>>>>>>> Information for user <<<<<<<<<<<<<<<<<<<<<<<<<
************************************************************************
Requirement already up-to-date: parton in /home/slawlor/.local/lib/python3.7/site-packages (0.1)
Requirement already satisfied, skipping upgrade: scipy in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (1.3.0)
Requirement already satisfied, skipping upgrade: numpy in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (1.18.1)
Requirement already satisfied, skipping upgrade: pyyaml in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (5.1)
Requirement already satisfied, skipping upgrade: appdirs in /home/slawlor/.local/lib/python3.7/site-packages (from parton) (1.4.4)
Requirement already satisfied, skipping upgrade: setuptools in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from parton) (41.0.0.post20200313)
Requirement already up-to-date: mplhep in /home/slawlor/.local/lib/python3.7/site-packages (0.2.9)
Requirement already satisfied, skipping upgrade: matplotlib>=3.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from mplhep) (3.1.0)
Requirement already satisfied, skipping upgrade: scipy>=1.1.0 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from mplhep) (1.3.0)
Requirement already satisfied, skipping upgrade: numpy>=1.16.0 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from mplhep) (1.18.1)
Requirement already satisfied, skipping upgrade: requests~=2.21 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from mplhep) (2.22.0)
Requirement already satisfied, skipping upgrade: packaging in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from mplhep) (19.0)
Requirement already satisfied, skipping upgrade: cycler>=0.10 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from matplotlib>=3.1->mplhep) (0.10.0)
Requirement already satisfied, skipping upgrade: kiwisolver>=1.0.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from matplotlib>=3.1->mplhep) (1.0.1)
Requirement already satisfied, skipping upgrade: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,>=2.0.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from matplotlib>=3.1->mplhep) (2.4.0)
Requirement already satisfied, skipping upgrade: python-dateutil>=2.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from matplotlib>=3.1->mplhep) (2.8.0)
Requirement already satisfied, skipping upgrade: chardet<3.1.0,>=3.0.2 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from requests~=2.21->mplhep) (3.0.4)
Requirement already satisfied, skipping upgrade: idna<2.9,>=2.5 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from requests~=2.21->mplhep) (2.8)
Requirement already satisfied, skipping upgrade: urllib3!=1.25.0,!=1.25.1,<1.26,>=1.21.1 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from requests~=2.21->mplhep) (1.25.3)
Requirement already satisfied, skipping upgrade: certifi>=2017.4.17 in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from requests~=2.21->mplhep) (2019.3.9)
Requirement already satisfied, skipping upgrade: six in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from packaging->mplhep) (1.12.0)
Requirement already satisfied, skipping upgrade: setuptools in /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages (from kiwisolver>=1.0.1->matplotlib>=3.1->mplhep) (41.0.0.post20200313)

[1mRooFit v3.60 -- Developed by Wouter Verkerke and David Kirkby[0m 
                Copyright (C) 2000-2013 NIKHEF, University of California & Stanford University
                All rights reserved, please read http://roofit.sourceforge.net/license.txt

Starting program...

Importing files...
p0: 0.7933596019725393
p1: -234.1182991816887
p2: 61575.12643748015
p3: -10460036.578378828
p4: 963491071.2946823
p5: -36798875936.82348
p6: -9.10564559877461e-06
p7: 4.220995572403854e-10
Files imported.

Reading settings...
Mass_5
6625.0
K
1554.0
Setting read

Signal/Bkg evaluated.

Generating average map...

0
100
200
300
400
500
600
700
800
900
Loading model...
Starting new point...
Evaluating signal...
Beginnning toy experiment of background + signal
Toy experiments of background + signal done.

Beginnning toy experiment of background only
Toy experiments of background only done.

P_Value_List_Method1_Exclusion
[0.1500000000000001]
P_Value_List_Method1_Discovery
[0.1240000000000001]
P_Value_List_Method2_Exclusion
[0.16479782518099462]
P_Value_List_Method2_Discovery
[0.11987678441464045]
