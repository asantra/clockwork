############################
#   Autoencoder Trainer   #
###########################

#Explanations
# - This code trains an autoencoder to encode a background.

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys, scipy.stats
import time
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)


### start time
start = time.perf_counter()
#Directory
Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Autoencoder/"


#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))
from Custom_Modules import Basic, Background, LSign, TestS, WT, Couplings
import SigBkg_Functions as SB


#Machine learning
import keras, sklearn
import tensorflow as tf
from keras.callbacks         import EarlyStopping
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D, Reshape, UpSampling2D
from keras.layers.merge      import add, concatenate
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras                   import backend as K

#Function to pad matrices

#3D padding
def padder3D(matIn, L, M, N):

  #Convert to numpy array
  mattemp = numpy.array(matIn)

  #Read old dimensions
  dimXold = mattemp.shape[0]
  dimYold = mattemp.shape[1]
  dimZold = mattemp.shape[2]

  #Decide new dimensions
  dimXnew = math.ceil(dimXold/L)*L
  dimYnew = math.ceil(dimYold/M)*M
  dimZnew = math.ceil(dimZold/N)*N

  #Initialize new matrices with proper dimensions
  matOut = numpy.zeros((dimXnew, dimYnew, dimZnew))

  #Fill in the initial numbers
  matOut[:dimXold, :dimYold, :dimZold] = mattemp

  #Return padded matrix
  return matOut 


import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')
argus = parser.parse_args()


#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning

if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 4200
  nBins   = 3975 #2935
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2

massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = bool(argus.Cone) #False
NconeR = int(argus.NconeR) #2


print("Files imported.")
print("")


#Define event type to give interger or float number of events - USE float!!! Will remove interger option
#if(argus.event=="int"):
  #eventType = "int"
#if(argus.event=="float"):
  #eventType = "float"

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  luminosity = 139
  year = '2015-18'
  ### train on the uncertainty
  if argus.uncertainty == "yes":
    #### take this ToySystNumber to be 1, just the first one
    #### when training, we will change the ToySystNumber upto 5000
    BkgFix = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, 1)  
  else:
    BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)
  

if(argus.b=="Diphoton_36fb_Bkg"):
  luminosity = 36.1
  year = '2015-16'
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  luminosity = 139
  year = '2015-18'
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

#Lets name a folder based on the bkg to write all the outputs too and make it 
try:
  if not os.path.exists("Autoencoder_Offline_Outputs/"):
    os.mkdir("Autoencoder_Offline_Outputs/")
    
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""
  os.mkdir("Autoencoder_Offline_Outputs/"+Output_Folder)
  os.mkdir("Autoencoder_Offline_Outputs/"+Output_Folder+"/AutoencoderWeights_Offline")
  os.mkdir("Autoencoder_Offline_Outputs/"+Output_Folder+"/History_Plots")
  os.mkdir("Autoencoder_Offline_Outputs/"+Output_Folder+"/Pvalues")
  os.mkdir("Autoencoder_Offline_Outputs/"+Output_Folder+"/TestStatistic_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)



afterDir = time.perf_counter()
print(f"Time taken to prepare directories {afterDir-start:0.4f} seconds")
#File to save weights and biases
nameModelFile = Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_Background_NNweights_biases.txt"

#Number of trials for p-value map and number of training backgrounds for neural network
nTrials   = int(argus.train) #5000
nTraining = int(argus.train) #5000
 

#Number of threads to use
NumberThreads = 8

#Number of epochs to train
NumberEpochs = 100 #100

print("Setting read")
print("")



print("Background evaluated.")
print("")

#Grid of toy experiments
print("Generating average map...")
print("")
beforeToyExp = time.perf_counter()

toyExpGrid1, _                 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "Poisson", argus.uncertainty, nBins)

afterToyExp = time.perf_counter()
print(f"Time taken to get toyExp outside of loop  {afterToyExp - beforeToyExp:0.4f} seconds")
wMinGrid, wMaxGrid, pvalueGrid = LSign.pvalueGridMaker(toyExpGrid1)
afterpValue = time.perf_counter()
print(f"Time taken to get pValue outside of loop  {afterpValue - afterToyExp:0.4f} seconds")


#Training neural network
print("Beginning generation of training events...")
print("")



#Generate events for training and testing
X_train = []
Y_train = []

trainingTime = time.perf_counter()
print(f"Time taken to go to before loop from the begining {trainingTime - afterDir:0.4f} seconds")

for i in range(0, nTraining):
    
  ### select different bkg fix for different training loop
  beforeLoop = time.perf_counter()
  
  
  if argus.uncertainty == "yes":
        BkgFix = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i)
  
  
  #Generate random binned events
  eventsInt = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
  
  eventsIntGet = time.perf_counter()
  print(f"Time taken to get eventsInt in the loop {eventsIntGet - beforeLoop:0.4f} seconds")
  
  # print (eventsInt)
  #Do wavelet transform
  cwtmatBSIntNorm, _, _ =  WT.WaveletTransform(eventsInt, mergeX, mergeY, Cone, NconeR)
  cwtmatBSIntNormdAv    = -numpy.log(LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBSIntNorm))
  
  wtGet = time.perf_counter()
  print(f"Time taken to get WT in the loop {wtGet - eventsIntGet:0.4f} seconds")
  
  #Reshape matrix
  sizeX = 60
  sizeY = 56
  cwtmatBSIntNorm = cwtmatBSIntNormdAv[0:sizeX, 0:sizeY]

  #Append results
  X_train.append(cwtmatBSIntNorm)
  Y_train.append(cwtmatBSIntNorm)
    
  #Make report
  if i%100 == 0:
    print(i)


trainLoop = time.perf_counter()
print(f"Time taken to finish train loop {trainLoop - wtGet:0.4f} seconds")
#Convert to numpy array
X_train = numpy.array(X_train)
Y_train = numpy.array(Y_train)


#Pad input to make 4 x 4
if(argus.b=="Dielectron_Bkg"):
  X_train = padder3D(X_train, 1, 4, 4)
  Y_train = padder3D(Y_train, 1, 4, 4)

#Dimension info
dimWX = (X_train.shape[1], X_train.shape[2])
dimWY = (Y_train.shape[1], Y_train.shape[2])

#Save dimWX shape to use in testing, save to weights folder
numpy.save(Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_dimWX.npy", dimWX)
#Save dimWY shape to use in testing, save to weights folder
numpy.save(Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_dimWY.npy", dimWY)



#Format
X_train = numpy.ravel(X_train)
Y_train = numpy.ravel(Y_train)

X_train = numpy.reshape(X_train, (nTraining, dimWX[0], dimWX[1], 1))
Y_train = numpy.reshape(Y_train, (nTraining, dimWX[0], dimWX[1], 1))

print('Generation of training events completed.')
print("")



#Define model for autoencoder
config = tf.compat.v1.ConfigProto()
config.intra_op_parallelism_threads = NumberThreads
config.inter_op_parallelism_threads = NumberThreads
sess = tf.compat.v1.Session(config=config)

model1 = Sequential()

#Add model layers
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same', input_shape=(dimWX[0], dimWX[1], 1)))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(Flatten())
model1.add(Dense(40, activation='elu'))
model1.add(Dense(20, activation='elu')) #Encoded layer
model1.add(Dense(40, activation='elu'))
model1.add(Dense(int(dimWX[0]*dimWX[1]/16*128), activation='elu'))
model1.add(Reshape((int(dimWX[0]/4), int(dimWX[1]/4), 128)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(1, kernel_size=(3, 3), activation='elu', padding='same'))

#Compile
model1.compile(optimizer='adam',loss='mean_squared_error', metrics=['accuracy'])

modelTime = time.perf_counter()
print(f"Time taken to model compile {modelTime - trainLoop:0.4f} seconds")

#Checkpoint
checkpoint = keras.callbacks.ModelCheckpoint(nameModelFile, verbose=1, monitor='val_loss',save_best_only=True, mode='auto') 

#Train model
train_history = model1.fit(X_train, Y_train, batch_size=1000, epochs=NumberEpochs, validation_split=0.2, callbacks=[checkpoint])

trainTime = time.perf_counter()
print(f"Time taken to train model {trainTime - modelTime:0.4f} seconds")

fig_history, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(train_history.history['loss'],     label = 'loss')
plt.plot(train_history.history['val_loss'], label = 'val loss')
plt.legend(loc=0)
plt.ylabel('loss')
plt.xlabel('Epochs')
plt.margins(0)
plt.legend(['train', 'val'], loc='upper left')
fig_history.savefig(Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/History_Plots/history_modelloss.png")

# summarize history for accuracy
fig_accuracy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(train_history.history['acc'])
plt.plot(train_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
fig_accuracy.savefig(Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/History_Plots/history_accuracy.png")


model1.save(Directory+"Autoencoder_Offline_Outputs/"+Output_Folder+"/AutoencoderWeights_Offline/model_Background.h5") 
print('Complete')

final = time.perf_counter()
print(f"Plotting time taken {final-trainTime:0.4f} s")
