#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)
#Import custom

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
argus = parser.parse_args()

import mplhep as hep
hep.set_style(hep.style.ROOT)

kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)


Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""

K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)

N, M = len(K_List), len(M5_List)
Z = numpy.zeros((N, M))
K_List_2D, M5_List_2D = numpy.meshgrid(M5_List, K_List)
for i, (x,y) in enumerate(product(K_List,M5_List)):
    Z[numpy.unravel_index(i, (N,M))] = loadtxt("/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Autoencoder/Autoencoder_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2_Discovery"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")


#Make Contour plots of Pvalues from this Searcha nd compare tot he theory results
fig_pvalue_contour,ax =plt.subplots(1,1)
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 36.1, ax=ax)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
c_alpha_At = plt.contour(K_List_2D, M5_List_2D, Z_At, levels = (0.0445,0.0455), linestyles = 'dashed' , colors = ['blue'])
h1,_ = c_alpha_At.legend_elements()
plt.xlabel('M5 [GeV]', ha='right', x=1.0)
plt.ylabel('k [GeV]', ha='right', y=1.0)
plt.semilogy()
plt.xlim((1000,8000))
plt.ylim((200,2600))
plt.setp(c_alpha_At.collections , linewidth=3)
plt.legend([h1[0]], ['Autoencoder'],loc = 'lower left')
# fig_pvalue_contour.colorbar(c_alpha) # Add a colorbar to a plot
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap_Method.png')
