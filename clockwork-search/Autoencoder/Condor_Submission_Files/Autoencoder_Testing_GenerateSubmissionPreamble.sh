#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
#### lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib"
pip3 install --user ipywidgets==7.5.1
pip3 install --user mplhep==0.2.9
pip3 install --user matplotlib==3.4.2
pip3 install --user parton --upgrade
python3 Condor_Autoencoder_Testing.py -k $1 -M5 $2 -s $3 -b $4 -test $5 -bin $6 -train $7 -Cone $8 -NconeR $9 -uncertainty ${10}
