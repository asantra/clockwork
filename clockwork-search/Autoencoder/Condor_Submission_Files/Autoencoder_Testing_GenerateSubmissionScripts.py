# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, scipy, random, os, sys
from itertools import product

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')
argus = parser.parse_args()

#Directory
Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Autoencoder/"


Sig = str(argus.s)
Bkg = str(argus.b)
kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)
test = int(argus.test)
bins = int(argus.bin)
train = int(argus.train)
Cone   = str(argus.Cone)
NconeR = int(argus.NconeR)
Uncertainty   = str(argus.uncertainty)

# try:
  # Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder)
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/Error_Condor")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/History_Plots")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/Out_Condor")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor")
#   os.mkdir(Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/TestStatistic_plots")
# except OSError:
#     print ("Creation of the directory %s failed" % Output_Folder)
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder)
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/Error_Condor")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/History_Plots")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/Out_Condor")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor")
#     # os.mkdir("Autoencoder_Condor_Outputs/"+Output_Folder+"/TestStatistic_plots")
# else:
#     print ("Successfully created the directory %s" % Output_Folder)


# Open file and write common part
cfile = open('condor_testing_Autoencoder.sub','w')
common_command = \
'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = Autoencoder_Testing_GenerateSubmissionPreamble.sh \n\
initialdir      = /afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/ \n\
Should_transfer_files = YES \n\
+JobFlavour = "nextweek" \n\
when_to_transfer_output = ON_EXIT \n\
'
cfile.write(common_command)
 
# Loop over various values of an argument and create different output file for each
# Then put it in the queue
K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)
# K_List = numpy.linspace(250,5500,30)
# M5_List = numpy.linspace(1000, 20000, 25)
for i, (k,m) in enumerate(product(K_List,M5_List)):
    run_command =  \
'arguments  = %d %d %s %s %d %d %d %s %d %s \n\
error                 = Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Error_Condor/Autoencoder_testing_%d_%d_$(Process).err \n\
log                   = Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Logs_Condor/Autoencoder_testing_%d_%d_$(Process).log \n\
transfer_input_files= SignalBackground_Generation/Inputs_ee, SignalBackground_Generation/Inputs_yy, SignalBackground_Generation/Inputs_ee/ToysfromMC.root, SignalBackground_Generation/Inputs_ee/ee_accEffFunction.root, SignalBackground_Generation/Inputs_ee/ee_relResolFunction.root, SignalBackground_Generation/Inputs_ee/finalbkg.root, SignalBackground_Generation/Inputs_ee/finalbkg_MC.root, SignalBackground_Generation/Inputs_ee/mInv_spectrum_combined_ll_rel21_fullRun2.root, SignalBackground_Generation/Inputs_ee/nominal_ee.root, SignalBackground_Generation/Inputs_yy/TH1D_h024_HighLowMass_data.root, SignalBackground_Generation/Inputs_yy/finalbkg_yy.root, SignalBackground_Generation/Custom_Modules, SignalBackground_Generation/Custom_Modules/Background.py, SignalBackground_Generation/Custom_Modules/Basic.py, SignalBackground_Generation/Custom_Modules/Couplings.py, SignalBackground_Generation/Custom_Modules/Graviton.py, SignalBackground_Generation/Custom_Modules/KKcascade.py, SignalBackground_Generation/Custom_Modules/LSign.py, SignalBackground_Generation/Custom_Modules/Luminosity.py, SignalBackground_Generation/Custom_Modules/Merger.py, SignalBackground_Generation/Custom_Modules/PDFcalc.py, SignalBackground_Generation/Custom_Modules/Smearing.py, SignalBackground_Generation/Custom_Modules/TestS.py, SignalBackground_Generation/Custom_Modules/WT.py, SignalBackground_Generation/SigBkg_Functions.py, SignalBackground_Generation/Graviton_Properties, SignalBackground_Generation/Uncertainties/toys_allUncertainties.root, SignalBackground_Generation/Graviton_Properties/Dielectron_Transform_01GeV.txt, SignalBackground_Generation/Graviton_Properties/GravitonPropertiesDielectron.txt, SignalBackground_Generation/Graviton_Properties/GravitonPropertiesDiphoton36fb.txt, SignalBackground_Generation/Graviton_Properties/HighMass_GravitonProperties.txt, Autoencoder/Condor_Autoencoder_Testing.py, Autoencoder/Condor_Submission_Files/Autoencoder_Testing_GenerateSubmissionPreamble.sh,  Autoencoder/SigBkg_Autoencoder.py, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/AutoencoderWeights_Condor, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Logs_Condor, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/TestStatistic_plots, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Error_Condor, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Out_Condor, Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Pvalues_Condor   \n\
output     = Autoencoder/Autoencoder_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Out_Condor/Autoencoder_testing_%d_%d_out$(Process).txt\n\
queue 1\n\n' %(k,m,Sig,Bkg,test,bins,train,Cone,NconeR,Uncertainty, Bkg,Sig,test,train,Cone,NconeR,Uncertainty,k,m,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,k,m,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,Bkg,Sig,test,train,Cone,NconeR,Uncertainty,k,m) 
    cfile.write(run_command)


