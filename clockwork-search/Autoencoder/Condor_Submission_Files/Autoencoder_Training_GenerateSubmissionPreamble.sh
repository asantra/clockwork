#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# rm -rf /afs/cern.ch/user/a/asantra/.local/lib/python3.7/site-packages/mplhep
# rm -rf /afs/cern.ch/user/a/asantra/.local/lib/python3.7/site-packages/matplotlib
lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
#### lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib"
pip3 install --user ipywidgets==7.5.1
pip3 install --user mplhep==0.2.9
pip3 install --user matplotlib==3.4.2
pip3 install --user parton --upgrade
# python << EOF
# import mplhep as hep
# hep.set_style(hep.style.ROOT)
# EOF
python3 Condor_Autoencoder_Trainer.py -s $1 -b $2 -test $3 -train $4 -Cone $5 -NconeR $6 -uncertainty $7
