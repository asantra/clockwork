#################################
#   Autoencoder Signal FInder   #
#################################

#Explanations
# - This code uses a previously trained autoencoder to find signals

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys, scipy.stats

#Directory
Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Autoencoder/"

AutoencoderDirectory = "Autoencoder_Offline_Outputs/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

from Custom_Modules import Merger, LSign, TestS, WT, Couplings
import SigBkg_Functions as SB

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)

#Machine learning
import keras, sklearn
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D, Reshape, UpSampling2D
from keras.layers.merge      import add, concatenate
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras.utils import plot_model

#Function to pad matrices

#3D padding
def padder3D(matIn, L, M, N):

  #Convert to numpy array
  mattemp = numpy.array(matIn)

  #Read old dimensions
  dimXold = mattemp.shape[0]
  dimYold = mattemp.shape[1]
  dimZold = mattemp.shape[2]

  #Decide new dimensions
  dimXnew = math.ceil(dimXold/L)*L
  dimYnew = math.ceil(dimYold/M)*M
  dimZnew = math.ceil(dimZold/N)*N

  #Initialize new matrices with proper dimensions
  matOut = numpy.zeros((dimXnew, dimYnew, dimZnew))

  #Fill in the initial numbers
  matOut[:dimXold, :dimYold, :dimZold] = mattemp

  #Return padded matrix
  return matOut 

#2D padding
def padder2D(matIn, M, N):

  #Convert to numpy array
  mattemp = numpy.array(matIn)

  #Read old dimensions
  dimXold = mattemp.shape[0]
  dimYold = mattemp.shape[1]

  #Decide new dimensions
  dimXnew = math.ceil(dimXold/M)*M
  dimYnew = math.ceil(dimYold/N)*N

  #Initialize new matrices with proper dimensions
  matOut = numpy.zeros((dimXnew, dimYnew))

  #Fill in the initial numbers
  matOut[:dimXold, :dimYold] = mattemp

  #Return padded matrix
  return matOut  


print("Files imported.")
print("")

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')

argus = parser.parse_args()

#Settings
print("Reading settings...")

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775 #2935
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2

massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = bool(argus.Cone)
NconeR = int(argus.NconeR) #2

#Define Test Statitic Binning
bins = int(argus.bin)

#Read in k/M5

M5 = float(argus.M5)
k = float(argus.k)


print("Mass_5")
print(M5)
print("K")
print (k)

#Signal Parameter strength
aS = 1


#Empty Test statitic lists
Lambda_BS = []
Lambda_BS_Uncertainty = []
Lambda_Bonly = []
Lambda_Bonly_Uncertainty = []

print("Setting read")
print("")

#Generate signal and Background
#####Signal#####
if(argus.s=="Tail_Damped_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax)

if(argus.s=="Theory_Diphoton_36fb_Sig"):
  luminosity = 36.1
  year = '2015-16'
  SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins)

if(argus.s=="Theory_Dielectron_Sig"):
  luminosity = 139
  year = '2015-18'
  #SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
  if(argus.uncertainty=="no"):
    SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
  if(argus.uncertainty=="yes"):
    SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, lum = SB.SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, "All")

if(argus.s=="Theory_HighMass_Diphoton_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins)

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

if(argus.b=="Diphoton_36fb_Bkg"):
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

print("Signal/Bkg evaluated.")
print("")

#Lets load a folder based on the signal and bkg used in the terminal to write all the outputs too , should match the training
Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""


#Make empty lists to appen Pvalues, and 1 Sigma,2 sigma bands
#P_values evalauted at ther median

P_Value_List_Method2_Exclusion = []
P_Value_List_Method2_Discovery = []
P_Value_List_Method2_Exclusion_Uncertainty = []
P_Value_List_Method2_Discovery_Uncertainty = []

#P_values list at the 1,2 sigma bands for exclusion 
P_Value_List_plus_1sigma_Exclusion = []
P_Value_List_minus_1sigma_Exclusion = []
P_Value_List_plus_1sigma_Exclusion_Uncertainty = []
P_Value_List_minus_1sigma_Exclusion_Uncertainty = []
#P_values list at the 1,2 sigma bands for discovery 
P_Value_List_plus_1sigma_Discovery = []
P_Value_List_minus_1sigma_Discovery = []
P_Value_List_plus_1sigma_Discovery_Uncertainty = []
P_Value_List_minus_1sigma_Discovery_Uncertainty = []




nameModelFile = Directory+AutoencoderDirectory+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_Background_NNweights_biases.txt"
ModelFile = Directory+AutoencoderDirectory+Output_Folder+"/AutoencoderWeights_Offline/model_Background.h5"
Pvalue_out_Method2_Exclusion = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method2_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method2_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Discovery_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method2_Discovery_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Exclusion_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method2_Exclusion_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


Pvalue_out_Method2_Exclusion_plus_1sigma = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_plus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_plus_1sigma = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_plus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Exclusion_plus_1sigma_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_plus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_plus_1sigma_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_plus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


Pvalue_out_Method2_Exclusion_minus_1sigma = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_minus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_minus_1sigma = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_minus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Exclusion_minus_1sigma_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_minus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_minus_1sigma_Uncertainty = open(Directory+AutoencoderDirectory+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_minus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


#Number of trials for p-value map, background test statistic and signal + background test statistic
nTrials  = int(argus.test) #500
nTrials1 = int(argus.test) #500
nTrials2 = int(argus.test) #500


#Grid of toy experiments
print("Generating average map...")
print("")
toyExpGrid1, _                 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "Poisson")
wMinGrid, wMaxGrid, pvalueGrid = LSign.pvalueGridMaker(toyExpGrid1)

# Load dimWX
dimWX = numpy.load(Directory+AutoencoderDirectory+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_dimWX.npy")
dimWY = numpy.load(Directory+AutoencoderDirectory+Output_Folder+"/AutoencoderWeights_Offline/Autoencoder_dimWY.npy")
#Define model for region finder
print('Loading model...')

sizeX = 60
sizeY = 56

model1 = Sequential()

model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same', input_shape=(dimWX[0], dimWX[1], 1)))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(Flatten())
model1.add(Dense(40, activation='elu'))
model1.add(Dense(20, activation='elu')) #Encoded layer
model1.add(Dense(40, activation='elu'))
model1.add(Dense(int(dimWX[0]*dimWX[1]/16*128), activation='elu'))
model1.add(Reshape((int(dimWX[0]/4), int(dimWX[1]/4), 128)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(1, kernel_size=(3, 3), activation='elu', padding='same'))


model1.compile(optimizer='adam',loss='mean_squared_error')
model1.load_weights(nameModelFile)


#Apply machine learning
print("Starting new point...")
print("Evaluating signal...")


#Generate trial background + signal
#Initialize variables
print("Beginnning toy experiment of background + signal")

statTest1 = []

for i in range(0, nTrials1):

  if (argus.uncertainty=="no"):
    
    #Generate random binned event
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)

    #Do wavelet transform
    cwtmatBSTrial1Norm, _, _ =  WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
    cwtmatBSTrial1NormdAv    = LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBSTrial1Norm)
    cwtmatBSTrial1Norm       =  cwtmatBSTrial1NormdAv[0:sizeX, 0:sizeY]

    if(argus.b=="Diphoton_36_Bkg"):
      cwtmatBSTrial1Norm = numpy.array(cwtmatBSTrial1Norm)
    if(argus.b=="Dielectron_Bkg"):
      cwtmatBSTrial1Norm = padder2D(cwtmatBSTrial1Norm, 1, 4)

    #Format for predictions
    cwtmatBSTrial1Flat      = numpy.ravel(cwtmatBSTrial1Norm)
    cwtmatBSTrial1Formatted = numpy.reshape(cwtmatBSTrial1Flat,(1, dimWX[0], dimWX[1], 1))

    #Make prediction
    temp1 = model1.predict(cwtmatBSTrial1Formatted)[0]

    #Apply test statistic
    rIn  = numpy.ravel(cwtmatBSTrial1Norm)
    rOut = numpy.ravel(temp1)
    ts   = TestS.testStatCalc5(rIn, rOut)

    #Calculate test statistic FT
    Lambda_BS.append(ts)

  if (argus.uncertainty=="yes"):


    Sig_Unc = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, BkgFix, xmin, xmax,i,"WT")
    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i, eventType)

    
    #Generate random binned event
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    eventsTrial1_Uncertainity = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, Sig_Unc, 1, 1)

    #Do wavelet transform
    cwtmatBSTrial1Norm, _, _ =  WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
    cwtmatBTrial1Norm_Uncertainity, _, _ = WT.WaveletTransform(eventsTrial1_Uncertainity, mergeX, mergeY, Cone, NconeR)

    cwtmatBSTrial1NormdAv    =  LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBSTrial1Norm)
    cwtmatBSTrial1NormdAv_Uncertainity    =  LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBTrial1Norm_Uncertainity)

    cwtmatBSTrial1Norm       =  cwtmatBSTrial1NormdAv[0:sizeX, 0:sizeY]
    cwtmatBSTrial1Norm_Uncertainity       =  cwtmatBSTrial1NormdAv_Uncertainity[0:sizeX, 0:sizeY]

    if(argus.b=="Diphoton_36_Bkg"):
      cwtmatBSTrial1Norm = numpy.array(cwtmatBSTrial1Norm)
    if(argus.b=="Dielectron_Bkg"):
      cwtmatBSTrial1Norm = padder2D(cwtmatBSTrial1Norm, 1, 4)
      cwtmatBSTrial1Norm_Uncertainity = padder2D(cwtmatBSTrial1Norm_Uncertainity, 1, 4)

    #Format for predictions
    cwtmatBSTrial1Flat      = numpy.ravel(cwtmatBSTrial1Norm)
    cwtmatBSTrial1Flat_Uncertainity      = numpy.ravel(cwtmatBSTrial1Norm)

    cwtmatBSTrial1Formatted = numpy.reshape(cwtmatBSTrial1Flat,(1, dimWX[0], dimWX[1], 1))
    cwtmatBSTrial1Formatted_Uncertainity = numpy.reshape(cwtmatBSTrial1Flat_Uncertainity,(1, dimWX[0], dimWX[1], 1))

    #Make prediction
    temp1 = model1.predict(cwtmatBSTrial1Formatted)[0]
    temp1_Uncertainity = model1.predict(cwtmatBSTrial1Formatted_Uncertainity)[0]

    #Apply test statistic
    rIn  = numpy.ravel(cwtmatBSTrial1Norm)
    rOut = numpy.ravel(temp1)
    ts   = TestS.testStatCalc5(rIn, rOut)

    rIn_Uncertainity  = numpy.ravel(cwtmatBSTrial1Norm_Uncertainity)
    rOut_Uncertainity = numpy.ravel(temp1_Uncertainity)
    ts_Uncertainity   = TestS.testStatCalc5(rIn_Uncertainity, rOut_Uncertainity)

    #Calculate test statistic FT
    Lambda_BS.append(ts)
    Lambda_BS_Uncertainity.append(ts_Uncertainity)

print("Toy experiments of background + signal done.")
print("")




#Generate trial background + signal
#Initialize variables
print("Beginnning toy experiment of background only")
statTest2 = []

for i in range(0, nTrials2):

  if (argus.uncertainty=="no"):
    
    #Generate random binned event
    eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    #Do wavelet transform
    cwtmatBSTrial2Norm, _, _ =  WT.WaveletTransform(eventsTrial2, mergeX, mergeY, Cone, NconeR)
    cwtmatBSTrial2NormdAv    = -numpy.log(LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBSTrial2Norm))
    cwtmatBSTrial2Norm       =  cwtmatBSTrial2NormdAv[0:sizeX, 0:sizeY]

    if(argus.b=="Diphoton_36_Bkg"):
      cwtmatBSTrial2Norm = numpy.array(cwtmatBSTrial2Norm)
    if(argus.b=="Dielectron_Bkg"):
      cwtmatBSTrial2Norm = padder2D(cwtmatBSTrial2Norm, 1, 4)

    

    #Format for predictions
    cwtmatBSTrial2Flat      = numpy.ravel(cwtmatBSTrial2Norm)
    cwtmatBSTrial2Formatted = numpy.reshape(cwtmatBSTrial2Flat,(1, dimWX[0], dimWX[1], 1))

    #Make prediction
    temp2 = model1.predict(cwtmatBSTrial2Formatted)[0]

    #Apply test statistic
    rIn  = numpy.ravel(cwtmatBSTrial2Norm)
    rOut = numpy.ravel(temp2)
    ts   = TestS.testStatCalc5(rIn, rOut)

    #Calculate test statistic FT
    Lambda_Bonly.append(ts)

  if (argus.uncertainty=="yes"):
    
    Sig_Unc = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, BkgFix, xmin, xmax,i,"WT")
    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i, eventType)

    
    #Generate random binned event
    eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    eventsTrial2_Uncertainity = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, Sig_Unc, 1, 1)

    #Do wavelet transform
    cwtmatBSTrial2Norm, _, _ =  WT.WaveletTransform(eventsTrial2, mergeX, mergeY, Cone, NconeR)
    cwtmatBTrial2Norm_Uncertainity, _, _ = WT.WaveletTransform(eventsTrial2_Uncertainity, mergeX, mergeY, Cone, NconeR)

    cwtmatBSTrial2NormdAv    =  LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBSTrial2Norm)
    cwtmatBSTrial2NormdAv_Uncertainity    =  LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBTrial2Norm_Uncertainity)

    cwtmatBSTrial2Norm       =  cwtmatBSTrial2NormdAv[0:sizeX, 0:sizeY]
    cwtmatBSTrial2Norm_Uncertainity       =  cwtmatBSTrial2NormdAv_Uncertainity[0:sizeX, 0:sizeY]

    if(argus.b=="Diphoton_36_Bkg"):
      cwtmatBSTrial2Norm = numpy.array(cwtmatBSTrial2Norm)
    if(argus.b=="Dielectron_Bkg"):
      cwtmatBSTrial2Norm = padder2D(cwtmatBSTrial2Norm, 1, 4)
      cwtmatBSTrial2Norm_Uncertainity = padder2D(cwtmatBSTrial2Norm_Uncertainity, 1, 4)

    #Format for predictions
    cwtmatBSTrial2Flat      = numpy.ravel(cwtmatBSTrial2Norm)
    cwtmatBSTrial2Flat_Uncertainity      = numpy.ravel(cwtmatBSTrial2Norm)

    cwtmatBSTrial2Formatted = numpy.reshape(cwtmatBSTrial2Flat,(1, dimWX[0], dimWX[1], 1))
    cwtmatBSTrial2Formatted_Uncertainity = numpy.reshape(cwtmatBSTrial2Flat_Uncertainity,(1, dimWX[0], dimWX[1], 1))

    #Make prediction
    temp2 = model1.predict(cwtmatBSTrial2Formatted)[0]
    temp2_Uncertainity = model1.predict(cwtmatBSTrial2Formatted_Uncertainity)[0]

    #Apply test statistic
    rIn  = numpy.ravel(cwtmatBSTrial2Norm)
    rOut = numpy.ravel(temp2)
    ts   = TestS.testStatCalc5(rIn, rOut)

    rIn_Uncertainity  = numpy.ravel(cwtmatBSTrial2Norm_Uncertainity)
    rOut_Uncertainity = numpy.ravel(temp2_Uncertainity)
    ts_Uncertainity   = TestS.testStatCalc5(rIn_Uncertainity, rOut_Uncertainity)

    #Calculate test statistic FT
    Lambda_BS.append(ts)
    Lambda_BS_Uncertainity.append(ts_Uncertainity)

print("Toy experiments of background only done.")
print("")
################################################################################################
################################################################################################
################################################################################################
################################################################################################

if (argus.uncertainty=="no"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(Lambda_Bonly)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Lambda_Bonly)))]

  #Try and caluclate upper and lower 1 sigma values for Exclusion
  B_Only_LowerQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.159)
  B_Only_UpperQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.841)

  #Discovery Median
  BS_SortedList     = numpy.sort(Lambda_BS)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(Lambda_BS)))]

  #Try and caluclate upper and lower 1 sigma values for Discovery Calculate median, and 1 s test statistic using 0.159, 0.5, 0.841 quantiles.
  BS_LowerQuantile_1sigma = numpy.quantile(Lambda_BS, 0.159)
  BS_UpperQuantile_1sigma = numpy.quantile(Lambda_BS, 0.841)

  #Make histograms
  Lambda_BS_hist = numpy.histogram(Lambda_BS, bins)
  Lambda_Bonly_hist = numpy.histogram(Lambda_Bonly, bins)
  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)

  #Try and caluclate P-value for Exclusion median and write to list
  P_Value_Exclusion = (Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
  P_Value_List_Method2_Exclusion.append(P_Value_Exclusion)

  #Work out P-values for Exclusion at different sigma positions and write to list to save at the end of file
  P_Value_plus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_LowerQuantile_1sigma))

  P_Value_List_plus_1sigma_Exclusion.append(P_Value_plus_1sigma_Exclusion)
  P_Value_List_minus_1sigma_Exclusion.append(P_Value_minus_1sigma_Exclusion)


  #Try and caluclate P-value for Discovery and write to list
  P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
  P_Value_List_Method2_Discovery.append(P_Value_Discovery)

  print("Method2: Min Exclusion value is:", min(P_Value_List_Method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(P_Value_List_Method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(P_Value_List_Method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(P_Value_List_Method2_Discovery,default=0))

  #Work out P-values for Discovery at different sigma positions and write to luist to save at the end of file
  P_Value_plus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_LowerQuantile_1sigma))
  P_Value_List_plus_1sigma_Discovery.append(P_Value_plus_1sigma_Discovery)
  P_Value_List_minus_1sigma_Discovery.append(P_Value_minus_1sigma_Discovery)
  #Wrtite all Pvalues,Sigma bands to seperate files to avoid errors to beiung hidden withihn one file
  #Pvalue at the median

  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")
  #Pvalue at the Upper 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma, P_Value_List_plus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma, P_Value_List_plus_1sigma_Discovery, fmt="%1.10f")
  #Pvalue at the Lower 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma, P_Value_List_minus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma, P_Value_List_minus_1sigma_Discovery, fmt="%1.10f")



  #Test Statistic Plotting

  Probably_Dist, ax = plt.subplots()
  ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
  plt.hist(Lambda_Bonly, bins,label="Bkg Only", alpha=0.4)
  plt.hist(Lambda_BS, bins, label="Bkg + Signal" , alpha=0.4)
  plt.ticklabel_format(style = 'plain')
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
  # ax.set_ylabel('', ha='right', y=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc='best')
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+AutoencoderDirectory+Output_Folder+"/TestStatistic_plots/TestDistribution_"+str(k)+"_"+str(M5)+".png")

if (argus.uncertainty=="yes"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(Lambda_Bonly)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Lambda_Bonly)))]
  B_Only_SortedList_Uncertainty     = numpy.sort(Lambda_Bonly_Uncertainty)
  B_OnlyMedian_Uncertainty = B_Only_SortedList_Uncertainty[int(numpy.floor(0.5*len(Lambda_Bonly_Uncertainty)))]
  #Try and caluclate upper and lower 1 sigma values for Exclusion
  B_Only_LowerQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.159)
  B_Only_UpperQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.841)
  B_Only_LowerQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_Bonly_Uncertainty, 0.159)
  B_Only_UpperQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_Bonly_Uncertainty, 0.841)

  #Discovery Median
  BS_SortedList     = numpy.sort(Lambda_BS)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(Lambda_BS)))]
  BS_SortedList_Uncertainty     = numpy.sort(Lambda_BS_Uncertainty)
  BS_Median_Uncertainty = BS_SortedList_Uncertainty[int(numpy.floor(0.5*len(Lambda_BS_Uncertainty)))]

  #Try and caluclate upper and lower 1 sigma values for Discovery Calculate median, and 1 s test statistic using 0.159, 0.5, 0.841 quantiles.
  BS_LowerQuantile_1sigma = numpy.quantile(Lambda_BS, 0.159)
  BS_UpperQuantile_1sigma = numpy.quantile(Lambda_BS, 0.841)
  BS_LowerQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_BS_Uncertainty, 0.159)
  BS_UpperQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_BS_Uncertainty, 0.841)

  #Make histograms
  Lambda_BS_hist = numpy.histogram(Lambda_BS, bins)
  Lambda_BS_hist_Uncertainty = numpy.histogram(Lambda_BS_Uncertainty, bins)
  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist)
  Lambda_BS_distribution_rv_Uncertainty = scipy.stats.rv_histogram(Lambda_BS_hist_Uncertainty)

  Lambda_Bonly_hist = numpy.histogram(Lambda_Bonly, bins)
  Lambda_Bonly_hist_Uncertainty = numpy.histogram(Lambda_Bonly_Uncertainty, bins)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)
  Lambda_Bonly_rv_Uncertainty = scipy.stats.rv_histogram(Lambda_Bonly_hist_Uncertainty)

  #Try and caluclate P-value for Exclusion median and write to list
  P_Value_Exclusion = (Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
  P_Value_List_Method2_Exclusion.append(P_Value_Exclusion)
  P_Value_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_OnlyMedian_Uncertainty)) ###check
  P_Value_List_Method2_Exclusion_Uncertainty.append(P_Value_Exclusion_Uncertainty)

  #Work out P-values for Exclusion at different sigma positions and write to list to save at the end of file
  P_Value_plus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_LowerQuantile_1sigma))
  P_Value_plus_1sigma_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_Only_UpperQuantile_1sigma_Uncertainty)) ###check
  P_Value_minus_1sigma_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_Only_LowerQuantile_1sigma_Uncertainty)) ###check


  P_Value_List_plus_1sigma_Exclusion.append(P_Value_plus_1sigma_Exclusion)
  P_Value_List_minus_1sigma_Exclusion.append(P_Value_minus_1sigma_Exclusion)
  P_Value_List_plus_1sigma_Exclusion_Uncertainty.append(P_Value_plus_1sigma_Exclusion_Uncertainty)
  P_Value_List_minus_1sigma_Exclusion_Uncertainty.append(P_Value_minus_1sigma_Exclusion_Uncertainty)

  #Try and caluclate P-value for Discovery and write to list
  P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
  P_Value_List_Method2_Discovery.append(P_Value_Discovery)
  P_Value_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_Median_Uncertainty)) ###check
  P_Value_List_Method2_Discovery_Uncertainty.append(P_Value_Discovery_Uncertainty)


  #Work out P-values for Discovery at different sigma positions and write to luist to save at the end of file
  P_Value_plus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_LowerQuantile_1sigma))
  P_Value_plus_1sigma_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_UpperQuantile_1sigma_Uncertainty)) ###check
  P_Value_minus_1sigma_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_LowerQuantile_1sigma_Uncertainty)) ###check

  P_Value_List_plus_1sigma_Discovery.append(P_Value_plus_1sigma_Discovery)
  P_Value_List_minus_1sigma_Discovery.append(P_Value_minus_1sigma_Discovery)
  P_Value_List_plus_1sigma_Discovery_Uncertainty.append(P_Value_plus_1sigma_Discovery_Uncertainty)
  P_Value_List_minus_1sigma_Discovery_Uncertainty.append(P_Value_minus_1sigma_Discovery_Uncertainty)


  print("Method2: Min Exclusion value is:", min(P_Value_List_Method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(P_Value_List_Method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(P_Value_List_Method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(P_Value_List_Method2_Discovery,default=0))

  print("Method2: Min Exclusion value with Uncertainty  is:", min(P_Value_List_Method2_Exclusion_Uncertainty, default=0))
  P_Value_List_Method2_Exclusion_Uncertainty.append(min(P_Value_List_Method2_Exclusion_Uncertainty,default=0))

  print("Method2: Min Discovery value with Uncertainty is:", min(P_Value_List_Method2_Discovery_Uncertainty,default=0))
  P_Value_List_Method2_Discovery_Uncertainty.append(min(P_Value_List_Method2_Discovery_Uncertainty,default=0))



  #Wrtite all Pvalues,Sigma bands to seperate files to avoid errors to beiung hidden withihn one file
  #Pvalue at the median

  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")

  numpy.savetxt(Pvalue_out_Method2_Discovery_Uncertainty, P_Value_List_Method2_Discovery_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion_Uncertainty, P_Value_List_Method2_Exclusion_Uncertainty, fmt="%1.10f")
  #Pvalue at the Upper 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma, P_Value_List_plus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma, P_Value_List_plus_1sigma_Discovery, fmt="%1.10f")
  
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma_Uncertainty, P_Value_List_plus_1sigma_Exclusion_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma_Uncertainty, P_Value_List_plus_1sigma_Discovery_Uncertainty, fmt="%1.10f")
  
  #Pvalue at the Lower 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma, P_Value_List_minus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma, P_Value_List_minus_1sigma_Discovery, fmt="%1.10f")

  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma_Uncertainty, P_Value_List_minus_1sigma_Exclusion_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma_Uncertainty, P_Value_List_minus_1sigma_Discovery_Uncertainty, fmt="%1.10f")


  #Test Statistic Plotting

  Probably_Dist, ax = plt.subplots()
  ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
  plt.hist(Lambda_Bonly, bins,label="Bkg Only", alpha=0.4)
  plt.hist(Lambda_BS, bins, label="Bkg + Signal" , alpha=0.4)
  plt.hist(Lambda_BS_Uncertainty, bins, label="Bkg + Signal Unc" , alpha=0.4)
  plt.ticklabel_format(style = 'plain')
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  plt.axvline(x=BS_Median_Uncertainty,color='green', linestyle='--', label = "BS_Median Unc")
  plt.axvline(x=B_OnlyMedian_Uncertainty,color='blue', linestyle='--', label = "B_OnlyMedian Unc")
  plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
  # ax.set_ylabel('', ha='right', y=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc='best')
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+AutoencoderDirectory+Output_Folder+"/TestStatistic_plots/Uncertainty_TestDistribution_"+str(k)+"_"+str(M5)+".png")


print("time taken for whole file:")
print(time.process_time() - start0)






#Method1 - Hugues/Yevgeny Median

# #Distribution of statistical significance
# significanceDistribution_Exclusion = []
# significanceDistribution_Discovery = []
# # P_Value_List_TheoryLimit = []

# #Exclusion
    
# for TS2 in Lambda_Bonly:
    
#   #Initialize variable
#   temp = 0
    
#   #Loop over background events
#   for TS1 in Lambda_BS:
#     if TS2 >= TS1:
#       temp += 1/nTrials2
    
#   #Append to list
#   significanceDistribution_Exclusion.append(temp)




# #Median Exclusion Limit
# t1     = numpy.sort(significanceDistribution_Exclusion)
# median = t1[int(numpy.floor(0.5*len(significanceDistribution_Exclusion)))]
# P_Value_List_Method1_Exclusion.append(median)
# print("P_Value_List_Method1_Exclusion")
# print(P_Value_List_Method1_Exclusion)

# #Discovery
    
# for TS1 in Lambda_BS:
    
#   #Initialize variable
#   temp = 0
    
#   #Loop over background events
#   for TS2 in Lambda_Bonly:
#     if TS2 >= TS1:
#       temp += 1/nTrials2
    
#   #Append to list
#   significanceDistribution_Discovery.append(temp)




# #Median for Discovery Limit
# t1     = numpy.sort(significanceDistribution_Discovery)
# median = t1[int(numpy.floor(0.5*len(significanceDistribution_Discovery)))]
# P_Value_List_Method1_Discovery.append(median)
# print("P_Value_List_Method1_Discovery")
# print(P_Value_List_Method1_Discovery)




