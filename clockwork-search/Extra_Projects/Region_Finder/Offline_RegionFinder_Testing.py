#############
#   RF TS   #
#############

#Explanations
# - This code uses a anomaly finder and a test statistic to set limits.

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys, scipy.stats

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/"

#Import custom
sys.path.insert(0, Directory+'Custom_Modules')
from Custom_Modules import Merger, LSign, TestS, WT
import SigBkg_RegionFinder as SB

#Machine learning
import keras, sklearn
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D
from keras.layers.merge      import add, concatenate
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split

# import mplhep as hep
# plt.style.use(hep.style.ROOT)

print("Files imported.")
print("")

import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-m', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
# parser.add_argument('-u', metavar='', help='Profile Uncertainties of the Background and Signal models')
argus = parser.parse_args()

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="Diphoton_36_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2


#Mass and scale Lists
massList = numpy.linspace(xmin, xmax, nBins)

#Settings
print("Reading settings...")


#Number of possible maximum values of the minimum value of the NN checked
NminV = 200

#Cone of influence
Cone   = False
NconeR = 2

#Define Test Statitic Binning
bins = 10

#Number of trials for p-value map, background test statistic and signal + background test statistic
nTrials  = 50 #500
nTrials1 = 10 #500
nTrials2 = 10 #500

#Read value for this line
# M5 = y
# k  = x
M5 = float(argus.m)
k = float(argus.k)

#Generate signal and Background
#Signal
if(argus.s=="Tail_Damped_Sig"):
  SigFix = SB.SigFixed_withCB(k, M5, xmin, xmax)
if(argus.s=="Theory_Diphoton_36_Sig"):
  SigFix, _ = SB.Sfix_DefaultTheory_Diphoton(k, M5, xmin, xmax, nBins)
if(argus.s=="Theory_Dielectron_Sig"):
  SigFix, _ = SB.Sfix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
#Background
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.BkgFixed(xmin, xmax)
if(argus.b=="Diphoton_36_Bkg"):
  BkgFix = SB.Bfix_Diphoton_Theory(massList)
print("Signal evaluated.")
print("")

#Lets load a folder based on the signal and bkg used in the terminal to write all the outputs too , should match the training
Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""

#Make empty lists to appen Pvalues
P_Value_List_Method1_Exclusion = []
P_Value_List_Method2_Exclusion = []
P_Value_List_Method1_Discovery = []
P_Value_List_Method2_Discovery = []

Pvalue_out_Method1_Exclusion = open(Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/Pvalues_Offline/Pvalue_Method1_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Exclusion = open(Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/Pvalues_Offline/Pvalue_Method2_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method1_Discovery = open(Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/Pvalues_Offline/Pvalue_Method1_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery = open(Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/Pvalues_Offline/Pvalue_Method2_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")





#Significance grid
print("Beginning evaluation of p-value grid...")
print("")

#Grid of toy experiments
if(argus.t=="Poisson"):
  toyExpGrid1, listavNorm1 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "Poisson")
if(argus.t=="RooFit"):
  toyExpGrid1, listavNorm1 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "RooFit")

#Grid of pvalues
wMinGrid, wMaxGrid, pvalueGrid = LSign.pvalueGridMaker(toyExpGrid1)

print("Evaluation of p-value grid done.")
print("")



#Define model for region finder
print('Loading model...')
model2 = Sequential()

#add model layers
model2.add(Conv2D(32,  kernel_size=5, activation='softplus', input_shape=(pvalueGrid.shape[0], pvalueGrid.shape[1], 1)))
model2.add(MaxPooling2D(pool_size=(2, 2)))
model2.add(Conv2D(64,  kernel_size=5, activation='softplus'))
model2.add(MaxPooling2D(pool_size=(2, 2)))
model2.add(Conv2D(128,  kernel_size=5, activation='softplus'))
model2.add(Flatten())
model2.add(Dense(5000, activation='softplus'))
model2.add(Dense(pvalueGrid.shape[0]*pvalueGrid.shape[1], activation='softplus'))

#Compile
model2.compile(optimizer='adam',loss='mean_squared_error')

#Load best weights
model2.load_weights(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Offline/RegionFinder_Weights.txt")
print('Model loaded.')


#Generate trial background + signal
print("Beginnning toy experiment of background + signal")
#Make test statitic list
Lambda_BS = []

for i in range(0, nTrials1):
    
  #Generate random binned event
  if(argus.t=="Poisson"):
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
  if(argus.t=="RooFit"):  
    eventsTrial1 = SB.ToyDataGenerator_withSignal(k, M5, xmin, xmax, "bkgsig", 1)
      

  #Do wavelet transform
  cwtmatBSTrial1Norm, _, _ = WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
  cwtmatBSTrial1NormPerAvg = cwtmatBSTrial1Norm/listavNorm1

  #Format for predictions
  cwtmatBSTrial1Flat      = numpy.ravel(cwtmatBSTrial1NormPerAvg)
  cwtmatBSTrial1Formatted = numpy.reshape(cwtmatBSTrial1Flat,(1, pvalueGrid.shape[0], pvalueGrid.shape[1], 1))

  #Make prediction
  temp1 = model2.predict(cwtmatBSTrial1Formatted)[0]
  temp2 = numpy.reshape(temp1, (cwtmatBSTrial1Norm.shape[0], cwtmatBSTrial1Norm.shape[1]))

  #Calculate test statistic FT
  Lambda_BS.append(TestS.testStatCalc2(cwtmatBSTrial1Norm, temp2, NminV, wMinGrid, wMaxGrid, pvalueGrid, massList, mergeY))

print("Toy experiments of background + signal done.")
print("")



#Generate trial background only
print("Beginnning toy experiment of background only")
#Make test statitic list
Lambda_Bonly = []

for i in range(0, nTrials2):
    
  #Generate random binned event
  if(argus.t=="Poisson"):
    eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
  if(argus.t=="RooFit"):  
    eventsTrial2 = SB.ToyDataGenerator_Bkgonly(xmin, xmax)

  #Do wavelet transform
  cwtmatBTrial2Norm, _, _ = WT.WaveletTransform(eventsTrial2, mergeX, mergeY, Cone, NconeR)
  cwtmatBTrial2NormPerAvg = cwtmatBTrial2Norm/listavNorm1

  #Format for predictions
  cwtmatBTrial2Flat      = numpy.ravel(cwtmatBTrial2NormPerAvg)
  cwtmatBTrial2Formatted = numpy.reshape(cwtmatBTrial2Flat,(1, pvalueGrid.shape[0], pvalueGrid.shape[1], 1))

  #Make prediction
  temp1 = model2.predict(cwtmatBTrial2Formatted)[0]
  temp2 = numpy.reshape(temp1, (cwtmatBTrial2Norm.shape[0], cwtmatBTrial2Norm.shape[1]))

  #Calculate test statistic FT
  Lambda_Bonly.append(TestS.testStatCalc2(cwtmatBTrial2Norm, temp2, NminV, wMinGrid, wMaxGrid, pvalueGrid, massList, mergeY))

print("Toy experiments of background only.")
print("")



#Distribution of statistical significance wavelet transform usinf two methods both Hugues/Yevgeny and Glen/Seans
print("Computing test statistics...")

significanceDistributionWTTotal = []
significanceDistribution_ExclusionWTTotal = []
significanceDistribution_DiscoveryWTTotal = []
# P_Value_List = []
# P_Value_List_TheoryLimit = []

#Hugues/Yevgeny method1

for n in range(0, NminV):

  significanceDistribution_ExclusionWT = []
  significanceDistribution_DiscoveryWT = []
  # P_Value_List_TheoryLimit = []

  #Exclusion
  for i in range(0, nTrials1):
      
    #Initialize variable
    temp = 0
      
    #Loop over background events
    for j in range(0, nTrials2):
      if statTest2[j][n] <= statTest1[i][n]:
        temp += 1/nTrials2
      
    #Append to list
    significanceDistribution_ExclusionWT.append(temp)

  #Append to list of significances
  significanceDistribution_ExclusionWTTotal.append(significanceDistributionWT)     
 

  #Discovery
      
  for i in range(0, nTrials1):
      
    #Initialize variable
    temp = 0
      
    #Loop over background events
    for j in range(0, nTrials2):
      if statTest2[j][n] >= statTest1[i][n]:
        temp += 1/nTrials2
      
    #Append to list
    significanceDistribution_DiscoveryWT.append(temp)

  #Append to list of significances
  significanceDistribution_DiscoveryWTTotal.append(significanceDistributionWT)

templist_method1_Exclusion = []
templist_method1_Discovery = []

for i in range(0, NminV):
  t1 = numpy.sort(significanceDistribution_ExclusionWTTotal[i])
  temp = t1[int(numpy.floor(0.5*len(significanceDistribution_ExclusionWTTotal[i])))]
  print(i, temp)
  templist_method1_Exclusion.append(temp)

print("Method1: Min Exclusion value is:", min(templist_Exclusion))
P_Value_List_Method1_Exclusion.append(numpy.min(templist_Exclusion))

for i in range(0, NminV):
  t1 = numpy.sort(significanceDistribution_DiscoveryWTTotal[i])
  temp = t1[int(numpy.floor(0.5*len(significanceDistribution_DiscoveryWTTotal[i])))]
  print(i, temp)
  templist_method1_Discovery.append(temp)

print("Method1: Min Discovery value is:", min(templist_method1_Discovery))
P_Value_List_Method1_Discovery.append(numpy.min(templist_method1_Discovery))




#Glen/Seans Method2

for i in range(0, NminV):

  #Sort and work out medians of both Bkg and B+S

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(statTestFT2[n])
  B_OnlyMedian = numpy.median(B_Only_SortedList[n])

  #Discovery Median
  BS_SortedList     = numpy.sort(statTestFT1[n])
  BS_Median = numpy.median(BS_SortedList[n])


  Lambda_BS_hist = numpy.histogram(statTestFT1[n], bins)
  Lambda_Bonly_hist = numpy.histogram(statTestFT2[n], bins)

  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)

  templist_method2_Exclusion = []
  templist_method2_Discovery = []

  #Try and caluclate P-value for Exclusion
  P_Value_Exclusion = (1-Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
  templist_method2_Exclusion.append(P_Value_Exclusion)


  #Try and caluclate P-value for Discovery
  P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
  templist_method2_Discovery.append(P_Value_Discovery)


print("Method2: Min Exclusion value is:", min(templist_method2_Exclusion))
P_Value_List_Method2_Exclusion.append(numpy.min(templist_method2_Exclusion))

print("Method2: Min Discovery value is:", min(templist_method2_Discovery))
P_Value_List_Method2_Discovery.append(numpy.min(templist_method2_Discovery))

print("Print statistics computations done.")
print("")

#Save Pvalues from the 2 methods, Limts from exclusion and discovery 
numpy.savetxt(Pvalue_out_Method1_Discovery, P_Value_List_Method1_Discovery, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method1_Exclusion, P_Value_List_Method1_Exclusion, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")

for i in range(0, NminV):

  #Test Statistic Plotting

  Probably_Dist, ax = plt.subplots()
  # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
  plt.hist(statTestFT2[n], bins,label="Bkg Only", alpha=0.4)
  plt.hist(statTestFT1[n], bins, label="Bkg + Signal" , alpha=0.4)
  plt.ticklabel_format(style = 'plain')
  plt.axvline(x=B_OnlyMedian)
  plt.axvline(x=BS_Median)
  plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
  # ax.set_ylabel('', ha='right', y=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/TestStatisitc_plots/TestDistribution_"+str(k)+"_"+str(M5)+".png")


