##################
#   RF trainer   #
##################

#Explanations
# - This code trains a neural network to discover anomalies.

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys, scipy.stats

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

import WT,LSign
import SigBkg_Functions as SB

#Machine learning
import keras, sklearn
import tensorflow as tf
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D
from keras.layers.merge      import add, concatenate
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras                   import backend as K

print("Files imported.")
print("")

import argparse
parser = argparse.ArgumentParser(description='Toys Type')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()

#Settings
print("Reading settings...")

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775 #2935
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="HighMass_Diphoton"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2
massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = False
NconeR = 2

#Simulation settings
nTrials            = 5000# 500
nTrainingPerPoints = 100 # 100

#Number of threads to use
NumberThreads = 8 #8

#Fraction of background
fracBackground = 0.5

#Read value for this line
# M5 = 6000
# k  = 500 

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  luminosity = 139
  year = '2015-18'
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins, eventType)

if(argus.b=="Diphoton_36_Bkg"):
  luminosity = 36.1
  year = '2015-16'
  BkgFix = SB.Bfix_Diphoton_Theory(massList, eventType)

if(argus.b=="HighMass_Diphoton"):
  luminosity = 139
  year = '2015-18'
  BkgFix = SB.Diphoton_Bkg(xmin, xmax, nBins, eventType)
  
#Lets name a folder based on the signal and bkg to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder)
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinder_SignalGeneration")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Error_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/History_Plots")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Logs_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Out_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/TestStatisitc_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/ClassiferWeights_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Error_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/History_Plots")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Out_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/TestStatisitc_plots")
else:
    print ("Successfully created the directory %s" % Output_Folder)



nameModelFile = Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Condor/RegionFinder_Weights.txt"


#Mass and scale Lists
# massList = numpy.linspace(minMass, maxMass, nBins)

#Significance grid
print("Beginning evaluation of p-value grid...")
print("")

#Grid of toy experiments

if(argus.t=="Poisson"):
  toyExpGrid1, listavNorm1 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "Poisson")
if(argus.t=="RooFit"):
  toyExpGrid1, listavNorm1 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "RooFit")

#Grid of pvalues
wMinGrid, wMaxGrid, pvalueGrid = LSign.pvalueGridMaker(toyExpGrid1)

print("Evaluation of p-value grid done.")
print("")



#Training
nTrainingPoints = 0
X_train = []
Y_train = []

print('Generating training data...')

#Generate signals
with open(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinder_SignalGeneration/RegionFinder_Signals.txt") as fin:
  for line in fin:

    #Generate fix signal
    data = line.split()
    SigFix = []
    nTrainingPoints += 1

    for i in range(2, len(data)):
      SigFix.append(float(data[i]))

    #Local maps
    cwtmatIntPNorm, _, _ = WT.WaveletTransform(SigFix, mergeX, mergeY, Cone, NconeR)
    cwtmatIntSTemp       = cwtmatIntPNorm/listavNorm1
    localMap             = cwtmatIntSTemp

    for j in range(0, nTrainingPerPoints):

      #Generate random binned events
      temp = random.random()

      if temp > fracBackground:
        aS = 1
      else:
        aS = 0

      if(argus.t=="Poisson"):
        eventsInt = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, aS)
      if(argus.t=="RooFit"):  
        eventsInt = SB.ToyDataGenerator_withSignal(k, M5, xmin, xmax, "bkgsig", aS)
      

      #Do wavelet transform
      cwtmatBSIntNorm, _, _ = WT.WaveletTransform(eventsInt, mergeX, mergeY, Cone, NconeR)
      cwtmatIntPTemp       = cwtmatIntPNorm/listavNorm1
      inputX_train          = cwtmatIntPTemp

      if aS==1:
        outputY_train = localMap.copy()
      else:
        outputY_train = numpy.full((listavNorm1.shape[0], listavNorm1.shape[1]), 0)

      #Append results
      X_train.append(inputX_train)
      Y_train.append(outputY_train)

#Convert to numpy array
X_train = numpy.array(X_train)
Y_train = numpy.array(Y_train)

#Dimension info
dimWX = (X_train.shape[1], X_train.shape[2])
dimWY = (Y_train.shape[1], Y_train.shape[2])

#Format
X_train = numpy.ravel(X_train)
Y_train = numpy.ravel(Y_train)

X_train = numpy.reshape(X_train, (nTrainingPoints*nTrainingPerPoints, dimWX[0], dimWX[1], 1))
Y_train = numpy.reshape(Y_train, (nTrainingPoints*nTrainingPerPoints, dimWY[0]*dimWY[1]))

#Inform completion
print('Events generated')



#Define model for region finder
print('Starting training')

config = tf.compat.v1.ConfigProto()
config.intra_op_parallelism_threads = NumberThreads
config.inter_op_parallelism_threads = NumberThreads
sess = tf.compat.v1.Session(config=config)

model2 = Sequential()

#add model layers
model2.add(Conv2D(32,  kernel_size=5, activation='softplus', input_shape=(X_train.shape[1], X_train.shape[2], 1)))
model2.add(MaxPooling2D(pool_size=(2, 2)))
model2.add(Conv2D(64,  kernel_size=5, activation='softplus'))
model2.add(MaxPooling2D(pool_size=(2, 2)))
model2.add(Conv2D(128,  kernel_size=5, activation='softplus'))
model2.add(Flatten())
model2.add(Dense(5000, activation='softplus'))
model2.add(Dense(Y_train.shape[1], activation='softplus'))

#Compile
model2.compile(optimizer='adam',loss='mean_squared_error')

#Checkpoint
checkpoint = keras.callbacks.ModelCheckpoint(nameModelFile, verbose=1, monitor='val_loss', save_best_only=True, mode='auto') 

#Train model default epoch 100
train_history = model2.fit(X_train, Y_train, batch_size=1000, epochs=200, validation_split=0.2, callbacks=[checkpoint])

#Save best weights
model2.save(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Condor/model_RegionFinder.h5")
print('Training done')




