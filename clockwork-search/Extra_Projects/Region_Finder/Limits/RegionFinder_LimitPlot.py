#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)
#Import custom
# sys.path.insert(0, '/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Classifier/Custom_Modules')
# from Custom_Modules import WT
# import SigBkg_Classifier as SB

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()

#import ATLAS MPL Style
# import mplhep as hep
# plt.style.use(hep.style.ROOT)
# import atlas_mpl_style

S = str(argus.s)
B = str(argus.b)

K_List = numpy.linspace(200,5000,25)
M5_List = numpy.linspace(1000, 20000, 25)

N, M = len(K_List), len(M5_List)
Z = numpy.zeros((N, M))
K_List_2D, M5_List_2D = numpy.meshgrid(M5_List, K_List)
for i, (x,y) in enumerate(product(K_List,M5_List)):
    Z[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/RegionFinder_CondorOutputs/Outputs_"+B+"_"+S+"/Pvalues_Condor/Pvalue_Method2_Exclusion"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")

#Make Contour plots of Pvalues
fig_pvalue_contour,ax=plt.subplots(1,1)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z)
c_alpha = ax.contourf(K_List_2D, M5_List_2D, Z, levels = (0.047,0.053))
ax.set_xlabel('M5 [GeV]', ha='right', x=1.0)
ax.set_ylabel('k [GeV]', ha='right', y=1.0)
ax.semilogy()
plt.xlim((1000,12000))
fig_pvalue_contour.colorbar(c_alpha) # Add a colorbar to a plot
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap.png')

