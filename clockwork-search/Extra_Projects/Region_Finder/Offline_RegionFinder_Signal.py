#########################
#   Signal generators   #
#########################

#Explanations
# - Generate a series of signals

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, random , os, sys

#Import custom
import SigBkg_RegionFinder as SB

print("Files imported.")
print("")

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()


#Settings
print("Reading settings...")

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2


#Number of signals to create
nTrials = 10

#Range to scan
M5min = 1000
M5max = 50000
kmin  = 130
kmax  = 6000

print("Setting read")
print("")

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/"

#Lets name a folder based on the signal and bkg to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder)
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Condor")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/Error_Condor")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/History_Plots")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/Logs_Condor")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/Out_Condor")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir("RegionFinder_Offline_Outputs/"+Output_Folder+"/TestStatisitc_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/ClassiferWeights_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Error_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/History_Plots")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Out_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/TestStatisitc_plots")
else:
    print ("Successfully created the directory %s" % Output_Folder)

#Create file to write
nameOutput = Directory+"RegionFinder_Offline_Outputs/"+Output_Folder+"/RegionFinder_SignalGeneration/RegionFinder_Signals.txt"
ftemp      = open(nameOutput,"+w")
ftemp.close()



#Start loop
# massList = numpy.linspace(minMass, maxMass, nBins)

print("Starting generation")

for i in range(0, nTrials):

  #Read value for this line
  M5  = int(random.uniform(M5min, M5max))
  lnk = random.uniform(math.log(kmin), math.log(kmax))
  k   = int(math.exp(lnk))

  #Generate fix signal
  if(argus.s=="Tail_Damped_Sig"):
    SigFix = SB.SigFixed_withCB(k, M5, xmin, xmax)
  if(argus.s=="Theory_Diphoton_36_Sig"):
    SigFix, _ = SB.Sfix_DefaultTheory_Diphoton(k, M5, xmin, xmax, nBins)
  if(argus.s=="Theory_Dielectron_Sig"):
    SigFix, _ = SB.Sfix_Theory_Dielectron(k, M5, xmin, xmax, nBins)

  #Saving results
  Awrite = [M5] + [k] + SigFix
  fout = open(nameOutput, "a")
  fout.write(" ".join(map(str, Awrite)) + "\n")
  fout.close()

  #Make report
  if i%10 == 0:
    print(i)




