# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, scipy, random, os, sys
from itertools import product


import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/"


S = str(argus.s)
B = str(argus.b)
# try:
#   Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder)
#   os.mkdir(Directory+"RegionFinder_CondorOutputsrOutputs/"+Output_Folder+"/RegionFinderWeights_Condor")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Error_Condor")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/History_Plots")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Logs_Condor")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Out_Condor")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Pvalues_Condor")
#   os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/TestStatisitc_plots")
# except OSError:
#     print ("Creation of the directory %s failed" % Output_Folder)
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder)
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Condor")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Error_Condor")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/History_Plots")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Logs_Condor")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Out_Condor")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/Pvalues_Condor")
#     # os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/TestStatisitc_plots")
# else:
#     print ("Successfully created the directory %s" % Output_Folder)


# Open file and write common part
cfile = open('condor_testing.sub','w')
common_command = \
'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = RegionFinder_Testing_GenerateSubmissionPreamble.sh \n\
initialdir      = /scratch2/slawlor/Clockwork_Project/clockwork-search/ \n\
Should_transfer_files = YES \n\
+JobFlavour = "testmatch" \n\
'
cfile.write(common_command)
 
# Loop over various values of an argument and create different output file for each
# Then put it in the queue
K_List = numpy.linspace(200,5000,25)
M5_List = numpy.linspace(1000, 20000, 25)
for i, (k,m) in enumerate(product(K_List,M5_List)):
    run_command =  \
'arguments  = Poisson %d %d %s %s \n\
error                 = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Error_Condor/RegionFinder_testing%d_%d_Clockwork$(Process).err \n\
log                   = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Logs_Condor/RegionFinder_testing%d_%d_Clockwork$(Process).log \n\
transfer_input_files  = Inputs, Region_Finder/Condor_RegionFinder_Testing.py, Region_Finder/Condor_Submission_Files/RegionFinder_Testing_GenerateSubmissionPreamble.sh , Region_Finder/Custom_Modules, Region_Finder/SigBkg_RegionFinder.py, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/RegionFinderWeights_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Error_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Logs_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Pvalues_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Out_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Error_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/TestStatisitc_plots, Region_Finder/Graviton_Properties \n\
output     = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Out_Condor/RegionFinderTesting_out.%d_%d.txt\n\
queue 1\n\n' %(k,m,S,B,B,S,k,m,B,S,k,m,B,S,B,S,B,S,B,S,B,S,B,S,B,S,B,S,k,m)
    cfile.write(run_command)