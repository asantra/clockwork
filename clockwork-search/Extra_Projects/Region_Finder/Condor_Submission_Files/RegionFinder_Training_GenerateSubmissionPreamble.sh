#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "lcgenv -p LCG_98python3 x86_64-centos7-gcc8-opt ROOT"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scipy"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scikitlearn"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt numpy"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt pywt"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt tensorflow"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt keras"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt wrapt"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt pip"
pip3 install --user parton --upgrade
python3 Condor_RegionFinder_Trainer.py -t $1 -s $2 -b $3