# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, scipy, random, os, sys
from itertools import product

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
argus = parser.parse_args()

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Region_Finder/"

S = str(argus.s)
B = str(argus.b)
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+""
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder)
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinderWeights_Condor")
  os.mkdir("RegionFinder_CondorOutputs/"+Output_Folder+"/RegionFinder_SignalGeneration")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Error_Condor")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/History_Plots")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Logs_Condor")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Out_Condor")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/Pvalues_Condor")
  os.mkdir(Directory+"RegionFinder_CondorOutputs/"+Output_Folder+"/TestStatisitc_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder)
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/ClassiferWeights_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Error_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/History_Plots")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Out_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor")
    # os.mkdir("Classifier_Condor_Outputs/"+Output_Folder+"/TestStatisitc_plots")
else:
    print ("Successfully created the directory %s" % Output_Folder)

# Open file and write common part
cfile = open('condor_signal.sub','w')
common_command = \
'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = RegionFinder_SignalGeneration_SubmissionPreamble.sh \n\
arguments  = %s %s \n\
initialdir      = /scratch2/slawlor/Clockwork_Project/clockwork-search/ \n\
error                 = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Error_Condor/RegionFinder_SignalGen_Clockwork$(Process).err \n\
log                   = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Logs_Condor/RegionFinder_SignalGen_Clockwork$(Process).log \n\
output     = Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Out_Condor/RegionFinderTraining_out.txt\n\
Should_transfer_files = YES \n\
transfer_input_files  = Inputs,Region_Finder/Condor_RegionFinder_Signal.py, Region_Finder/Condor_Submission_Files/RegionFinder_SignalGeneration_SubmissionPreamble.sh , Region_Finder/Custom_Modules, Region_Finder/SigBkg_RegionFinder.py,Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Error_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Logs_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/Out_Condor, Region_Finder/RegionFinder_CondorOutputs/Outputs_%s_%s/RegionFinder_SignalGeneration, Region_Finder/Graviton_Properties  \n\
request_CPUs = 8 \n\
request_memory = 4GB \n\
request_disk = 4GB \n\
+JobFlavour = "nextweek" \n\
queue 1\n\n' %(S,B,B,S,B,S,B,S,B,S,B,S,B,S,B,S)

cfile.write(common_command)

