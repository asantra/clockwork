from __future__ import division
import sys,copy
sys.path = ["source"] + sys.path
# sys.path = ["../source"] + sys.path
# from setup import *
import numpy as np 
import setup
import ROOT


########################################
# simple histograms class
########################################

def interpolate(x1=None,x2=None,
                y1=None,y2=None,
                x=None):
    assert x2!=x1
    m=(y2-y1)/(x2-x1)
    y = y1+m*(x-x1)
    return y

class hist:
    def __init__(self,th1):
        # self.th1 = th1
        self.name = th1.GetName()
        self.nBins = th1.GetNbinsX()
        self.lo = th1.GetBinLowEdge(1)
        self.hi = th1.GetBinLowEdge(self.nBins+1)
        self.width = th1.GetBinLowEdge(2)-th1.GetBinLowEdge(1)
        self.y = np.array([th1.GetBinContent(i) for i in range(1,self.nBins+1)])
        self.x = np.array([th1.GetBinCenter(i) for i in range(1,self.nBins+1)])

        self.y = self.y[self.x>=130]
        self.x = self.x[self.x>=130]

        self.y = self.y[self.x<6e3]
        self.x = self.x[self.x<6e3]

    def th1(self):
        """ Return TH1 output """
        h = ROOT.TH1F(self.name,self.name,self.nBins,self.lo,self.hi)
        for i,x in enumerate(self.x):
            h.Fill(x,self.y[i])
        return h

    def zero(self):
        """ Zero y axis """
        self.y = np.zeros(len(self.y))

    def smooth(self):
        """ Smooth histogram by averaging with neighbors """
        # return
        newY = np.zeros(len(self.x))
        size = 10 # 2x how many bins to average over
        size = 10000
        # 10 this would be 2 GeV
        for i,x in enumerate(self.x):
            dn = max(0,i-size)
            up = min(len(self.x),i+size)
            newY[i] = np.mean(self.y[dn:up])
        self.y = newY

    def adjustBinning(self,target):
        # smooths to target's binning
        # target's binning should be finer
        # print yellow("Scaling by factor",len(self.x)/len(target.x))
        self.y*=len(self.x)/len(target.x)
        iOld=1 # index for lower bin in interp
        newY = np.zeros(len(target.x))
        for iNew,x in enumerate(target.x):
            # interpolate between nearest 
            while iOld<len(self.x)-1 and x>self.x[iOld]: iOld+=1
            loBin = self.x[iOld-1]
            hiBin = self.x[iOld]
            loVal = self.y[iOld-1]
            hiVal = self.y[iOld]
            # interpolate y for (x,y)
            y = interpolate(x1=loBin,y1=loVal,x2=hiBin,y2=hiVal,x=x)
            newY[iNew] = y
        self.y = newY
        self.x = np.array(target.x)
        self.lo = np.array(target.lo)
        self.hi = np.array(target.hi)
        self.width = np.array(target.width)

    def __str__(self):
        return "Hist {0} nBins={1} [{2},{3}]".format(self.name,self.nBins,self.lo,self.hi)

