from __future__ import division

from matplotlib.ticker import FuncFormatter
import sys
import numpy as np
import pystrap as ps
import roowrap as rw
import numpy.ma as ma
import matplotlib

from modules import *
from math import *
import matplotlib.pyplot as plt

try:
    from matplotlib.ticker import ScalarFormatter
    import matplotlib.ticker as ticker
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from matplotlib import rc
    from  matplotlib import cm as cm
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    # mpl.use("pgf")
    if mpl.__version__.split(".")[0]!="0":
        pgf_with_rc_fonts = {
            "font.family": "sans-serif",
            "font.size" : "15",
            "font.sans-serif": ["Helvetica"],
            "pgf.texsystem":"pdflatex",
            "pgf.preamble":[
                            r"\usepackage{amsmath}",\
                            r"\usepackage[english]{babel}",\
                            # r"\usepackage{arev}",\
                           ]
        }
        mpl.rcParams.update(pgf_with_rc_fonts)
        mpl.rcParams['text.usetex'] = True
        mpl.rcParams['text.latex.preamble'] = [r'\usepackage[LGRgreek]{mathastext}',r'\usepackage{amsmath}',r'\usepackage{upgreek}']
        # mpl.rcParams['font'] = {'family':'serif','serif':['Palatino']}
        # plt.rcParams["patch.force_edgecolor"] = True
        # plt.clf(); plt.cla()
        # fig = plt.figure(1)
        # rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
except: pass


colors = {"sig":"#c92702",
          "bkg":"#0232cf",
          "mod":"#cc8502",
          "dat":"#222222",
         }

plotOn=True

####################################################################################################
# This holds the plotter "rooPlotter" class for making matplotlib plots with pystrap objects
# it is designed to be used with rooWrap, a wrapper for root objects
# Contents:
#  * Style functions
#  * Plotting functions
####################################################################################################

####################################################################################################
# Style functions
####################################################################################################


def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Internal",size=12,lumi=None,subnote=""):
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    if lumi==None:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV }$",
                ]
    else:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV, }"+str(lumi)+r"\text{ fb}^{\text{-1}}$",
                 # r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
                ]
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{\text{s}}$ = 13 TeV, "+str(lumi)+r" fb$^{\text{-1}}$",
                 # r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
                ]
    if subnote: lines.append(subnote)
    labelString = "\n".join(lines)
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif",size=size)
    ticksInside()


def backgroundFitPlot(extrap,title="",path="plots/output.png"):
    """ Plot background fit """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    x = extrap.get("x")
    bkg = rw.rooWrap(extrap.get("model"),x)
    rdh = rw.rooWrap(extrap.get("template"),x)
    bkg.normalizeTo(rdh)
    plt.plot(bkg.x[1:-1],bkg.y[1:-1],"b",linewidth=6,alpha=0.5,label="Background Model")
    plt.plot(rdh.x[1:-1],rdh.y[1:-1],"r",linewidth=2,label="Template")
    plt.yscale("log")
    plt.title(title)
    plt.legend()

    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def sbFitPlot(extrap,title="",path="plots/output.png"):
    """ Plot S+B fit """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    signalNorm = extrap.get("signalNorm").getVal()
    backgroundNorm = extrap.get("backgroundNorm").getVal()

    print (yellow("signalNorm",signalNorm))
    print (yellow("backgroundNorm",backgroundNorm))

    # print yellow("backgroundInt",extrap.get("backgroundModel").createIntegral(RooArgSet(extrap.get("x"))).getVal())
    # print yellow("signalInt",extrap.get("signalModel").createIntegral(RooArgSet(extrap.get("x"))).getVal())
    # print yellow("modelInt",extrap.get("model").createIntegral(RooArgSet(extrap.get("x"))).getVal())

    print ("Making roowraps")
    x = extrap.get("x")
    print ("Loading mod")
    mod = rw.rooWrap(extrap.get("model"),x)
    print (yellow("mod"), mod.y)
    print ("Loading bkg")
    bkg = rw.rooWrap(extrap.get("backgroundModel"),x)
    print (yellow("bkg"), bkg.y)
    print ("Loading sig")
    sig = rw.rooWrap(extrap.get("signalModel"),x)
    print (yellow("sig"), sig.y)
    print ("Loading sbRdh")
    sbRdh = rw.rooWrap(extrap.get("template"),x)
    print (yellow("sbRdh"), sbRdh.y)
    print ("Loading bRdh")
    bRdh = rw.rooWrap(extrap.get("backgroundTemplate"),x)
    print (yellow("bRdh"), bRdh.y)


    # n = 100
    # mod.rebin(n)
    # bkg.rebin(n)
    # sig.rebin(n)
    # sbRdh.rebin(n)
    # bRdh.rebin(n)


    print ("Plotting")
    # mod.normalize(signalNorm+backgroundNorm,minRange=fitMin,maxRange=fitMax)
    mod.normalizeTo(sbRdh,minRange=fitMin,maxRange=fitMax)
    # sig.normalize(signalNorm,minRange=fitMin,maxRange=fitMax)
    # sig.scale(signalNorm)
    bkg.normalize(backgroundNorm,minRange=fitMin,maxRange=fitMax)

    plt.plot(sbRdh.x[1:-1],sbRdh.y[1:-1],"ko",linewidth=0,label="Template")
    plt.plot(bRdh.x[1:-1], bRdh.y[1:-1], "c", linewidth=2,label="Background Template")
    plt.plot(mod.x[1:-1],  mod.y[1:-1],  "y", linewidth=8,alpha=0.8,label="Model")
    # plt.plot(sig.x[1:-1],  sig.y[1:-1],  "r", linewidth=2,alpha=1,label="Signal Model")
    plt.plot(bkg.x[1:-1],  bkg.y[1:-1],  "b", linewidth=2,alpha=1,label="Background Model")
    plt.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="FitMax")

    plt.yscale("log")
    plt.xscale("log")
    plt.title(title)
    plt.legend()

    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def pdfPlot(pdf,x,title="",path="plots/output.png"):
    """ Plot background fit """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    bkg = rw.rooWrap(pdf,x)
    plt.plot(bkg.x,bkg.y,"b",linewidth=6,alpha=0.5,label="PDF")
    plt.title(title)
    plt.legend()
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def plotTwo(pdf,rdh,x,title="",path="plots/output.png"):
    """ Plot background fit """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    rdh = rw.rooWrap(rdh,x)
    bkg = rw.rooWrap(pdf,x)
    bkg.normalizeTo(rdh)
    plt.plot(bkg.x,bkg.y,"b",linewidth=3,alpha=0.5,label="PDF")
    plt.plot(rdh.x,rdh.y,"ko",linewidth=0,alpha=1,label="RDH")
    plt.title(title)
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def roofitPlot(pdf,rdh,x,path="plots/output.png",errorsOn=True):
    print (green("Making plot"))
    frame=x.frame(RooFit.Title("Example"))
    rdh.plotOn(frame,RooFit.LineColor(kBlack),DataError(2))
    pdf.plotOn(frame,RooFit.LineColor(kRed))
    c = TCanvas("x","x",800,300)
    frame.Draw()
    c.SetLogy()
    c.Draw()
    img = TImage.Create()
    img.FromPad(c)
    img.WriteImage(path)


def plot_rw(extrap,useSbModel,channel="mm",lumi=139,path="plots/output_rw.png"):
    print (green("Plotting",path))
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    grid = gridspec.GridSpec(2, 1, height_ratios=[3,1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])


    x = extrap.get("x")
    mod = extrap.get("model")
    bkg = extrap.get("backgroundModel")
    rdh = extrap.get("template")
    bkgRdh = extrap.get("backgroundTemplate")
    bkgNorm= extrap.get("backgroundNorm")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    # import roowrap as rw
    # Make plot using rooWrap
    rdh = rw.rooWrap(rdh,x)
    bkgRdh = rw.rooWrap(bkgRdh,x)
    mod = rw.rooWrap(mod,x)
    bkg = rw.rooWrap(bkg,x)

    mod.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    bkg.normalize(bkgNorm.getVal(),minRange=fitMin,maxRange=fitMax)
    # ratio
    pull=False
    ratioBins = rdh.x
    ratioVals = rdh.y/mod.y
    ratioBins = ratioBins[ratioVals>0]
    ratioVals = ratioVals[ratioVals>0]

    # pull
    pull=True
    ratioBins = rdh.x
    ratioVals = (rdh.y-mod.y)/np.sqrt(mod.y)

    # bkgRdh.normalizeTo(rdh)
    ax.plot(rdh.x,rdh.y,"ko",label="Template")
    if useSbModel:
        ax.plot(mod.x,mod.y,"C3",label="S+B Model")
        ax.plot(bkgRdh.x,bkgRdh.y,"o",color="C1",label="Background Template")
    ax.plot(bkg.x,bkg.y,"C0",linewidth=4,label="B Model")
    ax.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="Fit Limits")
    ax.plot([fitMin]*2,plt.ylim(),"c",linewidth=10,alpha=0.5)

    # legend setup
    plt.legend(fontsize=12,loc=0, frameon=False)
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ Selection".format(channel.replace("mm",r"\mu\mu")))
    plt.xlim(left=min(rdh.x[rdh.y>0]))
    xlim = list(plt.gca().get_xlim())
    ylim = list(plt.gca().get_ylim())
    plt.xlim(xlim)
    plt.ylim(top=ylim[1]*5000)
    plt.ylim(bottom=min(mod.y[mod.y>0]))
    plt.yscale("log")
    plt.xscale("log")
    plt.ylabel("Events/Bin ({0:.1f} GeV)".format(rdh.x[1]-rdh.x[0]))

    ######################
    # bottom
    ######################
    ax = plt.subplot(grid[1])
    if pull:
        top=5
        bottom = -5
        mid = 0
    else:
        top=2
        bottom = 0
        mid=1
    plt.ylim((bottom,top))
    plt.xlim(xlim)
    arrowsUp = ratioBins[ratioVals>top]
    arrowsDn = ratioBins[ratioVals<bottom]
    ax.plot(xlim,[mid]*2,"r",alpha=0.25)
    ax.plot(ratioBins,ratioVals,"ko",markersize=1,label="Ratio")
    ax.plot(arrowsUp,[top-0.1]*len(arrowsUp),"r^")
    ax.plot(arrowsDn,[bottom+0.1]*len(arrowsDn),"bv")
    ax.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="Fit Limits")
    ax.plot([fitMin]*2,plt.ylim(),"c",linewidth=10,alpha=0.5)
    plt.xscale("log")
    ticksInside()

    if pull: plt.ylabel("Template-Model Pull",fontsize=10)
    else: plt.ylabel("Template/Model")
    plt.xlabel("$M_{{{0}}}$ [GeV]".format(channel.replace("mm",r"\mu\mu")))
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def uncertHisto(dist,nominalMcSs,nominalDataSs,used,showNominal=False,xlabel="",path="plots/demo.png"):
    """ Make histo
        Distribution, nominal fit result, and value used
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    dist=np.array(dist)
    nBins = int(len(dist)/10)
    nBins = max(nBins,10)
    nBins = min(nBins,50)

    plt.hist(dist,nBins,label=r"Variation $\mu={0:.2f}$, $\sigma={1:.2f}$".format(dist.mean(),dist.std()))
    height = plt.ylim()[1]
    plt.plot([nominalMcSs]*2,[0,height],"c",linewidth=10,alpha=0.5,label="MC Fit Value ${0:.2f}$".format(nominalMcSs))
    if showNominal:
        plt.plot([nominalDataSs]*2,[0,height],"r",linewidth=10,alpha=0.5,label="Data Fit Value ${0:.2f}$".format(nominalDataSs))
    plt.plot([used]*2,[0,height],"g",linewidth=10,alpha=0.5,label="Uncertainty ${0:.2f}$".format(used))

    plt.ylim(0,plt.ylim()[1]*2)
    plt.xlabel(xlabel)
    atlasInternal()
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")


def backgroundEstimateCompare(b_pdfSs=None,b_fitSs=None,b_nBkg=None,sb_pdfSs=None,sb_fitSs=None,sb_nBkg=None,
                              path="plots/demo.png",
                              label=""
                             ):
    """ Make plot comparing B-only and SB background yields
    """
    print (green("Background plotting for",path,label))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    #b-only
    bTotErr = sqrt(b_pdfSs**2+b_fitSs**2)
    plt.errorbar([-0.2],b_nBkg,yerr=bTotErr,marker="o",color="k",markersize=8,linewidth=2)
    plt.errorbar([ 0.0],b_nBkg,yerr=b_pdfSs,marker="o",color="C1",markersize=4,linewidth=2)
    plt.errorbar([ 0.2],b_nBkg,yerr=b_fitSs,marker="o",color="C3",markersize=4,linewidth=2)

    ##sb
    plt.plot([1],sb_nBkg,marker="o",color="k",markersize=8,linewidth=2)
    #sbTotErr = sqrt(sb_pdfSs**2+sb_fitSs**2)
    #plt.errorbar([ 0.8],sb_nBkg,yerr=sbTotErr,marker="o",color="k",markersize=8,linewidth=2)
    #plt.errorbar([ 1.0],sb_nBkg,yerr=sb_pdfSs,marker="o",color="C1",markersize=4,linewidth=2)
    #plt.errorbar([ 1.2],sb_nBkg,yerr=sb_fitSs,marker="o",color="C3",markersize=4,linewidth=2)

    # legend
    plt.plot([],[],marker="o",color="k",markersize=8,linewidth=2,label="Total uncertainty")
    plt.plot([],[],marker="o",color="C1",markersize=4,linewidth=2,label="Spurious Signal (PDF)")
    plt.plot([],[],marker="o",color="C3",markersize=4,linewidth=2,label="Fit Uncertainty")

    plt.xticks([-1,0,1,2],["","B-only fit\n{0:.2f}".format(b_nBkg),"S+B fit\n{0:.2f}".format(sb_nBkg),""])
    plt.ylabel("Background Estimate")

    # top = plt.ylim()[1]
    # bot = plt.ylim()[0]
    # mid = (top+bot)/2
    # dif = top-bot
    # plt.ylim(mid-dif,mid+dif)

    plt.ylim(0,plt.ylim()[1]*2)

    atlasInternal(subnote=label)
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def limitPlot_nSig(limitDicts,nsig=True,path="plots/limits.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    import lambdaConversion as lc
    plt.clf(); plt.cla()
    plt.figure(figsize=(8,6))
    if not nsig: lambdaConvert = lc.lambdaLimit() # class to calculate limits
    # loop over limits
    xlabels = []
    names = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))

    prevLimits  = [ \
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2},
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0},
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8},
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7},
                    {"channel":"mm","chirality":"LL","interference":"const","prevLimExp":23.8},
                    {"channel":"mm","chirality":"LR","interference":"const","prevLimExp":22.5},
                    {"channel":"mm","chirality":"RL","interference":"const","prevLimExp":22.4},
                    {"channel":"mm","chirality":"RR","interference":"const","prevLimExp":22.2},
                    {"channel":"mm","chirality":"LL","interference":"dest", "prevLimExp":18.4},
                    {"channel":"mm","chirality":"LR","interference":"dest", "prevLimExp":19.7},
                    {"channel":"mm","chirality":"RL","interference":"dest", "prevLimExp":19.8},
                    {"channel":"mm","chirality":"RR","interference":"dest", "prevLimExp":18.4},
               ]

    for iName,name in enumerate(names):
        # extract limits (number of signal events)
        l = limitDicts[name]
        print (yellow("Plotting",name))
        observed   = l["upperLimit"]
        expected   = l["expLimit"]
        uOneSig    = l["uOneSig"]
        uTwoSig    = l["uTwoSig"]
        lOneSig    = l["lOneSig"]
        lTwoSig    = l["lTwoSig"]
        channel    = l["channel"]
        extrapMin  = l["extrapMin"]
        extrapMax  = l["extrapMax"]
        lumi       = l["lumi"]
        chirality  = l["chirality"]
        interference  = l["interference"]
        observed_data   = l["upperLimit-data"]
        expected_data   = l["expLimit-data"]
        xlabels.append(chirality+" "+interference)
        # prevLimit = [p for p in prevLimits if p["channel"]==channel and p["chirality"]==chirality and p["interference"]==interference][0]["prevLimExp"]

        # if not nsig limits, convert to Lambda limits
        if not nsig:
            observed_data = lambdaConvert.getLambdaLimit(observed_data,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            expected_data = lambdaConvert.getLambdaLimit(expected_data,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            observed = lambdaConvert.getLambdaLimit(observed,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            expected = lambdaConvert.getLambdaLimit(expected,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            uOneSig  = lambdaConvert.getLambdaLimit(uOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            uTwoSig  = lambdaConvert.getLambdaLimit(uTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            lOneSig  = lambdaConvert.getLambdaLimit(lOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
            lTwoSig  = lambdaConvert.getLambdaLimit(lTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)


        width=1
        plt.bar(width*iName,uTwoSig-lTwoSig,edgecolor="none",width=width*0.9,bottom=lTwoSig,color="#D6D600",label="$2\\sigma$ Expected")
        plt.bar(width*iName,uOneSig-lOneSig,edgecolor="none",width=width*0.9,bottom=lOneSig,color="#18B100",label="$1\\sigma$ Expected")
        # obs=plt.errorbar(width*iName,observed,fmt="ko",xerr=width/2*0.9,label="Observid Limit (MC)")
        obs=plt.errorbar(width*iName,observed_data,fmt="ko",xerr=width/2*0.9,label="Observed Limit (Data)")
        obs=plt.errorbar(width*iName,expected_data,fmt="ro",xerr=width/2*0.9,label="Expected Limit (Data)")
        exp=plt.errorbar(width*iName,expected,fmt="k",xerr=width/2 *0.9,label="Expected Limit (${0}fb^{{-1}}$)".format(lumi))
        exp[-1][0].set_linestyle('--')
        if not nsig:
            prev=plt.plot([iName-0.43,iName+0.43],[prevLimit]*2,"r",label="Previous Limit (${0}fb^{{-1}}$)".format(36.1))
        if iName==0: plt.legend(fontsize=12,loc=0, frameon=False)

    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel))
    if nsig:
        plt.ylabel("95\% CL on Signal Events")
    else:
        plt.ylabel("95\% CL on $\Lambda$ [TeV]")
        path = path.replace(".png","Lambda.png")
    # plt.title(title)
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
    plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
    # plt.yscale("log")
    # plt.ylim(0.10,1600)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png", ".pdf"), bbox_inches="tight")
    
def limitPlot_comb(limitDicts,path="plots/limits.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(8,6))
    # loop over limits
    xlabels = []
    names = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))

    for iName,name in enumerate(names):
        # extract limits (number of signal events)
        l = limitDicts[name]
        print (yellow("Plotting", name))
        chirality = l["chirality"]
        observed   = l["lambda_upperLimit"]
        expected   = l["lambda_expLimit"]
        uOneSig    = l["lambda_uOneSig"]
        uTwoSig    = l["lambda_uTwoSig"]
        lOneSig    = l["lambda_lOneSig"]
        lTwoSig    = l["lambda_lTwoSig"]
        channel    = l["channel"]
        lumi       = l["lumi"]
        
        interference  = l["interference"]
        xlabels.append(chirality+" "+interference)

        width=1
        plt.bar(width*iName,uTwoSig-lTwoSig,edgecolor="none",width=width*0.9,bottom=lTwoSig,color="#D6D600",label="$2\\sigma$ Expected")
        plt.bar(width*iName,uOneSig-lOneSig,edgecolor="none",width=width*0.9,bottom=lOneSig,color="#18B100",label="$1\\sigma$ Expected")
        # obs=plt.errorbar(width*iName,observed,fmt="ko",xerr=width/2*0.9,label="Observid Limit (MC)")
        obs=plt.errorbar(width*iName,observed,fmt="ko",xerr=width/2*0.9,label="Observed Limit")
        exp=plt.errorbar(width*iName,expected,fmt="k",xerr=width/2 *0.9,label="Expected Limit")
        exp[-1][0].set_linestyle('--')
        if iName==0: plt.legend(fontsize=12,loc=0, frameon=False)

    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel))
    plt.ylabel("95\% CL on $\Lambda$ [TeV]")
    path = path.replace(".png","Lambda.png")
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
    plt.ylim(0,plt.gca().get_ylim()[1]*1.5)

    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png", ".pdf"), bbox_inches="tight")
    

def poissonPlot(nBkgs,lims,path="plots/unittest-poisson.png"):
    """ Make plot of limit vs nBkg
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    for npWidth in nBkgs.keys():
        print (yellow(nBkgs[npWidth]))
        print (yellow(lims[npWidth]))

        plt.plot(nBkgs[npWidth],lims[npWidth],label="NP={0}".format(npWidth))

    plt.xlabel("N Background")
    plt.ylabel("Limit (N Signal)")
    atlasInternal()
    top = plt.ylim()[1]
    plt.ylim(top=top*1.5)
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def scanLimitPlot(data,xLabel,yLabel,variable="x",vmax=None,setup=None,vmin=None,channel="ee",lumi=139,path="plots/output.png"):
    """ Make 2d scan plot 
        Data is dict[y][x]
    """
    print (green("Plotting",variable))
    plt.clf(); plt.cla()
    large=len(data.keys())>50
    size=[6,14][large]
    plt.figure(figsize=(size,size))
    import random
    # get x/y vals
    yVals = sorted(data.keys())
    xVals = sorted(list(set([x for y in yVals for x in data[y].keys()])))
    # make and fill square array
    squareData = [[np.nan]*len(xVals) for i in range(len(yVals))]
    squareDataHiLo = [[np.nan]*len(xVals) for i in range(len(yVals))]
    for yi,y in enumerate(yVals):
        for xi,x in enumerate(xVals):
            if x not in data[y].keys(): continue
            if data[y][x] == "low":
                squareDataHiLo[yi][xi] = 1
            elif data[y][x] == "high":
                squareDataHiLo[yi][xi] = 1
            else:
                squareData[yi][xi] = data[y][x]
    squareData = np.flip(np.array(squareData),0)
    squareDataHiLo = np.flip(np.array(squareDataHiLo),0)

    heatmap = plt.imshow(squareDataHiLo, cmap=cm.Blues,vmin=-1,vmax=1,interpolation='nearest',extent=[0,len(xVals),0,len(yVals)])
    color = cm.autumn
    if "color" in setup.keys(): color=setup["color"]
    if vmax!=None:
        if vmin!=None:
            heatmap = plt.imshow(squareData, cmap=color,vmin=vmin,vmax=vmax,extent=[0,len(xVals),0,len(yVals)])
        else:
            heatmap = plt.imshow(squareData, cmap=color,vmax=vmax,extent=[0,len(xVals),0,len(yVals)])
    else:
        heatmap = plt.imshow(squareData, cmap=color,extent=[0,len(xVals),0,len(yVals)])

    # ticks
    nMarginTop=[12,18][large]
    xlabels=[int(i) for i in xVals]
    ylabels=[int(i) for i in yVals]+["x"]*nMarginTop
    plt.xticks(np.arange(0.5,len(xlabels)+0.5,1),xlabels,rotation='vertical',fontsize=8)
    plt.yticks(np.arange(0.5,len(ylabels)+0.5,1),ylabels,fontsize=8)
    plt.ylabel(yLabel)
    plt.xlabel(xLabel)
    plt.gca().set_yticks(plt.gca().get_yticks()[:-nMarginTop])
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel.replace("mm",r"\mu\mu")))

    # colorbar
    dataMin=np.nanmin(squareData)
    dataMax=np.nanmax(squareData)
    if vmax!=None: dataMax=min(vmax,dataMax)
    if vmin!=None: dataMin=max(vmin,dataMin)
    divider = make_axes_locatable(plt.gca())
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(heatmap,ticks=[dataMin,dataMax],cax=cax)
    cbar.ax.set_yticks([dataMin,dataMax])
    cbar.ax.set_yticklabels([round(dataMin,1),round(dataMax,1)])
    # cbar.ax.set_ylabel(variable,rotation=270,labelpad=-30)
    cbar.ax.set_ylabel(variable,rotation=270,labelpad=20)

    plt.savefig(path)
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def signalComparisonPlot(signals,hists,label="",path="plots/output.png"):
    """ Make plot for signal comparison unit test """
    print (green("Plotting",path))
    plt.clf(); plt.cla()

    # Histogram
    for iLambd,lambd in enumerate(sorted(hists.keys())):
        h = hists[lambd]
        nBins = h.GetNbinsX()
        bins = [h.GetBinCenter(i)  for i in range(1,nBins+1)]
        vals = [h.GetBinContent(i) for i in range(1,nBins+1)]
        plt.plot(bins,np.abs(vals),marker="o",color="k",linewidth=0,alpha=0.25)
        print (yellow("Histo:",len(bins)))
    plt.plot([],[],marker="o",color="k",linewidth=0,label="TH1 Templates")

    # morphed PDF
    for iLambd,lambd in enumerate(sorted(signals.keys())):
        sig = signals[lambd]
        siglabel = r"$\Lambda={0}$ TeV".format(lambd)
        plt.plot(sig.x,np.abs(sig.y),label=siglabel)
        # plt.plot(sig.x,sig.y,label=siglabel)
        print (yellow("PDF:",len(sig.x)))

    plt.yscale("log")

    top = plt.ylim()[1]
    plt.ylim(top=top*100)

    # plt.ylim(top=4)
    # plt.ylim(bottom=-8)
    # plt.ylim(bottom=1e-20)
    plt.xlim(left=100)
    plt.xscale("log")
    atlasInternal(position="nw",subnote=label)
    plt.xlabel(r"$m_{ll}$ GeV",fontsize=12)
    plt.ylabel("Absolute value of events",fontsize=12)
    plt.legend(loc=1,frameon=0,fontsize=10)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")


def plot_details(extrap,path="plots/output_rw.png"):
    """ Make detailed plot with ratio, showing error bands """
    print (green("Plotting detailed plot"))
    x = extrap.get("x")
    mod = extrap.get("model")
    bkg = extrap.get("backgroundModel")
    rdh = extrap.get("template")
    bkgRdh = extrap.get("backgroundTemplate")
    bkgNorm= extrap.get("backgroundNorm")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    import matplotlib.pyplot as plt
    import roowrap as rw
    # Make plot using rooWrap
    plt.clf(); plt.cla()
    rdh = rw.rooWrap(rdh,x)
    bkgRdh = rw.rooWrap(bkgRdh,x)
    mod = rw.rooWrap(mod,x)
    bkg = rw.rooWrap(bkg,x)
    mod.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    bkg.normalize(bkgNorm.getVal(),minRange=fitMin,maxRange=fitMax)

    # bkgRdh.normalizeTo(rdh)
    plt.plot(rdh.x,rdh.y,"ko",markersize=3,label="Template")
    # plt.plot(bkgRdh.x,bkgRdh.y,"o",color="k",label="Background Template")
    plt.plot(bkg.x,bkg.y,"C0",linewidth=2,label="B Model")


    # plot toy fits
    toyBandLo = [ [] for i in range(len(mod.x))]
    toyBandHi = [ [] for i in range(len(mod.x))]
    for toyFit in extrap._toyFitResults:
        toyFit.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
        for i,val in enumerate(toyFit.y):
            if val>mod.y[i]:
                toyBandHi[i].append(abs(val-mod.y[i]))
            else:
                toyBandLo[i].append(abs(val-mod.y[i]))
    toyBandLo = np.array([np.array(d).std() for d in toyBandLo])
    toyBandHi = np.array([np.array(d).std() for d in toyBandHi])
    plt.fill_between(mod.x,mod.y-toyBandLo,mod.y+toyBandHi,linewidth=0,facecolor="g",label=r"$1\sigma_{toys}$",alpha=0.5)

    # colors = ["r","y","g","b","c","m"]
    # for i,toy in enumerate(extrap._toyHistResults):
    #     c=colors[i%len(colors)]
    #     fit = extrap._toyFitResults[i]
    #     fit.normalizeTo(toy,minRange=fitMin,maxRange=fitMax)
    #     # plt.plot(toy.x,toy.y,linewidth=0,marker=".",color=c,alpha=0.25)
    #     plt.plot(fit.x,fit.y,linewidth=1,color=c,alpha=0.25)


    plt.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="Fit Limits")
    plt.plot([fitMin]*2,plt.ylim(),"c",linewidth=10,alpha=0.5)
    plt.xlabel(r"$m_{ll}$ GeV",fontsize=12)

    plt.yscale("log")
    plt.xscale("log")
    top = plt.ylim()[1]
    plt.ylim(top=top*1000)
    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize = 15)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.0, numticks=15))
    ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    ax.set_xticks([200,500,1000,6000])


    atlasInternal(position="nw")
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def injectionPlotFlat(yields,ss,interference,chirality,channel,path="plots/output.png",plotOn=True):
    """ Make plot for injection/nBackground test """
    print (green("Plotting injection/reco",path))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    masses = sorted(yields.keys())
    nReal= [yields[m]["nBkgMc"]  for m in masses][0]
    inj  = [yields[m]["nSigMc"]  for m in masses]
    # reco = [yields[m]["nBkg"]+1 for m in masses]
    reco = [yields[m]["nBkg"] for m in masses]
    upper = max(max(inj),max(reco))

    # plot error bar on bottom
    error = sqrt(ss["pdfSs"]**2 + ss["fitSs"]**2)
    plt.fill_between([0,upper],[nReal-2*error]*2,[nReal+2*error]*2,color="#fdff85",alpha=1,label=r"$2\sigma$ on background")
    plt.fill_between([0,upper],[nReal-error]*2,[nReal+error]*2,color="#89ff85",alpha=1,label=r"$1\sigma$ on background")

    plt.plot([0,upper],[nReal]*2,"r",label="MC Background")
    plt.plot(inj,reco,linewidth=0,marker="o",label="Injections")
    score = sum([(x-nReal)**2 for x in reco])/len(reco)


    # props={'ha':'left','va':'bottom',"fontsize":8,"rotation":90}
    # for m in masses:
    #     plt.text(yields[m]["nSigMc"],yields[m]["nBkg"],r"$\lambda={0}$ TeV".format(m),props)

    lumi = 139
    top = nReal
    plt.ylim(top=top*3)
    plt.ylim(bottom=0)
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ {1}-{2}".format(channel,interference,chirality))
    plt.xlabel("Signal Injected in SR",fontsize=12)
    plt.ylabel("Expected Background in SR (Extrapolation)",fontsize=12)
    plt.legend(loc=1,frameon=0,fontsize=12)
    if plotOn:
        plt.savefig(path,bbox_inches="tight")
        plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    return score



def injectionPlot(yields,ss,interference,chirality,channel,path="plots/output.png"):
    """ Make plot for injection/reco test """
    print (green("Plotting injection/reco",path))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    masses = sorted(yields.keys())
    inj  = [yields[m]["nSigMc"]  for m in masses]
    reco = [yields[m]["nReco"] for m in masses]
    upper = max(max(inj),max(reco))

    # plot error bar on bottom
    error = sqrt(ss["pdfSs"]**2 + ss["fitSs"]**2)
    plt.fill_between([0,upper],[2*error,upper+2*error],[-2*error,upper-2*error],color="#fdff85",alpha=1,label=r"$2\sigma$ on background")
    plt.fill_between([0,upper],[error,upper+error],[-error,upper-error],color="#89ff85",alpha=1,label=r"$1\sigma$ on background")

    plt.plot([0,upper],[0,upper],"r",label="1:1")
    plt.plot(inj,reco,linewidth=0,marker="o",label="Injections")

    for m in masses:
        plt.text(yields[m]["nSigMc"],yields[m]["nReco"],r"$\lambda={0}$ TeV".format(m),fontsize=8)

    lumi = 139
    top = plt.ylim()[1]
    plt.ylim(top=top*1.5)
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ {1}-{2}".format(channel,interference,chirality))
    plt.xlabel("Signal Injected",fontsize=12)
    plt.ylabel("Signal Recovered",fontsize=12)
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def compareSignalShapes(shapes,interference,chirality,channel,lumi=139):
    """ Do comparison of new and old signal shapes """
    print (green("Plotting"))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    for mass in shapes.keys():
        plt.plot(shapes[mass]["xOld"],abs(shapes[mass]["yOld"]),markersize=0,linestyle="-",alpha=0.5,linewidth=7,label=r"$\Lambda={0}$ TeV".format(mass))
        plt.plot(shapes[mass]["xNew"],abs(shapes[mass]["yNew"]),markersize=1,marker=".",linestyle=" ",color="k")
    plt.plot([],[],"k",linewidth=4,label="New Template")
    #
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ {1}-{2}".format(channel,interference,chirality))
    plt.xlabel(r"$m_{ll}$ GeV",fontsize=12)
    plt.ylabel("Events/Bin",fontsize=12)
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.yscale("log")
    plt.xscale("log")
    top = plt.ylim()[1]
    plt.ylim(top=top*100)
    path = "plots/sigComp-{0}-{1}-{2}".format(channel,interference,chirality)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def pdfSsRatioPlot(limitDicts,path="plots/pdfSsUncert.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(7,5))
    names = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))
    for iName, name in enumerate(names):
        lumi   = limitDicts[name]["lumi"]
        pdfSs  = limitDicts[name]["pdfSs"]
        fitSs  = limitDicts[name]["fitSs"]
        nBkg   = limitDicts[name]["nBkgMc"]
        channel= limitDicts[name]["channel"]
        pdfSsDist  = np.array(limitDicts[name]["pdfSsDist"]).std()
        fitSsDist  = limitDicts[name]["fitSsDist"]
        plt.errorbar([iName],[pdfSs/nBkg],lolims=False,uplims=True,yerr=[pdfSs/nBkg],marker="o",color="C3",markersize=8,linewidth=2)
        # plt.errorbar([iName],[pdfSs/nBkg],yerr=[pdfSsDist/nBkg],marker="o",color="C3",markersize=8,linewidth=2)
    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel))
    # plt.ylabel("Spurious Signal / Expected Background")
    plt.ylabel(r"$\frac{\text{Spurious Signal}}{\text{Expected Background}}$")
    # plt.title(title)
    top = plt.ylim()[1]
    plt.ylim(top=2,bottom=-0.5)
    names = [n.replace("ee-","") for n in names]
    names = [n.replace("-"," ") for n in names]
    plt.xticks(np.arange(0.0,len(names)+0.0,1),names,rotation=45)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def fitSsRatioPlot(limitDicts,path="plots/fitSsUncert.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(7,5))
    names = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))
    for iName, name in enumerate(names):
        lumi   = limitDicts[name]["lumi"]
        pdfSs  = limitDicts[name]["pdfSs"]
        fitSs  = limitDicts[name]["fitSs"]
        nBkg   = limitDicts[name]["nBkgMc"]
        channel= limitDicts[name]["channel"]
        pdfSsDist  = limitDicts[name]["pdfSsDist"]
        fitSsDist  = limitDicts[name]["fitSsDist"]
        plt.errorbar([iName],[fitSs/nBkg],yerr=[fitSsDist/nBkg],marker="o",color="C0",markersize=8,linewidth=2)
    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel))
    # plt.ylabel("Spurious Signal / Expected Background")
    plt.ylabel(r"$\frac{\text{CR Uncertainty}}{\text{Expected Background}}$")
    # plt.title(title)
    top = plt.ylim()[1]
    plt.ylim(top=2,bottom=-0.5)
    names = [n.replace("ee-","") for n in names]
    names = [n.replace("-"," ") for n in names]
    plt.xticks(np.arange(0.0,len(names)+0.0,1),names,rotation=45)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def signalShapePlot(signals,hists,label="",path="plots/output.png"):
    """ Make plot for signal comparison unit test """
    print (green("Plotting",path))
    plt.clf(); plt.cla()
    plt.figure(figsize=(5,5))

    # morphed PDF
    for iLambd,lambd in enumerate(sorted(signals.keys())):
        sig = signals[lambd]
        siglabel = r"{0}".format(lambd)
        sig.cut(minRange=300)
        plt.plot(sig.x,sig.y,label=siglabel)
        # plt.plot(sig.x,sig.y,label=siglabel)
        print (yellow("PDF:",len(sig.x)))

    # plt.ylim(top=top*100)
    # plt.ylim(top=4)
    # plt.ylim(bottom=-8)
    # plt.ylim(bottom=1e-20)
    # plt.xlim(left=300)
    top = plt.ylim()[1]
    plt.ylim(top=max(8,top*1.5))
    # plt.yscale("log")
    plt.xscale("log")
    atlasInternal(position="nw",subnote=label)
    plt.xlabel(r"$m_{ll}$ GeV",fontsize=12)
    plt.ylabel("Events/{0:.0f} GeV".format(sig._binWidths[0]),fontsize=12)
    plt.legend(loc=1,frameon=0,fontsize=10)

    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize = 15)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.0, numticks=15))
    ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    ax.set_xticks([300,500,1000,6000])

    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def yieldsPlot(limitDicts,nsig=True,path="plots/yields.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(8,6))
    # loop over limits
    xlabels = []
    names = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))
    width=1

    for iName,name in enumerate(names):
        # extract limits (number of signal events)
        l = limitDicts[name]
        print (yellow("Plotting",name))
        observed   = l["upperLimit"]
        expected   = l["expLimit"]
        uOneSig    = l["uOneSig"]
        uTwoSig    = l["uTwoSig"]
        lOneSig    = l["lOneSig"]
        lTwoSig    = l["lTwoSig"]
        channel    = l["channel"]
        extrapMin  = l["extrapMin"]
        extrapMax  = l["extrapMax"]
        lumi       = l["lumi"]
        chirality  = l["chirality"]
        interference  = l["interference"]
        observed_data   = l["upperLimit-data"]
        expected_data   = l["expLimit-data"]
        xlabels.append(chirality+" "+interference)

        nBkgObs_mc   = l["nBkgMc"]
        nBkgExp_mc   = l["nBkg"]
        nBkgObs_data = l["nObs-data"]
        nBkgExp_data = l["nBkg-data"]

        plt.plot(width*iName,nBkgObs_mc,          "bo")
        plt.errorbar(width*iName,nBkgExp_mc,  fmt="bo",xerr=width/2*0.9)
        plt.plot(width*iName,nBkgObs_data,        "ko")
        plt.errorbar(width*iName,nBkgExp_data,fmt="ko",xerr=width/2*0.9)
    plt.plot([],[],                               "bo",label="Observed - MC")
    plt.errorbar([],[],                       fmt="bo",xerr=width/2*0.9,label="Expected - MC")
    plt.plot([],[],                               "ko",label="Observed - Data")
    plt.errorbar([],[],                       fmt="ko",xerr=width/2*0.9,label="Expected - Data")


    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ channel\n\\emph{{Internal}}".format(channel))
    plt.legend(fontsize=12,loc=0, frameon=False)
    plt.ylabel("Events")
    # plt.title(title)
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
    plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
    # plt.yscale("log")
    # plt.ylim(0.10,1600)
    path = path.replace(".png",channel+".png")
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def histoFitMinStability(data,xlabel="",lumi=139,interference="const",channel="ee",path="plots/output.png"):
    """ Make histo
        Distribution, nominal fit result, and value used
    """
    plt.clf(); plt.cla()
    nBins = 20
    plt.figure(figsize=(8,6))

    dist=np.array(data.values())
    lo = min(data.keys())
    hi = max(data.keys())
    label=r"FitMin $\in{0}-{1}$".format(lo,hi)+"\n"
    label+=r"$\mu={0:.2f}$, $\sigma={1:.2f}$".format(dist.mean(),dist.std())
    n,bins,p = plt.hist(dist,nBins,color="C0",label=label)
    # print red(bins); quit()

    lo = 280
    hi = 350
    filtered = [data[x] for x in data.keys() if x>lo and x<hi+1]
    dist=np.array(filtered)
    label=r"FitMin $\in{0}-{1}$".format(lo,hi)+"\n"
    label+=r"$\mu={0:.2f}$, $\sigma={1:.2f}$".format(dist.mean(),dist.std())
    plt.hist(dist,bins,histtype='step',hatch='//',linewidth=1,edgecolor='C3',label=label)

    height = plt.ylim()[1]
    plt.ylim(0,plt.ylim()[1]*2)
    plt.xlabel(xlabel)
    path = "plots/fitMinStability-{0}-{1}.png".format(interference,channel)
    channel = channel.replace("mm",r"\mu\mu")
    atlasInternal(position="nw",lumi=lumi,subnote="{0}, ${1}$ channel".format(interference,channel))
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def propPlotFit(data,extrap,sigs,channel="mm",interference="const",lumi=139,path="plots/output_rw.png"):
    """ Nice looking plot with log-binned data """
    print (green("Plotting",path))

    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(8,6))
    grid = gridspec.GridSpec(2, 1, height_ratios=[3,1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])
    x = extrap.get("x")
    mod = extrap.get("model")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    extrapMin = extrap.get("extrapMin").getVal()

    # blind=True
    blind=False
    objs = []

    ###############
    # Data
    ###############
    loErr = np.array(data["loErr"])
    hiErr = np.array(data["hiErr"])
    datBins = np.array(data["x"])
    datVals = np.array(data["y"])
    datBinWidths = [datBins[i+1]-datBins[i] for i in range(len(datBins)-1)]
    datBinWidths+=[datBinWidths[-1]]
    datBinWidths=np.array(datBinWidths)
    # datBinWidths = [1]*len(datBinWidths) # temporary
    datValsAbs = np.array(datVals) # value not div bin width
    datVals/=datBinWidths
    loErr/=datBinWidths
    hiErr/=datBinWidths
    if blind:
        plt.errorbar(datBins[datBins<fitMax],datVals[datBins<fitMax],yerr=[loErr[datBins<fitMax],hiErr[datBins<fitMax]],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    else:
        plt.errorbar(datBins,datVals,yerr=[loErr,hiErr],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    dataPlotObject = plt.plot([],[],"ko",label="Data")

    ###############
    # Fit
    ###############

    fitBins = []
    fitVals = []
    fitValsAbs = []
    for i in range(len(datBins)-1):
        lo = datBins[i]
        hi = datBins[i+1]
        rName = "r"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(datBins[i])
        fitVals.append(integral/datBinWidths[i])
        fitValsAbs.append(integral)
    fitBins.append(datBins[i])
    fitVals.append(integral/datBinWidths[i])
    fitValsAbs.append(integral)
    fitValsAbs = np.array(fitValsAbs)
    fitVals    = np.array(fitVals)
    fitBins    = np.array(fitBins)
    # perform normalization, in CR
    fitValsCr  = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr  = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)
    fitValsCrAbs = fitValsAbs[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCrAbs = datValsAbs[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitValsAbs*= sum(datValsCrAbs)/sum(fitValsCrAbs)
    # ax.plot(fitBins,fitVals,"C3",label="Background-only fit")
    # ratio values taken from actual fit
    ratioVals = (datValsAbs-fitValsAbs)/np.sqrt(datValsAbs)
    # ratioVals = (datVals-fitVals)/datVals
    ratioBins = datBins
    ratioErrLo = data["loErr"]/np.sqrt(datValsAbs)
    ratioErrHi = data["hiErr"]/np.sqrt(datValsAbs)


    ###############
    # Signal shapes
    ###############
    # get background baseline for addint to signals
    fitBins = []
    fitVals = []
    sigBins = sigs[sigs.keys()[0]]["x"]
    sigBinWidths = [sigBins[i+1]-sigBins[i] for i in range(len(sigBins)-1)]

    # sigBinWidths = [1]*len(datBinWidths) # temporary
    for i in range(len(sigBins)-1):
        lo = sigBins[i]
        hi = sigBins[i+1]
        rName = "s"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(sigBins[i])
        fitVals.append(integral/datBinWidths[i])
    fitBins.append(sigBins[i])
    fitVals.append(integral)
    fitVals = np.array(fitVals)
    fitBins = np.array(fitBins)
    fitValsCr = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)
    # ax.plot(fitBins,fitVals,"C2",label="Test")

    styles = ["dotted","dashed","dashdot","loosely dotted"]
    signalPlotObjects = []
    for i,mass in sorted(enumerate(sigs.keys()),key=lambda x:x[1]):
        sig = sigs[mass]
        sigBins = sig["x"]
        sigVals = sig["y"]
        sigVals/=datBinWidths[i]
        signalPlotObject = ax.plot(sigBins[fitBins>fitMin],(fitVals+sigVals)[fitBins>fitMin],"C0",linestyle=styles[i],label=r"Signal $\Lambda$={0} TeV".format(mass))
        signalPlotObjects.append(signalPlotObject)
    bkgOnlyPlotObject = ax.plot(fitBins[np.logical_and(fitBins>fitMin,fitBins<=fitMax)],
            fitVals[np.logical_and(fitBins>fitMin,fitBins<=fitMax)],
            "C3",linewidth=3,label="Background-only fit")
    ax.plot(fitBins[fitBins>fitMax],fitVals[fitBins>fitMax],"C3",linewidth=3,linestyle=(0,(5,1)))

    plt.ylim(0.001,10e3)
    crSrBoundaryPlotObject = ax.plot([fitMax]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5,label="CR/SR Boundary")
    if fitMax!=extrapMin:
        ax.plot([extrapMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMax*0.9,plt.ylim()[1],r"$\leftarrow$ CR",alpha=0.5,horizontalalignment="right",verticalalignment='top')
    ax.text(extrapMin*1.1,plt.ylim()[1],r"SR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    ax.plot([fitMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMin*1.1,plt.ylim()[1],r"CR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    plt.xscale("log")
    plt.yscale("log")
    chan = channel.replace("mm",r"$\upmu\upmu$")
    atlasInternal(position="nw",lumi=lumi,subnote="{0}, {1} selection".format(interference,chan))
    plt.ylim(0.001,10e6)
    plt.ylabel(r"Events/Bin Width [GeV$^\text{-1}$]")
    xlim = list(plt.gca().get_xlim())
    xlim[0] = min(datBins[datVals>0])
    xlim[1] = max(datBins[datVals>0])*1.4
    # xlim[0] = fitMin
    plt.xlim(xlim)
    # make legend
    objs = [dataPlotObject, bkgOnlyPlotObject, crSrBoundaryPlotObject]
    objs+= signalPlotObjects
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc="upper right",ncol=2,frameon=False)
    # plt.legend(fontsize=12,ncol=2,loc="upper right",frameon=False)
    # ax.set_xticks([300,1000,2000])

    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=np.arange(0.0,1,0.1),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12) 
    # locmaj = matplotlib.ticker.LogLocator(base=10,subs="all",numticks=120) 
    ax.yaxis.set_major_locator(locmaj)

    ######################
    # bottom
    ######################
    ax = plt.subplot(grid[1])
    top=5
    bottom = -5
    mid=0
    if blind:
        ratioVals = ratioVals[ratioBins<fitMax]
        ratioErrLo = ratioErrLo[ratioBins<fitMax]
        ratioErrHi = ratioErrHi[ratioBins<fitMax]
        ratioBins = ratioBins[ratioBins<fitMax]
    ratioVals = ratioVals[ratioBins>fitMin]
    ratioErrLo = ratioErrLo[ratioBins>fitMin]
    ratioErrHi = ratioErrHi[ratioBins>fitMin]
    ratioBins = ratioBins[ratioBins>fitMin]

    arrowsUp = ratioBins[ratioVals>top]
    arrowsDn = ratioBins[ratioVals<=bottom]
    ax.plot(xlim,[mid]*2,"r",alpha=0.25)
    # ax.plot(ratioBins,ratioVals,"ko",markersize=1,label="Ratio")
    # ratioErrLo = ratioErrLo[ratioVals>0]
    # ratioErrHi = ratioErrHi[ratioVals>0]
    # ratioBins  = ratioBins[ratioVals>0]
    # ratioVals  = ratioVals[ratioVals>0]
    ax.errorbar(ratioBins,ratioVals,yerr=[ratioErrLo,ratioErrHi],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    ax.plot(arrowsUp,[top-0.1]*len(arrowsUp),marker="^",color="C3",linewidth=0)
    ax.plot(arrowsDn,[bottom+0.1]*len(arrowsDn),marker="v",color="C0",linewidth=0)
    ax.plot([fitMax]*2,[bottom-1,top+1],"k",linestyle=":",linewidth=1,alpha=0.5,label="CR/SR Boundary")
    ax.plot([fitMin]*2,[bottom-1,top+1],"k",linestyle=":",linewidth=1,alpha=0.5)
    plt.ylim((bottom-1,top+1))

    ticksInside()


    ax.set_xscale("log")
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=np.arange(0.0,1,0.1),numticks=12)
    ax.xaxis.set_minor_locator(locmin)
    ax.xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    ax.set_xticks([300,500,1000,2000,4000])
    formatter = FuncFormatter(lambda x,y:"${0:.1f}$".format(x/1000))
    ax.xaxis.set_major_formatter(formatter)
    # ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    plt.xlim(xlim)


    ax.set_yticks([-4,-2,0,2,4])
    # ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    chan = channel.replace("mm",r"\upmu\upmu")
    chan = chan.replace("ee",r"\text{ee}")
    plt.xlabel(r"$\text{{m}}_{{{0}}}$ [TeV]".format(chan))
    plt.ylabel(r"(Data-Fit)/$\pmb{\sigma}$",labelpad=16)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def limitPlot_lambda_data(limitDicts,scale=None,path="plots/limits.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(12,6))
    # loop over limits
    xlabels = []
    names = sorted(limitDicts.keys(),key=lambda x: 10*("ee" in x)+("dest" in x))

    shift=-1
    chiralities = ["LL","LR","RL","RR"]
    for iName,name in enumerate(names):
        for iChirality,chirality in enumerate(chiralities):
            shift+=1
            # extract limits (number of signal events)
            l = limitDicts[name]
            print (yellow("Plotting",name))
            observed   = l["{0}_lambda_upperLimit".format(chirality)]
            expected   = l["{0}_lambda_expLimit".format(chirality)]
            uOneSig    = l["{0}_lambda_uOneSig".format(chirality)]
            uTwoSig    = l["{0}_lambda_uTwoSig".format(chirality)]
            lOneSig    = l["{0}_lambda_lOneSig".format(chirality)]
            lTwoSig    = l["{0}_lambda_lTwoSig".format(chirality)]
            channel    = l["channel"]
            extrapMin  = l["extrapMin"]
            extrapMax  = l["extrapMax"]
            lumi       = l["lumi"]
            interference  = l["interference"]
            # observed_data   = l["upperLimit-data"]
            # expected_data   = l["expLimit-data"]
            xchannel = channel.replace("mm",r"$\upmu\upmu$")
            xchannel = xchannel.replace("ee",r"$ee$")
            if len(names)*len(chiralities)>=9:
                chan = channel.replace("mm",r"$\upmu\upmu$")
                xlabels.append(chan+" "+chirality+" "+interference)
            else:
                xlabels.append(chirality+" "+interference)
            prevLimit = [p for p in dictionary.prevLimits if p["channel"]==channel and p["chirality"]==chirality and p["interference"]==interference][0]["prevLimExp"]

            # if not nsig limits, convert to Lambda limits
            if 1:
                ylabel="95\% CL on $\Lambda$ [TeV]"

                if scale=="neuron":
                    tevToEcoli = (1.783e-24)*1e15
                    observed *= tevToEcoli
                    expected *= tevToEcoli
                    uOneSig  *= tevToEcoli
                    uTwoSig  *= tevToEcoli
                    lOneSig  *= tevToEcoli
                    lTwoSig  *= tevToEcoli
                    ylabel="e. coli cells"
                if scale=="transistor":
                    trans = (1.783e-24)/3.1246e-24
                    observed *= trans
                    expected *= trans
                    uOneSig  *= trans
                    uTwoSig  *= trans
                    lOneSig  *= trans
                    lTwoSig  *= trans
                    ylabel="14nm Transistors"
                if scale=="kg":
                    tevToKg = 1.783e-24
                    observed *= tevToKg
                    expected *= tevToKg
                    uOneSig  *= tevToKg
                    uTwoSig  *= tevToKg
                    lOneSig  *= tevToKg
                    lTwoSig  *= tevToKg
                    ylabel="kilograms"
                if scale=="r":
                    tevToM=1.973e-19 # hbar*c/[TeV]
                    observed = tevToM/observed
                    expected = tevToM/expected
                    uOneSig  = tevToM/uOneSig 
                    uTwoSig  = tevToM/uTwoSig 
                    lOneSig  = tevToM/lOneSig 
                    lTwoSig  = tevToM/lTwoSig 
                    ylabel="Lepton Radius"

            width=1
            plt.bar(width*shift,uTwoSig-lTwoSig,edgecolor="none",width=width*0.9,bottom=lTwoSig,color="#D6D600",label=r"2$\upsigma$ Expected")
            plt.bar(width*shift,uOneSig-lOneSig,edgecolor="none",width=width*0.9,bottom=lOneSig,color="#18B100",label=r"1$\upsigma$ Expected")
            obs=plt.errorbar(width*shift,observed,fmt="ko",xerr=width/2*0.9,label="Observed Limit")
            exp=plt.errorbar(width*shift,expected,fmt="k",xerr=width/2 *0.9,label="Expected Limit")
            exp[-1][0].set_linestyle('--')


            if shift==0: plt.legend(fontsize=12,loc=0, frameon=False)

    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\upmu\upmu")
    atlasInternal(position="nw",lumi=lumi,status="Internal")
    atlasInternal(position="nw",lumi=lumi,status="Internal",subnote="${0}$ channel".format(channel))
    plt.ylabel(ylabel)
    path = path.replace(".png","-Lambda.png")
    # plt.title(title)
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
    plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
    # plt.yscale("log")
    # plt.ylim(0.10,1600)
    path = path.replace("-.png",".png")
    path = path.replace("--","-")
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def limitPlot_nSig_data(limitDicts,nsig=True,scale=None,path="plots/limits.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    import lambdaConversion as lc
    plt.clf(); plt.cla()
    if nsig:
        plt.figure(figsize=(6,6))
    else:
        plt.figure(figsize=(12,6))
    if not nsig: lambdaConvert = lc.lambdaLimit() # class to calculate limits
    # loop over limits
    xlabels = []
    names = sorted(limitDicts.keys(),key=lambda x: 10*("ee" in x)+("dest" in x))

    shift=-1
    if nsig:
        chiralities = ["LL"]
    else:
        chiralities = ["LL","LR","RL","RR"]
    for iName,name in enumerate(names):
        for iChirality,chirality in enumerate(chiralities):
            shift+=1
            # extract limits (number of signal events)
            l = limitDicts[name]
            print (yellow("Plotting",name))
            observed   = l["nSig_upperLimit"]
            expected   = l["nSig_expLimit"]
            uOneSig    = l["nSig_uOneSig"]
            uTwoSig    = l["nSig_uTwoSig"]
            lOneSig    = l["nSig_lOneSig"]
            lTwoSig    = l["nSig_lTwoSig"]
            channel    = l["channel"]
            extrapMin  = l["extrapMin"]
            extrapMax  = l["extrapMax"]
            lumi       = l["lumi"]
            interference  = l["interference"]
            # observed_data   = l["upperLimit-data"]
            # expected_data   = l["expLimit-data"]
            xchannel = channel.replace("mm",r"$\upmu\upmu$")
            xchannel = xchannel.replace("ee",r"$ee$")
            if nsig:
                chan = channel.replace("mm",r"$\upmu\upmu$")
                xlabels.append(chan+" "+interference)
            else:
                if len(names)*len(chiralities)>=8:
                    chan = channel.replace("mm",r"$\upmu\upmu$")
                    xlabels.append(chan+" "+chirality+" "+interference)
                else:
                    xlabels.append(chirality+" "+interference)
            prevLimit = [p for p in dictionary.prevLimits if p["channel"]==channel and p["chirality"]==chirality and p["interference"]==interference][0]["prevLimExp"]

            # if not nsig limits, convert to Lambda limits
            if not nsig:
                # observed_data = lambdaConvert.getLambdaLimit(observed_data,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                # expected_data = lambdaConvert.getLambdaLimit(expected_data,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                observed = lambdaConvert.getLambdaLimit(observed,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                expected = lambdaConvert.getLambdaLimit(expected,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                uOneSig  = lambdaConvert.getLambdaLimit(uOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                uTwoSig  = lambdaConvert.getLambdaLimit(uTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                lOneSig  = lambdaConvert.getLambdaLimit(lOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                lTwoSig  = lambdaConvert.getLambdaLimit(lTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
                ylabel="95\% CL on $\Lambda$ [TeV]"

                if scale=="neuron":
                    tevToEcoli = (1.783e-24)*1e15
                    observed *= tevToEcoli
                    expected *= tevToEcoli
                    uOneSig  *= tevToEcoli
                    uTwoSig  *= tevToEcoli
                    lOneSig  *= tevToEcoli
                    lTwoSig  *= tevToEcoli
                    ylabel="e. coli cells"
                if scale=="transistor":
                    trans = (1.783e-24)/3.1246e-24
                    observed *= trans
                    expected *= trans
                    uOneSig  *= trans
                    uTwoSig  *= trans
                    lOneSig  *= trans
                    lTwoSig  *= trans
                    ylabel="14nm Transistors"
                if scale=="kg":
                    tevToKg = 1.783e-24
                    observed *= tevToKg
                    expected *= tevToKg
                    uOneSig  *= tevToKg
                    uTwoSig  *= tevToKg
                    lOneSig  *= tevToKg
                    lTwoSig  *= tevToKg
                    ylabel="kilograms"
                if scale=="r":
                    tevToM=1.973e-19 # hbar*c/[TeV]
                    observed = tevToM/observed
                    expected = tevToM/expected
                    uOneSig  = tevToM/uOneSig 
                    uTwoSig  = tevToM/uTwoSig 
                    lOneSig  = tevToM/lOneSig 
                    lTwoSig  = tevToM/lTwoSig 
                    ylabel="Lepton Radius"

            width=1
            plt.bar(width*shift,uTwoSig-lTwoSig,edgecolor="none",width=width*0.9,bottom=lTwoSig,color="#D6D600",label=r"2$\upsigma$ Expected")
            plt.bar(width*shift,uOneSig-lOneSig,edgecolor="none",width=width*0.9,bottom=lOneSig,color="#18B100",label=r"1$\upsigma$ Expected")
            obs=plt.errorbar(width*shift,observed,fmt="ko",xerr=width/2*0.9,label="Observed Limit")
            exp=plt.errorbar(width*shift,expected,fmt="k",xerr=width/2 *0.9,label="Expected Limit")
            exp[-1][0].set_linestyle('--')

            # if not nsig:
            #     prev=plt.plot([shift-0.43,shift+0.43],[prevLimit]*2,"r",label="Previous Expected Limit (${0}fb^{{-1}}$)".format(36.1))

            if shift==0: plt.legend(fontsize=12,loc=0, frameon=False)

    lumi = limitDicts.values()[0]["lumi"]
    channel = channel.replace("mm",r"\mu\mu")
    if nsig:
        atlasInternal(position="nw",lumi=lumi,status="Internal")
    else:
        atlasInternal(position="nw",lumi=lumi,status="Internal",subnote="${0}$ channel".format(channel))
    if nsig:
        plt.ylabel("95\% CL on Signal Events")
    else:
        plt.ylabel(ylabel)
        path = path.replace(".png","Lambda.png")
    # plt.title(title)
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
    if nsig:
        plt.ylim(-5,plt.gca().get_ylim()[1]*1.5)
    else:
        plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
    # plt.yscale("log")
    # plt.ylim(0.10,1600)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def plotToys(nominal,path="plots/demo.png"):
    """ Plot toys and backgroundTemplate
    """
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(12,7))
    grid = gridspec.GridSpec(2, 1, height_ratios=[3,1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])

    # nominal._toyNames.append(toyName)
    print (nominal._toyNames)
    x = nominal.get("x")
    fitMin = nominal.get("fitMin").getVal()
    fitMax = nominal.get("fitMax").getVal()
    bkg = rw.rooWrap(nominal.get("backgroundTemplate"),x)
    plt.plot(bkg.x,bkg.y,"ko")

    for name in nominal._toyNames:
        toy = rw.rooWrap(nominal.get(name),x)
        ax.plot(toy.x,toy.y,marker=".",lw=0)
        print (yellow(sum(toy.y[np.logical_and(toy.x>fitMin,toy.x<fitMax)])))
    # count backgroundTemplate
    print (yellow(sum(bkg.y[np.logical_and(bkg.x>fitMin,bkg.x<fitMax)])))
    ax.set_yscale("log")
    ax.set_xscale("log")
    xlim=list(ax.get_xlim())

    # BOTTOM
    ax = plt.subplot(grid[1])
    for name in nominal._toyNames:
        toy = rw.rooWrap(nominal.get(name),x)
        ax.plot(toy.x,toy.y/bkg.y,marker=".",lw=0)
    ax.set_yscale("linear")
    ax.set_xscale("log")
    ax.set_xlim(xlim)

    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def poisson_interval(v):
    if v==0: return 0,0
    # from Ntuple reader code:
    y1 = v + 1.0
    d1 = 1.0 - 1.0/(9.0*y1) + 1.0/(3*sqrt(abs(y1)))
    hi = y1*d1*d1*d1 - v
    y2 = v
    d2 = 1.0 - 1.0/(9.0*y2) - 1.0/(3.0*sqrt(abs(y2)))
    lo = v - y2*d2*d2*d2
    lo = np.array(lo)
    hi = np.array(hi)
    return [lo,hi]

def plot_rw_detail(extrap,useSbModel,channel="mm",lumi=139,path="plots/output_rw.png"):
    print (green("Plotting",path))
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(12,7))
    grid = gridspec.GridSpec(2, 1, height_ratios=[3,1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])


    x = extrap.get("x")
    mod = extrap.get("model")
    bkg = extrap.get("backgroundModel")
    rdh = extrap.get("template")
    bkgRdh = extrap.get("backgroundTemplate")
    bkgNorm= extrap.get("backgroundNorm")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    # import roowrap as rw
    # Make plot using rooWrap
    rdh = rw.rooWrap(rdh,x)
    bkgRdh = rw.rooWrap(bkgRdh,x)
    mod = rw.rooWrap(mod,x)
    bkg = rw.rooWrap(bkg,x)

    mod.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    bkg.normalize(bkgNorm.getVal(),minRange=fitMin,maxRange=fitMax)
    # ratio
    pull=False
    ratioBins = rdh.x
    ratioVals = rdh.y/mod.y
    ratioBins = ratioBins[ratioVals>0]
    ratioVals = ratioVals[ratioVals>0]

    # pull
    pull=True
    ratioBins = rdh.x
    ratioVals = (rdh.y-mod.y)/np.sqrt(mod.y)

    # bkgRdh.normalizeTo(rdh)
    loErr = np.array([poisson_interval(i)[0] for i in rdh.y])
    hiErr = np.array([poisson_interval(i)[1] for i in rdh.y])
    # print list(loErr); quit()
    # ax.errorbar(rdh.x,rdh.y,yerr=[loErr,hiErr],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    ax.plot(rdh.x,rdh.y,markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    if useSbModel:
        ax.plot(mod.x,mod.y,"C3",label="S+B Model")
        ax.plot(bkgRdh.x,bkgRdh.y,"o",color="C1",label="Background Template")
    ax.plot(bkg.x,bkg.y,"C0",linewidth=4,label="B Model")
    ax.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="Fit Limits")
    ax.plot([fitMin]*2,plt.ylim(),"c",linewidth=10,alpha=0.5)

    # legend setup
    plt.legend(fontsize=12,loc=0, frameon=False)
    atlasInternal(position="nw",lumi=lumi,subnote="${0}$ Selection".format(channel.replace("mm",r"\mu\mu")))
    plt.xlim(left=min(rdh.x[rdh.y>0]))
    xlim = list(plt.gca().get_xlim())
    ylim = list(plt.gca().get_ylim())
    plt.xlim(xlim)
    plt.ylim(top=1e7,bottom=1e-2)
    plt.yscale("log")
    plt.xscale("log")
    plt.ylabel("Events/Bin ({0:.1f} GeV)".format(rdh.x[1]-rdh.x[0]))

    ######################
    # bottom
    ######################
    ax = plt.subplot(grid[1])
    if pull:
        top=5
        bottom = -5
        mid = 0
    else:
        top=2
        bottom = 0
        mid=1
    plt.ylim((bottom,top))
    plt.xlim(xlim)
    arrowsUp = ratioBins[ratioVals>top]
    arrowsDn = ratioBins[ratioVals<bottom]
    ax.plot(xlim,[mid]*2,"r",alpha=0.25)
    ax.plot(ratioBins,ratioVals,"ko",markersize=1,label="Ratio")
    ax.plot(arrowsUp,[top-0.1]*len(arrowsUp),"r^")
    ax.plot(arrowsDn,[bottom+0.1]*len(arrowsDn),"bv")
    ax.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="Fit Limits")
    ax.plot([fitMin]*2,plt.ylim(),"c",linewidth=10,alpha=0.5)
    plt.xscale("log")
    ticksInside()

    if pull: plt.ylabel("Template-Model Pull",fontsize=10)
    else: plt.ylabel("Template/Model")
    plt.xlabel("$M_{{{0}}}$ [GeV]".format(channel.replace("mm",r"\mu\mu")))
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def propPlotFitClean(data,extrap,sigs,channel="mm",interference="const",lumi=139,path="plots/output_rw.png"):
    """ Nice looking plot with log-binned data """
    print (green("Plotting",path))

    import matplotlib.gridspec as gridspec
    # plt.figure(figsize=(8,6))
    plt.figure(figsize=(6,3.5))
    grid = gridspec.GridSpec(1, 1, height_ratios=[1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])
    x = extrap.get("x")
    mod = extrap.get("model")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    extrapMin = extrap.get("extrapMin").getVal()

    # blind=True
    blind=False
    objs = []

    ###############
    # Data
    ###############
    loErr = np.array(data["loErr"])
    hiErr = np.array(data["hiErr"])
    datBins = np.array(data["x"])
    datVals = np.array(data["y"])
    datBinWidths = [datBins[i+1]-datBins[i] for i in range(len(datBins)-1)]
    datBinWidths+=[datBinWidths[-1]]
    datBinWidths=np.array(datBinWidths)
    # datBinWidths = [1]*len(datBinWidths) # temporary
    datVals/=datBinWidths
    loErr/=datBinWidths
    hiErr/=datBinWidths
    # plt.errorbar(datBins,datVals,yerr=[loErr,hiErr],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")

    ###############
    # Fit
    ###############

    fitBins = []
    fitVals = []
    for i in range(len(datBins)-1):
        lo = datBins[i]
        hi = datBins[i+1]
        rName = "r"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(datBins[i])
        fitVals.append(integral/datBinWidths[i])
    fitBins.append(datBins[i])
    fitVals.append(integral/datBinWidths[i])
    fitVals    = np.array(fitVals)
    fitBins    = np.array(fitBins)
    # perform normalization, in CR
    fitValsCr  = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr  = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)

    ###############
    # Signal shapes
    ###############
    # get background baseline for addint to signals
    fitBins = []
    fitVals = []
    sigBins = sigs[sigs.keys()[0]]["x"]
    sigBinWidths = [sigBins[i+1]-sigBins[i] for i in range(len(sigBins)-1)]

    # sigBinWidths = [1]*len(datBinWidths) # temporary
    for i in range(len(sigBins)-1):
        lo = sigBins[i]
        hi = sigBins[i+1]
        rName = "s"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(sigBins[i])
        fitVals.append(integral/datBinWidths[i])
    fitBins.append(sigBins[i])
    fitVals.append(integral)
    fitVals = np.array(fitVals)
    fitBins = np.array(fitBins)
    fitValsCr = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)
    # ax.plot(fitBins,fitVals,"C2",label="Test")
    # scale for miss-binning! TODO: remove!!!
    fitVals*=0.8

    styles = ["dotted","dashed","dashdot","loosely dotted"]
    signalPlotObjects = []
    for i,mass in enumerate(sigs.keys()):
        sig = sigs[mass]
        sigBins = sig["x"]
        sigVals = sig["y"]
        sigVals/=datBinWidths[i]
        signalPlotObject = ax.plot(sigBins[fitBins>fitMin][:-1],(fitVals+sigVals)[fitBins>fitMin][:-1],"C0",linestyle=styles[i],label="Signal")
        signalPlotObjects.append(signalPlotObject)
        break
    bkgOnlyPlotObject = ax.plot(fitBins[:-1],fitVals[:-1],"k",label="Background")

    plt.ylim(min(fitVals),10e3)
    crSrBoundaryPlotObject = ax.plot([fitMax]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5,label="CR/SR Boundary")
    if fitMax!=extrapMin:
        ax.plot([extrapMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMax*0.9,plt.ylim()[1],r"$\leftarrow$ CR",alpha=0.5,horizontalalignment="right",verticalalignment='top')
    ax.text(extrapMin*1.1,plt.ylim()[1],r"SR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    ax.plot([fitMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMin*1.1,plt.ylim()[1],r"CR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    plt.xscale("log")
    plt.yscale("log")
    atlasInternal(position="nw",lumi=lumi)
    plt.ylim(min(fitVals),10e8)
    plt.ylabel(r"Events")
    xlim = list(plt.gca().get_xlim())
    xlim[0] = min(datBins[datVals>0])
    xlim[1] = max(datBins[datVals>0])*1.4
    # xlim[0] = fitMin
    plt.xlim(xlim)
    # make legend
    objs = [bkgOnlyPlotObject, crSrBoundaryPlotObject]
    objs+= signalPlotObjects
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc="upper right",ncol=1,frameon=False)

    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.0, numticks=15))
    ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    # ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
    ax.set_xticks([300,1000,2000,6000])
    ax.set_yticks([])

    plt.xlabel(r"$m_{\ell\ell}$ [GeV]")
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def nSigFunc(data,path="plots/demo.png"):
    """ Plot nSignal function
    """
    print (green("Plotting",path))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))

    for iName,name in enumerate(data.keys()):
        xname = name.replace("mm-",r"$\upmu\upmu$-")
        xname = xname.replace("ee-",r"ee ")
        plt.plot(data[name]["lambdas"],data[name]["nSigs"],color="C{0}".format(iName),label=xname)


    plt.yscale("log")
    plt.ylim(top=plt.ylim()[1]*10)
    atlasInternal(position="nw",lumi=139)
    ticksInside(removeXLabel=False)
    plt.ylabel("Signal Events in SR")
    plt.xlabel(r"$\Lambda$ [TeV]")
    plt.legend(fontsize=12,loc=0, frameon=False)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def simpleHist(dist,mark=None,path="plots/demo.png"):
    """ Make histo
    """
    print (green("Plotting",path))
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    dist=np.array(dist)
    nBins = int(len(dist)/10)
    # nBins = max(nBins,10)
    nBins = min(nBins,50)
    nBins = 20
    lo = -1.1
    hi = 1.1
    bins = np.arange(lo,hi,(hi-lo)/nBins)
    print (lo, hi, (lo-hi)/nBins)

    plt.hist(dist,bins=bins,label=r"")
    if mark!=None:
        plt.plot([mark],[plt.ylim()[1]],"ro")

    plt.ylim(0,plt.ylim()[1]*2)
    plt.xlim([lo,hi])

    # xlim = list(plt.xlim())
    # w = xlim[1]-xlim[0]
    # m = (xlim[1]+xlim[0])/2
    # plt.xlim([m-w,m+w])

    atlasInternal(size=15)
    plt.xlabel("Significance of distortion, displaced from nominal")
    plt.legend(loc=1,frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def systPlot(data,syst="",lumi=139,path="plots/uncert.png"):
    """ Make plot of multiple limits 
        Plots in terms of number of signal events
    """
    plt.clf(); plt.cla()
    plt.figure(figsize=(5,5))
    names = sorted(data.keys(),key=lambda x: 100*("mm" in x)+10*("dest" in x))
    c = {"fit":"C0","pdf":"C1","func":"C3"}[syst]
    label = {"fit":"CR Uncertainty","pdf":"SS Uncertainty","func":"Function Choice Uncertainty"}[syst]

    for iName,name in enumerate(names):
        plt.plot(iName,data[name],color=c,marker="s",markersize=12)
    atlasInternal(position="nw",lumi=lumi)
    plt.ylabel(r"$\frac{\text{"+label+r"}}{\text{Expected Background}}$")
    # plt.title(title)
    top = plt.ylim()[1]
    bot = plt.ylim()[0]
    plt.ylim(top=top+0.5*(top-bot))
    plt.ylim(bottom=bot-0.5*(top-bot))
    names = [n.replace("-"," ") for n in names]
    names = [n.replace("mm",r"$\upmu\upmu$") for n in names]
    plt.xticks(np.arange(0.0,len(names)+0.0,1),names,rotation=45)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")

def propPlotFitNoData(data,extrap,sigs,channel="mm",interference="const",lumi=139,path="plots/output_rw.png"):
    """ Nice looking plot log binning, no data"""
    print (green("Plotting",path))

    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(5,5))
    grid = gridspec.GridSpec(1, 1, height_ratios=[1])
    grid.update(wspace=0.025, hspace=0)
    ax = plt.subplot(grid[0])
    x = extrap.get("x")
    mod = extrap.get("model")
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    extrapMin = extrap.get("extrapMin").getVal()

    # blind=True
    blind=False
    objs = []

    ###############
    # Data
    ###############
    loErr = np.array(data["loErr"])
    hiErr = np.array(data["hiErr"])
    datBins = np.array(data["x"])
    datVals = np.array(data["y"])
    datBinWidths = [datBins[i+1]-datBins[i] for i in range(len(datBins)-1)]
    datBinWidths+=[datBinWidths[-1]]
    datBinWidths=np.array(datBinWidths)
    # datBinWidths = [1]*len(datBinWidths) # temporary
    datValsAbs = np.array(datVals) # value not div bin width
    datVals/=datBinWidths
    loErr/=datBinWidths
    hiErr/=datBinWidths
    # if blind:
    #     plt.errorbar(datBins[datBins<fitMax],datVals[datBins<fitMax],yerr=[loErr[datBins<fitMax],hiErr[datBins<fitMax]],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    # else:
    #     plt.errorbar(datBins,datVals,yerr=[loErr,hiErr],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data")
    # dataPlotObject = plt.plot([],[],"ko",label="Data")

    ###############
    # Fit
    ###############

    fitBins = []
    fitVals = []
    fitValsAbs = []
    for i in range(len(datBins)-1):
        lo = datBins[i]
        hi = datBins[i+1]
        rName = "r"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(datBins[i])
        fitVals.append(integral/datBinWidths[i])
        fitValsAbs.append(integral)
    fitBins.append(datBins[i])
    fitVals.append(integral/datBinWidths[i])
    fitValsAbs.append(integral)
    fitValsAbs = np.array(fitValsAbs)
    fitVals    = np.array(fitVals)
    fitBins    = np.array(fitBins)
    # perform normalization, in CR
    fitValsCr  = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr  = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)
    fitValsCrAbs = fitValsAbs[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCrAbs = datValsAbs[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitValsAbs*= sum(datValsCrAbs)/sum(fitValsCrAbs)
    # ax.plot(fitBins,fitVals,"C3",label="Background-only fit")
    # ratio values taken from actual fit
    ratioVals = (datValsAbs-fitValsAbs)/np.sqrt(datValsAbs)
    # ratioVals = (datVals-fitVals)/datVals
    ratioBins = datBins
    ratioErrLo = data["loErr"]/np.sqrt(datValsAbs)
    ratioErrHi = data["hiErr"]/np.sqrt(datValsAbs)


    ###############
    # Signal shapes
    ###############
    # get background baseline for addint to signals
    fitBins = []
    fitVals = []
    sigBins = sigs[sigs.keys()[0]]["x"]
    sigBinWidths = [sigBins[i+1]-sigBins[i] for i in range(len(sigBins)-1)]

    # sigBinWidths = [1]*len(datBinWidths) # temporary
    for i in range(len(sigBins)-1):
        lo = sigBins[i]
        hi = sigBins[i+1]
        rName = "s"+str(i)
        x.setRange(rName,lo,hi)
        integral = mod.createIntegral(RooArgSet(x),RooFit.Range(rName)).getVal()
        fitBins.append(sigBins[i])
        fitVals.append(integral/datBinWidths[i])
    fitBins.append(sigBins[i])
    fitVals.append(integral)
    fitVals = np.array(fitVals)
    fitBins = np.array(fitBins)
    fitValsCr = fitVals[np.logical_and(fitBins>fitMin,fitBins<fitMax)]
    datValsCr = datVals[np.logical_and(datBins>fitMin,datBins<fitMax)]
    fitVals*= sum(datValsCr)/sum(fitValsCr)

    styles = ["dotted","dashed","dashdot","loosely dotted"]
    signalPlotObjects = []
    for i,mass in sorted(enumerate(sigs.keys()),key=lambda x:x[1]):
        sig = sigs[mass]
        sigBins = sig["x"]
        sigVals = sig["y"]
        sigVals/=datBinWidths[i]
        signalPlotObject = ax.plot(sigBins[fitBins>fitMin][:-1],(fitVals+sigVals)[fitBins>fitMin][:-1],"C0",linestyle=styles[i],label=r"Signal $\Lambda$={0} TeV".format(mass))
        signalPlotObjects.append(signalPlotObject)
    bkgOnlyPlotObject = ax.plot(fitBins[np.logical_and(fitBins>fitMin,fitBins<=fitMax)],
            fitVals[np.logical_and(fitBins>fitMin,fitBins<=fitMax)],
            "C3",
            linewidth=3,
            label="Background-only fit")
    ax.plot(fitBins[fitBins>fitMax][:-1],fitVals[fitBins>fitMax][:-1],"C3",linewidth=3,linestyle=(0,(5,1)))

    # plt.ylim(0.000001,10e3)
    plt.ylim(top=10e3)
    crSrBoundaryPlotObject = ax.plot([fitMax]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5,label="CR/SR Boundary")
    if fitMax!=extrapMin:
        ax.plot([extrapMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMax*0.9,plt.ylim()[1],r"$\leftarrow$ CR",alpha=0.5,horizontalalignment="right",verticalalignment='top')
    ax.text(extrapMin*1.1,plt.ylim()[1],r"SR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    ax.plot([fitMin]*2,plt.ylim(),"k",linestyle=":",linewidth=1,alpha=0.5)
    ax.text(fitMin*1.1,plt.ylim()[1],r"CR $\rightarrow$",alpha=0.5,horizontalalignment="left",verticalalignment='top')
    plt.xscale("log")
    plt.yscale("log")
    chan = channel.replace("mm",r"$\upmu\upmu$")
    atlasInternal(position="nw",lumi=lumi,subnote="{0}, {1} selection".format(interference,chan))
    plt.ylim(0.00001,10e7)
    plt.ylabel(r"Events/Bin Width [GeV$^\text{-1}$]")
    xlim = list(plt.gca().get_xlim())
    xlim[0] = fitMin
    xlim[1] = 6000
    # xlim[0] = fitMin
    plt.xlim(xlim)
    # make legend
    fillerObj=plt.plot([],[]," ",label=r"\phantom{x}")
    objs = [bkgOnlyPlotObject]
    # objs+= [ crSrBoundaryPlotObject]
    # objs+= [fillerObj]
    objs+= signalPlotObjects
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc="upper right",ncol=1,frameon=False)
    # plt.legend(fontsize=12,ncol=2,loc="upper right",frameon=False)
    # ax.set_xticks([300,1000,2000])

    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=np.arange(0.0,1,0.2),numticks=120)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ax.set_xscale("log")
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=np.arange(0.0,1,0.1),numticks=120)
    ax.xaxis.set_minor_locator(locmin)
    ax.xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    ax.set_xticks([300,500,1000,2000,4000])
    formatter = FuncFormatter(lambda x,y:"${0:.1f}$".format(x/1000))
    ax.xaxis.set_major_formatter(formatter)
    # ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    plt.xlim(xlim)



    chan = channel.replace("mm",r"\upmu\upmu")
    chan = chan.replace("ee",r"\text{ee}")
    plt.xlabel(r"$\text{{m}}_{{{0}}}$ [TeV]".format(chan))
    plt.ylabel(r"(Data-Fit)/$\pmb{\sigma}$",labelpad=16)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
