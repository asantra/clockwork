########################################################################
#   Wavelets Generatorion and Distributions of Signal + Background   #
########################################################################

#Explanations
# - Generate a series of signals and Backgrounds from functions defined in

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, array, sys, os
import matplotlib
matplotlib.use("Agg")
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.ticker as plticker
from matplotlib.pyplot import cm


sys.path.append(os.path.abspath('../SignalBackground_Generation'))
# sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))


#Import custom
# import Merger, WT
import SigBkg_Functions as SB

#import ATLAS MPL Style
import mplhep as hep
hep.set_style(hep.style.ROOT)


print("Files imported.")
print("")



#Settings
print("Reading settings...")

luminosity = 139
year = '2015-18'



xmin_ee = 500
xmax_ee = 4800
nBins_ee   = 4300
nBins_ee_01GeV   = 48000

xmin_yy = 0
xmax_yy = 2000
nBins_yy   = 2000


eventType = "float"

#Parameters of the signal

k  = 500
M5 = 6000
#Signal Parameter strength
aS = 1

massList_ee = numpy.linspace(xmin_ee, xmax_ee, nBins_ee)
massList_ee_01GeV = numpy.linspace(xmin_ee, xmax_ee, nBins_ee_01GeV)

massList_yy = numpy.linspace(xmin_yy, xmax_yy, nBins_yy)+1

print("Setting read.")
print("")

# nBins_ee_Generic  = xmax_ee - k
# SigFix_Generic = SB.SigFix_Generic(0.02, xmin_ee, xmax_ee, nBins_ee, eventType)
# massList_SigFix_Generic = numpy.linspace(k, xmax_ee, nBins_ee_Generic)
# fig_SigFix_Generic, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# plt.plot(massList_ee,SigFix_Generic, 'b', alpha=1, label=r"Signal Generic ee")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# # plt.xscale('log')
# fig_SigFix_Generic.savefig('plots_MassDistribution/SigGeneric_ee.pdf')


SigFix_yy_MCReco = SB.MC_Signal_Diphoton_Reconstructed(xmin_yy, xmax_yy, nBins_yy)
SigFix_yy_AnalyticalTransformed_Gauss, _ = SB.SigFix_Theory_HighMass_Diphoton_GaussSmeared(k, M5, xmin_yy, xmax_yy, nBins_yy)
# SigFix_yy_AnalyticalTransformed_TF, _ =  SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin_yy, xmax_yy, nBins_yy)
# SigFix_yy_MCTransformed = SB.MC_Signal_Diphoton_TFApplied(xmin_yy, xmax_yy, nBins_yy)

# SigFix_ee_AnalyticalTransformed, _ = SB.SigFix_Theory_Dielectron_NewConvolution(k, M5, xmin_ee, xmax_ee, nBins_ee)
# # SigFix_ee_MCTransformed = SB.MC_Signal_Dielectron_TFApplied(xmin_ee, xmax_ee, nBins_ee)
# SigFix_ee_MCReco = SB.MC_Signal_Dielectron_Reconstructed(xmin_ee, xmax_ee, nBins_ee)

fig_Sig_yy_Reco1, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList_yy,SigFix_yy_AnalyticalTransformed_Gauss, 'g', alpha=1, label=r"Signal Transformed yy Guassian Resoltuion")
plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.margins(0)
plt.legend(loc='best')
plt.ylim(bottom=1e-4)
# plt.yscale('log')
# plt.xscale('log')
fig_Sig_yy_Reco1.savefig('plots_MassDistribution/SigyyRecoComparison2.pdf')
fig_Sig_yy_Reco2, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList_yy,SigFix_yy_MCReco, 'b', alpha=1, label=r"Signal Reco MC yy")
plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.margins(0)
plt.legend(loc='best')
plt.ylim(bottom=1e-4)
# plt.yscale('log')
# plt.xscale('log')
fig_Sig_yy_Reco2.savefig('plots_MassDistribution/SigyyRecoComparison1.pdf')
#RatioPlot
fig_RatioRecoCheckyy, ax = plt.subplots()
MCReco_Ratioyy = SB.RatioMaker(SigFix_yy_AnalyticalTransformed_Gauss,SigFix_yy_MCReco)
# plt.xscale('log')
plt.ylabel('Ratio Analytic Transform/MC transform yy', ha='right', y=1.0)
plt.ylim(top=1.5)
plt.ylim(bottom=0.5)
plt.plot(massList_yy,MCReco_Ratioyy, 'black', label = "Ratio Analytic Truth/MC yy")
plt.figure(figsize=(20,10))
# plt.margins(0)
# plt.legend()
fig_RatioRecoCheckyy.savefig('plots_MassDistribution/MCRecoyy_Ratio.png')

# fig_Sig_yy_Transform, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# plt.plot(massList_yy,SigFix_yy_MCTransformed, 'b', alpha=1, label=r"Signal Transformed MC yy")
# plt.plot(massList_yy,SigFix_yy_AnalyticalTransformed_TF, 'g', alpha=1, label=r"Signal Transformed yy TF Resoltuion")
# plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# # plt.xscale('log')
# fig_Sig_yy_Transform.savefig('plots_MassDistribution/SigyytransformedComparison.pdf')
# #RatioPlot
# fig_RatioTransformedCheckyy, ax = plt.subplots()
# MCTransformed_Ratioyy = SB.RatioMaker(SigFix_yy_AnalyticalTransformed_TF,SigFix_yy_MCTransformed)
# # plt.xscale('log')
# plt.ylabel('Ratio Analytic Transform/MC transform yy', ha='right', y=1.0)
# plt.ylim(top=1.2)
# plt.ylim(bottom=0.8)
# plt.plot(massList_yy,MCTransformed_Ratioyy, 'black', label = "Ratio Analytic Truth/MC yy")
# plt.figure(figsize=(20,10))
# # plt.margins(0)
# # plt.legend()
# fig_RatioTransformedCheckyy.savefig('plots_MassDistribution/MCTransformedyy_Ratio.png')



#Truth Comparisons

# SigFix_ee_AnalyticalTruth = SB.SigFix_Theory_Dielectron_TruthShape(k, M5, xmin_ee, xmax_ee, nBins_ee_01GeV, eventType)
# SigFix_ee_Truth = SB.MCTruth_Signal_Dielectron(xmin_ee, xmax_ee, nBins_ee_01GeV)


# #Fixed Signal vs MC for ee Truth
# fig_Sig_ee, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# plt.plot(massList_ee_01GeV,SigFix_ee_AnalyticalTruth, 'r', alpha=1, label=r"Signal Toy Truth")
# plt.plot(massList_ee_01GeV,SigFix_ee_Truth, 'b', alpha=1, label=r"Sig_ee MCTruth")
# # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# # plt.xscale('log')
# fig_Sig_ee.savefig('plots_MassDistribution/Sigee_Truthplot.pdf')
# #RatioPlot
# fig_RatioMCTruthCheckee, ax = plt.subplots()
# MCTruth_Ratioee = SB.RatioMaker(SigFix_ee_Truth,SigFix_ee_AnalyticalTruth)
# # plt.xscale('log')
# plt.ylabel('Ratio Analytic Truth/MC truth ee', ha='right', y=1.0)
# plt.plot(massList_ee_01GeV,MCTruth_Ratioee, 'black', label = "Ratio Analytic Truth/MC ee")
# plt.figure(figsize=(20,10))
# # plt.margins(0)
# # plt.legend()
# fig_RatioMCTruthCheckee.savefig('plots_MassDistribution/MCTruthee_Ratio.png')


# #Fixed Signal vs MC for ee Transformed
# fig_Sig_eeTransformed, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# plt.plot(massList_ee,SigFix_ee_MCTransformed, 'b', alpha=1, label=r"Sig_ee MCTransformed")
# plt.plot(massList_ee,SigFix_ee_AnalyticalTransformed, 'r', alpha=1, label=r"Signal Toy Transformed")
# # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# # plt.xscale('log')
# fig_Sig_eeTransformed.savefig('plots_MassDistribution/Sigee_Transformedplot.pdf')
# #RatioPlot
# fig_RatioTransformedCheck, ax = plt.subplots()
# MCTransformed_Ratio = SB.RatioMaker(SigFix_ee_AnalyticalTransformed,SigFix_ee_MCTransformed)
# # plt.xscale('log')
# plt.ylabel('Ratio Analytic Transform/MC transform ee', ha='right', y=1.0)
# plt.ylim(top=1.2)
# plt.ylim(bottom=0.8)
# plt.plot(massList_ee,MCTransformed_Ratio, 'black', label = "Ratio Analytic Trransform/MC ee")
# plt.figure(figsize=(20,10))
# plt.margins(0)
# # plt.legend()
# fig_RatioTransformedCheck.savefig('plots_MassDistribution/MCTransformed_Ratio.png')



# #Fixed Signal vs MC for ee Reco
# fig_Sig_eeReco, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# plt.plot(massList_ee,SigFix_ee_AnalyticalTransformed, 'r', alpha=1, label=r"Signal Toy Transformed")
# plt.plot(massList_ee,SigFix_ee_MCReco, 'b', alpha=1, label=r"Sig_ee MCReco")
# # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# # plt.xscale('log')
# fig_Sig_eeReco.savefig('plots_MassDistribution/Sigee_Recoplot.pdf')
#RatioPlot
# fig_RatioTransformedCheck, ax = plt.subplots()
# MCTransformed_Ratio = SB.RatioMaker(SigFix_ee_AnalyticalTransformed,SigFix_ee_MCReco)
# # plt.xscale('log')
# plt.ylabel('Ratio Analytic Transform/MC transform ee', ha='right', y=1.0)
# plt.ylim(top=1.2)
# plt.ylim(bottom=0.8)
# plt.plot(massList_ee,MCTransformed_Ratio, 'black', label = "Ratio Analytic Trransform/MC ee")
# plt.figure(figsize=(20,10))
# plt.margins(0)
# # plt.legend()
# fig_RatioTransformedCheck.savefig('plots_MassDistribution/MCTransformed_Ratio.png')











quit()

#Start generation of signal and Backgrounds
print("Starting generation...")

SigFix_ee, _ = SB.SigFix_Theory_Dielectron(k, M5, xmin_ee, xmax_ee, nBins_ee, eventType)
# SigFix_yy, _ = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin_yy, xmax_yy, nBins_yy, eventType)

#####Background#####
BkgFix_ee = SB.Dielectron_Bkg(xmin_ee, xmax_ee,	nBins_ee, eventType)
BkgFix_yy = SB.HighMassDiphoton_Bkg(xmin_yy, xmax_yy, nBins_yy, eventType)

# ######################################################################
# ################    Fixed Signal and Backgrounds      ################
# ######################################################################

SigBkgFix_ee = SB.SigBkgFixed(BkgFix_ee,SigFix_ee)
# SigBkgFix_yy = SB.SigBkgFixed(BkgFix_yy,SigFix_yy)

######################################################################
################  Poisson Signal and Backgrounds Toys ################
######################################################################

#Generate random Signal 
Sigrandom_ee = SB.SigBkgPoissonToy(BkgFix_ee, SigFix_ee, 0, 1)
#Random Signal Plotting
fig_SigRandom_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# plt.yscale('log')
# plt.xscale('log')
plt.plot(massList_ee,Sigrandom_ee, 'g',linewidth=2, label=r"ee Signal Toy")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-1)
plt.xlim(right=2700)
plt.margins(0)
plt.legend()
# ax.semilogy()
# ax.semilogx()
fig_SigRandom_ee.savefig('plots_MassDistribution/SigRandom_plotee.pdf')

#Generate random Signal 
# Sigrandom_yy = SB.SigBkgPoissonToy(BkgFix_yy, SigFix_yy, 0, 1)
# #Random Signal Plotting
# fig_SigRandom_yy, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# # plt.yscale('log')
# # plt.xscale('log')
# plt.plot(massList_yy,Sigrandom_yy, 'g',linewidth=2, label=r"yy Fluctuated Signal")
# plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.ylim(bottom=1e-1)
# plt.xlim(right=2700)
# plt.margins(0)
# plt.legend()
# # ax.semilogy()
# # ax.semilogx()
# fig_SigRandom_yy.savefig('plots_MassDistribution/SigRandom_plot_yy.pdf')

# #Generate random Background 
Bkgrandom_ee = SB.SigBkgPoissonToy(BkgFix_ee, SigFix_ee, 1, 0)
#Random Background Plotting
fig_BkgRandom_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_ee,Bkgrandom_ee, 'r',linewidth=2, label=r"ee Background Toy")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
# plt.ylim(top=4e3)
# plt.xlim(right=6e3)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_BkgRandom_ee.savefig('plots_MassDistribution/BkgRandom_plot_ee.pdf')

#Generate random Background 
Bkgrandom_yy = SB.SigBkgPoissonToy(BkgFix_yy, BkgFix_yy, 1, 0)
#Random Background Plotting
fig_BkgRandom_yy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_yy,Bkgrandom_yy, 'r',linewidth=2, label=r"yy Background Toy")
plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
# plt.ylim(top=4e3)
# plt.xlim(right=6e3)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_BkgRandom_yy.savefig('plots_MassDistribution/BkgRandom_plot_yy.pdf')

#Generate random signal + background
SigBkgrandom_ee = SB.SigBkgPoissonToy(BkgFix_ee, SigFix_ee, 1, 1)
#Random Background Plotting
fig_SigBkgRandom_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_ee,SigBkgrandom_ee, 'b',linewidth=2, label=r"ee Background + Signal Toy")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_SigBkgRandom_ee.savefig('plots_MassDistribution/SigBkgRandom_plot_ee.pdf')

# #Generate random signal + background
# SigBkgrandom_yy = SB.SigBkgPoissonToy(BkgFix_yy, SigFix_yy, 1, 1)
# #Random Background Plotting
# fig_SigBkgRandom_yy, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# plt.plot(massList_yy,SigBkgrandom_yy, 'b',linewidth=2, label=r"yy Fluctuated Background + Signal")
# plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.ylim(bottom=1e-3)
# plt.xlim(right=2700)
# plt.yscale('log')
# plt.xscale('log')
# plt.margins(0)
# plt.legend()
# fig_SigBkgRandom_yy.savefig('plots_MassDistribution/SigBkgRandom_plot_yy.pdf')


#Generate random Signal 
#Random Signal Plotting
fig_SigFix_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# plt.yscale('log')
# plt.xscale('log')
plt.plot(massList_ee,SigFix_ee, 'g',linewidth=2, label=r"ee Signal Toy")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-1)
plt.xlim(right=2700)
plt.margins(0)
plt.legend()
# ax.semilogy()
# ax.semilogx()
fig_SigFix_ee.savefig('plots_MassDistribution/SigRandom_plot_ee.pdf')

# # #Generate random Signal 
# # #Random Signal Plotting
# # fig_SigFix_yy, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# # # plt.yscale('log')
# # # plt.xscale('log')
# # plt.plot(massList_yy,SigFix_yy, 'g',linewidth=2, label=r"yy Fluctuated Analytical Signal ")
# # plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.ylim(bottom=1e-1)
# # plt.xlim(right=2700)
# # plt.margins(0)
# # plt.legend()
# # # ax.semilogy()
# # # ax.semilogx()
# # fig_SigFix_yy.savefig('plots_MassDistribution/SigRandom_plot_yy.pdf')



#Generate random Background 
#Random Background Plotting
fig_BkgFix_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_ee,BkgFix_ee, 'r',linewidth=4, label=r"ee Background")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
# plt.ylim(bottom=1e-2)
# plt.ylim(top=4e3)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_BkgFix_ee.savefig('plots_MassDistribution/BkgFix_ee.pdf')

# #Generate random Background 
# #Random Background Plotting
fig_BkgFix_yy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_yy,BkgFix_yy, 'r',linewidth=4, label=r"yy Background")
plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
# plt.ylim(bottom=1e-2)
# plt.ylim(top=4e3)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_BkgFix_yy.savefig('plots_MassDistribution/BkgFix_yy.pdf')


# #Generate random signal + background
SigBkgFix_ee =  SB.SigBkgFixed(BkgFix_ee, SigFix_ee)		
#Random Background Plotting
fig_SigBkgFix_ee, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(massList_ee,SigBkgFix_ee, 'b',linewidth=2, label=r"ee Background + Signal")
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
plt.ylim(bottom=1e-3)
plt.margins(0)
plt.xlim(right=2700)
plt.yscale('log')
plt.xscale('log')
plt.legend()
fig_SigBkgFix_ee.savefig('plots_MassDistribution/SigBkgFix_plot_ee.pdf')

# #Generate random signal + background
# SigBkgFix_yy =  SB.SigBkgFixed(BkgFix_yy, SigFix_yy)
# #Random Background Plotting
# fig_SigBkgFix_yy, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
# plt.plot(massList_yy,SigBkgFix_yy, 'b',linewidth=2, label=r"yy Background + Signal")
# plt.xlabel(r'$\mathrm{m_{yy} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.ylim(bottom=1e-3)
# plt.margins(0)
# plt.xlim(right=2700)
# plt.yscale('log')
# plt.xscale('log')
# plt.legend()
# fig_SigBkgFix_yy.savefig('plots_MassDistribution/SigBkgFix_plot_yy.pdf')

#Final report
print("Done.")

# #Generate fixed background from fit to data
# # BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins, "float")
# # SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, lum = SB.SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, "float", "All")


# SigFix_Toy_ee, _ = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins, "float")
# SigFix_Toy_ee_01GeV, _ = SB.SigFix_Theory_Dielectron_01GeV(k, M5, xmin, xmax, nBins, "float")


# # SigFix_Toy_yy, _ = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins,"float")



# # Fixed Signal vs MC for ee
# # fig_Sig_ee, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# # # massListR = numpy.delete(massList, -1)
# # # plt.plot(massList,SigFix_ee, 'b',linewidth=2, label=r"Signal_ee MC")
# # # plt.plot(massList,SigFix_Toy_ee, 'r',linewidth=2, label=r"Sig_ee Analytical")
# # plt.plot(massList_MCSignal,SigFix_ee_AnalyticalTruth, 'y', alpha=1, label=r"Signal Toy Truth")
# # plt.plot(massList_MCSignal,SigFix_ee_Truth, 'b', alpha=1, label=r"Sig_ee MCTruth")
# # # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# # plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.margins(0)
# # plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # # plt.yscale('log')
# # plt.xscale('log')
# # fig_Sig_ee.savefig('plots_MassDistribution/Sigee_Truthplot.pdf')

# # #Fixed Signal vs MC for ee
# # fig_Sig_yy, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# # # massListR = numpy.delete(massList, -1)
# # # plt.plot(massList,SigFix_ee, 'b',linewidth=2, label=r"Signal_ee MC")
# # # plt.plot(massList,SigFix_Toy_ee, 'r',linewidth=2, label=r"Sig_ee Analytical")
# # plt.plot(massList,SigFix_yy_Truth, 'b',linewidth=2, alpha=1, label=r"Sig_yy MCTruth")
# # plt.plot(massList,SigFix_yy_AnalyticalTruth, 'y',linewidth=2, alpha=1, label=r"Signal Toy Truth")
# # # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# # plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.margins(0)
# # plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # # plt.yscale('log')
# # plt.xscale('log')
# # fig_Sig_yy.savefig('plots_MassDistribution/Sigyy_Truthplot.pdf')


# # Fixed Signal vs MC for yy
# # fig_Sig_yy, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# # plt.plot(massList,SigFix_Toy_yy, 'r',linewidth=2, label=r"Signal_yy Analytical Shape")
# # plt.plot(massList,SigFix_yy, 'b',linewidth=2, alpha=0.5, label=r"Signal_yy MC")
# # # plt.plot(massList,RooFit, 'g',linewidth=2, label=r"Signal RooFit Toy")
# # # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# # plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.margins(0)
# # plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # # plt.yscale('log')
# # plt.xscale('log')
# # fig_Sig_yy.savefig('plots_MassDistribution/Sigyy_Transformed_plot.pdf')

# # Fixed Signal vs MC for ee
# # fig_Sig_ee, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# # plt.plot(massList,SigFix_ee_MCReco, 'b',linewidth=2, label=r"Reco MC")
# # plt.plot(massList,SigFix_Toy_ee, 'r',linewidth=2, label=r"Analytical ")
# # # plt.plot(massList,RooFit, 'g',linewidth=2, label=r"Signal RooFit Toy")
# # # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# # plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.margins(0)
# # plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # # plt.yscale('log')
# # plt.xscale('log')
# # fig_Sig_ee.savefig('plots_MassDistribution/Sigee_Reco_v_Analytical_plot.pdf')

# # #Fixed Signal vs MC for yy
# # fig_Sig_yy, ax = plt.subplots()
# # ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# # plt.plot(massList,SigFix_Toy_yy, 'r',linewidth=2, label=r"Signal_yy Analytical Shape")
# # # plt.plot(massList,RooFit, 'g',linewidth=2, label=r"Signal RooFit Toy")
# # # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# # plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# # plt.ylabel('Events', ha='right', y=1.0)
# # plt.margins(0)
# # plt.legend(loc='best')
# # plt.ylim(bottom=1e-4)
# # # plt.yscale('log')
# # plt.xscale('log')
# # fig_Sig_yy.savefig('plots_MassDistribution/Sigyy_plot.pdf')

# fig_Sig_ee_01GeV, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# plt.plot(massList_01GeV,SigFix_Toy_ee_01GeV, 'r',linewidth=2, label=r"Analytical sIGNAL")
# # plt.plot(massList,RooFit, 'g',linewidth=2, label=r"Signal RooFit Toy")
# # plt.plot(massList,BkgFix, 'g',linewidth=2, label=r"Background")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# plt.xscale('log')
# fig_Sig_ee_01GeV.savefig('plots_MassDistribution/Sig_ee_01GeV_plot.pdf')

# quit()

# n = 4
# colors = plt.cm.jet(numpy.linspace(0,1,n))

# fig_Sig_ee_Uncert, ax = plt.subplots()
# ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # plt.plot(massList,SigBkgPoissonToy, 'b',linewidth=2, label=r"Signal + Background")
# for i in range(0,n):
# 	Sig_Unc, _ = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, BkgFix, xmin, xmax,i,"WT")
# 	plt.plot(massList,Sig_Unc, color=colors[i],linewidth=2, label=r"Signal_ee MC")
# 	if i == n-1: 
# 		plt.plot(massList,SigFix, 'r',linewidth=2, label=r"Signal_ee Nominal Shape")
# 		plt.legend(loc='best')
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# plt.margins(0)
# # plt.legend(loc='best')
# plt.ylim(bottom=1e-4)
# # plt.yscale('log')
# plt.xscale('log')
# fig_Sig_ee_Uncert.savefig('plots_MassDistribution/Sigee_Uncert_plot.pdf')

# quit()


