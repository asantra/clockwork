########################################################################
#   Wavelets Generatorion and Distributions of Signal + Background   #
########################################################################

#Explanations
# - Generate a series of signals and Backgrounds from functions defined in

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy
import matplotlib
matplotlib.use("Agg")
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.ticker as plticker
from matplotlib.pyplot import cm
# from scipy.stats import binned_statistic
# #Import interpolation
# from scipy.interpolate import make_interp_spline, BSpline

import array
#Import custom
# import Merger, WT
import SigBkg_Plotting as SB

#import ATLAS MPL Style
import mplhep as hep
# plt.style.use(hep.style.ATLAS)
plt.style.use(hep.style.ROOT)

print("Files imported.")
print("")



#Settings
print("Reading settings...")

#Binning
Nbins = 5870 #2470
xmin = 130 #230
xmax = 6000 #2700

massList = numpy.linspace(xmin, xmax, Nbins)


#Parameters of the signal

k  = 500
M5 = 6000
#Signal Parameter strength
aS = 1

print("Setting read.")
print("")

#Start generation of signal and Backgrounds
print("Starting generation...")


######################################################################
################    Fixed Signal and Backgrounds      ################
######################################################################
#Generate fixed background from fit to data
BkgFix = SB.BkgFixed(xmin, xmax)

# #Generate fixed background from fit to MC
# BkgFix_MC = SB.BkgFixed_MC(xmin, xmax)


#Generate fixed signal 
SigFix = SB.SigFixed(k, M5, xmin, xmax)

# Sig_U = SB.SigUncertainty_Toy(k, M5, xmin, xmax, nTests)

#Generate Fixed Signal with Crystal Ball included

SigFix_withCB = SB.SigFixed_withCB(k, M5, xmin, xmax)
SigFix_withCB_RESUPVar = SB.SigFixed_withCB_UPvariation(k, M5, xmin, xmax)
SigFix_withCB_RESDOWNvar = SB.SigFixed_withCB_DOWNvariation(k, M5, xmin, xmax)

# # #Generate Fixed Signal with just tower of Gaussians with relative resoultion systematic UP and DOWN 

SigFixGauss_RESUPVar = SB.SigFixed_Resolution_SystematicUP(k, M5, xmin, xmax)
SigFixGuass_RESDOWNvar = SB.SigFixed_Resolution_SystematicDOWN(k, M5, xmin, xmax)

# # #Generate Fixed Signal with just tower of Gaussians with Iso_efficiency systematic UP and DOWN of the Ae function

SigFixed_Ae_Iso_efficiency_UPVar = SB.SigFixed_Ae_Iso_efficiency_UP(k, M5, xmin, xmax)
SigFixed_Ae_Iso_efficiency_DOWNVar = SB.SigFixed_Ae_Iso_efficiency_DOWN(k, M5, xmin, xmax)

# # #Generate Fixed Signal with just tower of Gaussians with ID_efficiency systematic UP and DOWN of the Ae function

SigFixed_Ae_ID_efficiency_UPVar = SB.SigFixed_Ae_ID_efficiency_UP(k, M5, xmin, xmax)
SigFixed_Ae_ID_efficiency_DOWNVar = SB.SigFixed_Ae_ID_efficiency_DOWN(k, M5, xmin, xmax)

# SigFixed_Truth2Reco = SB.SigFixed_Truth2Reco(k, M5, xmin, xmax)

# Fixed combined Signal + Background
SigBkgFix = SB.SigBkgFixed(BkgFix,SigFix_withCB)
SigBkgPoissonToy = SB.SigBkgPoissonToy(BkgFix,SigFix_withCB,1,1)

#Background Plotting for fixed Background
# fig_Bkg, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, BkgFix, 'red', lw=1, label=r"Background")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Events', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# plt.yscale('log')
# plt.xscale('log')
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# ax.semilogy()
# ax.semilogx()
# fig_Bkg.savefig('plots_MassDistribution/Bkg_plot.pdf')

# Toy to Background minus the nominal analytical shape
# Diff__UPVar = SB.BkgFitToyDifference(SigFix_withCB,SigFix_withCB_UPVar)
# Diff__DOWNVar = SB.BkgFitToyDifference(SigFix_withCB,SigFix_withCB_DOWNvar)
# fig_CBResDiff, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # plt.plot(massList,Diff__DOWNVar,linewidth=1, label=r"CB_Res_DOWNVar")
# plt.fill_between(massList,0,Diff__DOWNVar ,linewidth=30,label=r"CB_Res_DOWNVar", alpha=1, zorder=0)
# plt.fill_between(massList,0,Diff__UPVar ,linewidth=30,label=r"CB_Res_UPVar", alpha=1, zorder=1)
# # plt.plot(massList,Diff__UPVar,linewidth=1, label=r"CB_Res_UPVar")
# # plt.plot(massList,Diff__DOWNVar,linewidth=1, label=r"CB_Res_DOWNVar")
# plt.margins(0)
# plt.legend()
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events - Fit', ha='right', y=1.0)
# # plt.xscale('log')
# fig_CBResDiff.savefig('plots_MassDistribution/CBResDiff.png')



# fig_Signal, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# # for i in range(0,100):
# # 	# Sig_U = SB.SigUncertainty_Toy(k, M5, xmin, xmax)
# # 	Sig_U, Sig_D = SB.SigUncertainty_Toy(k, M5, xmin, xmax)
# # 	plt.plot(massList, Sig_U, 'red', lw=1)
# # 	plt.plot(massList, Sig_D, 'red', lw=1)
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# plt.plot(massList, SigFixed_Truth2Reco, 'blue', lw=1, label=r"Signal")
# plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# plt.ylabel('Events', ha='right', y=1.0)
# # leg = plt.legend(borderpad=0.4, frameon=False, loc=2)
# # plt.legend()
# # plt.margins(0)
# # plt.yscale('log')
# # plt.xscale('log')
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # plt.ylim(bottom=0)
# # plt.xlim(right=2700)
# # ax.semilogy()
# # ax.semilogx()
# fig_Signal.savefig('plots_MassDistribution/Signal_plot.pdf')


fig_SignalCB, ax = plt.subplots()

ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList, SigFix_withCB_RESDOWNvar, 'blue', lw=8, label=r"Signal_withCB_DOWNvar")
plt.plot(massList, SigFix_withCB, 'red', lw=8, label=r"Signal_withCB")
# ax.plot(massList, SigFix, 'green', lw=2, label=r"Signal")
plt.plot(massList, SigFix_withCB_RESUPVar, 'orange', lw=8, label=r"Signal_withCB_UPVar")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
# leg = plt.legend(borderpad=0.3, frameon=False, loc=2)
# plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
# hep.atlas.label()
# hep.atlas.label()
# ax.grid()
# plt.ylim(bottom=1e-1)
# plt.xlim(right=2700)
# ax.semilogy()
# ax.semilogx()
fig_SignalCB.savefig('plots_MassDistribution/SignalCB_plot.pdf')
# quit()


fig_SignalGauss, ax = plt.subplots()
SigFixGauss_RESUPVar
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList, SigFixGuass_RESDOWNvar, 'blue', lw=8, label=r"Signal_Gauss_RESDOWN")
plt.plot(massList, SigFix, 'red', lw=8, label=r"Signal_Guass")
# ax.plot(massList, SigFix, 'green', lw=2, label=r"Signal")
plt.plot(massList, SigFixGauss_RESUPVar, 'orange', lw=8, label=r"Signal_Gauss_RESUP")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Events', ha='right', y=1.0)
# leg = plt.legend(borderpad=0.1, frameon=False)
# plt.legend()
plt.margins(0)
# plt.yscale('log')
plt.xscale('log')
# hep.atlas.label()
# hep.atlas.label()
# ax.grid()
# plt.ylim(bottom=1e-1)
# plt.xlim(right=2700)
# ax.semilogy()
# ax.semilogx()
fig_SignalGauss.savefig('plots_MassDistribution/SignalGauss_plot.pdf')

fig_SignalCB_Ratio, ax = plt.subplots()
UPRatio = SB.RatioMaker(SigFix_withCB,SigFix_withCB_RESUPVar)
DOWNRatio = SB.RatioMaker(SigFix_withCB_RESDOWNvar, SigFix_withCB)
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList, DOWNRatio, 'blue', lw=3, label=r"CB_DOWNvar Ratio")
# plt.plot(massList, SigFix_withCB, 'red', lw=10, label=r"Signal_withCB")
# ax.plot(massList, SigFix, 'green', lw=2, label=r"Signal")
plt.plot(massList, UPRatio, 'orange', lw=3, label=r"CB_UPVar Ratio")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Ratio to Nominal', ha='right', y=1.0)
# leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
# plt.margins(0)
# plt.yscale('log')
# plt.xscale('log')
# hep.atlas.label()
# hep.atlas.label()
# ax.grid()
# plt.ylim(top=1e-1)
# plt.xlim(right=2700)
# ax.semilogy()
# ax.semilogx()
fig_SignalCB_Ratio.savefig('plots_MassDistribution/Ratio_SignalCB_plot.pdf')

fig_SignalGuass_Ratio, ax = plt.subplots()
UPRatio = SB.RatioMaker(SigFix,SigFixGauss_RESUPVar)
DOWNRatio = SB.RatioMaker(SigFixGuass_RESDOWNvar, SigFix)
ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
plt.plot(massList, DOWNRatio, 'blue', lw=3, label=r"Guass_DOWNvar Ratio")
# plt.plot(massList, SigFix_withCB, 'red', lw=10, label=r"Signal_withCB")
# ax.plot(massList, SigFix, 'green', lw=2, label=r"Signal")
plt.plot(massList, UPRatio, 'orange', lw=3, label=r"Guass_UPVar Ratio")
# ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel('Ratio to Nominal', ha='right', y=1.0)
leg = plt.legend(borderpad=0.5, frameon=False, loc=2)
plt.legend()
# plt.margins(0)
# plt.yscale('log')
# plt.xscale('log')
# hep.atlas.label()
# hep.atlas.label()
# ax.grid()
# plt.ylim(top=1e-1)
# plt.xlim(right=2700)
# ax.semilogy()
# ax.semilogx()
fig_SignalGuass_Ratio.savefig('plots_MassDistribution/Ratio_SignalGuass_plot.pdf')

# fig_Signal_Res, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, SigFix_DOWNvar, 'blue', lw=2, label=r"Signal_with_Res_DOWNvar")
# ax.plot(massList, SigFix, 'red', lw=2, label=r"Signal")
# ax.plot(massList, SigFix_UPVar, 'orange', lw=2, label=r"Signal_with_Res_UPVar")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Events', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# # plt.xscale('log')
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # plt.ylim(bottom=1e-1)
# # plt.xlim(right=2700)
# # ax.semilogy()
# # ax.semilogx()
# fig_Signal_Res.savefig('plots_MassDistribution/Signal_Gauss_Res_systs_plot.pdf')


# fig_Signal_Iso_efficiency, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, SigFixed_Ae_Iso_efficiency_DOWNVar, 'blue', lw=2, label=r"SigFixed_Ae_Iso_efficiency_DOWNVar")
# ax.plot(massList, SigFix, 'red', lw=2, label=r"Signal")
# ax.plot(massList, SigFixed_Ae_Iso_efficiency_UPVar, 'orange', lw=2, label=r"SigFixed_Ae_Iso_efficiency_UPVar")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Events', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# # plt.xscale('log')
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # plt.ylim(bottom=1e-1)
# plt.xlim(right=2700)
# # ax.semilogy()
# # ax.semilogx()
# fig_Signal_Iso_efficiency.savefig('plots_MassDistribution/Signal_Iso_efficiency_systs_plot.pdf')

# fig_Signal_ID_efficiency, ax = plt.subplots()

# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, SigFixed_Ae_ID_efficiency_DOWNVar, 'blue', lw=2, label=r"SigFixed_Ae_Iso_efficiency_DOWNVar")
# ax.plot(massList, SigFix, 'red', lw=2, label=r"Signal")
# ax.plot(massList, SigFixed_Ae_ID_efficiency_UPVar, 'orange', lw=2, label=r"SigFixed_Ae_Iso_efficiency_UPVar")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('Events', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# # plt.xscale('log')
# # hep.atlas.label()
# # hep.atlas.label()
# # ax.grid()
# # plt.ylim(bottom=1e-1)
# plt.xlim(right=2700)
# # ax.semilogy()
# # ax.semilogx()
# fig_Signal_ID_efficiency.savefig('plots_MassDistribution/Signal_ID_efficiency_systs_plot.pdf')

########################################################################
########################################################################
########################################################################


#Plot all the response function varables to comapre to the Resonant Internal Note

# fig_mu_CB, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, mu_CB_test, 'green', lw=2, label=r"mu_CB")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('mu_CB', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# # plt.ylim(bottom=-0.012)
# plt.xlim(right=6000)
# fig_mu_CB.savefig('plots_MassDistribution/mu_CB_plot.pdf')


# fig_sigma_CB, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, sigma_CB_test, 'green', lw=2, label=r"sigma_CB")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('sigma_CB', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# plt.ylim(bottom=0)
# plt.xlim(right=6000)
# fig_sigma_CB.savefig('plots_MassDistribution/sigma_CB_plot.pdf')

# fig_n_CB, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, n_CB_test, 'green', lw=2, label=r"n_CB")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('n_CB_test', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# plt.ylim(bottom=0)
# plt.xlim(right=6000)
# fig_n_CB.savefig('plots_MassDistribution/n_CB_plot.pdf')

# fig_mu_G, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, mu_G_test, 'green', lw=2, label=r"mu_G")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('mu_G', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# plt.ylim(bottom=-0.004)
# plt.xlim(right=6000)
# fig_mu_G.savefig('plots_MassDistribution/mu_G_plot.pdf')

# fig_sigma_G, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, sigma_G_test, 'green', lw=2, label=r"sigma_G")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('sigma_G', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# # plt.ylim(bottom=1e-1)
# plt.xlim(right=6000)
# fig_sigma_G.savefig('plots_MassDistribution/sigma_G_plot.pdf')

# fig_k_param, ax = plt.subplots()
# ax = hep.atlas.atlaslabel(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# ax.plot(massList, k_param_test, 'green', lw=2, label=r"k_param")
# # ax.fill_between(massList,BkgFix, facecolor='red', label="Background", alpha=0.7)
# ax.set_xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
# ax.set_ylabel('k_param', ha='right', y=1.0)
# leg = ax.legend(borderpad=0.5, frameon=False, loc=2)
# plt.legend()
# plt.margins(0)
# # plt.yscale('log')
# plt.xscale('log')
# plt.ylim(bottom=0)
# plt.xlim(right=6000)
# fig_k_param.savefig('plots_MassDistribution/k_param_plot.pdf')



#Final report
print("Done.")




