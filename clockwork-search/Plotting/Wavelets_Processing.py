########################################################################
#   Wavelets Generatorion and Distributions of Signal + Background   #
########################################################################

#Explanations
# - Generate a series of signals and Backgrounds from functions defined in

#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, sys, os
import matplotlib
matplotlib.use("Agg")
import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.ticker as plticker
from matplotlib.pyplot import cm
from scipy.stats import binned_statistic
#Import interpolation
from scipy.interpolate import make_interp_spline, BSpline

import array
#Import custom


#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

from Custom_Modules import Merger, WT
import SigBkg_Functions as SB

import mplhep as hep
hep.set_style(hep.style.ROOT)

print("Files imported.")
print("")



#Settings
print("Reading settings...")

#Binning
nBins = 1775 #2470
xmin = 225 #230
xmax = 2000 #2700

massList = numpy.linspace(xmin, xmax, nBins)
eventType = "float"

#Parameters of the signal

k  = 500
M5 = 6000
#Signal Parameter strength
aS = 1

print("Setting read.")
print("")

#Start generation of signal and Backgrounds
print("Starting generation...")

luminosity = 139
year = '2015-18'

SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins, eventType)
# SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins, eventType)

#####Background#####
BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins, eventType)
# BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins, eventType


######################################################################
################    Fixed Signal and Backgrounds      ################
######################################################################
#Generate fixed background from fit to data
# Fixed combined Signal + Background
SigBkgFix = SB.SigBkgFixed(BkgFix,SigFix)


######################################################################
################  Poisson Signal and Backgrounds Toys ################
######################################################################

#Generate random Signal 
Sigrandom = SB.SigBkgPoissonToy(BkgFix, SigFix, 0, 1)


#Generate random Background 
Bkgrandom = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)


#Generate random signal + background
SigBkgrandom = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)





######################################################################
##############  RooFit Toys, Differences to fixed  ###################
##############  Bkg, Averages for Number of Toys   ################### 
######################################################################

#Work is needed here on the averages, not the correct approach

# nToys_data = 1
# Toy = plt.figure()
# for i in range(0, nToys_data):
# 	Toy_SigBkg_RooFit = SB.ToyDataGenerator_withSignal(k, M5, xmin, xmax, "bkgsig", aS)
# 	Toy_FixedSig = SB.SigFixed(k, M5, xmin, xmax)
# 	Toy_FixedBkg = SB.BkgFixed(xmin, xmax)
# 	Toy_FixedBkg_Sig = SB.SigBkgFixed(BkgFix,SigFix)
# 	plt.yscale('log')
# 	plt.xscale('log')
	
# 	plt.plot(massList, Toy_SigBkg_RooFit, 'black',linewidth=1, label="$Toy_{s+b} RooFit$", alpha=1)
# 	plt.plot(massList, Toy_FixedSig, 'g',linewidth=2, label="Signal only", alpha=1)
# 	plt.plot(massList, Toy_FixedBkg_Sig, 'b',linewidth=1, label="Signal+Background", alpha=1)
# 	plt.plot(massList, Toy_FixedBkg, 'r',linewidth=1, label="Background", alpha=1)
# 	plt.ylim(bottom=1e-4)
# 	plt.ylim(top=3e5)
# 	plt.xlim(right=10e3)
# 	plt.xlim(left=130)
# 	plt.margins(0)
# 	plt.legend()
# plt.xlabel('$m_{ee}$ (GeV)')
# plt.ylabel('Events')
# Toy.savefig('plots_InputBkgSig/RooFitToysPlot_Data.pdf')
# # quit()
# #Do Toys with subtraction of Bkg and Sig+Bkg from Bkg fit

# nToys = 10
# Total_Multiple_SigBkg = []
# Total_Multiple_Bkgonly = []
# for i in range(0, nToys):
# 	# Diff_SigBkg = SB.BfitToyDiff(BkgFix,SBrandom)
# 	# Diff_Bkg = SB.BfitToyDiff(BkgFix,Brandom)
# 	# Total_Multiple_SigBkg.append(Diff_SigBkg)
# 	# Total_Multiple_Bkgonly.append(Diff_Bkg)
# 	Diff_SigBkg = SB.BkgFitToyDifference(BkgFix,RooFit_SigBkg)
# 	Diff_Bkg = SB.BkgFitToyDifference(BkgFix,RooFit_Bkg)
# 	Total_Multiple_SigBkg.append(Diff_SigBkg)
# 	Total_Multiple_Bkgonly.append(Diff_Bkg)
# Average_SigBkg = []
# Average_SigBkg = [numpy.array(x) for x in Total_Multiple_SigBkg]
# PostAverage_SigBkg = [numpy.mean(k) for k in zip(*Average_SigBkg)]

# Average_Bkgonly = []
# Average_Bkgonly = [numpy.array(x) for x in Total_Multiple_Bkgonly]
# PostAverage_Bkgonly = [numpy.mean(k) for k in zip(*Average_Bkgonly)]
# plt.clf()

######################################################################
#######################  Wavelet Transforms  #########################
######################################################################


#Do wavelet transform of the signal + background with statistical fluctuations
mergeX = 1
mergeY = 1

# Cone = True
Ncone = 2

yticks1 = [math.log(1), math.log(3), math.log(10), math.log(30), math.log(100), math.log(300), math.log(1000)]
yticks2 = (1, 3, 10, 30, 100, 300, 1000)
binwidth = (xmax - xmin)/nBins

# plt.gcf().subplots_adjust(left=0.135, bottom=0.14)
# plt.rcParams.update({'font.size': 14})


################ Wavelet Transforms of all Fixed/ Toy Sig, Bkg ################ 


#Do wavelet transform of the Fixed signal
fig_WaveletFixedSig, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr1, _, _ = WT.WaveletTransform(SigFix, mergeX, mergeY, False, Ncone)
pos = plt.imshow(numpy.flipud(numpy.transpose(cwtmatr1)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr1.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# plt.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
fig_WaveletFixedSig.savefig('plots_wavelets/SigFix.pdf')


#Do wavelet transform of the Fixed Background
fig_WaveletFixedBkg, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr2, _, _ = WT.WaveletTransform(BkgFix, mergeX, mergeY, False, Ncone)
pos = plt.imshow(numpy.flipud(numpy.transpose(cwtmatr2)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr2.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# fig_WaveletFixedBkg.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
fig_WaveletFixedBkg.savefig('plots_wavelets/BkgFix.pdf')

#Do wavelet transform of the Fixed Background Variation 

# cwtmatr3, _, _ = WT.WaveletTransform(Bkg_variationDOWN, mergeX, mergeY)
# plt.imshow(numpy.flipud(numpy.transpose(cwtmatr3)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr3.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
# plt.yticks(yticks1, yticks2)
# plt.xlabel('$m_{ee}$ [GeV]')
# plt.ylabel('$a$ [GeV]')
# axes = plt.gca()
# plt.savefig('plots_wavelets/Bkg_variationDOWN.png')
# plt.clf()


#Do wavelet transform of the Fixed Background and Signal
fig_WaveletFixedBkgSig, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr4, _, _ = WT.WaveletTransform(SigBkgFix, mergeX, mergeY, False, Ncone)
pos = plt.imshow(numpy.flipud(numpy.transpose(cwtmatr4)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr4.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# fig_WaveletFixedBkgSig.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
fig_WaveletFixedBkgSig.savefig('plots_wavelets/SigBkgFix.pdf')



# #Do wavelet transform of the random Background
fig_WaveletBkgToy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr5, _, _ = WT.WaveletTransform(Bkgrandom, mergeX, mergeY, False, Ncone)
pos =plt.imshow(numpy.flipud(numpy.transpose(cwtmatr5)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr5.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# fig_WaveletBkgToy.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
fig_WaveletBkgToy.savefig('plots_wavelets/Bkgrandom.pdf')


# #Do wavelet transform of the random Signal
fig_WaveletSigToy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr6, _, _ = WT.WaveletTransform(Sigrandom, mergeX, mergeY, False, Ncone)
pos = plt.imshow(numpy.flipud(numpy.transpose(cwtmatr6)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr6.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# fig_WaveletSigToy.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
fig_WaveletSigToy.savefig('plots_wavelets/Sigrandom.pdf')

# #Do wavelet transform of the random Signal + Background
Fig_WT_SBrandom, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
cwtmatr7, _, _ = WT.WaveletTransform(SigBkgrandom, mergeX, mergeY, False, Ncone)
plt.imshow(numpy.flipud(numpy.transpose(cwtmatr7)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatr7.shape[1]*mergeY/WT.Nbins))], cmap='bwr', aspect='auto', vmax=2.5, vmin=0, interpolation='gaussian')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.ylim(top=900)
# fig_WaveletBkgToy.colorbar(pos, ax=ax, fraction=0.046, pad=0.04)
axes = plt.gca()
Fig_WT_SBrandom.savefig('plots_wavelets/SigBkgrandom.pdf')


#Do many toys and avarage and do transforms for Background and Signal+Background

# Need to formualte averages properly here

#Final report
print("Done.")




