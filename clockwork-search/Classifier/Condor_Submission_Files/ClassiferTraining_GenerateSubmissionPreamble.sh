#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
pip3 install --user parton --upgrade
pip3 install --user mplhep --upgrade
python3 Condor_ClassiferTraining.py -k $1 -M5 $2 -s $3 -b $4 -test $5 -train $6 -kmin $7 -M5min $8 -Cone $9 -NconeR ${10} -uncertainty ${11}
