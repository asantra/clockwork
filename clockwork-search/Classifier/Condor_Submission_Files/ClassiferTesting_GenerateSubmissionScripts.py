# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, scipy, random, os, sys
from itertools import product

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainty ensemble for toy experiments')
argus = parser.parse_args()


#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/"

Sig = str(argus.s)
Bkg = str(argus.b)
kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)
test = int(argus.test)
bins = int(argus.bin)
train = int(argus.train)
Cone   = str(argus.Cone)
NconeR = int(argus.NconeR)
uncertainty = str(argus.uncertainty)

  # Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"Uncertainty-"+(argus.uncertainty)+""


# Open file and write common part
cfile = open('condor_testing.sub','w')
common_command = \
'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = ClassiferTesting_GenerateSubmissionPreamble.sh \n\
initialdir      = /scratch2/slawlor/Clockwork_Project/clockwork-search/ \n\
Should_transfer_files = YES \n\
+JobFlavour = "medium" \n\
when_to_transfer_output = ON_EXIT \n\
'
cfile.write(common_command)
 
# Loop over various values of an argument and create different output file for each
# Then put it in the queue
K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)
# K_List = numpy.linspace(250,5500,30)
# M5_List = numpy.linspace(1000, 20000, 25)
for i, (k,m) in enumerate(product(K_List,M5_List)):
    run_command =  \
'arguments  = %d %d %s %s %d %d %d %d %d %s %d %s\n\
error                 = Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Error_Condor/testing_%d_%d_Clockwork$(Process).err \n\
log                   = Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Logs_Condor/testing_%d_%d_Clockwork$(Process).log \n\
transfer_input_files  = Classifier/Condor_ClassiferTesting.py,Classifier/Condor_Submission_Files/ClassiferTesting_GenerateSubmissionPreamble.sh , SignalBackground_Generation/Custom_Modules, SignalBackground_Generation/SigBkg_Functions.py,SignalBackground_Generation/Inputs_ee,SignalBackground_Generation/Inputs_yy,Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/ClassiferWeights_Condor, Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Error_Condor, Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Logs_Condor, Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Pvalues_Condor, Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Out_Condor, Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/TestStatistic_plots, SignalBackground_Generation/Graviton_Properties \n\
output     = Classifier/Classifier_Condor_Outputs/Outputs_%s_%s_test%d_train%d_Cone-%s_NconeR%d_Uncertainty-%s/Out_Condor/ClassifierTesting_out.%d_%d.txt\n\
queue 1\n\n' %(k,m,Sig,Bkg,test,bins,train,kmin,M5min,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,k,m,Bkg,Sig,test,train,Cone,NconeR,uncertainty,k,m,Bkg,Sig,test,train,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,Bkg,Sig,test,train,Cone,NconeR,uncertainty,k,m) 
    cfile.write(run_command)
