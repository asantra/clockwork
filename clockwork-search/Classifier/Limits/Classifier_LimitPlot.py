#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)
#Import custom
# sys.path.insert(0, '/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Classifier/Custom_Modules')
# from Custom_Modules import WT
# import SigBkg_Classifier as SB
import mplhep as hep
hep.set_style(hep.style.ROOT)

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
# parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
# parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
argus = parser.parse_args()


kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)


Output_Folder = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone"+(argus.Cone)+""


K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)


N, M = len(K_List), len(M5_List)
Z = numpy.zeros((N, M))
Z_Plus1Sigma = numpy.zeros((N, M))
Z_Minus1Sigma = numpy.zeros((N, M))
Z_Unc = numpy.zeros((N, M))
K_List_2D, M5_List_2D = numpy.meshgrid(M5_List, K_List)
for i, (x,y) in enumerate(product(K_List,M5_List)):
    Z[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2_Exclusion"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt", skiprows = 1)
    # Z_Plus1Sigma[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method1_Exclusion_plus_1sigma"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")
    # Z_Minus1Sigma[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method1_Exclusion_minus_1sigma"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt",)
    Z_Unc[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/"+Output_Folder+"/Pvalues_Condor/Pvalue_Method2_Exclusion_Uncertainty"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt", skiprows = 1)


# #Make Contour plots of Pvalues
# fig_pvalue_contour,ax=plt.subplots(1,1)
# cp_Unc = ax.contourf(K_List_2D, M5_List_2D, Z_Unc, levels = (0.049,0.051))
# # c_Plus1Sigma = ax.contourf(K_List_2D, M5_List_2D, Z_Plus1Sigma, levels = (0.049,0.051))
# # c_Minus1Sigma = ax.contourf(K_List_2D, M5_List_2D, Z_Minus1Sigma, levels = (0.049,0.051))
# c_alpha = ax.contourf(K_List_2D, M5_List_2D, Z, levels = (0.049,0.051))
# ax.set_xlabel('M5 [GeV]', ha='right', x=1.0)
# ax.set_ylabel('k [GeV]', ha='right', y=1.0)
# ax.semilogy()
# plt.xlim((1000,15000))
# fig_pvalue_contour.colorbar(c_alpha) # Add a colorbar to a plot
# fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap.png')

fig_pvalue_contour,ax =plt.subplots(1,1)
ax = hep.atlas.label(data=False, paper=False,year='2015-2018', lumi=140 , fontsize=19, ax=ax)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
c_alpha = plt.contour(K_List_2D, M5_List_2D, Z, levels = (0.0445,0.0455), linestyles = 'dashed', colors = ['r'])
cp_Unc = plt.contour(K_List_2D, M5_List_2D, Z_Unc, levels = (0.0445,0.0455), linestyles = 'dashed' , colors = ['blue'])
#Theory contours
# c_alpha_Cl_Theory = plt.contour(xi_Classifier_Theory , yi_Classifier_Theory , zi_Classifier_Theory , levels = (0.0445,0.0455), colors = ['r'], alpha=0.2)
# c_alpha_At_Theory = plt.contour(xi_Autoencoder_Theory , yi_Autoencoder_Theory , zi_Autoencoder_Theory , levels = (0.0445,0.0455), colors = ['blue'], alpha=0.2)
# c_alpha_FT_Theory = plt.contour(xi_FT_Theory , yi_FT_Theory , zi_FT_Theory , levels = (0.0445,0.0455), colors = ['black'], alpha=0.2)
#Additional points and lines
# Classifier_FulRun2Point = plt.scatter(7500,510, color='green',marker='s',linewidths=8)
# Classifier_FulRun2Point_Unc = plt.scatter(7300,490, color='brown',marker='8',linewidths=8)
# CMSresult = plt.plot(CMS_M5,CMS_K,color='purple', linewidth=5)
h1,_ = c_alpha.legend_elements()
h2,_ = cp_Unc.legend_elements()
# h4,_ = c_alpha_Cl_Theory.legend_elements()
# h5,_ = c_alpha_At_Theory.legend_elements()
# h6,_ = c_alpha_FT_Theory.legend_elements()
plt.xlabel('M5 [GeV]', ha='right', x=1.0)
plt.ylabel('k [GeV]', ha='right', y=1.0)
plt.semilogy()
plt.xlim((1000,9000))
plt.ylim((200,2000))
plt.setp(c_alpha.collections , linewidth=3)
plt.setp(cp_Unc.collections , linewidth=3)
plt.legend()
leg = plt.legend([h1[0], h2[0]], ['Classifier', 'Classifier_Unc'],loc = 'lower left')
for line in leg.get_lines():
    line.set_linewidth(3.0)
    line.set_alpha(1)
# fig_pvalue_contour_yy.colorbar(c_alpha_yy) # Add a colorbar to a plot,Patch(facecolor='r'), alpha=0.2
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap_Method_New.pdf')


