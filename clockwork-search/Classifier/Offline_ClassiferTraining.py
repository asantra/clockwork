#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import time
start0 = time.process_time()
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))
from Custom_Modules import WT
import SigBkg_Functions as SB

#import ATLAS MPL Style
import mplhep as hep
hep.set_style(hep.style.ROOT)

from itertools import product


#Machine learning
import keras, sklearn
import tensorflow as tf
from keras.callbacks         import EarlyStopping
from keras.constraints import unit_norm
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D
from keras.layers.merge      import add
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras                   import backend as K
#Options for overfitting(If needed)
# from keras.callbacks import EarlyStopping


import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')

argus = parser.parse_args()

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775 #2935
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2

massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = bool(argus.Cone)
NconeR = int(argus.NconeR) #2

print("Files imported.")
print("")


#Parameters of the signal 
M5 = float(argus.M5)
k = float(argus.k)


print("Mass_5")
print(M5)
print("K")
print (k)


#Generate signal and Background
#####Signal#####
if(argus.s=="Tail_Damped_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax)

if(argus.s=="Theory_Diphoton_36fb_Sig"):
  luminosity = 36.1
  year = '2015-16'
  SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins)

if(argus.s=="Theory_Dielectron_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)

if(argus.s=="Theory_HighMass_Diphoton_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins)

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

if(argus.b=="Diphoton_36_Bkg"):
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

print("Signal/Bkg evaluated.")
print("")

#Lets name a folder based on the signal and bkg to write all the outputs too and make it 
try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""
  os.mkdir("Classifier_Offline_Outputs/"+Output_Folder)
  os.mkdir("Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Offline")
  os.mkdir("Classifier_Offline_Outputs/"+Output_Folder+"/History_Plots")
  os.mkdir("Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues")
  os.mkdir("Classifier_Offline_Outputs/"+Output_Folder+"/TestStatistic_plots")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)





#File to save weights and biases
nameModelFile = Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Offline/Classifier_NNweights_biases_"+str(k)+"_"+str(M5)+".txt"
#Settings
print("Reading settings...")


#Simulation settings
nTraining = int(argus.train) #4000

nTrials1 = int(argus.test)

#Number of threads to use
NumberThreads = 8 #8

print("Setting read")
print("")



#Training neural network
print("Beginning generation of training events...")
print("")

#Generate events for training and testing
X_train = []
Y_train = []

for i in range(0, nTraining):

  #Generate random parameters for signal
  aStemp = random.randint(0, 1)

  #Generate random binned events
  eventsInt = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, aStemp)
  #Do wavelet transform
  cwtmatBSIntNorm, _, _ = WT.WaveletTransform(eventsInt, mergeX, mergeY, Cone, NconeR)

  #Append results
  X_train.append(cwtmatBSIntNorm)
  Y_train.append([aStemp])

  #Make report
  if i%100 == 0:
    print(i)

#Convert to numpy array
X_train = numpy.array(X_train)
Y_train = numpy.array(Y_train)

#Dimension info
dimWX = (X_train.shape[1], X_train.shape[2])
dimWY = (Y_train.shape[1])

#Format
X_train = numpy.ravel(X_train)
Y_train = numpy.ravel(Y_train)

X_train = numpy.reshape(X_train, (nTraining, dimWX[0], dimWX[1], 1))
Y_train = numpy.reshape(Y_train, (nTraining, dimWY))

#Save Xtrain shape to use in testing, save to weights folder, don't need to do this for every M5/k the shape depends mainly on the training steps do just call on M5/k to reduce ememory use. Pick the lowest k/M5
if k==int(argus.kmin) and M5==int(argus.M5min):
  numpy.save(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Offline/Classifier_Xtrain_"+(str(argus.kmin))+".0_"+(str(argus.M5min))+".0.npy", X_train)

print('Generation of training events completed.')
print("")

#Define model for classifier
config = tf.compat.v1.ConfigProto()
jit_level = tf.compat.v1.OptimizerOptions.ON_1
config.graph_options.optimizer_options.global_jit_level = jit_level
config.intra_op_parallelism_threads = NumberThreads
config.inter_op_parallelism_threads = NumberThreads
sess = tf.compat.v1.Session(config=config)


model1 = Sequential()

#Add model layers
model1.add(Conv2D(4,  kernel_size=3, activation='elu', input_shape=(X_train.shape[1], X_train.shape[2], 1)))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(8,  kernel_size=3, activation='sigmoid'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(16, kernel_size=3, activation='sigmoid'))
model1.add(Flatten())
model1.add(Dense(200, activation='sigmoid')) #kernel_constraint=unit_norm()
model1.add(Dense(100, activation='sigmoid')) #kernel_constraint=unit_norm()
model1.add(Dense(Y_train.shape[1], activation='sigmoid'))

# simple early stopping
# es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=200)

#Compile
model1.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

#Checkpoint
checkpoint = keras.callbacks.ModelCheckpoint(nameModelFile, verbose=1, monitor='val_loss', save_best_only=True, mode='auto') 

#Train model #epoch defualt 500, batch size 1000, shuffle=True????
train_history = model1.fit(X_train, Y_train, batch_size=1000, epochs=500, validation_split=0.2, callbacks=[checkpoint]) #epochs = 500, default callback is checkpoint, testing early stopping with callbacks=[es,checkpoint]

fig_history, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(train_history.history['loss'],     label = 'loss')
plt.plot(train_history.history['val_loss'], label = 'val loss')
plt.legend(loc=0)
plt.ylabel('loss')
plt.xlabel('Epochs')
plt.margins(0)
plt.legend(['train', 'val'], loc='best')
fig_history.savefig(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/History_Plots/history_modelloss_"+str(k)+"_"+str(M5)+".png")

# summarize history for accuracy

fig_accuracy, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.plot(train_history.history['acc'])
plt.plot(train_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='best')
fig_accuracy.savefig(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/History_Plots/history_accuracy_"+str(k)+"_"+str(M5)+".png")


model1.save(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Offline/model_"+str(k)+"_"+str(M5)+".h5") 
print('Complete')
print("time taken for whole file:")
print(time.process_time() - start0)

