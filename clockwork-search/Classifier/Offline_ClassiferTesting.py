#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import time
start0 = time.process_time()
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/"

#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))
from Custom_Modules import WT, Merger
import SigBkg_Functions as SB
#import ATLAS MPL Style
import mplhep as hep
hep.set_style(hep.style.ROOT)


from itertools import product

#Machine learning
import keras, sklearn
import tensorflow as tf
from keras.callbacks         import EarlyStopping
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D
from keras.layers.merge      import add
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras                   import backend as K


# import mplhep as hep
# plt.style.use(hep.style.ROOT)

print("Files imported.")
print("")



#Settings
print("Reading settings...")

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')
argus = parser.parse_args()


#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 1780
  nBins   = 1555
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 1275
  mergeX  = 20 #20
  mergeY  = 2 #2
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2



massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = bool(argus.Cone)
NconeR = int(argus.NconeR) #2

#Define Test Statitic Binning
bins = int(argus.bin)


#Read in k/M5

M5 = float(argus.M5)
k = float(argus.k)


print("Mass_5")
print(M5)
print("K")
print (k)



#Empty Test statitic lists
Lambda_BS = []
Lambda_BS_Uncertainty = []
Lambda_Bonly = []
Lambda_Bonly_Uncertainty = []

#Simulation settings
nTrials1  = int(argus.test)
nTrials2  = int(argus.test)



#Generate signal and Background
#####Signal#####
if(argus.s=="Tail_Damped_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix = SB.SigFixed_ee_withCB(k, M5, xmin, xmax)

if(argus.s=="Theory_Diphoton_36fb_Sig"):
  luminosity = 36.1
  year = '2015-16'
  SigFix, lum = SB.SigFix_Theory36fb_Diphoton(k, M5, xmin, xmax, nBins)

if(argus.s=="Theory_Dielectron_Sig"):
  luminosity = 139
  year = '2015-18'
  if(argus.uncertainty=="no"):
    SigFix, lum = SB.SigFix_Theory_Dielectron(k, M5, xmin, xmax, nBins)
  if(argus.uncertainty=="yes"):
    SigFix, SigFix_EnergyResolution_DOWN, SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, lum = SB.SigFix_Theory_Dielectron_Uncertainties(k, M5, xmin, xmax, nBins, "All")

if(argus.s=="Theory_HighMass_Diphoton_Sig"):
  luminosity = 139
  year = '2015-18'
  SigFix, lum = SB.SigFix_Theory_HighMass_Diphoton(k, M5, xmin, xmax, nBins)

#####Background#####
if(argus.b=="Dielectron_Bkg"):
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

if(argus.b=="Diphoton_36fb_Bkg"):
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)

print("Signal/Bkg evaluated.")
print("")

#Number of threads to use
NumberThreads = 8 #8

print("Setting read")
print("")



#Lets load a folder based on the signal and bkg used in the terminal to write all the outputs too , should match the training
try:
Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""
#Make empty lists to appen Pvalues, and 1 Sigma,2 sigma bands
#P_values evalauted at their median
P_Value_List_Method2_Exclusion = []
P_Value_List_Method2_Discovery = []
P_Value_List_Method2_Exclusion_Uncertainty = []
P_Value_List_Method2_Discovery_Uncertainty = []

#P_values list at the 1,2 sigma bands for exclusion 
P_Value_List_plus_1sigma_Exclusion = []
P_Value_List_minus_1sigma_Exclusion = []
P_Value_List_plus_1sigma_Exclusion_Uncertainty = []
P_Value_List_minus_1sigma_Exclusion_Uncertainty = []
#P_values list at the 1,2 sigma bands for discovery 
P_Value_List_plus_1sigma_Discovery = []
P_Value_List_minus_1sigma_Discovery = []
P_Value_List_plus_1sigma_Discovery_Uncertainty = []
P_Value_List_minus_1sigma_Discovery_Uncertainty = []

#Import Models and make text files to store all Pvalues recorded 
nameModelFile = Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Condor/Classifier_NNweights_biases_"+str(k)+"_"+str(M5)+".txt"
ModelFile = Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Condor/model_"+str(k)+"_"+str(M5)+".h5"
Pvalue_out_Method2_Exclusion = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method2_Exclusion"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method2_Discovery"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Discovery_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method2_Discovery_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Exclusion_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method2_Exclusion_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


Pvalue_out_Method2_Exclusion_plus_1sigma = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_plus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_plus_1sigma = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_plus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Exclusion_plus_1sigma_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_plus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_plus_1sigma_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_plus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


Pvalue_out_Method2_Exclusion_minus_1sigma = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_minus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_minus_1sigma = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_minus_1sigma"+str(k)+"_"+str(M5)+"out.txt","w")

Pvalue_out_Method2_Exclusion_minus_1sigma_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Exclusion_minus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")
Pvalue_out_Method2_Discovery_minus_1sigma_Uncertainty = open(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/Pvalues/Pvalue_Method1_Discovery_minus_1sigma_Uncertainty"+str(k)+"_"+str(M5)+"out.txt","w")


# Load X_train- set this to one of the X train shapes manually
X_train = numpy.load(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/ClassiferWeights_Offline/Classifier_Xtrain_"+(str(argus.kmin))+".0_"+(str(argus.M5min))+".0.npy")
#Load best weights
model1 = tf.keras.models.load_model(ModelFile)
model1.compile(optimizer='adam', loss='binary_crossentropy',  metrics=['accuracy'])

#Generate trial background + signal
#Initialize variables
print("Beginning toy experiments of background + signal")
print("")


start = time.process_time()
for i in range(0, nTrials1):

  if (argus.uncertainty=="no"):
    
    #Generate random binned events
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)

    #Do wavelet transform
    cwtmatBTrial1Norm, _, _ = WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
    cwtmatBTrial1Flat      = numpy.ravel(cwtmatBTrial1Norm)
    cwtmatBTrial1Formatted = numpy.reshape(cwtmatBTrial1Flat,(1, X_train.shape[1], X_train.shape[2], 1))
    #Make prediction
    predTrial1 = model1.predict(cwtmatBTrial1Formatted)[0]
    Lambda_BS.append(predTrial1[0])
    # Lambda_BS.append(math.log(predTrial1[0]/(1-predTrial1[0])))
    print(Lambda_BS)

  if (argus.uncertainty=="yes"):

    Sig_Unc = SB.Sig_ee_Uncertainty_Toy(SigFix,SigFix_EnergyResolution_DOWN,SigFix_EnergyResolution_UP, SigFix_EnergyScale_DOWN, SigFix_EnergyScale_UP, BkgFix, xmin, xmax,i,"WT")
    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i, eventType)


   #Generate random binned events
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 1)
    eventsTrial1_Uncertainity = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, Sig_Unc, 1, 1)

    #Do wavelet transform for nominal shapes and profiled uncertainties
    cwtmatBTrial1Norm, _, _ = WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
    cwtmatBTrial1Norm_Uncertainity, _, _ = WT.WaveletTransform(eventsTrial1_Uncertainity, mergeX, mergeY, Cone, NconeR)

    cwtmatBTrial1Flat      = numpy.ravel(cwtmatBTrial1Norm)
    cwtmatBTrial1Flat_Uncertainity      = numpy.ravel(cwtmatBTrial1Norm_Uncertainity)

    cwtmatBTrial1Formatted = numpy.reshape(cwtmatBTrial1Flat,(1, X_train.shape[1], X_train.shape[2], 1))
    cwtmatBTrial1Formatted_Uncertainity = numpy.reshape(cwtmatBTrial1Flat_Uncertainity,(1, X_train.shape[1], X_train.shape[2], 1))

    #Make prediction
    predTrial1 = model1.predict(cwtmatBTrial1Formatted)[0]
    predTrial1_Uncertainity = model1.predict(cwtmatBTrial1Formatted_Uncertainity)[0]

    Lambda_BS.append(predTrial1[0])
    Lambda_BS_Uncertainty.append(predTrial1_Uncertainity[0])
    # Lambda_BS.append(math.log(predTrial1[0]/(1-predTrial1[0])))
    # print(Lambda_BS)
 


print("time taken B+S whole loop:")
print(time.process_time() - start)

print("Toy experiments of background + signal done.")
print("")



#Generate trial background + signal
#Initialize variables
print("Beginning toy experiments of background only")
print("")

start2 = time.process_time()
for i in range(0, nTrials2):

  if (argus.uncertainty=="no"):
    
    #Generate random binned events
     eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)

    #Do wavelet transform
    cwtmatBTrial2Norm, _, _ = WT.WaveletTransform(eventsTrial2, mergeX, mergeY, Cone, NconeR)
    #Format for predictions
    cwtmatBTrial2Flat      = numpy.ravel(cwtmatBTrial2Norm)
    cwtmatBTrial2Formatted = numpy.reshape(cwtmatBTrial2Flat,(1, X_train.shape[1], X_train.shape[2], 1))
    predTrial2 = model1.predict(cwtmatBTrial2Formatted)[0]
    Lambda_Bonly.append(predTrial2[0])
    # Lambda_Bonly.append(math.log(predTrial2[0]/(1-predTrial2[0])))
    print(Lambda_Bonly)

  if (argus.uncertainty=="yes"):

    BkgFix_UncertianitesAll  = SB.BkgFixed_Dielectron_AllUncertainties_ToysThrown_VarAll(xmin, xmax, nBins, i, eventType)

    #Generate random binned events
    eventsTrial2 = SB.SigBkgPoissonToy(BkgFix, SigFix, 1, 0)
    eventsTrial2_Uncertainty = SB.SigBkgPoissonToy(BkgFix_UncertianitesAll, SigFix, 1, 0)

    #Do wavelet transform
    cwtmatBTrial2Norm, _, _ = WT.WaveletTransform(eventsTrial2, mergeX, mergeY, Cone, NconeR)
    cwtmatBTrial2Norm_Uncertainty, _, _ = WT.WaveletTransform(eventsTrial2_Uncertainty, mergeX, mergeY, Cone, NconeR)

    #Format for predictions
    cwtmatBTrial2Flat      = numpy.ravel(cwtmatBTrial2Norm)
    cwtmatBTrial2Flat_Uncertainty      = numpy.ravel(cwtmatBTrial2Norm_Uncertainty)
    cwtmatBTrial2Formatted = numpy.reshape(cwtmatBTrial2Flat,(1, X_train.shape[1], X_train.shape[2], 1))
    cwtmatBTrial2Formatted_Uncertainty = numpy.reshape(cwtmatBTrial2Flat_Uncertainty,(1, X_train.shape[1], X_train.shape[2], 1))

    predTrial2 = model1.predict(cwtmatBTrial2Formatted)[0]
    predTrial2_Uncertainty = model1.predict(cwtmatBTrial2Formatted_Uncertainty)[0]
    Lambda_Bonly.append(predTrial2[0])
    Lambda_Bonly_Uncertainty.append(predTrial2_Uncertainty[0])
    # Lambda_Bonly.append(math.log(predTrial2[0]/(1-predTrial2[0])))
    print(Lambda_Bonly)
   
 


print("time taken B whole loop:")
print(time.process_time() - start2)


print("Toy experiments of background only done.")
print("")

################################################################################################
################################################################################################
################################################################################################

#Method2 - Sean/Glen's Method for Median limit setting

if (argus.uncertainty=="no"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(Lambda_Bonly)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Lambda_Bonly)))]

  #Try and caluclate upper and lower 1 sigma values for Exclusion
  B_Only_LowerQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.159)
  B_Only_UpperQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.841)

  #Discovery Median
  BS_SortedList     = numpy.sort(Lambda_BS)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(Lambda_BS)))]

  #Try and caluclate upper and lower 1 sigma values for Discovery Calculate median, and 1 s test statistic using 0.159, 0.5, 0.841 quantiles.
  BS_LowerQuantile_1sigma = numpy.quantile(Lambda_BS, 0.159)
  BS_UpperQuantile_1sigma = numpy.quantile(Lambda_BS, 0.841)

  #Make histograms
  Lambda_BS_hist = numpy.histogram(Lambda_BS, bins)
  Lambda_Bonly_hist = numpy.histogram(Lambda_Bonly, bins)
  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)

  #Try and caluclate P-value for Exclusion median and write to list
  P_Value_Exclusion = (Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
  P_Value_List_Method2_Exclusion.append(P_Value_Exclusion)

  #Work out P-values for Exclusion at different sigma positions and write to list to save at the end of file
  P_Value_plus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_LowerQuantile_1sigma))

  P_Value_List_plus_1sigma_Exclusion.append(P_Value_plus_1sigma_Exclusion)
  P_Value_List_minus_1sigma_Exclusion.append(P_Value_minus_1sigma_Exclusion)


  #Try and caluclate P-value for Discovery and write to list
  P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
  P_Value_List_Method2_Discovery.append(P_Value_Discovery)

  print("Method2: Min Exclusion value is:", min(P_Value_List_Method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(P_Value_List_Method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(P_Value_List_Method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(P_Value_List_Method2_Discovery,default=0))

  #Work out P-values for Discovery at different sigma positions and write to luist to save at the end of file
  P_Value_plus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_LowerQuantile_1sigma))
  P_Value_List_plus_1sigma_Discovery.append(P_Value_plus_1sigma_Discovery)
  P_Value_List_minus_1sigma_Discovery.append(P_Value_minus_1sigma_Discovery)
  #Wrtite all Pvalues,Sigma bands to seperate files to avoid errors to beiung hidden withihn one file
  #Pvalue at the median

  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")
  #Pvalue at the Upper 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma, P_Value_List_plus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma, P_Value_List_plus_1sigma_Discovery, fmt="%1.10f")
  #Pvalue at the Lower 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma, P_Value_List_minus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma, P_Value_List_minus_1sigma_Discovery, fmt="%1.10f")



  #Test Statistic Plotting

  Probably_Dist, ax = plt.subplots()
  ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
  plt.hist(Lambda_Bonly, bins,label="Bkg Only", alpha=0.4)
  plt.hist(Lambda_BS, bins, label="Bkg + Signal" , alpha=0.4)
  plt.ticklabel_format(style = 'plain')
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
  # ax.set_ylabel('', ha='right', y=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc='best')
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/TestStatistic_plots/TestDistribution_"+str(k)+"_"+str(M5)+".png")

if (argus.uncertainty=="yes"):

  #Exclusion Median
  B_Only_SortedList     = numpy.sort(Lambda_Bonly)
  B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Lambda_Bonly)))]
  B_Only_SortedList_Uncertainty     = numpy.sort(Lambda_Bonly_Uncertainty)
  B_OnlyMedian_Uncertainty = B_Only_SortedList_Uncertainty[int(numpy.floor(0.5*len(Lambda_Bonly_Uncertainty)))]
  #Try and caluclate upper and lower 1 sigma values for Exclusion
  B_Only_LowerQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.159)
  B_Only_UpperQuantile_1sigma = numpy.quantile(Lambda_Bonly, 0.841)
  B_Only_LowerQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_Bonly_Uncertainty, 0.159)
  B_Only_UpperQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_Bonly_Uncertainty, 0.841)

  #Discovery Median
  BS_SortedList     = numpy.sort(Lambda_BS)
  BS_Median = BS_SortedList[int(numpy.floor(0.5*len(Lambda_BS)))]
  BS_SortedList_Uncertainty     = numpy.sort(Lambda_BS_Uncertainty)
  BS_Median_Uncertainty = BS_SortedList_Uncertainty[int(numpy.floor(0.5*len(Lambda_BS_Uncertainty)))]

  #Try and caluclate upper and lower 1 sigma values for Discovery Calculate median, and 1 s test statistic using 0.159, 0.5, 0.841 quantiles.
  BS_LowerQuantile_1sigma = numpy.quantile(Lambda_BS, 0.159)
  BS_UpperQuantile_1sigma = numpy.quantile(Lambda_BS, 0.841)
  BS_LowerQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_BS_Uncertainty, 0.159)
  BS_UpperQuantile_1sigma_Uncertainty = numpy.quantile(Lambda_BS_Uncertainty, 0.841)

  #Make histograms
  Lambda_BS_hist = numpy.histogram(Lambda_BS, bins)
  Lambda_BS_hist_Uncertainty = numpy.histogram(Lambda_BS_Uncertainty, bins)
  Lambda_BS_distribution_rv = scipy.stats.rv_histogram(Lambda_BS_hist)
  Lambda_BS_distribution_rv_Uncertainty = scipy.stats.rv_histogram(Lambda_BS_hist_Uncertainty)

  Lambda_Bonly_hist = numpy.histogram(Lambda_Bonly, bins)
  Lambda_Bonly_hist_Uncertainty = numpy.histogram(Lambda_Bonly_Uncertainty, bins)
  Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)
  Lambda_Bonly_rv_Uncertainty = scipy.stats.rv_histogram(Lambda_Bonly_hist_Uncertainty)

  #Try and caluclate P-value for Exclusion median and write to list
  P_Value_Exclusion = (Lambda_BS_distribution_rv.cdf(B_OnlyMedian))
  P_Value_List_Method2_Exclusion.append(P_Value_Exclusion)
  P_Value_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_OnlyMedian_Uncertainty)) ###check
  P_Value_List_Method2_Exclusion_Uncertainty.append(P_Value_Exclusion_Uncertainty)

  #Work out P-values for Exclusion at different sigma positions and write to list to save at the end of file
  P_Value_plus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Exclusion = (Lambda_BS_distribution_rv.cdf(B_Only_LowerQuantile_1sigma))
  P_Value_plus_1sigma_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_Only_UpperQuantile_1sigma_Uncertainty)) ###check
  P_Value_minus_1sigma_Exclusion_Uncertainty = (Lambda_BS_distribution_rv_Uncertainty.cdf(B_Only_LowerQuantile_1sigma_Uncertainty)) ###check


  P_Value_List_plus_1sigma_Exclusion.append(P_Value_plus_1sigma_Exclusion)
  P_Value_List_minus_1sigma_Exclusion.append(P_Value_minus_1sigma_Exclusion)
  P_Value_List_plus_1sigma_Exclusion_Uncertainty.append(P_Value_plus_1sigma_Exclusion_Uncertainty)
  P_Value_List_minus_1sigma_Exclusion_Uncertainty.append(P_Value_minus_1sigma_Exclusion_Uncertainty)

  #Try and caluclate P-value for Discovery and write to list
  P_Value_Discovery = (1-Lambda_Bonly_rv.cdf(BS_Median))
  P_Value_List_Method2_Discovery.append(P_Value_Discovery)
  P_Value_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_Median_Uncertainty)) ###check
  P_Value_List_Method2_Discovery_Uncertainty.append(P_Value_Discovery_Uncertainty)


  #Work out P-values for Discovery at different sigma positions and write to luist to save at the end of file
  P_Value_plus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_UpperQuantile_1sigma))
  P_Value_minus_1sigma_Discovery = (1-Lambda_Bonly_rv.cdf(BS_LowerQuantile_1sigma))
  P_Value_plus_1sigma_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_UpperQuantile_1sigma_Uncertainty)) ###check
  P_Value_minus_1sigma_Discovery_Uncertainty = (1-Lambda_Bonly_rv_Uncertainty.cdf(BS_LowerQuantile_1sigma_Uncertainty)) ###check

  P_Value_List_plus_1sigma_Discovery.append(P_Value_plus_1sigma_Discovery)
  P_Value_List_minus_1sigma_Discovery.append(P_Value_minus_1sigma_Discovery)
  P_Value_List_plus_1sigma_Discovery_Uncertainty.append(P_Value_plus_1sigma_Discovery_Uncertainty)
  P_Value_List_minus_1sigma_Discovery_Uncertainty.append(P_Value_minus_1sigma_Discovery_Uncertainty)


  print("Method2: Min Exclusion value is:", min(P_Value_List_Method2_Exclusion, default=0))
  P_Value_List_Method2_Exclusion.append(min(P_Value_List_Method2_Exclusion,default=0))

  print("Method2: Min Discovery value is:", min(P_Value_List_Method2_Discovery,default=0))
  P_Value_List_Method2_Discovery.append(min(P_Value_List_Method2_Discovery,default=0))

  print("Method2: Min Exclusion value with Uncertainty  is:", min(P_Value_List_Method2_Exclusion_Uncertainty, default=0))
  P_Value_List_Method2_Exclusion_Uncertainty.append(min(P_Value_List_Method2_Exclusion_Uncertainty,default=0))

  print("Method2: Min Discovery value with Uncertainty is:", min(P_Value_List_Method2_Discovery_Uncertainty,default=0))
  P_Value_List_Method2_Discovery_Uncertainty.append(min(P_Value_List_Method2_Discovery_Uncertainty,default=0))



  #Wrtite all Pvalues,Sigma bands to seperate files to avoid errors to beiung hidden withihn one file
  #Pvalue at the median
  numpy.savetxt(Pvalue_out_Method2_Discovery, P_Value_List_Method2_Discovery, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion, P_Value_List_Method2_Exclusion, fmt="%1.10f")

  numpy.savetxt(Pvalue_out_Method2_Discovery_Uncertainty, P_Value_List_Method2_Discovery_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Exclusion_Uncertainty, P_Value_List_Method2_Exclusion_Uncertainty, fmt="%1.10f")
  #Pvalue at the Upper 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma, P_Value_List_plus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma, P_Value_List_plus_1sigma_Discovery, fmt="%1.10f")
  
  numpy.savetxt(Pvalue_out_Method2_Exclusion_plus_1sigma_Uncertainty, P_Value_List_plus_1sigma_Exclusion_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_plus_1sigma_Uncertainty, P_Value_List_plus_1sigma_Discovery_Uncertainty, fmt="%1.10f")
  
  #Pvalue at the Lower 1 sigma band
  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma, P_Value_List_minus_1sigma_Exclusion, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma, P_Value_List_minus_1sigma_Discovery, fmt="%1.10f")

  numpy.savetxt(Pvalue_out_Method2_Exclusion_minus_1sigma_Uncertainty, P_Value_List_minus_1sigma_Exclusion_Uncertainty, fmt="%1.10f")
  numpy.savetxt(Pvalue_out_Method2_Discovery_minus_1sigma_Uncertainty, P_Value_List_minus_1sigma_Discovery_Uncertainty, fmt="%1.10f")


  #Test Statistic Plotting

  Probably_Dist, ax = plt.subplots()
  ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
  plt.hist(Lambda_Bonly, bins,label="Bkg Only", alpha=0.4)
  plt.hist(Lambda_BS, bins, label="Bkg + Signal" , alpha=0.4)
  plt.hist(Lambda_BS_Uncertainty, bins, label="Bkg + Signal Unc" , alpha=0.4)
  plt.ticklabel_format(style = 'plain')
  plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
  plt.axvline(x=BS_Median,color='red', linestyle='--', label = "BS_Median")
  plt.axvline(x=BS_Median_Uncertainty,color='green', linestyle='--', label = "BS_Median Unc")
  plt.axvline(x=B_OnlyMedian_Uncertainty,color='blue', linestyle='--', label = "B_OnlyMedian Unc")
  plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
  # ax.set_ylabel('', ha='right', y=1.0)
  leg = plt.legend(borderpad=0.5, frameon=False, loc='best')
  plt.legend()
  # plt.margins(0)
  Probably_Dist.savefig(Directory+"Classifier_Offline_Outputs/"+Output_Folder+"/TestStatistic_plots/Uncertainty_TestDistribution_"+str(k)+"_"+str(M5)+".png")


print("time taken for whole file:")
print(time.process_time() - start0)






