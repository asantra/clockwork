############
#   SBLD   #
############

### Explanations ###
# - Calculate the signal associated to a generic clockwork graviton model with dampling and Genrat

#Import standard
import sys
import math, os, scipy
import numpy as np 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import ROOT
from ROOT import RooFit
import array
from parton import mkPDF

# import root_numpy as rn

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes

#import ATLAS MPL Style
# import mplhep as hep
# plt.style.use(hep.style.ROOT)

import logging 
mpl_logger = logging.getLogger("matplotlib") 
mpl_logger.setLevel(logging.WARNING)  


############################################################################################################

#Directory
Directory = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/"

sys.path.insert(0, Directory+'Custom_Modules')
from Custom_Modules import Basic, Background, Couplings, Graviton, KKcascade, Luminosity, PDFcalc, Smearing

#Input directory 
indir = "/scratch2/slawlor/Clockwork_Project/clockwork-search/Inputs/"

############################################################


### get the nominal acceptance*efficiency
fPeterAcc = ROOT.TFile(indir+"ee_accEffFunction.root","READ")
fAcc = fPeterAcc.Get("accEff_TF1")
sfAcc = "[0]+[1]/x+[2]/pow(x,2)+[3]/pow(x,3)+[4]/pow(x,4)+[5]/pow(x,5)+[6]*x+[7]*pow(x,2)"
for n in range(0,fAcc.GetNpar()):
   print (fAcc.GetParName(n)+": "+str(fAcc.GetParameter(n)))
   sfAcc = str(sfAcc.replace("["+str(n)+"]",str(fAcc.GetParameter(n))))
fAcc.SetLineColor(ROOT.kBlack)

### get the Nominal background model
fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
fBkg = fbkgfinal.Get("bkgfit")


Nbins = 5870 #2470
xmin = 130 #230
xmax = 6000 #2700

#Define mass binning
massList = np.linspace(xmin, xmax, Nbins)

### get the UP/DOWN variation acceptance*efficiency, Ae x (1 + function)

#ee_ID efficiency systematic uncertainties function
sfAcc_ee_IDefficiency = ROOT.TF1("sfAcc_ee_IDefficiency","[0]+[1]*TMath::Exp([2]*(x))+[3]*log(x)+[4]*TMath::TanH([5]*(x-[6]))")
sfAcc_ee_IDefficiency.SetParameter(0,0.200345)
sfAcc_ee_IDefficiency.SetParameter(1,-0.219756)
sfAcc_ee_IDefficiency.SetParameter(2,-0.000110541)
sfAcc_ee_IDefficiency.SetParameter(3,0.00922559)
sfAcc_ee_IDefficiency.SetParameter(4,0.0169377)
sfAcc_ee_IDefficiency.SetParameter(5,0.00632574)
sfAcc_ee_IDefficiency.SetParameter(6,530.715)

#sfAcc_ee_Iso_efficiency systematic uncertainties function
sfAcc_ee_Iso_efficiency = ROOT.TF1("sfAcc_ee_Iso_efficiency","[0]+[1]*TMath::TanH([2]*(x-[3]))+[4]*TMath::TanH([5]*(x-[6]))+[7]*log(x)")
sfAcc_ee_Iso_efficiency.SetParameter(0,0.00897086)
sfAcc_ee_Iso_efficiency.SetParameter(1,0.018128)
sfAcc_ee_Iso_efficiency.SetParameter(2,-0.000801275)
sfAcc_ee_Iso_efficiency.SetParameter(3,-262.667)
sfAcc_ee_Iso_efficiency.SetParameter(4,0.0130715)
sfAcc_ee_Iso_efficiency.SetParameter(5,0.00145374)
sfAcc_ee_Iso_efficiency.SetParameter(6,453.596)
sfAcc_ee_Iso_efficiency.SetParameter(7,.000817472)

#Generate example of signal model using full Resonant TF shape instead of Guassian
#First define the individual components

#Nominal Crystal Ball Parameters

mu_CB = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)", xmin, xmax)
mu_CB.SetParameter(0, 0.13287)
mu_CB.SetParameter(1, -0.410663)
mu_CB.SetParameter(2, -0.0126743)
mu_CB.SetParameter(3, 0.0000029547)
# mu_CB_test = rn.evaluate(mu_CB,massList)


sigma_CB = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB.SetParameter(0, 0.0136624)
sigma_CB.SetParameter(1, 0.230678)
sigma_CB.SetParameter(2, 1.73254)
# sigma_CB_test = rn.evaluate(sigma_CB,massList)

alpha_CB = 1.59112

n_CB = ROOT.TF1("n_CB","[0]+[1]*TMath::Exp(-[2]*(x))", xmin, xmax)
n_CB.SetParameter(0, 1.13055)
n_CB.SetParameter(1, 0.76705)
n_CB.SetParameter(2, 0.00298312)
# n_CB_test = rn.evaluate(n_CB,massList)

#Nominal Guassian Parameters

mu_G = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)", xmin, xmax)
# mu_G = ROOT.TF1("mu_G","[0]+[1]*(x^-1) + [2]*x+[3]*log(x^3)+[4]*((x^2)^-1)+[5]*(x^2)", xmin, xmax)
mu_G.SetParameter(0, -0.00402708)
mu_G.SetParameter(1, 0.814172)
mu_G.SetParameter(2, -0.000000394281)
mu_G.SetParameter(3, 0.00000797076)
mu_G.SetParameter(4, -87.6397)
mu_G.SetParameter(5, -0.0000000000164806)
# mu_G_test = rn.evaluate(mu_G,massList)

sigma_G = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G.SetParameter(0, 0.00553858)
sigma_G.SetParameter(1, 0.140909)
sigma_G.SetParameter(2, 0.644418)
# sigma_G_test = rn.evaluate(sigma_G,massList)

#K-Parameters

k_param = ROOT.TF1("k_param","[0]+[1]*TMath::Exp(-[2]*(x))+[3]*x+[4]*pow(x,3)", xmin, xmax)
k_param.SetParameter(0, 0.347003)
k_param.SetParameter(1, 0.135768)
k_param.SetParameter(2, 0.00372497)
k_param.SetParameter(3, -0.000022822)
k_param.SetParameter(4, 0.000000000000506351)
# k_param_test = rn.evaluate(k_param,massList)

########################################################################
########################################################################
########################################################################

#Up-variation Crystal Ball Parameters

#energy scale
mu_CB_UPvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)")
mu_CB_UPvar.SetParameter(0, 0.143853)
mu_CB_UPvar.SetParameter(1, -0.544555)
mu_CB_UPvar.SetParameter(2, -0.00897897)
mu_CB_UPvar.SetParameter(3, 0.000000211052)
# mu_CB_UPvartest = rn.evaluate(mu_CB_UPvar,massList)

#energy resolution
sigma_CB_UPvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_UPvar.SetParameter(0, 0.0168464)
sigma_CB_UPvar.SetParameter(1, 0.244998)
sigma_CB_UPvar.SetParameter(2, 0.607754)
# sigma_CB_UPvartest = rn.evaluate(sigma_CB,massList)

#Up-variation Gaussian

#energy scale
mu_G_UPvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)")
mu_G_UPvar.SetParameter(0, 0.00437644)
mu_G_UPvar.SetParameter(1, -0.693393)
mu_G_UPvar.SetParameter(2, -0.00000124588)
mu_G_UPvar.SetParameter(3, 0.00000994365)
mu_G_UPvar.SetParameter(4, 3.85433)
mu_G_UPvar.SetParameter(5, 0.0000000000681002)
# mu_G_UPvartest = rn.evaluate(mu_G_UPvar,massList)

#energy resolution
sigma_G_UPvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_UPvar.SetParameter(0, 0.00968273)
sigma_G_UPvar.SetParameter(1, 0.110266)
sigma_G_UPvar.SetParameter(2, 1.0617)
# sigma_G_UPvartest = rn.evaluate(sigma_G_UPvar,massList)

########################################################################

#Down-variation Crystal Ball Parameters

#energy scale
mu_CB_DOWNvar = ROOT.TF1("mu_CB","[0]+[1]/log(x)+[2]*log(x)+[3]*pow(log(x),4)")
mu_CB_DOWNvar.SetParameter(0, 0.125578)
mu_CB_DOWNvar.SetParameter(1, -0.350447)
mu_CB_DOWNvar.SetParameter(2, -0.0144995)
mu_CB_DOWNvar.SetParameter(3, 0.00000373014)
# mu_CB__DOWNvartest = rn.evaluate(mu_CB_DOWNvar,massList)

#energy resolution
sigma_CB_DOWNvar = ROOT.TF1("sigma_CB","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_CB_DOWNvar.SetParameter(0, 0.011107)
sigma_CB_DOWNvar.SetParameter(1, 0.22346)
sigma_CB_DOWNvar.SetParameter(2, 1.92675)
# sigma_CB_DOWNvartest = rn.evaluate(sigma_CB_DOWNvar,massList)

#DOWN-variation Gaussian

#energy scale
mu_G_DOWNvar = ROOT.TF1("mu_G","[0]+[1]/x+[2]*x+[3]*pow(log(x),3)+[4]/pow(x,2)+[5]*pow(x,2)")
mu_G_DOWNvar.SetParameter(0, -0.00743235)
mu_G_DOWNvar.SetParameter(1, 0.650265)
mu_G_DOWNvar.SetParameter(2, 0.000000574646)
mu_G_DOWNvar.SetParameter(3, -0.00000498561)
mu_G_DOWNvar.SetParameter(4, 28.7656)
mu_G_DOWNvar.SetParameter(5, -0.0000000000452119)
# mu_G_DOWNvartest = rn.evaluate(mu_G_DOWNvar,massList)

#energy resolution
sigma_G_DOWNvar = ROOT.TF1("sigma_G","sqrt(pow([0],2)+pow([1]/sqrt(x),2)+pow([2]/x,2))", xmin, xmax)
sigma_G_DOWNvar.SetParameter(0, 0.00337693)
sigma_G_DOWNvar.SetParameter(1, 0.112002)
sigma_G_DOWNvar.SetParameter(2, 1.2219)
# sigma_G_DOWNvartest = rn.evaluate(sigma_G_DOWNvar,massList)



#Attempt to make a function that takes in k and M5 values and gives a toy of the ensemble emthod of evaluating uncertainties

def SigUncertainty_Toy(k, M5, xmin, xmax,i):

  nominal = SigFixed_withCB(k, M5, xmin, xmax)
  Res_SystUP = SigFixed_withCB_UPvariation(k, M5, xmin, xmax)
  Res_SystDOWN = SigFixed_withCB_DOWNvariation(k, M5, xmin, xmax)

  # Place list of iosalted variations

  Res_SystUP_diff = DifferenceMaker(nominal, Res_SystUP)
  # print (Res_SystUP_diff)
  Res_SystDOWN_diff = DifferenceMaker(Res_SystDOWN, nominal)
  # print (Res_SystDOWN_diff)
  def Uncert_diff(diffUP,diffDOWN):

    nuis1 = np.random.normal(0,1,1)
    while abs(nuis1)>1:nuis1 = np.random.normal()
    nuis2 = np.random.normal(0,1,1)
    while abs(nuis2)>1:nuis2 = np.random.normal()

    for x in diffUP,diffDOWN:
      U1 = np.asarray(diffUP)*abs(nuis1)
      U2 =  np.asarray(diffDOWN)*abs(nuis2)

    return U1,U2

  # for i in range(nTests):
  UP,DOWN = Uncert_diff(Res_SystUP_diff,Res_SystDOWN_diff)
  add1 = nominal - UP
  add2 = nominal + DOWN

  if (i % 2 == 0): 
    return abs(add2)
  else:
    return abs(add1)    







#Generate signal with no random fluctuations
def SigFixed_withCB(k, M5, xmin, xmax, eventType):

### get the resolution with nominal parameters

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(8300) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(1)
  sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,100):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M1={M}")
     if (M5>k) and (M>M5): break
     # print (f"M2={M}")
     #Gaussain
     mu_Gauss = str(mu_G.Eval(M)*M+M)
     # print (f"n={n},mu_Gauss:{mu_Gauss}")
     sigma_Guass = str(sigma_G.Eval(M)*M)
     # print (f"n={n},sigma_Guass:{sigma_Guass}")
     #Crystal_Ball
     alpha_CrystalBall = str(alpha_CB)
     n_CrystalBall = str(n_CB.Eval(M))
     sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
     # print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
     mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
     # print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
     # sigma_CrystalBall = str(sigma_CB.Eval(M))
     # mu_CrystalBall = str(mu_CB.Eval(M))
     #k
     k_p = str(k_param.Eval(M))
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): 
      sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      # print (f"Tower Formula n==1:{sResonances}")
     else:     
      sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
      # print (f"Tower Formula n>1:{sResonances}")

  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  # print (f"Tower Formula:{sResonances}")
  # print ("========================")
  # print (f"Final Formula:{fClockwork}")
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float":    
      listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  return listS





#Generate signal with no random fluctuations - just relative resolution using Gauissian from Resonant Search
def SigFixed(k, M5, xmin, xmax, eventType):

### get the resolution with nominal parameters
  fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
  fRes = fPeterRes.Get("relResol_dedicatedTF")
  sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
  for n in range(0,fRes.GetNpar()):
    print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
    sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
  fRes.SetLineColor(ROOT.kRed)

  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  
  hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
  hbkgsig = hfit.Clone("hbkgsig_ee")
  kR = str(10)
  scale = str(12500) #12500
  Lmean = str(1*float(k))
  Lsigma = str(0.2*float(Lmean))
  Norm_CB = str(6.1/5)
  sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
  sResonances = ""  
  for n in range(1,1000):
     M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
     # print (f"M={M}")
     if (M5>k) and (M>M5): break
     sM = str(M)
     sW = str(M*fRes.Eval(M))
     sn = str(n)
     frac = str((float(k)/M)*(float(k)/M))
     if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
     else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
  sClockwork = sClockwork.replace("RESONANCES",sResonances)
  fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
  listS = []
  hsig.SetLineColor(ROOT.kGreen)
  hsig.SetLineWidth(1)
  hfitsig = hfit.Clone("hfitsig_ee")
  for i in range(1,N+1):
     x = hsig.GetBinCenter(i)
     y  = fClockwork.Eval(x)
     hsig.SetBinContent(i,y)
     if eventType == "int":
      listS.append(int(y))
     if eventType == "float":    
      listS.append(y)
     ndata = hbkgsig.GetBinContent(i)
     if(ndata>0):
        hbkgsig.SetBinContent(i,int(ndata+y))
        hfitsig_ee.AddBinContent(i,y)
  # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
  # fOut.cd()
  # hsig.Write()
  # fOut.Write()
  # fOut.Close()
  return listS





#Generate signal with no random fluctuations
def Sfix_DefaultTheory_Diphoton(k, M5, xmin, xmax, nBins, eventType):

  #Parameter
  SLHC   = Couplings.SLHC
  Eff    = Couplings.Eff
  Intlum = 36.7

  #Load pdf information
  pdf = mkPDF('MSTW2008lo68cl', 0, pdfdir=Directory+'Custom_Modules')

  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1

  while mtemp < massList[-1]:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRPhotons = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRPhotons.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open("Graviton_Properties/GravitonPropertiesDiphoton.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRPhotons[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    mass       = massList[i]
    deltamass  = massList[i + 1] - massList[i]

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      if eventType == "int":
        total += int(listCS[particle]*Smearing.shape(mass, listMassKK[particle], listGamma[particle])*listBRPhotons[particle]*Eff*Intlum)
      if eventType == "float":
        total += listCS[particle]*Smearing.shape(mass, listMassKK[particle], listGamma[particle])*listBRPhotons[particle]*Eff*Intlum
    #Append to lists
    listS.append(total*deltamass)
    # print(total*deltamass)
    listCSplot.append(total/Intlum)

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
    # print("lum")
    # print(lum)
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig('Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

#Theory Shape

#Generate signal with no random fluctuations
def Sfix_Theory_Dielectron(k, M5, xmin, xmax, nBins, eventType):


  #Define resolution with crystal ball + gaussian from the dielectron analysis

  def Resolution(M,n,m):
    # scale = str(1)
    sClockwork = "((RESONANCES)*("+(sfAcc)+")"
    sResonances = ""  
    #Gaussain
    mu_Gauss = str(mu_G.Eval(M)*M+M)
    sigma_Guass = str(sigma_G.Eval(M)*M)
    #Crystal_Ball
    alpha_CrystalBall = str(alpha_CB)
    n_CrystalBall = str(n_CB.Eval(M))
    sigma_CrystalBall = str(sigma_CB.Eval(M)*M)
    mu_CrystalBall = str(mu_CB.Eval(M)*M+M)
    #k
    k_p = str(k_param.Eval(M))
    # frac = str((float(k)/M)*(float(k)/M))
    if(n==1): 
      sResonances += "("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
    else:     
      sResonances += "+("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    y  = fClockwork.Eval(m)
    return y



  #Define massList
  massList = np.linspace(xmin, xmax, nBins)

  #Parameter
  SLHC   = Couplings.SLHC
  Intlum = 139
  
  #Load pdf information
  pdf = mkPDF('MSTW2008lo68cl', 0, pdfdir=Directory+'Custom_Modules')

  #Calculate masses of KK gravitons
  listMassKK = [0]
  mtemp      = 0
  nint       = 1

  while mtemp < massList[-1]:
    mtemp = Basic.m(nint, M5, k)
    listMassKK.append(mtemp)
    nint += 1

  #Calculate decay widths, branching ratios to photons and cross sections
  listGamma     = [0]
  listBRee      = [0]
  listCS        = [0]

  for n in range(1, len(listMassKK)):
    listGamma.append(Graviton.Gamma(n, M5, k))
    listBRee.append(Graviton.BRgammagamma(n, M5, k, listGamma))
    listCS.append(Graviton.CrossSectionKKn(n, M5, k, listMassKK, pdf, SLHC))

  #Write properties
  if Couplings.writeProp:

    ftemp = open("Graviton_Properties/GravitonPropertiesDielectron.txt","w")

    ftemp.write("Summary of the graviton properties \n")
    ftemp.write("n     mass     Decay width     Cross section [fb]    BR to photons \n")

    for n in range(0, len(listMassKK)):
      ftemp.write(str(n) + " " + str(listMassKK[n]) + " " + str(listGamma[n]) + " " + str(listCS[n]) + " " + str(listBRee[n]) + " " + "\n")

    ftemp.close()

  #Apply smearing
  listS      = []
  listCSplot = []

  for i in range(0, len(massList) - 1):

    #Initialize variables
    total      = 0
    mass       = massList[i]
    deltamass  = massList[i + 1] - massList[i]
    # Eff = fAcc.Eval(mass)

    #Sum over particles
    for particle in range(1, len(listMassKK)):
      # Eff = fAcc.Eval(listMassKK[particle])
      # print(Eff)
      # Eff = 0.5
      # total += listCS[particle]*Smearing.shape(mass, listMassKK[particle], listGamma[particle])*listBRPhotons[particle]*Eff*Intlum
      # total += listCS[particle]*Resolution(listMassKK[particle],k)*listBRee[particle]*Eff*Intlum
      if eventType == "int":
        total += int(listCS[particle]*Resolution(listMassKK[particle], particle, mass)*listBRee[particle]*Intlum)
      if eventType == "float":
        total += listCS[particle]*Resolution(listMassKK[particle], particle, mass)*listBRee[particle]*Intlum
      
    #Append to lists
    listS.append(total*deltamass)
    listCSplot.append(total/Intlum)

  #Calculate parton luminosity
  lum = []

  for mass in massList:
    lum.append(Luminosity.ggLum(SLHC, mass, mass, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 1, 1, pdf) + 4/3*Luminosity.qqbarLum(SLHC, mass, mass, 2, 2, pdf))
  #Make plot
  if Couplings.makePlot:
    massListR = np.delete(massList, -2)
    plt.plot(massListR, listCSplot)
    axes = plt.gca()
    plt.xlabel('$m_{\gamma\gamma}$ [GeV]')
    plt.ylabel(' d$\sigma$/dm x $\epsilon$ x BR[fb/GeV]')
    plt.savefig('Signal.pdf')
    plt.clf()

  #Return
  return listS, lum

####################################################################################
#Background Functions


#Get background with no random fluctuations
def BkgFixed(xmin, xmax, eventType):

  fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
  fBkg = fbkgfinal.Get("bkgfit")
  # xmin = fBkg.GetXmin()
  # xmax = fBkg.GetXmax()
  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listBkg = []
  for i in range(1,hfit.GetNbinsX()+1):
     x = hfit.GetBinCenter(i)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     if eventType == "int":
      listBkg.append(int(y))
     if eventType == "float":    
      listBkg.append(y)
     hfit.SetBinContent(i,y)

  return listBkg



############################################################################################################
############################################################################################################
# Import Variations of Backgorund- test case up and down 

def BkgFixed_AllUncertainties_ToysThrown_VarAll(xmin, xmax, ToySystNumber, eventType):
  fbkgfinal = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Plotting/toys_allUncertainties.root","READ")
  Bkg = fbkgfinal.Get("ee_toy_"+str(ToySystNumber))
  Bkg.Rebin(10)
  N = int(xmax-xmin) ## 1 GeV bins
  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  toyHist = Bkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  toyList = []
  for i in range(131,htemp.GetNbinsX()+131):
    x = toyHist.GetBinCenter(i)
    # if(x<fit.GetXmin()): continue
    # y = fit.Eval(x)
    # x = toyHist.GetBinLowEdge(2)
    # if(x<toyHist.GetXaxis().GetXmin()): continue
    y = toyHist.GetBinContent(i)
    z = toyHist.GetBinError(i)
    if eventType == "int":
      toyList.append(int(y))
    if eventType == "float":    
      toyList.append(y)
  return toyList


#Generate background with no random fluctuations
def Bfix_Diphoton_Theory(massList, eventType):

  massBGlist = []
  BGlist     = []
  listB      = []

  with open('Custom_Modules/Background.txt') as f:
    for line in f:
      data = line.split()
      massBGlist.append(float(data[0]))
      BGlist.append(float(data[1]))

  for i in range(0, len(massList) - 1):
    mass      = massList[i]
    deltamass = massList[i + 1] - massList[i]
    if eventType == "int":
      listB.append(int(Background.BackgroundCal(massBGlist, BGlist, mass)/20*deltamass))
    if eventType == "float":    
      listB.append(Background.BackgroundCal(massBGlist, BGlist, mass)/20*deltamass)

  return listB



############################################################################################################
############################################################################################################

#Generate random fluctuations in function using numpy Poission with random generator for given lamda
def SigBkgPoissonToy(listBkgFixed, listSigFixed, aBkg, aSig):

  listBkgSigrandom = []

  for i in range(0, len(listSigFixed)):
    # print("bkg")
    # print(len(listBkgFixed))
    # print("Sig")
    # print(len(listSigFixed))    
    listBkgSigrandom.append(np.random.poisson(lam = aBkg*listBkgFixed[i] + aSig*listSigFixed[i]))

  return listBkgSigrandom

#Combination of nominal signal and background with no variations

def SigBkgFixed(listBkgFixed, listSigFixed):

  listBkgSigFixed = []

  for i in range(0, len(listSigFixed)):
    listBkgSigFixed.append(listBkgFixed[i] + listSigFixed[i])

  return listBkgSigFixed

############################################################################################################
############################################################################################################



#Difference between the Bkg Fit and toy

def BkgFitToyDifference(listBkgFixed, Toy):
  listBkgDiff = []

  for i in range(0, len(Toy)):
    listBkgDiff.append(Toy[i] - listBkgFixed[i])

  return listBkgDiff

  
############################################################################################################
############################################################################################################

#Signal Uncertainty shapes explicitly defined 

#Generate signal with no random fluctuations
# def SigFixed_Ae_Iso_efficiency_UP(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters
#   fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes = fPeterRes.Get("relResol_dedicatedTF")
#   sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   for n in range(0,fRes.GetNpar()):
#     print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
#     sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
#   fRes.SetLineColor(ROOT.kRed)

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      # print (f"M={M}")
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*(1+sfAcc_ee_Iso_efficiency.Eval(M))*fRes.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS

# #Generate signal with no random fluctuations
# def SigFixed_Ae_ID_efficiency_UP(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters
#   fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes = fPeterRes.Get("relResol_dedicatedTF")
#   sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   for n in range(0,fRes.GetNpar()):
#     print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
#     sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
#   fRes.SetLineColor(ROOT.kRed)

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      # print (f"M={M}")
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*(1+sfAcc_ee_IDefficiency.Eval(M))*fRes.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS

# def SigFixed_Ae_Iso_efficiency_DOWN(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters
#   fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes = fPeterRes.Get("relResol_dedicatedTF")
#   sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   for n in range(0,fRes.GetNpar()):
#     print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
#     sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
#   fRes.SetLineColor(ROOT.kRed)

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      # print (f"M={M}")
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*(1-sfAcc_ee_Iso_efficiency.Eval(M))*fRes.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS

# #Generate signal with no random fluctuations
# def SigFixed_Ae_ID_efficiency_DOWN(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters
#   fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes = fPeterRes.Get("relResol_dedicatedTF")
#   sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   for n in range(0,fRes.GetNpar()):
#     print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
#     sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
#   fRes.SetLineColor(ROOT.kRed)

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      # print (f"M={M}")
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*(1-sfAcc_ee_IDefficiency.Eval(M))*fRes.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS


# #Generate signal with no random fluctuations
# def SigFixed_withCB_UPvariation(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,100):
#      M = float(k)*ROOT.TMath.Sqrt(1+((n*n)/(float(kR)*float(kR))))
#      # print (f"M={M}")
#      # if(M>M5): break
#      if (M5>k) and (M>M5): break
#      #Gaussain
#      mu_Gauss = str(mu_G_UPvar.Eval(M)*M+M)
#      # print (f"n={n},mu_Gauss:{mu_Gauss}")
#      sigma_Guass = str(sigma_G_UPvar.Eval(M)*M)
#      # print (f"n={n},sigma_Guass:{sigma_Guass}")
#      #Crystal_Ball
#      alpha_CrystalBall = str(alpha_CB)
#      n_CrystalBall = str(n_CB.Eval(M))
#      sigma_CrystalBall = str(sigma_CB_UPvar.Eval(M)*M)
#      # print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
#      mu_CrystalBall = str(mu_CB_UPvar.Eval(M)*M+M)
#      # print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
#      # sigma_CrystalBall = str(sigma_CB.Eval(M))
#      # mu_CrystalBall = str(mu_CB.Eval(M))
#      #k
#      k_p = str(k_param.Eval(M))
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): 
#       sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
#       # print (f"Tower Formula n==1:{sResonances}")
#      else:     
#       sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
#       # print (f"Tower Formula n>1:{sResonances}")

#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   # print (f"Tower Formula:{sResonances}")
#   # print ("========================")
#   # print (f"Final Formula:{fClockwork}")
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   return listS

# #Generate signal with no random fluctuations
# def SigFixed_withCB_DOWNvariation(k, M5, xmin, xmax):

# ### get the resolution with nominal parameters

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   Norm_CB = str(1)
#   sClockwork = "("+(scale)+"*"+(Norm_CB)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,100):
#      M = float(k)*ROOT.TMath.Sqrt(1+((n*n)/(float(kR)*float(kR))))
#      # print (f"M={M}")
#      # if(M>M5): break
#      if (M5>k) and (M>M5): break
#      #Gaussain
#      mu_Gauss = str(mu_G_DOWNvar.Eval(M)*M+M)
#      # print (f"n={n},mu_Gauss:{mu_Gauss}")
#      sigma_Guass = str(sigma_G_DOWNvar.Eval(M)*M)
#      # print (f"n={n},sigma_Guass:{sigma_Guass}")
#      #Crystal_Ball
#      alpha_CrystalBall = str(alpha_CB)
#      n_CrystalBall = str(n_CB.Eval(M))
#      sigma_CrystalBall = str(sigma_CB_DOWNvar.Eval(M)*M)
#      # print (f"n={n},sigma_CrystalBall:{sigma_CrystalBall}")
#      mu_CrystalBall = str(mu_CB_DOWNvar.Eval(M)*M+M)
#      # print (f"n={n},mu_CrystalBall:{mu_CrystalBall}")
#      # sigma_CrystalBall = str(sigma_CB.Eval(M))
#      # mu_CrystalBall = str(mu_CB.Eval(M))
#      #k
#      k_p = str(k_param.Eval(M))
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): 
#       sResonances += "(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
#       # print (f"Tower Formula n==1:{sResonances}")
#      else:     
#       sResonances += "+(1-"+frac+")*(("+k_p+"*ROOT::Math::crystalball_pdf(x, "+alpha_CrystalBall+", "+n_CrystalBall+", "+sigma_CrystalBall+","+mu_CrystalBall+"))+((1-"+k_p+")*TMath::Gaus(x,"+mu_Gauss+","+sigma_Guass+",1)))"
#       # print (f"Tower Formula n>1:{sResonances}")
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   # print (f"Tower Formula:{sResonances}")
#   # print ("========================")
#   # print (f"Final Formula:{fClockwork}")
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   return listS


# def SigFixed_Resolution_SystematicUP(k, M5, xmin, xmax):

#   ### get the resolution with parameters shifted from resolution UP uncertainty 
#   fPeterRes_SystUP = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes_SystUP = fPeterRes_SystUP.Get("relResol_dedicatedTF")
#   sfRes_SystUP = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   fRes_SystUP.SetParameter(5, 0.0168464)
#   fRes_SystUP.SetParameter(6, 0.244998)
#   fRes_SystUP.SetParameter(7, 0.607754)
#   fRes_SystUP.SetParameter(8, 0.00968273)
#   fRes_SystUP.SetParameter(9, 0.110266)
#   fRes_SystUP.SetParameter(10, 1.0617)
#   for n in range(0,fRes_SystUP.GetNpar()):
#      print (fRes_SystUP.GetParName(n)+": "+str(fRes_SystUP.GetParameter(n)))
#      sfRes_SystUP = sfRes_SystUP.replace("["+str(n)+"]",str(fRes_SystUP.GetParameter(n)))
#   fRes_SystUP.SetLineColor(ROOT.kRed)

#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*fRes_SystUP.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS

# def SigFixed_Resolution_SystematicDOWN(k, M5, xmin, xmax):

#   ### get the resolution with parameters shifted from resolution DOWN uncertainty 
#   fPeterRes_SystDOWN = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
#   fRes_SystDOWN = fPeterRes_SystDOWN.Get("relResol_dedicatedTF")
#   sfRes_SystDOWN = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
#   fRes_SystDOWN.SetParameter(5, 0.011107)
#   fRes_SystDOWN.SetParameter(6, 0.22346)
#   fRes_SystDOWN.SetParameter(7, 1.92675)
#   fRes_SystDOWN.SetParameter(8, 0.00337693)
#   fRes_SystDOWN.SetParameter(9, 0.112002)
#   fRes_SystDOWN.SetParameter(10, 1.2219)
#   for n in range(0,fRes_SystDOWN.GetNpar()):
#      print (fRes_SystDOWN.GetParName(n)+": "+str(fRes_SystDOWN.GetParameter(n)))
#      sfRes_SystDOWN = sfRes_SystDOWN.replace("["+str(n)+"]",str(fRes_SystDOWN.GetParameter(n)))
#   fRes_SystDOWN.SetLineColor(ROOT.kRed)


#   N = int(xmax-xmin) ## 1 GeV bins

#   htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
#   htemp.SetLineColor(ROOT.kBlack)
#   htemp.SetMarkerColor(ROOT.kBlack)
#   htemp.SetMarkerStyle(20)
#   htemp.SetMinimum(1e-2)
#   fit = fBkg.Clone("fee")
#   hfit = htemp.Clone("hfit_ee")
#   hfit.Reset()
  
#   hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
#   hbkgsig = hfit.Clone("hbkgsig_ee")
#   kR = str(10)
#   scale = str(12500) #12500
#   Lmean = str(1*float(k))
#   Lsigma = str(0.2*float(Lmean))
#   sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
#   sResonances = ""  
#   for n in range(1,1000):
#      M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
#      if(M>M5): break
#      sM = str(M)
#      sW = str(M*fRes_SystDOWN.Eval(M))
#      sn = str(n)
#      frac = str((float(k)/M)*(float(k)/M))
#      if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#      else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
#   sClockwork = sClockwork.replace("RESONANCES",sResonances)
#   fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
#   listS = []
#   hsig.SetLineColor(ROOT.kGreen)
#   hsig.SetLineWidth(1)
#   hfitsig = hfit.Clone("hfitsig_ee")
#   for i in range(1,N+1):
#      x = hsig.GetBinCenter(i)
#      y  = fClockwork.Eval(x)
#      hsig.SetBinContent(i,y)
#      listS.append(y)
#      ndata = hbkgsig.GetBinContent(i)
#      if(ndata>0):
#         hbkgsig.SetBinContent(i,int(ndata+y))
#         hfitsig_ee.AddBinContent(i,y)
#   # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/SigToy.root","RECREATE")
#   # fOut.cd()
#   # hsig.Write()
#   # fOut.Write()
#   # fOut.Close()
#   return listS





############################################################################################################
############################################################################################################

####Functions to create toys from RooFit 

def RooFitToy_Bkgonly(xmin, xmax):

  fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
  fBkg = fbkgfinal.Get("bkgfit")
    # xmin = fBkg.GetXmin()
    # xmax = fBkg.GetXmax()
  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listB = []
  listX =[]
  for i in range(1+xmin,hfit.GetNbinsX()+131):
     x = hfit.GetBinCenter(i)
     listX.append(x)
     if(x<fit.GetXmin()): continue
     y = fit.Eval(x)
     listB.append(y)
     hfit.SetBinContent(i,y)

  x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
  x.setBins(hfit.GetNbinsX())
  rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
  histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
  n = int(rooHist.sumEntries())
  print ("Going to sample n=%g events" % (n))
  toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
  toy.Print()
## now dump the toy into the new hist:
  toyHist = hfit.Clone("mass_")
  toyHist.Reset()
  entries = toy.numEntries()
  print ("rooHist.sumEntries()=",rooHist.sumEntries())
  for i in range(entries):
    event = toy.get(i)
    var = event.find("x");
    val = var.getVal()
    toyHist.Fill(val)
    toyHist.SetMarkerStyle(20)
    toyHist.SetMarkerSize(1)
    toyHist.SetMarkerColor(ROOT.kBlack)
     # difHist = toyHist.Clone("diff_"+newname)
     # difHist.Add(bkgfithist,-1.)
  fit = fBkg.Clone("fee")
  toyList = []
  for i in range(1,toyHist.GetNbinsX()+1):
    # x = toyHist.GetBinCenter(i)
    # if(x<fit.GetXmin()): continue
    # y = fit.Eval(x)
    x = toyHist.GetBinLowEdge(2)
    if(x<toyHist.GetXaxis().GetXmin()): continue
    # if(x<fit.GetXmin()): continue
    y = fit.Eval(x)
    y = toyHist.GetBinContent(i)
    toyList.append(y)

  return toyList

def ToyDataGenerator_withSignal(k, M5, xmin, xmax, ToyType, aSig):



  if (ToyType == "bkgsig"):

      ### get the resolution with nominal parameters
    fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
    fRes = fPeterRes.Get("relResol_dedicatedTF")
    sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
    for n in range(0,fRes.GetNpar()):
      print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
      sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
    fRes.SetLineColor(ROOT.kRed)

    fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
    fBkg = fbkgfinal.Get("bkgfit")
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    for i in range(1+xmin,hfit.GetNbinsX()+131):
       x = hfit.GetBinCenter(i)
       if(x<fit.GetXmin()): continue
       y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfitsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfitsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    for i in range(1,toyHist.GetNbinsX()+1):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      toyList.append(y)

  if (ToyType == "sig"):
        ### get the resolution with nominal parameters
    fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
    fRes = fPeterRes.Get("relResol_dedicatedTF")
    sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
    for n in range(0,fRes.GetNpar()):
      print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
      sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
    fRes.SetLineColor(ROOT.kRed)

    fbkgfinal = ROOT.TFile(indir+"finalbkg.root","READ")
    fBkg = fbkgfinal.Get("bkgfit")
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig_ee")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig_ee.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hsig.GetXaxis().GetXmin(),hsig.GetXaxis().GetXmax())
    x.setBins(hsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    for i in range(1,toyHist.GetNbinsX()+1):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i)
      toyList.append(y)

  return toyList




def RooFitToy_MCBkgonly(xmin, xmax):

  fbkgfinal = ROOT.TFile(indir+"finalbkg_MC.root","READ")
  fBkg = fbkgfinal.Get("bkg_MC")
    # xmin = fBkg.GetXmin()
    # xmax = fBkg.GetXmax()
  N = int(xmax-xmin) ## 1 GeV bins

  htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(1e-2)
  fit = fBkg.Clone("fee")
  hfit = htemp.Clone("hfit_ee")
  hfit.Reset()
  listB = []
  listX =[]
  for i in range(1+xmin,hfit.GetNbinsX()+131):
     x = hfit.GetBinCenter(i)
     # listX.append(x)
     # if(x<fit.GetXmin()): continue
     y = fit.GetBinContent(i)
     # y = fit.Eval(x)
     # listB.append(y)
     hfit.SetBinContent(i,y)

  x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
  x.setBins(hfit.GetNbinsX())
  rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
  histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
  n = int(rooHist.sumEntries())
  print ("Going to sample n=%g events" % (n))
  toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
  toy.Print()
## now dump the toy into the new hist:
  toyHist = hfit.Clone("mass_")
  toyHist.Reset()
  entries = toy.numEntries()
  print ("rooHist.sumEntries()=",rooHist.sumEntries())
  for i in range(entries):
    event = toy.get(i)
    var = event.find("x");
    val = var.getVal()
    toyHist.Fill(val)
    toyHist.SetMarkerStyle(20)
    toyHist.SetMarkerSize(1)
    toyHist.SetMarkerColor(ROOT.kBlack)
     # difHist = toyHist.Clone("diff_"+newname)
     # difHist.Add(bkgfithist,-1.)
  fit = fBkg.Clone("fee")
  toyList = []
  for i in range(1,toyHist.GetNbinsX()+1):
    # x = toyHist.GetBinCenter(i)
    # if(x<fit.GetXmin()): continue
    # y = fit.Eval(x)
    x = toyHist.GetBinLowEdge(2)
    if(x<toyHist.GetXaxis().GetXmin()): continue
    # if(x<fit.GetXmin()): continue
    # y = fit.Eval(x)
    y = toyHist.GetBinContent(i)
    toyList.append(y)

  return toyList

def RooFitToy_MC_withSignal(k, M5, xmin, xmax, ToyType, aSig):

  if (ToyType == "bkgsig"):

    fbkgfinal = ROOT.TFile(indir+"finalbkg_MC.root","READ")
    fBkg = fbkgfinal.Get("bkg_MC")
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    for i in range(xmin+1,hfit.GetNbinsX()+131):
       x = hfit.GetBinCenter(i)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfitsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfitsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    for i in range(1,toyHist.GetNbinsX()+1):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      toyList.append(y)

  if (ToyType == "sig"):

    fbkgfinal = ROOT.TFile(indir+"finalbkg_MC.root","READ")
    fBkg = fbkgfinal.Get("bkg_MC")
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig_ee")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig_ee.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hsig.GetXaxis().GetXmin(),hsig.GetXaxis().GetXmax())
    x.setBins(hsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    for i in range(1,toyHist.GetNbinsX()+1):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i)
      toyList.append(y)

  return toyList





def Fitting_SignalInjection_MCBkg(k, M5, xmin, xmax, ToyType , aSig):



  if (ToyType == "Bkg_Only"):

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
      # xmin = fBkg.GetXmin()
      # xmax = fBkg.GetXmax()
    N = int(xmax-xmin) ## 1 GeV bins

    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    listB = []
    listX =[]
    # for i in range(1,hfit.GetNbinsX()+1):
    for i in range(131,hfit.GetNbinsX()+131):
    # for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # listX.append(x)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       # listB.append(y)
       hfit.SetBinContent(i,y)

    x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
    x.setBins(hfit.GetNbinsX())
    print (hfit.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
      toyHist.SetMarkerStyle(20)
      toyHist.SetMarkerSize(1)
      toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    fit = fBkg.Clone("fee")
    toyList = [] 
    toyListBinError = []
    # for i in range(1,toyHist.GetNbinsX()+1):
    for i in range(131, toyHist.GetNbinsX()+131):
    # for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      y = toyHist.GetBinContent(i)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListBinError.append(z)
    # save to csv file
    # np.savetxt('bkg_RooHistToy_BinValues.csv', toyList, delimiter=',')  
    # np.savetxt('bkg_RooHistToy_BinError.csv', toyListBinError, delimiter=',')  
    # save hist to ROOT file


  if (ToyType == "bkgsig"):

    ### get the resolution with nominal parameters
    fPeterRes = ROOT.TFile(indir+"ee_relResolFunction.root","READ")
    fRes = fPeterRes.Get("relResol_dedicatedTF")
    sfRes = "sqrt(pow(([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2))*sqrt(pow([5],2)+pow([6]/sqrt(x),2)+pow([7]/x,2)),2)+pow((1.0-([0]+[1]/x+[2]/pow(x,2)+[3]*x+[4]*pow(x,2)))*sqrt(pow([8],2)+pow([9]/sqrt(x),2)+pow([10]/x,2)),2))"
    for n in range(0,fRes.GetNpar()):
      print (fRes.GetParName(n)+": "+str(fRes.GetParameter(n)))
      sfRes = sfRes.replace("["+str(n)+"]",str(fRes.GetParameter(n)))
    fRes.SetLineColor(ROOT.kRed)

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    # for i in range(1,hfit.GetNbinsX()+1):
    for i in range(131, hfit.GetNbinsX()+131):
    # for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    rooHist_Sig = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    histPdf_Sig = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist_Sig)
    n = int(rooHist.sumEntries())
    n_Sig = int(rooHist_Sig.sumEntries())
    # print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy_Sig = histPdf_Sig.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n_Sig),ROOT.RooFit.AutoBinned(ROOT.kFALSE))

    # toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    toyHist_Sig = hsig.Clone("mass_")
    toyHist_Sig.Reset()
    entries_Sig = toy_Sig.numEntries()
    print ("rooHist.sumEntries()=",rooHist_Sig.sumEntries())
    for i in range(entries_Sig):
      event_Sig = toy_Sig.get(i)
      var_Sig = event_Sig.find("x");
      val_Sig = var_Sig.getVal()
      toyHist_Sig.Fill(val_Sig)
    toyHist.Add(toyHist_Sig,1)

    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    toyListBinError = []
    # for i in range(1,toyHist.GetNbinsX()+1):
    for i in range(131, toyHist.GetNbinsX()+131):
    # for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # print (x)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListBinError.append(z) 
    # # save hist to ROOT file
    # fOut = ROOT.TFile("/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/FFT/Noam_W/bkgsig_Toy.root","RECREATE")
    # fOut.cd()
    # toyHist.Write()
    # fOut.Write()
    # fOut.Close()

  N_MC = int(xmax-xmin) ## 1 GeV bins
  bkgfit_MC = ROOT.TF1("bkgfit_MC","[0] * [2]/((x-[1])^2 + [2]^2) * (1-(x/13000)^[3])^[4] * (x/13000)^([5]+[6]*log(x/13000)+[7]*log(x/13000)^2+[8]*log(x/13000)^3)",xmin,xmax)
  bkgfit_MC.SetParName(0,"N");  bkgfit_MC.SetParameter(0, 1)
  # bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 178000) #FixParameter above?
  bkgfit_MC.SetParName(1,"m0"); bkgfit_MC.SetParameter(1, +91.1876); bkgfit_MC.SetParLimits(1, 0,+150)
  bkgfit_MC.SetParName(2,"w0"); bkgfit_MC.SetParameter(2, +2.4952); bkgfit_MC.SetParLimits(2, -10,15)
  bkgfit_MC.SetParName(3,"c");  bkgfit_MC.SetParameter(3, +1) ; bkgfit_MC.SetParLimits(3, -11,10)
  bkgfit_MC.SetParName(4,"b");  bkgfit_MC.SetParameter(4, +1.5); bkgfit_MC.SetParLimits(4, -10,15)
  bkgfit_MC.SetParName(5,"p0"); bkgfit_MC.SetParameter(5, -12.38); bkgfit_MC.SetParLimits(5, -100,110)
  bkgfit_MC.SetParName(6,"p1"); bkgfit_MC.SetParameter(6, -4.29); bkgfit_MC.SetParLimits(6, -100,150)
  bkgfit_MC.SetParName(7,"p2"); bkgfit_MC.SetParameter(7, -0.919); bkgfit_MC.SetParLimits(7, -10,15)
  bkgfit_MC.SetParName(8,"p3"); bkgfit_MC.SetParameter(8, -0.0845); bkgfit_MC.SetParLimits(8, -10,15)
  bkgfit_MC_I = bkgfit_MC.Integral(xmin,xmax)
  print ("MC Integral:",bkgfit_MC_I)


  n = int(xmax-xmin) ## 1 GeV bins
  htemp = ROOT.TH1D("htemp_ee","",n,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(5e-5)
  MC_N = 0
  xup = -1
  xxx = -1
  isfound = False
  print (MC.GetNbinsX())
  for b in range(toyHist.GetNbinsX()):
    x = toyHist.GetBinCenter(b)
    integralx = toyHist.Integral(b,toyHist.GetNbinsX())
    MC_N += toyHist.GetBinContent(b)
    # htemp.SetBinContent(b-130,MC_N_Sig_perBin)
 
  
  normfactor_MC = MC_N/bkgfit_MC_I

  print ("MC normfactor:",normfactor_MC)
  bkgfit_MC.FixParameter(0,normfactor_MC)
  toyHist.Fit("bkgfit_MC","R", "0", 130, 6000)
  FittedToyList =[]
  bkghist_MC = toyHist.Clone("bkghist")
  bkghist_MC.Reset()
  # for b in range(1, toyHist.GetNbinsX()+1):
  for b in range(131, toyHist.GetNbinsX()+131):
  # for b in range(261, toyHist.GetNbinsX()+261):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    y = bkgfit_MC.Eval(x)
    FittedToyList.append(y)
    bkghist_MC.SetBinContent(b,y)


  return FittedToyList, toyList, toyListBinError



def MC_Fitting_PoissonToy(k, M5, xmin, xmax, ToyType , aSig):

  BkgFix = BkgFixed(130, 6000)
  htemp = ROOT.TH1D("htemp_ee","",5870,130,6000)
  Bkgroot = rn.array2hist(BkgFix,htemp,errors=None)

  if (ToyType == "Bkg_Only"):

    #need something here to convert to numpy to root files

    n = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    listB = []
    listX =[]
    # for i in range(131,hfit.GetNbinsX()+131):
    for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # listX.append(x)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       # listB.append(y)
       hfit.SetBinContent(i,y)

    x = ROOT.RooRealVar("x","x",hfit.GetXaxis().GetXmin(),hfit.GetXaxis().GetXmax())
    x.setBins(hfit.GetNbinsX())
    print (hfit.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfit)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfit.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
      toyHist.SetMarkerStyle(20)
      toyHist.SetMarkerSize(1)
      toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    fit = fBkg.Clone("fee")
    toyList = [] 
    toyListError = []
    # for i in range(131, toyHist.GetNbinsX()+131):
    for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      y = toyHist.GetBinContent(i)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListError.append(z)



  if (ToyType == "bkgsig"):

    fbkgfinal = ROOT.TFile(indir+"nominal_ee.root","READ")
    MC = fbkgfinal.Get("nominal_ee")
    fBkg = MC.Rebin(10)
    fBkg.GetXaxis().SetRangeUser(xmin,xmax)
    fBkg.SetMinimum(5.e-5)
    N = int(xmax-xmin) ## 1 GeV bins
    htemp = ROOT.TH1D("htemp_ee","",N,xmin,xmax)
    htemp.SetLineColor(ROOT.kBlack)
    htemp.SetMarkerColor(ROOT.kBlack)
    htemp.SetMarkerStyle(20)
    htemp.SetMinimum(1e-2)#-2
    fit = fBkg.Clone("fee")
    hfit = htemp.Clone("hfit_ee")
    hfit.Reset()
    # for i in range(131, hfit.GetNbinsX()+131):
    for i in range(261,hfit.GetNbinsX()+261):
       x = hfit.GetBinCenter(i)
       # if(x<fit.GetXmin()): continue
       y = fit.GetBinContent(i)
       # y = fit.Eval(x)
       hfit.SetBinContent(i,y)

    hsig = ROOT.TH1D("hsig_ee","",N,xmin,xmax)
    hbkgsig = hfit.Clone("hbkgsig_ee")
    kR = str(10)
    scale = str(12500) #12500
    Lmean = str(1*float(k))
    Lsigma = str(0.2*float(Lmean))
    sClockwork = "("+(scale)+"*TMath::Landau((x),"+(Lmean)+","+(Lsigma)+") * TMath::Exp(-(x)/"+str(k)+") * (RESONANCES) )*("+(sfAcc)+")"
    sResonances = ""  
    for n in range(1,1000):
       M = float(k)*ROOT.TMath.Sqrt(1+n*n/(float(kR)*float(kR)))
       if(M>M5): break
       sM = str(M)
       sW = str(M*fRes.Eval(M))
       sn = str(n)
       frac = str((float(k)/M)*(float(k)/M))
       if(n==1): sResonances += "(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
       else:     sResonances += "+(1-"+frac+")*TMath::Gaus(x,"+sM+","+sW+",1)"
    sClockwork = sClockwork.replace("RESONANCES",sResonances)
    fClockwork = ROOT.TF1("fClockwork",sClockwork,xmin,xmax)
    listS = []
    hsig.SetLineColor(ROOT.kGreen)
    hsig.SetLineWidth(1)
    hfitsig = hfit.Clone("hfitsig")
    for i in range(1,N+1):
       x = hsig.GetBinCenter(i)
       y  = fClockwork.Eval(x)
       hsig.SetBinContent(i,y)
       listS.append(y)
       ndata = hbkgsig.GetBinContent(i)
       if(ndata>0):
          hbkgsig.SetBinContent(i,int(ndata+y))
          hfitsig.AddBinContent(i,y)
    x = ROOT.RooRealVar("x","x",hfitsig.GetXaxis().GetXmin(),hfitsig.GetXaxis().GetXmax())
    x.setBins(hfitsig.GetNbinsX())
    rooHist = ROOT.RooDataHist("rooHist_","rooHist",ROOT.RooArgList(x),hfitsig)
    histPdf = ROOT.RooHistPdf("histPdf_","histPdf",ROOT.RooArgSet(x),rooHist)
    n = int(rooHist.sumEntries())
    print ("Going to sample n=%g events" % (n))
    toy = histPdf.generate(ROOT.RooArgSet(x),ROOT.RooFit.NumEvents(n),ROOT.RooFit.AutoBinned(ROOT.kFALSE))
    toy.Print()
  ## now dump the toy into the new hist:
    toyHist = hfitsig.Clone("mass_")
    toyHist.Reset()
    entries = toy.numEntries()
    print ("rooHist.sumEntries()=",rooHist.sumEntries())
    for i in range(entries):
      event = toy.get(i)
      var = event.find("x");
      val = var.getVal()
      toyHist.Fill(val)
    #   toyHist.SetMarkerStyle(20)
    #   toyHist.SetMarkerSize(1)
    #   toyHist.SetMarkerColor(ROOT.kBlack)
       # difHist = toyHist.Clone("diff_"+newname)
       # difHist.Add(bkgfithist,-1.)
    # fit = fBkg.Clone("fee")
    toyList = []
    toyListError = []
    # for i in range(131, toyHist.GetNbinsX()+131):
    for i in range(261,toyHist.GetNbinsX()+261):
      x = toyHist.GetBinCenter(i)
      # print (x)
      # if(x<fit.GetXmin()): continue
      # y = fit.Eval(x)
      # x = toyHist.GetBinLowEdge(2)
      if(x<toyHist.GetXaxis().GetXmin()): continue
      y = toyHist.GetBinContent(i*aSig)
      z = toyHist.GetBinError(i)
      toyList.append(y)
      toyListError.append(z)

  N_MC = int(xmax-xmin) ## 1 GeV bins
  bkgfit_MC = ROOT.TF1("bkgfit_MC","[0] * [2]/((x-[1])^2 + [2]^2) * (1-(x/13000)^[3])^[4] * (x/13000)^([5]+[6]*log(x/13000)+[7]*log(x/13000)^2+[8]*log(x/13000)^3)",xmin,xmax)
  bkgfit_MC.SetParName(0,"N");  bkgfit_MC.SetParameter(0, 1)
  # bkgfit.SetParName(0,"N");  bkgfit.SetParameter(0, 178000) #FixParameter above?
  bkgfit_MC.SetParName(1,"m0"); bkgfit_MC.SetParameter(1, +91.1876); bkgfit_MC.SetParLimits(1, 0,+150)
  bkgfit_MC.SetParName(2,"w0"); bkgfit_MC.SetParameter(2, +2.4952); bkgfit_MC.SetParLimits(2, -10,15)
  bkgfit_MC.SetParName(3,"c");  bkgfit_MC.SetParameter(3, +1) ; bkgfit_MC.SetParLimits(3, -11,10)
  bkgfit_MC.SetParName(4,"b");  bkgfit_MC.SetParameter(4, +1.5); bkgfit_MC.SetParLimits(4, -10,15)
  bkgfit_MC.SetParName(5,"p0"); bkgfit_MC.SetParameter(5, -12.38); bkgfit_MC.SetParLimits(5, -100,110)
  bkgfit_MC.SetParName(6,"p1"); bkgfit_MC.SetParameter(6, -4.29); bkgfit_MC.SetParLimits(6, -100,150)
  bkgfit_MC.SetParName(7,"p2"); bkgfit_MC.SetParameter(7, -0.919); bkgfit_MC.SetParLimits(7, -10,15)
  bkgfit_MC.SetParName(8,"p3"); bkgfit_MC.SetParameter(8, -0.0845); bkgfit_MC.SetParLimits(8, -10,15)

  bkgfit_MC_I = bkgfit_MC.Integral(xmin,xmax)
  print ("MC Integral:",bkgfit_MC_I)


  n = int(xmax-xmin) ## 1 GeV bins
  htemp = ROOT.TH1D("htemp_ee","",n,xmin,xmax)
  htemp.SetLineColor(ROOT.kBlack)
  htemp.SetMarkerColor(ROOT.kBlack)
  htemp.SetMarkerStyle(20)
  htemp.SetMinimum(5e-5)
  MC_N = 0
  xup = -1
  xxx = -1
  isfound = False
  print (MC.GetNbinsX())
  for b in range(toyHist.GetNbinsX()):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    # print (x)
    # if(x<xmin): continue
    # if(x>xmax): break
    integralx = toyHist.Integral(b,toyHist.GetNbinsX())
    MC_N += toyHist.GetBinContent(b)
    # htemp.SetBinContent(b-130,MC_N_Sig_perBin)
 
      

  normfactor_MC = MC_N/bkgfit_MC_I

  print ("MC normfactor:",normfactor_MC)
  # Htemp = Bkghist_MCMaker_Sig_Prefit_WithSignal(bkgfit, 1, ROOT.kYellow+1)
  # bkgfit_MC.SetParameter(0,normfactor_MC_Sig)
  bkgfit_MC.FixParameter(0,normfactor_MC)
  toyHist.Fit("bkgfit_MC","R", "0", 130, 1700)
  # bkgfit_MC_Sig.Write("bkgsig_MC1")
  # InputToy_MC_FitResult = Bkghist_MCMaker_Sig(bkgfit_MC_Sig, ROOT.kOrange)
  # FittedToyList =[]
  # # hist_MC = toyHist.Clone("hist")
  # # hist_MC.Reset()
  # for b in range(toyHist.GetNbinsX()):
  #    x = toyHist.GetBinCenter(b)
  #    # print (x)
  #    # if(x<xmin): continue
  #    # if(x>xmax): break
  #    y = bkgfit_MC.Eval(x)
  #    FittedToyList.append(y)
  #    toyHist.SetBinContent(b,y)
  #    toyHist.SetLineColor(ROOT.kRed)

  FittedToyList =[]
  bkghist_MC = toyHist.Clone("bkghist")
  bkghist_MC.Reset()
  # for b in range(131, toyHist.GetNbinsX()+131):
  for b in range(261, toyHist.GetNbinsX()+261):
    x = toyHist.GetBinCenter(b)
    # if(x<xmin): continue
    # if(x>xmax): break
    y = bkgfit_Sig.Eval(x)
    FittedToyList.append(y)
    bkghist_MC.SetBinContent(b,y)

  # MC_FitResult = Bkghist_MCMaker_Sig(toyHist, bkgfit_MC)

  return MC_FitResult, toyList, toyListError






############################################################################################################
############################################################################################################
############################################################################################################

#Other functions that can be used fot ratios or residuals etc


def RatioMaker(listNumnerator, listDenominator):

  RatioArray =[]

  for i in range(0, len(listDenominator)):
    if listDenominator[i] != 0:
      z = listNumnerator[i]/listDenominator[i]
    else:
      z =0
    RatioArray.append(z)
  return RatioArray

def DifferenceMaker(listFit, listToy):

  ResidualArray =[]

  for i in range(0, len(listToy)):
    if listToy[i] != 0:
      z = listToy[i] - listFit[i]
    else:
      z =0
    ResidualArray.append(z)
  return ResidualArray

