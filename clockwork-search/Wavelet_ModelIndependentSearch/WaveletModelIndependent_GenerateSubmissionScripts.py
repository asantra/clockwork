# This create.py script search the data folder and
# create condor submission file (condor.sub) for same problem with different arguments
import math, numpy, random, os, sys
from itertools import product

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')
argus = parser.parse_args()



#Directory
Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Wavelet_ModelIndependentSearch/"



Sig = str(argus.s)
Bkg = str(argus.b)
#event = str(argus.event)
test = int(argus.test)
train = int(argus.train)
Cone   = str(argus.Cone)
NconeR = int(argus.NconeR)
Uncertainty   = str(argus.uncertainty)
bins = int(argus.bin)

try:
  Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""
  
  if not os.path.exists(Directory+"WaveletModelIndependent_Condor_Outputs"):
      os.makedirs(Directory+"WaveletModelIndependent_Condor_Outputs")
      
  os.mkdir(Directory+"WaveletModelIndependent_Condor_Outputs/"+Output_Folder)
  os.mkdir(Directory+"WaveletModelIndependent_Condor_Outputs/"+Output_Folder+"/WaveletModelIndependentWeights_Condor")
  os.mkdir(Directory+"WaveletModelIndependent_Condor_Outputs/"+Output_Folder+"/Error_Condor")
  os.mkdir(Directory+"WaveletModelIndependent_Condor_Outputs/"+Output_Folder+"/Logs_Condor")
  os.mkdir(Directory+"WaveletModelIndependent_Condor_Outputs/"+Output_Folder+"/Out_Condor")
except OSError:
    print ("Creation of the directory %s failed" % Output_Folder)
else:
    print ("Successfully created the directory %s" % Output_Folder)

# Open file and write common part
cfile = open('condor_Wavelet_ModelIndependent.sub','w')

### works for python version >=3.6
common_command = f'requirements = (OpSysAndVer =?= "CentOS7") \n\
Executable = WaveletModelIndependent_GenerateSubmissionPreamble.sh \n\
arguments  = {Sig} {Bkg} {test} {bins} {train} {Cone} {NconeR} {Uncertainty} \n\
initialdir      = /afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/ \n\
Should_transfer_files = YES \n\
+JobFlavour = "testmatch" \n\
when_to_transfer_output = ON_EXIT \n\
error                 = Wavelet_ModelIndependentSearch/WaveletModelIndependent_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/Error_Condor/Wavelet_ModelIndependent_Condor_$(Process).err \n\
log                   = Wavelet_ModelIndependentSearch/WaveletModelIndependent_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/Logs_Condor/Wavelet_ModelIndependent_Condor_$(Process).log \n\
transfer_input_files= SignalBackground_Generation/Inputs_ee, SignalBackground_Generation/Inputs_ee/ToysfromMC.root, SignalBackground_Generation/Inputs_ee/ee_accEffFunction.root, SignalBackground_Generation/Inputs_ee/ee_relResolFunction.root, SignalBackground_Generation/Inputs_ee/finalbkg.root, SignalBackground_Generation/Inputs_ee/finalbkg_MC.root, SignalBackground_Generation/Inputs_ee/mInv_spectrum_combined_ll_rel21_fullRun2.root, SignalBackground_Generation/Inputs_ee/nominal_ee.root, SignalBackground_Generation/Inputs_yy, SignalBackground_Generation/Inputs_yy/TH1D_h024_HighLowMass_data.root, SignalBackground_Generation/Inputs_yy/finalbkg_yy.root, SignalBackground_Generation/Custom_Modules, SignalBackground_Generation/Custom_Modules/Background.py, SignalBackground_Generation/Custom_Modules/Basic.py, SignalBackground_Generation/Custom_Modules/Couplings.py, SignalBackground_Generation/Custom_Modules/Graviton.py, SignalBackground_Generation/Custom_Modules/KKcascade.py, SignalBackground_Generation/Custom_Modules/LSign.py, SignalBackground_Generation/Custom_Modules/Luminosity.py, SignalBackground_Generation/Custom_Modules/Merger.py, SignalBackground_Generation/Custom_Modules/PDFcalc.py, SignalBackground_Generation/Custom_Modules/Smearing.py, SignalBackground_Generation/Custom_Modules/TestS.py, SignalBackground_Generation/Custom_Modules/WT.py, SignalBackground_Generation/SigBkg_Functions.py,SignalBackground_Generation/Graviton_Properties, SignalBackground_Generation/Graviton_Properties/Dielectron_Transform_01GeV.txt, SignalBackground_Generation/Graviton_Properties/GravitonPropertiesDielectron.txt, SignalBackground_Generation/Graviton_Properties/GravitonPropertiesDiphoton36fb.txt, SignalBackground_Generation/Graviton_Properties/HighMass_GravitonProperties.txt, Wavelet_ModelIndependentSearch/Wavelet_ModelIndependentSearch.py, Wavelet_ModelIndependentSearch/WaveletModelIndependent_GenerateSubmissionPreamble.sh, Autoencoder/Autoencoder_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/AutoencoderWeights_Condor, Autoencoder/Autoencoder_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/AutoencoderWeights_Condor/Autoencoder_dimWX.npy, Autoencoder/Autoencoder_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/AutoencoderWeights_Condor/Autoencoder_dimWY.npy \n\
output     = Wavelet_ModelIndependentSearch/WaveletModelIndependent_Condor_Outputs/Outputs_{Bkg}_{Sig}_test{test}_train{train}_Cone-{Cone}_NconeR{NconeR}_Uncertainty-{Uncertainty}/Out_Condor/Wavelet_ModelIndependent_Condor_Out_$(Process).txt \n\
request_GPUs = 1 \n\
queue 1\n\n'

cfile.write(common_command)
 
