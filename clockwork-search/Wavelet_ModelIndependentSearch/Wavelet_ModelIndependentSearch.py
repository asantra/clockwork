
#Import
print("Starting program...")
print("")
print("Importing files...")

#Import standard
import math, numpy, scipy, random, os, sys, scipy.stats

from scipy.stats import norm

#Directories
Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Wavelet_ModelIndependentSearch/"


if not os.path.exists(Directory+"Wavelet_ModelIndependent_Plots"):
    os.makedirs(Directory+"Wavelet_ModelIndependent_Plots")
    
if not os.path.exists(Directory+"Pvalues"):
    os.makedirs(Directory+"Pvalues")

AutoencoderSource_Directory = "/afs/cern.ch/work/a/asantra/private/ClockWork/April2021/clockwork_project/clockwork-search/Autoencoder/"


#Import custom
sys.path.append(os.path.abspath('../SignalBackground_Generation'))
sys.path.append(os.path.abspath('../SignalBackground_Generation/Custom_Modules'))

from Custom_Modules import Merger, LSign, TestS, WT, Couplings
import SigBkg_Functions as SB

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)

#Machine learning
import keras, sklearn
from keras.layers            import Activation, Dense, Dropout, Flatten, Input, Lambda
from keras.layers            import Convolution2D, Conv1D, Conv2D, Conv2DTranspose
from keras.layers            import AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, MaxPooling2D, Reshape, UpSampling2D
from keras.layers.merge      import add, concatenate
from keras.models            import load_model, Model, Sequential
from sklearn.model_selection import train_test_split
from keras.utils import plot_model

#Function to pad matrices

#3D padding
def padder3D(matIn, L, M, N):

  #Convert to numpy array
  mattemp = numpy.array(matIn)

  #Read old dimensions
  dimXold = mattemp.shape[0]
  dimYold = mattemp.shape[1]
  dimZold = mattemp.shape[2]

  #Decide new dimensions
  dimXnew = math.ceil(dimXold/L)*L
  dimYnew = math.ceil(dimYold/M)*M
  dimZnew = math.ceil(dimZold/N)*N

  #Initialize new matrices with proper dimensions
  matOut = numpy.zeros((dimXnew, dimYnew, dimZnew))

  #Fill in the initial numbers
  matOut[:dimXold, :dimYold, :dimZold] = mattemp

  #Return padded matrix
  return matOut 

#2D padding
def padder2D(matIn, M, N):

  #Convert to numpy array
  mattemp = numpy.array(matIn)

  #Read old dimensions
  dimXold = mattemp.shape[0]
  dimYold = mattemp.shape[1]

  #Decide new dimensions
  dimXnew = math.ceil(dimXold/M)*M
  dimYnew = math.ceil(dimYold/N)*N

  #Initialize new matrices with proper dimensions
  matOut = numpy.zeros((dimXnew, dimYnew))

  #Fill in the initial numbers
  matOut[:dimXold, :dimYold] = mattemp

  #Return padded matrix
  return matOut  


print("Files imported.")
print("")

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-bin', metavar='', help='number of bins for TestStatistic_plots')
parser.add_argument('-train', metavar='', help='number of training steps')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-NconeR', metavar='', help='radius of cone to remove')
parser.add_argument('-uncertainty', metavar='', help='profile from uncertainity ensemble for toy experiments')
argus = parser.parse_args()

#Settings
print("Reading settings...")

#Binning
#If running Diphoton 36.7 fb-1 limits needs different binning
if(argus.b=="Dielectron_Bkg"):
  xmin = 225
  xmax = 6000
  nBins   = 5775 #2935
  mergeX  = 40 #20
  mergeY  = 4 #2
if(argus.b=="Diphoton_36fb_Bkg"):
  xmin = 150
  xmax = 2700
  nBins   = 2550
  mergeX  = 1 #20
  mergeY  = 1 #2
if(argus.b=="HighMass_Diphoton_Bkg"):
  xmin = 150 
  xmax = 5000 
  nBins = 4850 
  mergeX  = 40 #20
  mergeY  = 4 #2

massList = numpy.linspace(xmin, xmax, nBins)

#Cone of influence
Cone   = bool(argus.Cone)
NconeR = int(argus.NconeR) #2

#Define Test Statitic Binning
bins = int(argus.bin)
# bins = 10

#Make empty lists to appen Pvalues and test statistics
P_Value_List_Data = []
Pvalue_out_Data = open(Directory+"Pvalues/Pvalue_Dataout.txt","w")



#####Background#####
if(argus.b=="Dielectron_Bkg"):
  luminosity = 139
  year = '2015-18'
  BkgFix = SB.Dielectron_Bkg(xmin, xmax, nBins)

if(argus.b=="Diphoton_36fb_Bkg"):
  luminosity = 36.1
  year = '2015-16'
  BkgFix = SB.BkgFix_Diphoton36fb_Theory(massList)

if(argus.b=="HighMass_Diphoton_Bkg"):
  BkgFix = SB.HighMassDiphoton_Bkg(xmin, xmax, nBins)


#Generate Data - Use background toy pre unblinding
Data = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)

print("Signal/Bkg evaluated.")
print("")

Output_Folder = "Outputs_"+(argus.b)+"_"+(argus.s)+"_test"+(argus.test)+"_train"+(argus.train)+"_Cone-"+(argus.Cone)+"_NconeR"+(argus.NconeR)+"_Uncertainty-"+(argus.uncertainty)+""

nameModelFile = AutoencoderSource_Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor/Autoencoder_Background_NNweights_biases.txt"
ModelFile = AutoencoderSource_Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor/model_Background.h5"


#Number of trials for p-value map, background test statistic and signal + background test statistic
nTrials  = int(argus.test) #500
# nTrials  = 100



#Grid of toy experiments
print("Generating average map...")
print("")
toyExpGrid1, _                 = LSign.ToyExperimentGridMaker(xmin, xmax, BkgFix, mergeX, mergeY, nTrials, "Poisson")
wMinGrid, wMaxGrid, pvalueGrid = LSign.pvalueGridMaker(toyExpGrid1)

# Load dimWX and dimWY from Autoencoder setup
dimWX = numpy.load(AutoencoderSource_Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor/Autoencoder_dimWX.npy")
dimWY = numpy.load(AutoencoderSource_Directory+"Autoencoder_Condor_Outputs/"+Output_Folder+"/AutoencoderWeights_Condor/Autoencoder_dimWY.npy")
#Define model for region finder
print('Loading model...')

sizeX = 60
sizeY = 56

model1 = Sequential()

model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same', input_shape=(dimWX[0], dimWX[1], 1)))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Conv2D(128, kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(Flatten())
model1.add(Dense(40, activation='elu'))
model1.add(Dense(20, activation='elu')) #Encoded layer
model1.add(Dense(40, activation='elu'))
model1.add(Dense(int(dimWX[0]*dimWX[1]/16*128), activation='elu'))
model1.add(Reshape((int(dimWX[0]/4), int(dimWX[1]/4), 128)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(128,  kernel_size=(3, 3), activation='elu', padding='same'))
model1.add(UpSampling2D((2 ,2)))
model1.add(Conv2D(1, kernel_size=(3, 3), activation='elu', padding='same'))


model1.compile(optimizer='adam',loss='mean_squared_error')
model1.load_weights(nameModelFile)


#Apply machine learning
print("Starting new point...")
print("Evaluating signal...")


#Generate trial background + signal
#Initialize variables
print("Beginnning toy experiment of background only")
Lambda_Bonly = []

for i in range(0, nTrials):

  if (argus.uncertainty=="no"):
    
    #Generate random binned event
    eventsTrial1 = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)
    #Do wavelet transform
    cwtmatBonlyTrialNorm, _, _ =  WT.WaveletTransform(eventsTrial1, mergeX, mergeY, Cone, NconeR)
    cwtmatBonlyTrialNormdAv    = -numpy.log(LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatBonlyTrialNorm))
    cwtmatBonlyTrialNorm       =  cwtmatBonlyTrialNormdAv[0:sizeX, 0:sizeY]

    if(argus.b=="Diphoton_36_Bkg"):
      cwtmatBonlyTrialNorm = numpy.array(cwtmatBonlyTrialNorm)
    if(argus.b=="Dielectron_Bkg"):
      cwtmatBonlyTrialNorm = padder2D(cwtmatBonlyTrialNorm, 1, 4)

    

    #Format for predictions
    cwtmatBonlyTrialFlat      = numpy.ravel(cwtmatBonlyTrialNorm)
    cwtmatBonlyTrialFormatted = numpy.reshape(cwtmatBonlyTrialFlat,(1, dimWX[0], dimWX[1], 1))

    #Make prediction
    temp1 = model1.predict(cwtmatBonlyTrialFormatted)[0]

    #Apply test statistic
    rIn  = numpy.ravel(cwtmatBonlyTrialNorm)
    rOut = numpy.ravel(temp1)
    ts   = TestS.testStatCalc5(rIn, rOut)

    #Calculate test statistic FT
    Lambda_Bonly.append(ts)


print("Toy experiments of background only done.")
print("")

print("Start Evaluation of Prediction of Autoencoder of Data.")

#Generate Data - Use background toy pre unblinding
Data = SB.SigBkgPoissonToy(BkgFix, BkgFix, 1, 0)


cwtmatDataTrialNorm, _, _ =  WT.WaveletTransform(Data, mergeX, mergeY, Cone, NconeR)
cwtmatDataTrialNormdAv    = LSign.pvalueCalc(wMinGrid, wMaxGrid, pvalueGrid, cwtmatDataTrialNorm)
cwtmatDataTrialNorm       =  cwtmatDataTrialNormdAv[0:sizeX, 0:sizeY]

if(argus.b=="Diphoton_36_Bkg"):
  cwtmatDataTrialNorm = numpy.array(cwtmatDataTrialNorm)
if(argus.b=="Dielectron_Bkg"):
  cwtmatDataTrialNorm = padder2D(cwtmatDataTrialNorm, 1, 4)

#Format for predictions
cwtmatDataTrialFlat      = numpy.ravel(cwtmatDataTrialNorm)
cwtmatDataTrialFormatted = numpy.reshape(cwtmatDataTrialFlat,(1, dimWX[0], dimWX[1], 1))

#Make prediction
tempData = model1.predict(cwtmatDataTrialFormatted)[0]

#Apply test statistic
rIn  = numpy.ravel(cwtmatDataTrialNorm)
rOut = numpy.ravel(tempData)
ts_Data   = TestS.testStatCalc5(rIn, rOut)

print("Start Evaluation of P-value using Mock Data.")
print("")

B_Only_SortedList     = numpy.sort(Lambda_Bonly)
B_OnlyMedian = B_Only_SortedList[int(numpy.floor(0.5*len(Lambda_Bonly)))]

#Make histograms
Lambda_Bonly_hist = numpy.histogram(Lambda_Bonly, bins)
Lambda_Bonly_rv = scipy.stats.rv_histogram(Lambda_Bonly_hist)

#Try and caluclate P-value for Exclusion median and write to list
P_Value_Data = 1-(Lambda_Bonly_rv.cdf(ts_Data))
P_Value_List_Data.append(P_Value_Data)


print("Method: Data P-value value is:", min(P_Value_List_Data, default=0))
P_Value_List_Data.append(min(P_Value_List_Data,default=0))

numpy.savetxt(Pvalue_out_Data, P_Value_List_Data, fmt="%1.10f")


#Test Statistic Plotting

Probably_Dist, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
plt.hist(Lambda_Bonly, bins,label="Bkg Only", alpha=0.4)
plt.ticklabel_format(style = 'plain')
plt.axvline(x=B_OnlyMedian,color='black', linestyle='--', label = "B_OnlyMedian")
plt.axvline(x=ts_Data,color='red', linestyle='--', label = "MockData Prediction")
plt.xlabel(r'$\mathrm{y(n)}$', ha='right', x=1.0)
# ax.set_ylabel('', ha='right', y=1.0)
leg = plt.legend(borderpad=0.5, frameon=False, loc='best')
plt.legend()
# plt.margins(0)
Probably_Dist.savefig(Directory+"Wavelet_ModelIndependent_Plots/TestDistribution.png")



#Generate trial background + signal
#Initialize variables
print("Beginnning Wavelet local p-value experiment of background only")

#Do wavelet transform
yticks1 = [math.log(1), math.log(3), math.log(10), math.log(30), math.log(100), math.log(300), math.log(1000)]
yticks2 = (1, 3, 10, 30, 100, 300, 1000)
binwidth = (xmax - xmin)/nBins


#Do wavelet transform of the Fixed signal
fig_Wavelet, ax = plt.subplots()
ax = hep.atlas.label(data=False, paper=False, year=year, fontsize=19, lumi = luminosity, ax=ax)
pos = plt.imshow(numpy.flipud(numpy.transpose(cwtmatDataTrialNormdAv)), extent=[xmin, xmax, math.log(binwidth), math.log(binwidth*2**(cwtmatDataTrialNormdAv.shape[1]*mergeY/WT.Nbins))], aspect='auto', vmax=abs(cwtmatDataTrialNormdAv).max(), vmin=0, interpolation='nearest')
plt.yticks(yticks1, yticks2)
plt.xlabel(r'$\mathrm{m_{ee} [GeV]}$', ha='right', x=1.0)
plt.ylabel(r'Scale a [$\mathrm{Frequency^{-1}}]$', ha='right', y=1.0)
plt.xlim(right=xmax)
plt.colorbar()
axes = plt.gca()
fig_Wavelet.savefig(Directory+"Wavelet_ModelIndependent_Plots/Loval_PvalueScalogram.png")  

print("Wavelet local p-value experiment done.")
print("Done")

################################################################################################
################################################################################################
