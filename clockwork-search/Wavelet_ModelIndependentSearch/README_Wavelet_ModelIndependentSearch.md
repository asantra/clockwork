To run the model indpendent wavelet script the following command structure is to be used:

python3 Wavelet_ModelIndependentSearch.py -s Theory_Diphoton_36fb_Sig -b Diphoton_36fb_Bkg  -test 1000 -bin 20 -train 5000 -Cone False -NconeR 2 -uncertainty no

In principle we can use any -b command:

Dielectron_Bkg
Diphoton_36fb_Bkg
HighMass_Diphoton_Bkg

HOWEVER there must be an Outputs folder made in the Autoiencoder folder, in the Autoencoder.

This script reads from that folder from the above arguments you give - so they must match up.

A 1000 trials will take a long time to run - but it is needed to read the relevant output file in this case -  this can be bypassed by using the command for 1000 trials and 20 bins  in the terminal, but where these options are defined commenting out the read in of the argument (not in the Output folder name though), for some smaller number. This is useful for debugging. It could be a good idea to add a debug option here [todo].
