import ROOT
from ROOT import *
import array
import math
import AtlasStyle
from os import path

style = AtlasStyle.AtlasStyle()
ROOT.gROOT.SetStyle( style.GetName() )
ROOT.gROOT.ForceStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE);
ROOT.TGaxis.SetMaxDigits( 4 )
c1 = TCanvas()
c1.SetFrameFillColor(ROOT.kWhite);
c1.SetFillColor(ROOT.kWhite);
ROOT.gStyle.SetOptStat(0)

ROOT.gROOT.SetBatch(1)
ROOT.TH1.StatOverflows(ROOT.kTRUE)
ROOT.TH1.SetDefaultSumw2(ROOT.kTRUE);

if path.exists('rootlogon.C'):
  ROOT.gROOT.Macro('rootlogon.C')

canvas = ROOT.TCanvas("canvas","", 800, 800)

def plotting(sample_set, variable, nbins, bmin, bmax, axis_title, output_name, leading=True): 
  
  f1 = ROOT.TFile("Pythia8_G"+str(sample_set[0])+"_k001_electron_2.root")
  f2 = ROOT.TFile("Pythia8_G"+str(sample_set[1])+"_k001_electron_2.root")
  f3 = ROOT.TFile("Pythia8_G"+str(sample_set[2])+"_k001_electron_2.root")
  f4 = ROOT.TFile("Pythia8_G"+str(sample_set[3])+"_k001_electron_2.root")
  f5 = 0
  if(len(sample_set) == 5):
    f5 = ROOT.TFile("Pythia8_G"+str(sample_set[4])+"_k001_electron_2.root")

  tree1 = f1.Get("myTree")
  tree2 = f2.Get("myTree")
  tree3 = f3.Get("myTree")
  tree4 = f4.Get("myTree")
  tree5 = 0
  if(len(sample_set) == 5):
    tree5 = f5.Get("myTree")

  h1 = ROOT.TH1D("h1","",nbins,bmin,bmax)
  h2 = ROOT.TH1D("h2","",nbins,bmin,bmax)
  h3 = ROOT.TH1D("h3","",nbins,bmin,bmax)
  h4 = ROOT.TH1D("h4","",nbins,bmin,bmax)
  h5 = 0
  if(len(sample_set) == 5):
    h5 = ROOT.TH1D("h5","",nbins,bmin,bmax)

  drawString1 = ""
  drawString2 = ""
  drawString3 = ""
  drawString4 = ""
  drawString5 = ""
  drawString = ""
  selString = ""

  if("selection" in variable):
    selString = "truth_lepton_pt[0] > 30 && truth_lepton_pt[1] > 30 && TMath::Abs(truth_lepton_eta[0]) < 2.47 && TMath::Abs(truth_lepton_eta[1]) < 2.47"
    entries1 = tree1.GetEntries()      
    cuts1 = tree1.GetEntries(selString)
    eff1 = float(cuts1)/float(entries1)
    entries2 = tree2.GetEntries()      
    cuts2 = tree2.GetEntries(selString)
    eff2 = float(cuts2)/float(entries2)
    entries3 = tree3.GetEntries()      
    cuts3 = tree3.GetEntries(selString)
    eff3 = float(cuts3)/float(entries3)
    entries4 = tree4.GetEntries()
    cuts4 = tree4.GetEntries(selString)
    eff4 = float(cuts4)/float(entries4)
    entries5 = 0
    cuts5 = 0
    eff5 = 0
    if(len(sample_set) == 5):
      entries5 = tree5.GetEntries()
      cuts5 = tree5.GetEntries(selString)
      eff5 = float(cuts5)/float(entries5)
    with open(output_name, 'a+') as f:
      stringy = "m_G = " + str(sample_set[0]) + " Efficiency = " + str(eff1) + "\n"
      stringy += "m_G = " + str(sample_set[1]) + " Efficiency = " + str(eff2) + "\n"
      stringy += "m_G = " + str(sample_set[2]) + " Efficiency = " + str(eff3) + "\n"
      stringy += "m_G = " + str(sample_set[3]) + " Efficiency = " + str(eff4) + "\n" 
      if(len(sample_set) == 5):
        stringy += "m_G = " + str(sample_set[4]) + " Efficiency = " + str(eff5)
      print >> f, stringy
    return 0
  if(variable == "mass"):
    drawString = "dimass(truth_lepton_pt[0], truth_lepton_eta[0], truth_lepton_phi[0],"
    drawString += "truth_lepton_m[0], truth_lepton_pt[1], truth_lepton_eta[1],"
    drawString += "truth_lepton_phi[1], truth_lepton_m[1])"
    selString = "truth_lepton_parent[0] == 5100039 && truth_lepton_parent[1] == 5100039"
  else:
    if(leading):
      drawString = str(variable)+"[0]"
      selString = "truth_lepton_pt[0] > truth_lepton_pt[1]"
    else:
      drawString = str(variable)+"[1]"
      selString = "truth_lepton_pt[0] > truth_lepton_pt[1]"
  drawString1 = drawString + " >> h1"
  drawString2 = drawString + " >> h2"
  drawString3 = drawString + " >> h3"
  drawString4 = drawString + " >> h4"
  if(len(sample_set) == 5):
    drawString5 = drawString + " >> h5"

  tree1.Draw(drawString1,selString)
  tree2.Draw(drawString2,selString)
  tree3.Draw(drawString3,selString)
  tree4.Draw(drawString4,selString)
  if(len(sample_set) == 5):
    tree5.Draw(drawString5,selString)

  if(variable != "mass"):
    if(leading):
      drawString = str(variable)+"[1]"
    else:
      drawString = str(variable)+"[0]"
    drawString1 = drawString + " >>+h1"
    drawString2 = drawString + " >>+h2"
    drawString3 = drawString + " >>+h3"
    drawString4 = drawString + " >>+h4"
    if(len(sample_set) == 5):
      drawString5 = drawString + " >>+h5"
    selString = "(truth_lepton_pt[0] < truth_lepton_pt[1])"
    tree1.Draw(drawString1,selString)
    tree2.Draw(drawString2,selString)
    tree3.Draw(drawString3,selString)
    tree4.Draw(drawString4,selString)
    if(len(sample_set) == 5):
      tree5.Draw(drawString5,selString)

  if(len(sample_set) == 5):
    print(h1.Integral(), h5.Integral())
  
  h1.Scale(1./h1.GetMaximum())
  h2.Scale(1./h2.GetMaximum())
  h3.Scale(1./h3.GetMaximum())
  h4.Scale(1./h4.GetMaximum())
  if(len(sample_set) == 5):
    h5.Scale(1./h5.GetMaximum())

  leg = ROOT.TLegend(0.65,0.7,0.83,0.9)
  leg.AddEntry(h1,"mG* = "+str(sample_set[0])+" GeV","l")
  leg.AddEntry(h2,"mG* = "+str(sample_set[1])+" GeV","l")
  leg.AddEntry(h3,"mG* = "+str(sample_set[2])+" GeV","l")
  leg.AddEntry(h4,"mG* = "+str(sample_set[3])+" GeV","l")
  if(len(sample_set) == 5):
    leg.AddEntry(h5,"mG* = "+str(sample_set[4])+" GeV","l")
  leg.SetTextSize(0.035)

  h1.SetLineColor(ROOT.kBlue)
  h2.SetLineColor(ROOT.kRed)
  h3.SetLineColor(ROOT.kMagenta)
  h4.SetLineColor(ROOT.kGreen)
  if(len(sample_set) == 5):
    h5.SetLineColor(ROOT.kCyan)
  h1.SetMaximum(1.5)
  h1.GetXaxis().SetTitle(axis_title)
  h1.GetYaxis().SetTitle("Arb. Units")
  if("eta" not in variable):
    h1.SetNdivisions(-505);

  h1.Draw("HIST")
  h2.Draw("HIST,SAME")
  h3.Draw("HIST,SAME")
  h4.Draw("HIST,SAME")
  if(len(sample_set) == 5):
    h5.Draw("HIST,SAME")

  leg.Draw("SAME")
  canvas.Print(output_name)
  canvas.Clear()

plotting([200, 300, 400, 500, 600], "mass", 500, 0, 1000, "m_{ee} [GeV]", "Graviton_Plots/dielec_mass_200_600.pdf")
plotting([700, 800, 900, 1500, 2500], "mass", 1250, 500, 3000, "m_{ee} [GeV]", "Graviton_Plots/dielec_mass_700_2500.pdf")
plotting([3500, 4500, 5500, 6000], "mass", 2000, 3000, 7000, "m_{ee} [GeV]", "Graviton_Plots/dielec_mass_3500_6000.pdf")

plotting([200, 300, 400, 500, 600], "truth_lepton_pt", 250, 0, 500, "p_{T}^{e_{1}} [GeV]", "Graviton_Plots/dielec_truth_leading_lepton_pt_200_600.pdf")
plotting([700, 800, 900, 1500, 2500], "truth_lepton_pt", 625, 250, 1500, "p_{T}^{e_{1}} [GeV]", "Graviton_Plots/dielec_truth_leading_lepton_pt_700_2500.pdf")
plotting([3500, 4500, 5500, 6000], "truth_lepton_pt", 1000, 1500, 3500, "p_{T}^{e_{1}} [GeV]", "Graviton_Plots/dielec_truth_leading_lepton_pt_3500_6000.pdf")

plotting([200, 300, 400, 500, 600], "truth_lepton_pt", 250, 0, 500, "p_{T}^{e_{2}} [GeV]", "Graviton_Plots/dielec_truth_subleading_lepton_pt_200_600.pdf", False)
plotting([700, 800, 900, 1500, 2500], "truth_lepton_pt", 625, 250, 1500, "p_{T}^{e_{2}} [GeV]", "Graviton_Plots/dielec_truth_subleading_lepton_pt_700_2500.pdf", False)
plotting([3500, 4500, 5500, 6000], "truth_lepton_pt", 1000, 1500, 3500, "p_{T}^{e_{2}} [GeV]", "Graviton_Plots/dielec_truth_subleading_lepton_pt_3500_6000.pdf", False)

plotting([200, 300, 400, 500, 600], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_leading_lepton_eta_200_600.pdf")
plotting([700, 800, 900, 1500, 2500], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_leading_lepton_eta_700_2500.pdf")
plotting([3500, 4500, 5500, 6000], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_leading_lepton_eta_3500_6000.pdf")

plotting([200, 300, 400, 500, 600], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{2}}", "Graviton_Plots/dielec_truth_subleading_lepton_eta_200_600.pdf", False)
plotting([700, 800, 900, 1500, 2500], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{2}}", "Graviton_Plots/dielec_truth_subleading_lepton_eta_700_2500.pdf", False)
plotting([3500, 4500, 5500, 6000], "truth_lepton_eta", 121, -6, 6, "#eta^{e_{2}}", "Graviton_Plots/dielec_truth_subleading_lepton_eta_3500_6000.pdf", False)
plotting([200,300,400,500,600], "truth_selection", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_table.txt")
plotting([700,800,900,1500,2500], "truth_selection", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_table.txt")
plotting([3500,4500,5500,6000], "truth_selection", 121, -6, 6, "#eta^{e_{1}}", "Graviton_Plots/dielec_truth_table.txt")
