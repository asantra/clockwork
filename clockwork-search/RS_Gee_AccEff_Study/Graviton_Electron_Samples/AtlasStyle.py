#!/usr/bin/env python
import ROOT
from ROOT import TCanvas
import array
import math

class AtlasStyle( ROOT.TStyle ):
    def __init__( self, name = "AtlasStyle", title = "ATLAS style object" ):
        ROOT.TStyle.__init__( self, name, title )
        self.SetName( name )
        self.SetTitle( title )
        self.configure()

        return
    def configure( self ):
        self.Info( "configure", "Configuring default ATLAS style" )
        icol = 0
        self.SetFrameBorderMode( 0 )
        self.SetFrameFillColor( icol )
        self.SetFrameFillStyle( 0 )
        self.SetCanvasBorderMode( 0 )
        self.SetPadBorderMode( 0 )
        self.SetPadColor( icol )
        self.SetCanvasColor( icol )
        self.SetStatColor( icol )
        self.SetPaperSize( 20, 26 )
        self.SetPadTopMargin( 0.05 )
        self.SetPadRightMargin( 0.05 )
        self.SetPadBottomMargin( 0.16 )
        self.SetPadLeftMargin( 0.16 )
        self.SetTitleXOffset(1.4)
        self.SetTitleYOffset(1.4)
        #For TH2's use the following
        #self.SetTitleYOffset(1.)
        #self.SetPadRightMargin( 0.16 )
        font_type = 42
        font_size = 0.05
        self.SetTextFont( font_type )
        self.SetTextSize( font_size )
        self.SetLabelFont( font_type, "x")
        self.SetLabelSize( font_size, "x" )
        self.SetTitleFont( font_type, "x" )
        self.SetTitleSize( font_size, "x" )
        self.SetLabelFont( font_type, "y" )
        self.SetLabelSize( font_size, "y" )
        self.SetTitleFont( font_type, "y" )
        self.SetTitleSize( font_size, "y" )
        self.SetLabelFont( font_type, "z" )
        self.SetLabelSize( font_size, "z" )
        self.SetTitleFont( font_type, "z" )
        self.SetTitleSize( font_size, "z" )
        
        self.SetMarkerStyle( 20 )
        self.SetMarkerSize( 1.2 )
        self.SetHistLineWidth( 2 )
        self.SetLineStyleString( 2, "[12 12]" )

        self.SetOptTitle( 0 )
        self.SetOptStat( 0 )
        self.SetOptFit( 0 )

        self.SetPadTickX( 1 )
        self.SetPadTickY( 1 )
        self.SetLegendFont(42)
        return
def AtlasLabel( x, y, color = 1):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont( 72 )
    l.SetTextColor( color )
    l.DrawLatex( x, y, "ATLAS" )
    # Draw the "Preliminary" part:
    l.SetTextFont( 42 )
    l.DrawLatex( x + 0.12, y, "Internal" )
    return
def AtlasSimLabel( x, y, color = 1):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont( 72 )
    l.SetTextColor( color )
    l.DrawLatex( x, y, "ATLAS" )
    # Draw the "Preliminary" part:
    l.SetTextFont( 42 )
    l.DrawLatex( x + 0.12, y, "Simulation" )
    return
def DrawText( x, y, text, color=1 ):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextColor( color )
    l.DrawLatex( x, y, text )
    return
def DrawLuminosity( x, y, lumi, color = 1 ):
    DrawText( x, y, "#intLdt = " + str( lumi ) + " fb^{-1}", color )
    return
