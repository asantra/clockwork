#!/bin/bash

##########################################
#############Classifier###################
##########################################

######Run 36fb-1 Diphoton estimates 
# cd clockwork-search/Classifier/Condor_Submission_Files/
# python3 ClassiferTesting_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -test 1000 -bin 20 -train 4000 -kmin 200 -M5min 1000 -kmax 2700 -M5max 10000 -M5points 30 -kpoints 30 -Cone False -NconeR 2
# condor_submit condor_testing.sub -batch-name Cl_ee_36_Test_float 
# cd ../../../
# echo "Run 36fb-1 yy Classifier Testing Done"


# ######Run 139fb-1 Dielectron estimates 
# cd clockwork-search/Classifier/Condor_Submission_Files/
# python3 ClassiferTesting_GenerateSubmissionScripts.py -t Poisson -s Theory_Dielectron_Sig -b Dielectron_Bkg -event float -test 1000 -bin 20 -train 4000 -kmin 200 -M5min 1000 -kmax 1700 -M5max 8000 -M5points 30 -kpoints 30 -Cone False -NconeR 2 -uncertainty yes
# condor_submit condor_testing.sub -batch-name Cl_ee_139_Test_float_uncertainties
# cd ../../../
# echo "Run 139fb-1 ee Classifier Done"


##########################################
#############Autoencoder##################
##########################################
# # Run 139fb-1 Dielectron estimates 
# cd clockwork-search/Autoencoder/Condor_Submission_Files/
# python3 Autoencoder_Testing_GenerateSubmissionScripts.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -bin 20 -train 5000 -kmin 200 -M5min 1000 -kmax 2700 -M5max 10000 -M5points 30 -kpoints 30 -Cone False -NconeR 2 -uncertainty no
# 
# ##### for local running
# ## python3 Offline_Autoencoder_Testing.py -k 200 -M5 3172 -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -bin 20 -train 5000 -Cone False -NconeR 2 -uncertainty yes
# 
# 
# 
# condor_submit condor_testing_Autoencoder.sub -batch-name At_ee_36_Test_float 
# cd ../../../
# echo "Run 139fb-1 ee Autoencoder Testing Done"



#############################################
##################FFT########################
#############################################

# cd clockwork-search/FFT/Condor_Submission_Files/
# # python3 Fourier_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -lumDiv True -test 1000 -kmin 200 -M5min 1000 -kmax 2700 -M5max 10000 -M5points 30 -kpoints 30 -bin 20 -stepFT 100 -bkgsub False -uncertainty no
# python3 Fourier_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -lumDiv True -test 10 -kmin 200 -M5min 1000 -kmax 200 -M5max 1000 -M5points 1 -kpoints 1 -bin 3 -stepFT 10 -bkgsub False -uncertainty no
# # python3 Fourier_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -test 1000 -kmin 2441 -M5min 6586 -kmax 2441 -M5max 6586 -M5points 1 -kpoints 1 -bin 20 -stepFT 100 -bkgsub False -u no
# condor_submit condor_Fourier.sub -batch-name FT_ee_36_float
# cd ../../../
# echo "Run 36fb-1 ee FFT Done"



###### Model Independent Wavelet study ########
### local running
cd clockwork-search/Wavelet_ModelIndependentSearch/
time python3 Wavelet_ModelIndependentSearch.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -bin 20 -train 5000 -Cone False -NconeR 2 -uncertainty no
