#!/bin/bash
#format for each project
#Classifier:
#RegionFinder:
#Autoencoder:
#FFT:

##########################################
#############Classifier###################
##########################################

# ######Run 36fb-1 Diphoton estimates 
# cd clockwork-search/Classifier/Condor_Submission_Files/
# python3 ClassiferTraining_GenerateSubmissionScripts.py -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -test 1000 -train 4000 -kmin 200 -M5min 1000 -kmax 2700 -M5max 10000 -M5points 30 -kpoints 30 -Cone False -NconeR 2 -uncertainty no
# condor_submit condor_training.sub -batch-name Cl_ee_36_Train_float 
# cd ../../../
# echo "Run 36fb-1 yy Classifier Done"


######Run 139fb-1 Dielectron estimates 
# cd clockwork-search/Classifier/Condor_Submission_Files/
# python3 ClassiferTraining_GenerateSubmissionScripts.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -train 4000 -kmin 200 -M5min 1000 -kmax 1700 -M5max 8000 -M5points 30 -kpoints 30 -Cone False -NconeR 2 -uncertainty yes
# condor_submit condor_training.sub -batch-name Cl_ee_139_Train_float
# cd ../../../
# echo "Run 139fb-1 ee Classifier Done"


##########################################
#############Autoencoder##################
##########################################
### local run ###
# cd clockwork-search/Autoencoder/
# python3 Offline_Autoencoder_Trainer.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 5 -train 20  -Cone False -NconeR 2 -uncertainty yes
# 
# cd ../../


#### condor run ###
##########################################

cd clockwork-search/Autoencoder/Condor_Submission_Files/
python3 Autoencoder_Training_GenerateSubmissionScripts.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -train 5000  -Cone False -NconeR 2 -uncertainty yes
condor_submit condor_training_Autoencoder.sub -batch-name At_ee_139_Train_float
cd ../../../
echo "Run 139fb-1 ee Autoencoder Done"

# cd clockwork-search/Autoencoder/Condor_Submission_Files/
# python3 Autoencoder_Training_GenerateSubmissionScripts.py -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -test 1000 -train 5000  -Cone False -NconeR -uncertainty yes
# condor_submit condor_training_Autoencoder.sub -batch-name At_ee_36_Train_float
# cd ../../../
# echo "Run 36fb-1 ee Autoencoder Done"

#############################################################
#############Autoencoder Model Independent ##################
#############################################################
# #### local running
# cd clockwork-search/Wavelet_ModelIndependentSearch
# time python3 Wavelet_ModelIndependentSearch.py -s Theory_Diphoton_36fb_Sig -b Diphoton_36fb_Bkg  -test 1000 -bin 20 -train 5000 -Cone False -NconeR 2 -uncertainty no
# cd ../..
# echo "Run 139fb-1 ee Autoencoder Done"


#### condor running

# cd clockwork-search/Wavelet_ModelIndependentSearch
# python3 WaveletModelIndependent_GenerateSubmissionScripts.py -s Theory_Dielectron_Sig -b Dielectron_Bkg -test 1000 -train 5000  -Cone False -NconeR 2 -uncertainty no -bin 20
# condor_submit condor_Wavelet_ModelIndependent.sub -batch-name At_ee_139_WaveletModelIndependent
# cd ../../
# 
# echo "Run 139fb-1 ee Wavelet_ModelIndependent Done"


#############################################
##################FFT########################
#############################################

# cd clockwork-search/FFT/Condor_Submission_Files/
# python3 Fourier_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -test 1000 -kmin 200 -M5min 1000 -kmax 2700 -M5max 10000 -M5points 30 -kpoints 30 -bins 20 -stepFT 100 -bkgsub False -u no
# condor_submit condor_training_Autoencoder.sub -batch-name At_ee_36_Train_float
# cd ../../../
# echo "Run 36fb-1 ee Autoencoder Done"


##########################################
#############RegionFinder#################
##########################################
