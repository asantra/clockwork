#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)

import mplhep as hep
hep.set_style(hep.style.ROOT)

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-s_ee', metavar='', help='Signal_ee Model type')
parser.add_argument('-b_ee', metavar='', help='Background_ee Model type')
parser.add_argument('-s_yy', metavar='', help='Signal_yy Model type')
parser.add_argument('-b_yy', metavar='', help='Background_yy Model type')
argus = parser.parse_args()

#import ATLAS MPL Style
# import mplhep as hep
# plt.style.use(hep.style.ROOT)
# import atlas_mpl_style

S_ee = str(argus.s_ee)
B_ee = str(argus.b_ee)

S_yy = str(argus.s_yy)
B_yy = str(argus.b_yy)

K_List_ee = numpy.linspace(200,5000,25)
M5_List_ee = numpy.linspace(1000, 20000, 25)

K_List_yy = numpy.linspace(175,4500,25)
M5_List_yy = numpy.linspace(1000, 20000, 25)

N_ee, M_ee = len(K_List_ee), len(M5_List_ee)
Z_ee = numpy.zeros((N_ee, M_ee))
K_List_2D_ee, M5_List_2D_ee = numpy.meshgrid(M5_List_ee, K_List_ee)
for i, (x_ee,y_ee) in enumerate(product(K_List_ee,M5_List_ee)):
    Z_ee[numpy.unravel_index(i, (N_ee,M_ee))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/Outputs_"+B_ee+"_"+S_ee+"/Pvalues_Condor/Pvalue_Method1_Discovery"+str(int(x_ee))+".0"+"_"+str(int(y_ee))+".0"+"out.txt")

N_yy, M_yy = len(K_List_yy), len(M5_List_yy)
Z_yy = numpy.zeros((N_yy, M_yy))
K_List_2D_yy, M5_List_2D_yy = numpy.meshgrid(M5_List_yy, K_List_yy)
for i, (x_yy,y_yy) in enumerate(product(K_List_yy,M5_List_yy)):
    Z_yy[numpy.unravel_index(i, (N_yy,M_yy))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search_yy/Classifier/Classifier_Condor_Outputs/Outputs_"+B_yy+"_"+S_yy+"/Pvalues_Condor/Pvalue_Method1_Discovery"+str(int(x_yy))+".0"+"_"+str(int(y_yy))+".0"+"out.txt")




# #Make Contour plots of Pvalues
# fig_pvalue_contour_ee,ax=plt.subplots(1,1)
# # cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
# c_alpha_ee = ax.contourf(K_List_2D_ee, M5_List_2D_ee, Z_ee, levels = (0.049,0.051))
# ax.set_xlabel('M5 [GeV]', ha='right', x=1.0)
# ax.set_ylabel('k [GeV]', ha='right', y=1.0)
# ax.semilogy()
# plt.xlim((1000,12000))
# fig_pvalue_contour_ee.colorbar(c_alpha_ee) # Add a colorbar to a plot
# fig_pvalue_contour_ee.savefig('Exclusion_plots/P-valueMap_ee.png')

# #Make Contour plots of Pvalues
# fig_pvalue_contour_yy,ax=plt.subplots(1,1)
# # cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
# c_alpha_yy = ax.contourf(K_List_2D_yy, M5_List_2D_yy, Z_yy, levels = (0.049,0.051))
# ax.set_xlabel('M5 [GeV]', ha='right', x=1.0)
# ax.set_ylabel('k [GeV]', ha='right', y=1.0)
# ax.semilogy()
# plt.xlim((1000,12000))
# fig_pvalue_contour_yy.colorbar(c_alpha_yy) # Add a colorbar to a plot
# fig_pvalue_contour_yy.savefig('Exclusion_plots/P-valueMap_yy.png')

# cntr1 = ax.contour(X, Y, Z1, colors='k')
# cntr2 = ax.contour(X, Y, Z2, colors='C0')
# h1,_ = cntr1.legend_elements()
# h2,_ = cntr2.legend_elements()
# ax.legend([h1[0], h2[0]], ['Contour 1', 'Contour 2'])

#Make Contour plots of Pvalues
fig_pvalue_contour,ax =plt.subplots(1,1)
ax = hep.atlas.label(data=False, paper=False, year='2015-18', fontsize=19, lumi = 139, ax=ax)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
c_alpha_ee = plt.contourf(K_List_2D_ee, M5_List_2D_ee, Z_ee, levels = (0.044,0.046), colors = ['g'])
c_alpha_yy = plt.contourf(K_List_2D_yy, M5_List_2D_yy, Z_yy, levels = (0.044,0.046),colors = ['b'])
h1,_ = c_alpha_ee.legend_elements()
h2,_ = c_alpha_yy.legend_elements()
plt.xlabel('M5 [GeV]', ha='right', x=1.0)
plt.ylabel('k [GeV]', ha='right', y=1.0)
plt.semilogy()
plt.xlim((1000,12000))
plt.ylim((200,4500))
plt.legend([h1[0], h2[0]], ['Classifier_ee', 'Classifier_yy'])
# fig_pvalue_contour_yy.colorbar(c_alpha_yy) # Add a colorbar to a plot
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap.png')

