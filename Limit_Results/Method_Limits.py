#Import standard
import math, numpy, scipy, random, os, sys
from numpy import loadtxt
# np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import scipy.stats
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
print(matplotlib.__version__)
#Import custom
# sys.path.insert(0, '/afs/cern.ch/user/s/slawlor/work/Draft_Clockwork_Analysis/clockwork-search/Classifier/Custom_Modules')
# from Custom_Modules import WT
# import SigBkg_Classifier as SB
import mplhep as hep
hep.set_style(hep.style.ROOT)

from itertools import product
import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-t', metavar='', help='Type of Toys used to generate distributions')
# parser.add_argument('-k', metavar='', help='K-value Of Clockwork Signal Model')
# parser.add_argument('-M5', metavar='', help='M5-value Of Clockwork Signal Model')
parser.add_argument('-s', metavar='', help='Signal Model type')
parser.add_argument('-b', metavar='', help='Background Model type')
parser.add_argument('-event', metavar='', help='Event type')
parser.add_argument('-test', metavar='', help='number of toy experiments')
parser.add_argument('-train_Cl', metavar='', help='number of training steps Classifier')
parser.add_argument('-train_At', metavar='', help='number of training steps Autoencoder')
parser.add_argument('-kmin', metavar='', help='min k value to scan')
parser.add_argument('-M5min', metavar='', help='min M5 value to scan')
parser.add_argument('-kmax', metavar='', help='max k value to scan')
parser.add_argument('-M5max', metavar='', help='max M5 value to scan')
parser.add_argument('-M5points', metavar='', help='M5 grid points')
parser.add_argument('-kpoints', metavar='', help='k grid points')
parser.add_argument('-Cone', metavar='', help='remove cone of influence')
parser.add_argument('-stepFT', metavar='', help='Step size of window - see Fourier_Transform.py for definition')
parser.add_argument('-bkgsub', metavar='', help='Subtract background from Bkg+Sig Toy')
argus = parser.parse_args()


kmin = int(argus.kmin)
M5min = int(argus.M5min)
kmax = int(argus.kmax)
M5max = int(argus.M5max)
M5points = int(argus.M5points)
kpoints = int(argus.kpoints)
stepFT = int(argus.stepFT)
bkgsub = str(argus.bkgsub)


Output_Folder_Cl = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_train"+(argus.train_Cl)+"_Cone"+(argus.Cone)+""
Output_Folder_At = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_train"+(argus.train_At)+"_Cone"+(argus.Cone)+""
Output_Folder_FT = "Outputs_"+(argus.t)+"_"+(argus.b)+"_"+(argus.s)+"_"+(argus.event)+"_test"+(argus.test)+"_bkgsub"+(argus.bkgsub)+"_stepFT"+(argus.stepFT)+""


#import ATLAS MPL Style
# import mplhep as hep
# plt.style.use(hep.style.ROOT)
# import atlas_mpl_style


K_List = numpy.linspace(kmin,kmax,kpoints)
M5_List = numpy.linspace(M5min, M5max, M5points)
# K_List = numpy.linspace(250,5500,30)
# M5_List = numpy.linspace(1000, 20000, 25)

N, M = len(K_List), len(M5_List)
Z_Cl = numpy.zeros((N, M))
Z_At = numpy.zeros((N, M))
Z_FT = numpy.zeros((N, M))
K_List_2D, M5_List_2D = numpy.meshgrid(M5_List, K_List)
for i, (x,y) in enumerate(product(K_List,M5_List)):
    Z_Cl[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Classifier/Classifier_Condor_Outputs/"+Output_Folder_Cl+"/Pvalues_Condor/Pvalue_Method2_Discovery"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")
    Z_At[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/Autoencoder/Autoencoder_Condor_Outputs/"+Output_Folder_At+"/Pvalues_Condor/Pvalue_Method2_Discovery"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")
    Z_FT[numpy.unravel_index(i, (N,M))] = loadtxt("/scratch2/slawlor/Clockwork_Project/clockwork-search/FFT/Fourier_Condor_Output/"+Output_Folder_FT+"/Pvalues_Condor/Pvalue_Method2_Discovery"+str(int(x))+".0"+"_"+str(int(y))+".0"+"out.txt")

#Import 36fb-1 from the theory paper
# N_Theory, M_Theory = len(K_List_Theory), len(M5_List_Theory)
#Classifier
Classifier_Theory = numpy.loadtxt('Classifiermerged.txt')
M5_List_Classifier_Theory = Classifier_Theory[:, 0]
# print(M5_List_Theory)
K_List_Classifier_Theory = Classifier_Theory[:, 1]
Pvalue_List_Classifier_Theory = Classifier_Theory[:, 3]
N = 15
xi_Classifier_Theory = numpy.linspace(M5_List_Classifier_Theory.min(), M5_List_Classifier_Theory.max(), N)
yi_Classifier_Theory  = numpy.linspace(K_List_Classifier_Theory.min(), K_List_Classifier_Theory.max(), N)
zi_Classifier_Theory  = scipy.interpolate.griddata((M5_List_Classifier_Theory, K_List_Classifier_Theory), Pvalue_List_Classifier_Theory, (xi_Classifier_Theory[None,:], yi_Classifier_Theory[:,None]), method='cubic')
#Autoencoder
Autoencoder_Theory = numpy.loadtxt('AutoEncodermerged.txt')
M5_List_Autoencoder_Theory = Autoencoder_Theory[:, 0]
# print(M5_List_Theory)
K_List_Autoencoder_Theory = Autoencoder_Theory[:, 1]
Pvalue_List_Autoencoder_Theory = Autoencoder_Theory[:, 3]
N = 15
xi_Autoencoder_Theory = numpy.linspace(M5_List_Autoencoder_Theory.min(), M5_List_Autoencoder_Theory.max(), N)
yi_Autoencoder_Theory  = numpy.linspace(K_List_Autoencoder_Theory.min(), K_List_Autoencoder_Theory.max(), N)
zi_Autoencoder_Theory  = scipy.interpolate.griddata((M5_List_Autoencoder_Theory, K_List_Autoencoder_Theory), Pvalue_List_Autoencoder_Theory, (xi_Autoencoder_Theory[None,:], yi_Autoencoder_Theory[:,None]), method='cubic')
#Fourier
FT_Theory = numpy.loadtxt('FTmerged.txt')
M5_List_FT_Theory = FT_Theory[:, 0]
K_List_FT_Theory = FT_Theory[:, 1]
Pvalue_List_FT_Theory = []
Pvalue_List_FT_Theory_Min = []
for i in range(0,441):
	# Pvalue_List_FT_Theory.append(FT_Theory[2:,i])
	Pvalue_List_FT_Theory.append(FT_Theory[i,0:])
for list in Pvalue_List_FT_Theory:
    Pvalue_List_FT_Theory_Min.append(min(list))
print(Pvalue_List_FT_Theory_Min)
N_FT = 20
# print(K_List_FT_Theory)
# xi_FT_Theory = numpy.linspace(numpy.argmin(M5_List_FT_Theory, axis=0), numpy.argmax(M5_List_FT_Theory, axis=0), N)
# yi_FT_Theory = numpy.linspace(numpy.argmin(K_List_FT_Theory, axis=0), numpy.argmax(K_List_FT_Theory, axis=0), N)
xi_FT_Theory = numpy.linspace(M5_List_FT_Theory.min(), M5_List_FT_Theory.max(), N_FT)
yi_FT_Theory = numpy.linspace(K_List_FT_Theory.min(), K_List_FT_Theory.max(), N_FT)
zi_FT_Theory  = scipy.interpolate.griddata((M5_List_FT_Theory, K_List_FT_Theory), Pvalue_List_FT_Theory_Min, (xi_FT_Theory[None,:], yi_FT_Theory[:,None]), method='linear')


#CMS
CMS_K = [2000,2500,3000,3500,4000,4500,5000]
CMS_M5 = [8000,5000,4000,3000,2000,1500,1000]

#Make Contour plots of Pvalues from this Searcha nd compare tot he theory results
fig_pvalue_contour,ax =plt.subplots(1,1)
ax = hep.atlas.label(data=False, paper=False,year='2015-2018', lumi=36.1 , fontsize=19, ax=ax)
# cp = ax.contourf(K_List_2D, M5_List_2D, Z), levels = (0.049,0.051))
c_alpha_Cl = plt.contour(K_List_2D, M5_List_2D, Z_Cl, levels = (0.0445,0.0455), linestyles = 'dashed', colors = ['r'])
c_alpha_At = plt.contour(K_List_2D, M5_List_2D, Z_At, levels = (0.0445,0.0455), linestyles = 'dashed' , colors = ['blue'])
c_alpha_FT = plt.contour(K_List_2D, M5_List_2D, Z_FT, levels = (0.0445,0.0455), linestyles = 'dashed' , colors = ['black'])
#Theory contours
# c_alpha_Cl_Theory = plt.contour(xi_Classifier_Theory , yi_Classifier_Theory , zi_Classifier_Theory , levels = (0.0445,0.0455), colors = ['r'], alpha=0.2)
# c_alpha_At_Theory = plt.contour(xi_Autoencoder_Theory , yi_Autoencoder_Theory , zi_Autoencoder_Theory , levels = (0.0445,0.0455), colors = ['blue'], alpha=0.2)
# c_alpha_FT_Theory = plt.contour(xi_FT_Theory , yi_FT_Theory , zi_FT_Theory , levels = (0.0445,0.0455), colors = ['black'], alpha=0.2)
#Additional points and lines
Classifier_FulRun2Point = plt.scatter(7500,510, color='green',marker='s',linewidths=8)
Classifier_FulRun2Point_Unc = plt.scatter(7300,490, color='brown',marker='8',linewidths=8)
# CMSresult = plt.plot(CMS_M5,CMS_K,color='purple', linewidth=5)
h1,_ = c_alpha_Cl.legend_elements()
h2,_ = c_alpha_At.legend_elements()
h3,_ = c_alpha_FT.legend_elements()
# h4,_ = c_alpha_Cl_Theory.legend_elements()
# h5,_ = c_alpha_At_Theory.legend_elements()
# h6,_ = c_alpha_FT_Theory.legend_elements()
plt.xlabel('M5 [GeV]', ha='right', x=1.0)
plt.ylabel('k [GeV]', ha='right', y=1.0)
plt.semilogy()
plt.xlim((1000,8000))
plt.ylim((200,2600))
plt.setp(c_alpha_Cl.collections , linewidth=3)
# plt.setp(c_alpha_Cl_Theory.collections , linewidth=15)
plt.setp(c_alpha_At.collections , linewidth=3)
# plt.setp(c_alpha_At_Theory.collections , linewidth=15)
plt.setp(c_alpha_FT.collections , linewidth=3)
# plt.setp(c_alpha_FT_Theory.collections , linewidth=15)
plt.legend()
leg = plt.legend([h1[0], h2[0], h3[0]], ['Classifier', 'Autoencoder', 'FT'],loc = 'lower left')
for line in leg.get_lines():
    line.set_linewidth(3.0)
    line.set_alpha(0.5)
# fig_pvalue_contour_yy.colorbar(c_alpha_yy) # Add a colorbar to a plot,Patch(facecolor='r'), alpha=0.2
fig_pvalue_contour.savefig('Exclusion_plots/P-valueMap_Method_New.pdf')

