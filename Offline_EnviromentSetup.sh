#!/bin/bash 

#### new version written by Sean
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
#### lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib"
pip3 install --user ipywidgets==7.5.1
pip3 install --user mplhep==0.2.9
pip3 install --user matplotlib==3.4.2
pip3 install --user parton --upgrade

#### copied from previous version

# # # # python3 -m venv --system-site-packages Clockwork_Env
# # # # source Clockwork_Env/bin/activate
# # # # pip3 install pip --upgrade
# # # # pip3 install setuptools --upgrade
# # # # pip3 install pillow --upgrade
# # # # # lsetup "root 6.20.06-x86_64-ceqntos7-gcc8-opt" 
# # # # pip3 install scipy --upgrade
# # # # pip3 install sklearn --upgrade
# # # # pip3 install numpy --upgrade
# # # # pip3 install matplotlib --upgrade
# # # # pip3 install PyWavelets --upgrade
# # # # # conda create -n tf python=3.6.8
# # # # # conda activate tf
# # # # # pip3 install cudnnenv --upgrade
# # # # pip3 install tensorflow==1.14 --upgrade
# # # # # pip3 install tensorflow-gpu==1.14 --upgrade
# # # # # conda install cudatoolkit==9.0
# # # # pip3 install keras==2.2.5 --upgrade
# # # # pip3 install rootpy --upgrade
# # # # pip3 install parton 
# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# # # # ## lsetup "lcgenv -p LCG_98python3 x86_64-centos7-gcc8-opt ROOT"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scipy"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scikitlearn"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt numpy"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt matplotlib"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt pywt"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt tensorflow"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt keras"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt wrapt"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt pip"
# lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt ROOT"
# # # # # # lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
# pip3 install --user parton 
# pip3 install --user mplhep
