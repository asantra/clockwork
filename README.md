###### instructions for running on AWS ########
1. Install ROOT from docker image:
- docker run --rm -it rootproject/root
2. clone this repository:
- git clone https://gitlab.cern.ch/asantra/clockwork.git
3. go inside the directory
- cd clockwork_project
4. install python packages using pip and then run the Autoencoder code
- source setupClockWork.sh


############################################################################### ############################################################################### ###############################################################################

NOTE:BE CAREFUL TO CHANGE ABSOLUTE PATHS IN ALL FILES

Welcome to the git respository for the Clockwork Search using dileptons. Please mail me (sean.dean.lawlor@cern.ch) if you have any questions about the README. I need to give credit to Hugues Beauchesne, Yevgeny Kats and Noam Tal Hod from whose code much of this project is built. Hugues Beauchesne and Yevgeny Kats developed the Nueral Network (NN) setups used in additon to the Wavelet transform code and Possion Toy code. Noam Tal Hod's code was used to create the Background, Signal generation functions in addition to the RooFit Toys. The project is using Python 3.7 is being used as a part of the anaylsis needs Tensorflow and Keras, which need python3. In the main directory I have a bash script that should setup a virtual enviroment with everything that is need to run the project:

Offline_EnviromentSetup.sh

Run this and it will activate the virutal enviroment which you need: source Clockwork_Env/bin/activate
Check what your. luster can load this use lcgenv stacks and and a couple of pip packages.
The project is organised into several folders.
Some steps to consider before using a particular project:
the clockwork_search covers the 'ee' channeland the 'yy' channel. 

########SignalBackground_Generation########
In this file we have everything we need to make signal and background shapes from the ee and yy analyses, this is done in each project by calling the file 'SigBkg_Functions.py' whixh has mnay functions in for analytical signals,Backgrounds, MC Signal etc

########Background########

/Background: this folder uses the full Run-2 data to generate a backgorund fit from it using a functional form and normalising the fit to data. The file: plotbkgfit.py Does this and the range of the fit can be set on line 14/15 of this fit and the xmin should be set to the minimum invariant mass range that you want to perform the search with. produces plots into: /plots_bkgfit
To run: plotbkgfit.py between xmin and xmax that is chosen for the search (default xmin = 230 GeV xmax = 6000 GeV). This creates a root file in the \Inputs folder that is used thorughout the other parts of the project.

########Inputs########

/Inputs - Contains files produced by the Background folder to import the background fit, Acceptance and Efficicncy files from previous Full Run-2 dilepton search to help generate a signal.


########Custom_Modules########
TestS.py - This file contains functions to evaluate the test statistics for the autoencoder and the region finder.
LSign.py - This file contains functions related to the evaluation of local p-values. It contains both functions to generate the pseudo-experiments and a function that maps a given scalogram to the corresponding p-value map. WT.py: This code contains a function to return the CWT of a given input. It uses the Morlet wavelet. It’s possible to have it remove the cone of influence if wanted, but the option is turned off by default. The scales considered are equally spaced on a logarithmic axis such that there are 12 bins between two scales differing by a factor of two.
Merger.py: This code contains a function to merge together bins in a scalogram to reduce the total number of bins. Signals with high frequency require bins to be small in mass space. This can result in a scalogram with a number of elements too high to practically serve as the input of a neural network.

########Uncertainties########

To run Uncertainties in any project for the background you will need to download the file: toys_allUncertainties.root 
into the Uncertainties folder in SignalBackground_Generation

###############################################################################

How to Run each project in the terminal:

Here we'll see how to run each project with some example commands in ther terminal:

- FFT: We run here for a specific k,M5 hypothesis:

python3 Condor_Fourier_TestStatistic.py -t Poisson -k 500 -M5 6000 -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -lumDiv True -test 1000  -bin 20 -stepFT 100 -bkgsub False -u no



Quick guide: Can run from main folder by using Training_Run.sh and Testing_Run.sh, e.g in the training bash file we have a command which runs the project from inputing the commands into the condor submit file and therefore the training NN script as well. An example of the code that would this in the bash script is:

cd clockwork-search/Autoencoder/Condor_Submission_Files/
python3 Autoencoder_Training_GenerateSubmissionScripts.py -t Poisson -s Theory_Diphoton_36_Sig -b Diphoton_36_Bkg -event float -test 1000 -train 5000  -Cone False -NconeR 2
condor_submit condor_training_Autoencoder.sub -batch-name At_ee_36_Train_float
cd ../../../
echo "Run 36fb-1 ee Autoencoder Done"

The different signals and background calls are commneted out at the start of both bash scripts
After this you can use the testing bash script to run using the same bkg signal call etc
Will add in more this week
